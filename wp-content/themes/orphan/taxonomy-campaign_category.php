<?php
/**
 * Campaign category archive.
 *
 * This template is only used if Charitable is active.
 *
 * @package     Orphan
 */

get_template_part( 'archive', 'campaign' );

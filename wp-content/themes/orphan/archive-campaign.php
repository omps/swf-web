<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package orphan
 */

get_header();

// Layout
$position = get_theme_mod('orphan_charitable_sidebar', 'sidebar-right');
?>

<div<?php echo orphan_helper::section(); ?>>
	<div<?php echo orphan_helper::container(); ?>>
		<div<?php echo orphan_helper::grid(); ?>>
			
			<div class="uk-width-expand">
				<main class="tm-content">

					<?php if (is_author()) { ?>
						<div class="author-archive">

							<div id="author-info" class="uk-clearfix uk-margin-large-bottom">
							    <div class="author-image uk-float-left uk-margin-right">
								    	<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta( 'ID' ))); ?>"><?php echo get_avatar( esc_attr(get_the_author_meta('user_email')), '80', '' ); ?></a>
								    </div>   
								    <div class="author-bio">
								       <h4 class="uk-margin-small-bottom"><?php esc_html_e('About', 'orphan'); ?> <?php the_author(); ?></h4>
								        <?php the_author_meta('description'); ?>
								    </div>
							</div>

						</div>
					<?php } 


						/**
						 * This renders a loop of campaigns that are displayed with the
						 * `reach/charitable/campaign-loop.php` template file.
						 *
						 * @see 	charitable_template_campaign_loop()
						 */
						charitable_template_campaign_loop( false, 3 );

					?>
					
					<?php get_template_part( 'template-parts/pagination' ); ?>
				</main> <!-- end main -->
			</div> <!-- end content -->
			

			<?php if($position == 'sidebar-left' || $position == 'sidebar-right') : ?>
				<aside<?php echo orphan_helper::sidebar($position); ?>>
				    <?php
						
						/* Charitable Sidebar */
						if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('campaign-widgets') );
					?>
				</aside> <!-- end aside -->
			<?php endif; ?>
			
		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>
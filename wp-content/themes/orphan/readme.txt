=== orphan ===

Contributors: automattic
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.0
Tested up to: 4.5.3
Stable tag: 1.1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Orphan Theme designed by BdThemes Ltd. For online documentation look at our documentation site. For get support please go to our support system.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Orphan support those plugin:
1. Visual Composer
2. Slider Revolution
3. BdThemes Shortcode
3. The Event Calender
4. Give Donation
5. BdThemes FAQ


== Changelog ==

= 1.2.0 - November 22 2016 =
+ Charitable plugin integration added
+ More colors options added
+ Fully Woocommerce plugin integration added
+ Google Font change option added
+ Header Style Added
+ Header Type added (Sticky, Smart Sticky)
+ Header style and header type override option added in page settings
# Customizer not chaging bug fixed
# Some css and javascript bug fixed

= 1.1.1 - October 16 2016 =
# Child Theme could not compile less
# Menu could not showing right text for others language
# Menu can't editable in customizer
# Minor css improved
# MailChimp Settings fixed
# Inline Icon and Icon list shortcode default value fixed
# Dropdown added background option
# Customizer live color value fixed for menu 2,3,4

= 1.1.0 - August 16 2016 =
+ One Click Demo Installation added
# Main menu character encoding problem fixed
# Less Compiler improved
# Customizer lighten/darken color problem fixed

= 1.0 - August 16 2016 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)

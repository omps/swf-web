<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package orphan
 */

	$layout     = get_theme_mod('orphan_global_layout', 'full');
	$cookie_bar = get_theme_mod( 'orphan_cookie');
	?>
	
	<?php if (!is_page_template( 'page-blank.php' )) : ?>
		<?php get_template_part( 'template-parts/bottom' ); ?>
		<?php get_template_part( 'template-parts/copyright' ); ?>
	<?php endif; ?>
	

	<?php if ($layout == 'boxed') : ?>
		</div><!-- #margin -->
	</div><!-- #tm-page -->
	<?php endif; ?>

	<?php get_template_part( 'template-parts/fixed-left' ); ?>	
	<?php get_template_part( 'template-parts/fixed-right' ); ?>


	<?php $orphan_top_link = get_theme_mod('orphan_top_link');
	if(!$orphan_top_link and !is_page_template( 'page-blank.php' )): ?>
		<a class="tm-totop-scroller" href="#"  uk-totop uk-scroll></a>
	<?php endif; ?>

    <?php if ($cookie_bar) : ?>
		<?php get_template_part('template-parts/cookie-bar'); ?>
	<?php endif ?>

	<?php wp_footer(); ?>

</body>
</html>

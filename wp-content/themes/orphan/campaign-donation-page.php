<?php

get_header( 'stripped' );

// Layout
$position = get_theme_mod('orphan_charitable_sidebar', 'sidebar-right');

?>
<div<?php echo orphan_helper::section(); ?>>
	<div<?php echo orphan_helper::container(); ?>>
		<div<?php echo orphan_helper::grid(); ?>>
			<main class="uk-width-expand tm-content">
				<?php

				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();

						$campaign = charitable_get_current_campaign();
						?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php //get_template_part( 'partials/banner' ); ?>
							<div class="block entry-block">
								<div class="entry">
									<?php $campaign->get_donation_form()->render() ?>
								</div><!-- .entry -->
							</div><!-- .entry-block -->
						</article><!-- post-<?php the_ID() ?> -->
						<?php

					endwhile;
				endif;
				?>
			</main><!-- #primary -->		

			<?php if(($position == 'sidebar-left' || $position == 'sidebar-right') && is_active_sidebar( 'campaign-widgets' )) : ?>
				<aside<?php echo orphan_helper::sidebar($position); ?>>
					<?php
						
						/* Charitable Sidebar */
						if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('campaign-widgets') );
					?>
				</aside> <!-- end aside -->
			<?php endif; ?>

		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->

<?php
get_footer( 'stripped' );

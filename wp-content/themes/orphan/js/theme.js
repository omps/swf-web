jQuery(document).ready(function($) {
    "use strict";

    jQuery(".drawer_toggle").click(function() {

        if (!$.easing["easeOutExpo"]) {
            $.easing["easeOutExpo"] = function(x, t, b, c, d) {
                return t == d ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b
            }
        }
        
        //Expand
        if (drawer_state == 0) {
            jQuery("div#tm-drawer").slideDown(400, 'easeOutExpo');
            jQuery('.drawer_toggle span').removeClass('uk-icon-chevron-down');
            jQuery('.drawer_toggle span').addClass('uk-icon-chevron-up');
            drawer_state = 1;
            //Collapse
        } else if (drawer_state == 1) {
            jQuery("div#tm-drawer").slideUp(400, 'easeOutExpo');
            jQuery('.drawer_toggle span').removeClass('uk-icon-chevron-up');
            jQuery('.drawer_toggle span').addClass('uk-icon-chevron-down');
            drawer_state = 0;
        }
    });

    var drawer_state = 0;

    if ($('body').hasClass('header-mode-fixed')) {
        var fixedPadding = $('.tm-header').height();        
        $('#tmTitleBar').css('padding-top', fixedPadding); 
    }
    
    // $("#tmMainMenu ul.uk-navbar-nav li.sub-dropdown").on('mouseenter mouseleave', function (e) {
    //     if ($('ul', this).length) {
    //         var elm = $(this);
    //         var off = elm.offset();
    //         var l = off.left;
    //         var w = elm.width();
    //         var docW = $("body").width();
    //         //console.log(l);
    //         var isEntirelyVisible = (l + w <= docW);
    //         //console.log(isEntirelyVisible);
    //         if (!isEntirelyVisible) {
    //             $(this).addClass('dropdown-classic-left');
    //         } else {
    //             $(this).removeClass('dropdown-classic-left');
    //         }
    //     }
    // });

    //to top scroller
    $(window).scroll(function() {
        var scrollPos = 300;
        if ($(this).scrollTop() > scrollPos) {
            $(".tm-totop-scroller").addClass("totop-show")
        } else {
            $(".tm-totop-scroller").removeClass("totop-show")
        }
    });


    var $carousel = $('[data-owl-carousel]');
    if ($carousel.length) {
         $carousel.each(function(index, el) {
              $(this).owlCarousel($(this).data('owl-carousel'));
         });
     }


    $('.image-lightbox').magnificPopup({
        type:'image',
        delegate: 'a',
        gallery: {
          enabled: true
        },
    });
    
    $('.video-lightbox').magnificPopup({
        type:'iframe',
        mainClass: 'mfp-img-mobile',
        removalDelay: 0
    });


    $('.bdt-lightbox-gallery').magnificPopup({
        type:'image',
        delegate: '.lightbox-item',
        gallery: {
          enabled: true
        },
    });

    

    $('.bdt-progress-pie').each(function () {
        var progressPie = '#' + $(this).attr('id');

        $(progressPie).asPieProgress({
            namespace: "pieProgress",
            classes: {
                svg: "bdt-progress-pie-svg",
                number: "bdt-progress-pie-number",
                content: "bdt-progress-pie-content"
            }
        })
        $(progressPie).asPieProgress("start");
    });
});
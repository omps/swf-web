<?php 

$campaign = $view_args['campaign'];
$progress = $campaign->get_percent_donated_raw();

do_shortcode( '[progress_bar 
					style="1" 
					percent="75" 
					show_percent="yes" 
					text="'.number_format( $progress, 2 ).'<sup>%</sup></span>'.esc_html__( 'Funded', 'orphan' ).'" 
					text_color="#FFFFFF" 
					bar_color="#f0f0f0" 
					fill_color="#4fc1e9" 
					animation="easeInOutExpo" 
					duration="1.5" 
					delay="0.3"
				]');


<?php
/**
 * Custom template tags used when crowdfunding is enabled.
 */

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

if ( ! function_exists( 'orphan_crowdfunding_campaign_nav' ) ) :

	/**
	 * The callback function for the campaigns navigation
	 *
	 * @param   bool $echo
	 * @return  string
	 * @since   1.0.2
	 */
	function orphan_crowdfunding_campaign_nav( $echo = true ) {
		$categories = get_categories( array(
			'taxonomy' => 'campaign_category',
			'orderby'  => 'name',
			'order'    => 'ASC',
		) );

		if ( empty( $categories ) ) {
			return;
		}

		$html = sprintf( '<ul class="menu menu-site"><li class="campaign_category with-icon" data-icon="&#xf02c;">%s', esc_html__( 'Categories', 'orphan' ) );
		$html .= sprintf( '<ul><li><a href="%s">%s</a></li>', get_post_type_archive_link( 'campaign' ), esc_html__( 'All', 'orphan' ) );

		foreach ( $categories as $category ) {
			$html .= sprintf( '<li><a href="%s">%s</a></li>', esc_url( get_term_link( $category ) ), $category->name );
		}

		$html .= '</li></ul>';

		if ( false === $echo ) {
			return $html;
		}

		echo wp_kses_post( $html );
	}

endif;

if ( ! function_exists( 'orphan_campaign_ended_text' ) ) :

	/**
	 * Return the text to display when a campaign has finished.
	 *
	 * @return  string
	 * @since   1.0.0
	 */
	function orphan_campaign_ended_text() {
		return wp_kses_post(__( '<span>Campaign</span> has ended', 'orphan' ));
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_loop_stats' ) ) :

	/**
	 * Display the campaign stats inside the campaign.
	 *
	 * @uses 	charitable_template
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_loop_stats( Charitable_Campaign $campaign ) {
		charitable_template( 'campaign-loop/stats.php', array( 'campaign' => $campaign ) );
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_loop_creator' ) ) :

	/**
	 * Display the campaign stats inside the campaign.
	 *
	 * @uses 	charitable_template
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_loop_creator( Charitable_Campaign $campaign ) {
		charitable_template( 'campaign-loop/creator.php', array( 'campaign' => $campaign ) );
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_summary' ) ) :

	/**
	 * Display the campaign summary block.
	 *
	 * @uses 	charitable_template
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_summary( Charitable_Campaign $campaign ) {
		charitable_template( 'campaign/summary.php', array( 'campaign' => $campaign ) );
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_title' ) ) :

	/**
	 * Display the campaign title.
	 *
	 * @uses 	charitable_template
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_title( Charitable_Campaign $campaign ) {
		charitable_template( 'campaign/title.php', array( 'campaign' => $campaign ) );
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_featured_image' ) ) :

	/**
	 * Display the campaign featured image.
	 *
	 * @uses 	charitable_template
	 *
	 * @param   Charitable_Campaign $campaign
	 * @param   string $context
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_featured_image( Charitable_Campaign $campaign, $context = 'summary' ) {
		charitable_template( 'campaign/featured-image.php', array( 'campaign' => $campaign, 'context' => $context ) );
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_progress_barometer' ) ) :

	/**
	 * Display the campaign progress barometer.
	 *
	 * @uses 	charitable_template
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_progress_barometer( Charitable_Campaign $campaign ) {
		if ( $campaign->has_goal() ) {
			charitable_template( 'campaign/progress-barometer.php', array( 'campaign' => $campaign ) );
		}
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_stats' ) ) :

	/**
	 * Display the campaign stats.
	 *
	 * @uses 	charitable_template
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_stats( Charitable_Campaign $campaign ) {
		charitable_template( 'campaign/stats.php', array( 'campaign' => $campaign ) );
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_time_left' ) ) :

	/**
	 * Display the amount of time left in the campaign in the summary block.
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  boolean     True if the template was displayed. False otherwise.
	 * @since   1.0.0
	 */
	function orphan_template_campaign_time_left( $campaign ) {
		if ( $campaign->is_endless() ) {
			return false;
		}

		charitable_template( 'campaign/summary-time-left.php', array( 'campaign' => $campaign ) );
		return true;
	}

endif;



if ( ! function_exists( 'orphan_template_campaign_share' ) ) :

	/**
	 * Display the campaign sharing icons.
	 *
	 * @uses 	charitable_template
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_share( Charitable_Campaign $campaign ) {
		//charitable_template( 'campaign/share.php', array( 'campaign' => $campaign ) );

		echo "Share Widget Here";
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_after_content_widget_area' ) ) :

	/**
	 * Add a widget-ready area below the campaign content.
	 *
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_after_content_widget_area() {
		get_sidebar( 'campaign-after' );
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_comments' ) ) :

	/**
	 * Add the campaign comments below the content.
	 *
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_comments() {
		comments_template();
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_media_before_summary' ) ) :

	/**
	 * Add the media element to display before the campaign summary.
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	function orphan_template_campaign_media_before_summary( Charitable_Campaign $campaign ) {
		
		if ( function_exists( 'charitable_videos_template_campaign_video' ) ) {
			charitable_videos_template_campaign_video( $campaign );
		} else {
			orphan_template_campaign_featured_image( $campaign, 'summary' );
		}
	}

endif;

if ( ! function_exists( 'orphan_template_campaign_media_before_content' ) ) :

	/**
	 * Add the media element to display before the campaign summary.
	 *
	 * @param   Charitable_Campaign $campaign
	 * @return  void
	 * @since   1.0.0
	 */
	// function orphan_template_campaign_media_before_content( Charitable_Campaign $campaign ) {
	// 	$media = orphan_get_theme()->get_theme_setting( 'campaign_media_placement', 'featured_image_in_summary' );

	// 	if ( 'video_in_summary' == $media ) {
	// 		orphan_template_campaign_featured_image( $campaign, 'content' );
	// 	} elseif ( function_exists( 'charitable_videos_template_campaign_video' ) ) {
	// 		charitable_videos_template_campaign_video( $campaign );
	// 	}
	// }

endif;

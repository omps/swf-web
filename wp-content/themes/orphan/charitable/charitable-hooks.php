<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

/**
 * Campaigns loop, before title.
 *
 * @see orphan_template_campaign_loop_stats
 * @see orphan_template_campaign_loop_creator
 */
add_action( 'charitable_campaign_content_loop_before_title', 'orphan_template_campaign_loop_stats', 12 );
remove_action( 'charitable_campaign_content_loop_after', 'charitable_template_campaign_progress_bar', 6 );
remove_action( 'charitable_campaign_content_loop_after', 'charitable_template_campaign_loop_donation_stats', 8 );
remove_action( 'charitable_campaign_content_loop_after', 'charitable_template_campaign_loop_donate_link', 10 );

/**
 * Single campaign, top of the page.
 *
 * @see orphan_template_campaign_summary
 */
if ( function_exists( 'charitable_ambassadors_template_edit_campaign_link' ) ) {
	add_action( 'charitable_single_campaign_before', 'charitable_ambassadors_template_edit_campaign_link', 2 );
}

add_action( 'charitable_single_campaign_before', 'orphan_template_campaign_summary', 2 );

/**
 * Single campaign, before summary.
 *
 * @see orphan_template_campaign_title
 * @see charitable_template_campaign_description
 * @see charitable_campaign_summary_before
 */

/**
 * Single campaign summary.
 *
 * @see charitable_template_campaign_finished_notice
 * @see charitable_template_donate_button
 * @see orphan_template_campaign_progress_barometer
 * @see orphan_template_campaign_stats
 */
add_action( 'charitable_campaign_summary', 'charitable_template_campaign_finished_notice', 9 );



//remove_action( 'charitable_campaign_summary', 'charitable_template_campaign_percentage_raised', 4 );
remove_action( 'charitable_campaign_summary', 'charitable_template_campaign_donation_summary', 6 );
remove_action( 'charitable_campaign_summary', 'charitable_template_campaign_donor_count', 8 );
remove_action( 'charitable_campaign_summary', 'charitable_template_campaign_time_left', 10 );
//remove_action( 'charitable_campaign_summary', 'charitable_template_donate_button', 12 );

/**
 * Single campaign, after summary.
 *
 * @see orphan_template_campaign_share
 */

/**
 * Single campaign, before content.
 *
 * @see orphan_template_campaign_media_before_content
 */
remove_action( 'charitable_campaign_content_before', 'charitable_ambassadors_template_edit_campaign_link', 2 );
remove_action( 'charitable_campaign_content_before', 'charitable_template_campaign_description', 4 );
remove_action( 'charitable_campaign_content_before', 'charitable_videos_template_campaign_video', 5 );
remove_action( 'charitable_campaign_content_before', 'charitable_template_campaign_summary', 6 );

/**
 * Single campaign, after content.
 *
 * @see orphan_template_campaign_after_content_widget_area
 * @see orphan_template_campaign_comments
 */
add_action( 'charitable_campaign_content_after', 'orphan_template_campaign_comments', 12 );



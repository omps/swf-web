<article id="campaign-<?php echo get_the_ID() ?>" <?php post_class( 'uk-article campaign-content' ) ?> data-permalink="<?php the_permalink(); ?>">

    <div class="entry">
        <h3 class="uk-margin-top"><?php esc_html_e( 'About the Campaign', 'orphan' ) ?></h3>

		<div class="">
			<?php the_content() ?>
		</div>
    </div>

</article>

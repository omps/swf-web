<?php

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

/**
 * @var 	Charitable_Campaign
 */
$campaign = $view_args['campaign'];

$classes = 'campaign-summary current-campaign uk-padding';


$progress = $campaign->get_percent_donated_raw();

if ( false === $progress ) {
	return;
}

$donation =  esc_html__( 'Donations:', 'orphan' ) .' '. charitable_format_money( $campaign->get_donated_amount() ) . ' / ' . charitable_format_money( $campaign->get_goal() );

?>

<?php if ( has_post_thumbnail( $campaign->ID ) ) : ?>
<div class="campaign-image">
	<?php
	echo charitable_template_campaign_status_tag( $campaign );

	echo get_the_post_thumbnail( $campaign->ID, 'orphan_blog' );
	?>
</div>

<?php endif; ?>

<div class="<?php echo esc_attr($classes) ?>">

	<h2 class="uk-margin-small-bottom"><?php echo get_the_title( $view_args['campaign']->ID ) ?></h2>

	<ul class="uk-subnav uk-subnav-divider uk-margin-small-top">
		<li class="campaign-raised">
			<span><?php echo charitable_format_money( $campaign->get_donated_amount() ) ?></span> 
			<?php esc_html_e( 'Donated', 'orphan' ) ?>
		</li>
		<?php if ( $campaign->has_goal() ) : ?>
			<li class="campaign-goal">
				<span><?php echo charitable_format_money( $campaign->get_goal() ) ?></span> 
				<?php esc_html_e( 'Goal', 'orphan' ) ?>
			</li>
		<?php endif ?>
		<li class="campaign-backers">
			<span><?php echo wp_kses_post( $campaign->get_donor_count()); ?></span> 
			<?php esc_html_e( 'Donors', 'orphan' ) ?>
		</li>
	</ul>

	<div class="uk-flex uk-flex-middle uk-margin" uk-grid>
		<div class="uk-width-expand@m">
			<progress class="uk-progress uk-margin-small-top" value="<?php echo number_format( $progress, 2 ); ?>" max="100"></progress>
			<div class="uk-text-bold">
				<?php printf( esc_html_x( '%s Raised', 'percentage raised', 'orphan' ), '<span>' . $campaign->get_percent_donated() . '</span>') ?>
			</div>
		</div>
		<div class="uk-width-1-3@m">
			<form class="uk-padding-remove-vertical uk-text-right" method="post">
				<?php wp_nonce_field( 'charitable-donate', 'charitable-donate-now' ) ?>
				<input type="hidden" name="charitable_action" value="start_donation" />
				<input type="hidden" name="campaign_id" value="<?php echo esc_attr($campaign->ID) ?>" />
				<button type="submit" name="charitable_submit" class="uk-button uk-button-primary uk-button-large uk-width-auto uk-link-reset"><?php esc_html_e( 'Donate Now', 'orphan' ) ?></button>
			</form>
		</div>
	</div>
	

	

</div><!-- .layout-wrapper -->
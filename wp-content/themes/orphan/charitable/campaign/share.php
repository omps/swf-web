<?php
/**
 * The template for displaying the campaign sharing icons on the campaign page.
 * Override this template by copying it to your-child-theme/charitable/campaign/summary.php
 */

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

$campaign   = $view_args['campaign'];
$permalink  = urlencode( get_the_permalink( $campaign->ID ) );
$title      = urlencode( get_the_title( $campaign->ID ) );
$widget_url = esc_url( charitable_get_permalink( 'campaign_widget_page' ) );

?>
<ul class="campaign-sharing share horizontal rrssb-buttons">
	<li><h6><?php esc_html_e( 'Share', 'orphan' ) ?></h6></li>
	<li class="share-twitter">
		<a href="http://twitter.com/home?status=<?php echo esc_url($title) ?>%20<?php echo esc_url($permalink) ?>" class="popup icon" data-icon="&#xf099;"></a>
	</li>
	<li class="share-facebook">
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url($permalink) ?>" class="popup icon" data-icon="&#xf09a;"></a>
	</li>
	<li class="share-googleplus">
		<a href="https://plus.google.com/share?url=<?php echo esc_url($title) . $permalink ?>" class="popup icon" data-icon="&#xf0d5;"></a>
	</li>
	<li class="share-linkedin">
		<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo esc_url($permalink) ?>&amp;title=<?php echo esc_url($title) ?>" class="popup icon" data-icon="&#xf0e1;"></a>
	</li>
	<li class="share-pinterest">
		<a href="http://pinterest.com/pin/create/button/?url=<?php echo esc_url($permalink) ?>&amp;description=<?php echo esc_url($title) ?>" class="popup icon" data-icon="&#xf0d2;"></a>
	</li>
</ul>

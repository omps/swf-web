<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

$campaign = $view_args['campaign'];

if ( ! has_post_thumbnail( $campaign->ID ) ) :
	return;
endif;

?>
<div class="campaign-image">
	<?php
	echo charitable_template_campaign_status_tag( $campaign );

	echo get_the_post_thumbnail( $campaign->ID, 'orphan_blog' );
	?>
</div>

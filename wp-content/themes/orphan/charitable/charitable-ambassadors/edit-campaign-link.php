<?php
/**
 * @var 	Charitable_Campaign
 */
$campaign = charitable_get_current_campaign();

/**
 * This will be the value from the post_status column,
 * or, if the campaign is published, 'active' or 'finished'.
 *
 * @var 	string
 */
$status = $campaign->get_status();

?>
<div class="charitable-ambassadors-campaign-creator-toolbar">
	<div class="layout-wrapper">
		<span class="campaign-status"><?php printf( _x( 'Status: %s', 'status for campaign', 'orphan' ),
		'<span class="status status-' . esc_attr( $status ) . '">' . ucwords( $status ) . '</span>' )?>
		</span>
		<a href="<?php echo esc_url( charitable_get_permalink( 'campaign_editing_page' ) ) ?>" class="edit-link">
			<?php esc_html_e( 'Edit campaign', 'orphan' ) ?>
		</a>
	</div><!-- .layout-wrapper -->
</div><!-- .charitable-ambassadors-campaign-creator-toolbar -->

<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

global $wp_query;
global $post;
global $paged;

$campaigns = $view_args['campaigns'];
$columns   = $view_args['columns'];
$grid_settings = '';
$limit = (get_post_meta( get_the_ID(), 'orphan_charitable_limit', true ) != null ) ? get_post_meta( get_the_ID(), 'orphan_charitable_limit', true ) : '12';
$categories = (get_post_meta( get_the_ID(), 'orphan_charitable_categories', true ) != null ) ? get_post_meta( get_the_ID(), 'orphan_charitable_categories', true ) : 'all';

$view_mode = (get_post_meta( get_the_ID(), 'orphan_charitable_view', true ) != null ) ? get_post_meta( get_the_ID(), 'orphan_charitable_view', true ) : 'grid';
$gutter = (get_post_meta( get_the_ID(), 'orphan_grid_gutter', true ) != null ) ? get_post_meta( get_the_ID(), 'orphan_grid_gutter', true ) : 'medium';
$args 	   = array();



if ( ! $campaigns->have_posts() ) :
	return;
endif;

if ($view_mode == 'grid') {
	$args['large_columns'] = (get_post_meta( get_the_ID(), 'orphan_grid_columns', true ) != null ) ? get_post_meta( get_the_ID(), 'orphan_grid_columns', true ) : '3';
	$args['medium_columns'] = (get_post_meta( get_the_ID(), 'orphan_grid_columns_medium', true ) != null ) ? get_post_meta( get_the_ID(), 'orphan_grid_columns_medium', true ) : '2';
	$args['small_columns'] = (get_post_meta( get_the_ID(), 'orphan_grid_columns_small', true ) != null ) ? get_post_meta( get_the_ID(), 'orphan_grid_columns_small', true ) : '1';
	$grid_settings = 'uk-grid-'.$gutter;
}



/**
 * @hook charitable_campaign_loop_before
 */
do_action( 'charitable_campaign_loop_before', $campaigns, $args );

?>
<div class="campaign-mode-<?php echo esc_attr($view_mode).' '.esc_attr($grid_settings); ?>" <?php echo ($view_mode != 'list') ?  'uk-grid' : ''; ?>>                           
	
	<?php while ( $campaigns->have_posts() ) : ?>

		<?php $campaigns->the_post() ?>

		<?php
		/**
		 * Loads `orphan/charitable/campaign-loop/campaign.php`
		 *
		 * @uses 	charitable_template()
		 */
		if ($view_mode == 'list') {
			charitable_template( 'campaign-loop/campaign-list.php', $args );
		} else {
			charitable_template( 'campaign-loop/campaign.php', $args );
		} ?>

	<?php endwhile; ?>                                                 
	
	 <?php wp_reset_postdata(); ?>

</div>
<?php
/**
 * @hook charitable_campaign_loop_after
 */
do_action( 'charitable_campaign_loop_after', $campaigns, $args );
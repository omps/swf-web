<?php
/**
 * The template for displaying campaign content within loops.
 * This overrides the default Charitable template defined at charitable/templates/campaign-loop/campaign.php
 */

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

$campaign = charitable_get_current_campaign();

$progress = $campaign->get_percent_donated_raw();

?>
<div id="campaign-<?php echo get_the_ID() ?>" class="campaign-item uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin-medium-bottom uk-border-rounded uk-overflow-hidden" uk-grid>
	
	<div class="uk-card-media-left uk-cover-container">
		<?php 

		/**
		 * @hook charitable_campaign_content_loop_before
		 */
		do_action( 'charitable_campaign_content_loop_before', $campaign, $view_args );

		?>

		    
			<?php
			/**
			 * Displays the status tag for the campaign.
			 *
			 * This renders the Charitable template at charitable/campaign/status-tag.php
			 */
			echo charitable_template_campaign_status_tag( $campaign );

			?>
			<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>" target="_parent" uk-cover>
				<?php echo get_the_post_thumbnail( $campaign->ID, 'orphan_charitable' ) ?>
			</a>
			<canvas width="600" height="400"></canvas>
		
		
	</div>

	<div>
		<div class="uk-card-body">
			<div class="title-wrapper">
				<h3 class="block-title">
					<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>" target="_parent" class="uk-link-reset"><?php the_title() ?></a>
				</h3>
			</div>

			<ul class="campaign-stats uk-list">
				<li class="campaign-pledged">
					<span><?php esc_html_e( 'Donations:', 'orphan' ) ?> <?php echo charitable_format_money( $campaign->get_donated_amount() ) ?> / <?php echo charitable_format_money( $campaign->get_goal() ) ?></span>
					               
				</li>
				<li class="progress-bar">
					<progress class="uk-progress uk-margin-remove-bottom" value="<?php echo number_format( $progress, 2 ); ?>" max="100"></progress>
				</li>
			</ul>

			<?php

			/**
			 * @hook charitable_campaign_content_loop_after_title
			 */
			do_action( 'charitable_campaign_content_loop_after_title', $campaign );

			/**
			 * @hook charitable_campaign_content_loop_after
			 */
			do_action( 'charitable_campaign_content_loop_after', $campaign );

			?>

			<form class="campaign-donation uk-margin-medium-top" method="post">
				<?php wp_nonce_field( 'charitable-donate', 'charitable-donate-now' ) ?>
				<input type="hidden" name="charitable_action" value="start_donation" />
				<input type="hidden" name="campaign_id" value="<?php echo esc_attr($campaign->ID) ?>" />
				<button type="submit" name="charitable_submit" class="uk-button uk-button-primary"><?php esc_attr_e( 'Donate Now', 'orphan' ) ?> <span class="uk-margin-small-left" uk-icon="icon: chevron-right"></span></button>
			</form>
		</div>
	</div>


	
</div>

<?php
/**
 * Campaign stats.
 * Override this template by copying it to your-child-theme/charitable/campaign-loop/stats.php
 * @package Orphan
 */

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

/**
 * @var 	Charitable_Campaign
 */
$campaign = $view_args['campaign'];

/**
 * Get the progress as a number. i.e. 66 = 66%.
 *
 * If the campaign does not have a goal, this will equal false.
 *
 * @var 	int|false
 */
$progress = $campaign->get_percent_donated_raw();

if ( false === $progress ) :
	return;
endif;

?>
<ul class="campaign-stats uk-list">
	<li class="campaign-pledged">
		<span><?php esc_html_e( 'Donations:', 'orphan' ) ?> <?php echo charitable_format_money( $campaign->get_donated_amount() ) ?> / <?php echo charitable_format_money( $campaign->get_goal() ) ?></span>
		               
	</li>
	<li class="progress-bar">
		<?php
		echo do_shortcode( '[bdt_progress_bar 
							style="1" 
							percent="'.number_format( $progress, 2 ).'" 
							show_percent="0"  
							text="" 
						]');
		?>
	</li>
</ul>

<?php
/**
 * The template for displaying campaign content within loops.
 * This overrides the default Charitable template defined at charitable/templates/campaign-loop/campaign.php
 */

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

$campaign = charitable_get_current_campaign();
$progress = $campaign->get_percent_donated_raw();

?>
<div id="campaign-<?php echo get_the_ID() ?>" class="campaign-item uk-width-1-<?php echo esc_attr($view_args['small_columns']); ?> uk-width-1-<?php echo esc_attr($view_args['medium_columns']); ?>@m uk-width-1-<?php echo esc_attr($view_args['large_columns']); ?>@l">

	<div class="uk-panel uk-background-default uk-box-shadow-small">
		<?php 

		/**
		 * @hook charitable_campaign_content_loop_before
		 */
		do_action( 'charitable_campaign_content_loop_before', $campaign, $view_args );

		?>

		<div class="campaign-item-image uk-position-relative">
			<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>" target="_parent">
				<?php echo get_the_post_thumbnail( $campaign->ID, 'orphan_charitable' ) ?>
			</a>
			
			<?php $donation =  esc_html__( 'Donations:', 'orphan' ) .' '. charitable_format_money( $campaign->get_donated_amount() ) . ' / ' . charitable_format_money( $campaign->get_goal() ); ?>

			<div class="donors-count uk-position-top-right uk-text-uppercase">
				<?php printf(
				_x( '%s Donors', 'number of donors', 'orphan' ),
				'<span>' . $campaign->get_donor_count() . '</span>'
				) ?>
			</div>

			<div class="campaign-progress uk-position-bottom uk-flex uk-flex-middle">
				<div class="uk-width-expand">
					<progress class="uk-progress uk-margin-remove-bottom" value="<?php echo number_format( $progress, 2 ); ?>" max="100"></progress>
				</div>
				<div class="uk-width-auto uk-margin-small-left uk-text-bold">
					<?php printf( esc_html__( '%s', 'orphan' ), '<span>' . intval($campaign->get_percent_donated()) . '%</span>') ?>
				</div>
			</div>
		</div>

		<?php $item_padding = ($view_args['large_columns'] > 3) ? 'uk-padding-medium' : 'uk-padding'; ?>

		<div class="campaign-item-desc <?php echo esc_attr($item_padding); ?>">
			
			<div class="title-wrapper">
				<h4 class="uk-margin-remove-bottom">
					<a href="<?php the_permalink() ?>" class="uk-link-reset" title="<?php the_title_attribute() ?>" target="_parent"><?php the_title() ?></a>
				</h4>
			</div>

			<ul class="uk-subnav uk-margin-small-top uk-text-uppercase uk-text-bold">
				<li class="campaign-raised">
					<span><?php echo charitable_format_money( $campaign->get_donated_amount() ) ?> 
				<?php esc_html_e( 'Donated', 'orphan' ) ?></span>
				</li>
				<?php if ( $campaign->has_goal() ) : ?>
				<li class="campaign-goal">
					<span><?php echo charitable_format_money( $campaign->get_goal() ) ?> 
				<?php esc_html_e( 'Goal', 'orphan' ) ?></span>
				</li>
				<?php endif ?>
			</ul>


			<?php

			/**
			 * @hook charitable_campaign_content_loop_after_title
			 */
			do_action( 'charitable_campaign_content_loop_after_title', $campaign );

			/**
			 * @hook charitable_campaign_content_loop_after
			 */
			do_action( 'charitable_campaign_content_loop_after', $campaign );

			?>


			<form class="campaign-donation" method="post">
				<?php wp_nonce_field( 'charitable-donate', 'charitable-donate-now' ) ?>
				<input type="hidden" name="charitable_action" value="start_donation" />
				<input type="hidden" name="campaign_id" value="<?php echo esc_attr($campaign->ID) ?>" />
				<button type="submit" name="charitable_submit" class="uk-button uk-padding-remove-horizontal uk-margin-top uk-margin-small-right uk-text-bold"><?php esc_attr_e( 'Donate Now', 'orphan' ) ?> <span uk-icon="icon: chevron-right;"></span></button>
			</form>

		</div>
	</div>
</div>

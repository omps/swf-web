<?php
/**
 * The template used to display error messages.
 *
 * @author  Studio 164a
 * @since   1.0.0
 * @version 1.3.0
 */

if ( ! isset( $view_args['errors'] ) ) {
	return;
}

$errors = $view_args['errors'];

?>
<div class="charitable-form-errors uk-alert-warning" uk-alert>
	<a class="uk-alert-close" uk-close></a>
	<ul class="uk-list">
		<?php foreach ( $errors as $error ) : ?>
			<li><?php echo esc_html($error) ?></li>
		<?php endforeach ?>
	</ul>
</div>

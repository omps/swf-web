<?php
if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

if ( ! class_exists( 'Orphan_Charitable' ) ) :

	/**
	 * Orphan_Charitable
	 *
	 * @since       1.2.0
	 */
	class Orphan_Charitable {

		/**
		 * This creates an instance of this class.
		 *
		 * If the orphan_theme_start hook has already run, this will not do anything.
		 *
		 * @param   Orphan_Theme     $theme
		 * @static
		 * @access  public
		 * @since   1.2.0
		 */
		public static function start( ) {
			new Orphan_Charitable();
		}


		/**
		 * Create object instance.
		 *
		 * @access  private
		 * @since   1.2.0
		 */
		private function __construct() {
			$this->attach_hooks_and_filters();
		}

		/**
		 * Set up hooks and filters.
		 *
		 * @return  void
		 * @access  private
		 * @since   1.2.0
		 */
		private function attach_hooks_and_filters() {
			add_action( 'after_setup_theme', array( $this, 'load_dependencies'), 20 ); // Priority must be greater than 10
			add_action( 'after_setup_theme', array($this, 'thumbnail_size' ), 20 ); // Priority must be greater than 10
			add_action( 'wp_enqueue_scripts', array( $this, 'dequeue_styles' ), 100 );
			add_filter( 'charitable_campaign_post_type', array( $this, 'turn_on_campaign_archives' ) );
			add_filter( 'body_class', array( $this, 'add_body_classes' ) );
			add_filter( 'orphan_banner_title', array( $this, 'set_banner_title' ) );
			add_filter( 'charitable_is_in_user_dashboard', array( $this, 'load_donation_receipt_in_user_dashboard' ) );
			add_filter( 'charitable_campaign_ended', 'orphan_campaign_ended_text' );
			add_filter( 'charitable_force_user_dashboard_template', '__return_true' );
			add_filter( 'charitable_use_campaign_template', '__return_false' );
			add_filter( 'charitable_campaign_video_embed_args', array( $this, 'video_embed_args' ), 5 );
			add_filter( 'charitable_add_custom_styles', '__return_false' );
			add_filter( 'charitable_campaign_widget_thumbnail_size', array( $this, 'set_campaign_widget_thumbnail_size' ) );
		}

		/**
		 * Custom Image Size
		 */
		public function thumbnail_size () {
			if ( function_exists('add_image_size')) {
				add_image_size( 'orphan_charitable', 640, 420, true); // Charitable Image Size
			}
		}

		/**
		 * Include required files.
		 *
		 * @return  void
		 * @access  public
		 * @since   1.2.0
		 */
		public function load_dependencies() {
			require_once get_parent_theme_file_path('charitable/charitable-hooks.php' );
			require_once get_parent_theme_file_path('charitable/charitable-tags.php' );
		}


		/**
		 * Dequeue styles.
		 *
		 * @return  void
		 * @access  public
		 * @since   0.9.24
		 */
		public function dequeue_styles() {
			wp_deregister_style( 'charitable-ambassadors-my-campaigns-css' );
			wp_deregister_style( 'charitable-ambassadors-styles' );
		}

		/**
		 * Turn on post type archives for the campaigns.
		 *
		 * @param   array $post_type_args
		 * @return  array
		 * @access  public
		 * @since   1.2.0
		 */
		public function turn_on_campaign_archives( $post_type_args ) {
			$post_type_args['has_archive'] = true;
			return $post_type_args;
		}

		/**
		 * If we are viewing a single campaign page, add a class to the body for the style of donation form.
		 *
		 * @param   string[] $classes
		 * @return  string[]
		 * @access  public
		 * @since   1.2.0
		 */
		public function add_body_classes( $classes ) {
			if ( charitable_is_campaign_page() ) {

				$campaign = new Charitable_Campaign( get_the_ID() );

				if ( $campaign->has_ended() ) {
					$classes[] = 'campaign-ended';
				} else {
					$classes[] = 'donation-form-display-' . charitable_get_option( 'donation_form_display', 'separate_page' );
				}
			}

			return $classes;
		}

		/**
		 * Set banner title for campaign donation page.
		 *
		 * @global  WP_Query    $wp_query
		 * @param   string      $title
		 * @return  string
		 * @access  public
		 * @since   1.2.0
		 */
		public function set_banner_title( $title ) {
			global $wp_query;

			if ( isset( $wp_query->query_vars['donate'] ) && is_singular( 'campaign' ) ) {
				$title = get_the_title();
			} elseif ( charitable_is_page( 'donation_receipt_page' ) ) {
				$title = apply_filters( 'orphan_banner_title_donation_receipt', esc_html__( 'Donation Receipt', 'orphan' ) );
			} elseif ( charitable_get_user_dashboard()->in_nav() ) {
				$title = apply_filters( 'orphan_banner_title_user_dashboard', esc_html__( 'Dashboard', 'orphan' ) );
			}

			return $title;
		}

		/**
		 * Load the donation receipt inside the user dashboard.
		 *
		 * @param   boolean $ret
		 * @return  boolean
		 * @access  public
		 * @since   1.2.0
		 */
		public function load_donation_receipt_in_user_dashboard( $ret ) {
			if ( is_front_page() || is_home() ) {
				return false;
			}

			if ( charitable_is_page( 'donation_receipt_page' ) ) {
				$ret = true;
			}

			return $ret;
		}

		/**
		 * Video embed width argument set to 1098px (fullwidth).
		 *
		 * @param   array   $args
		 * @return  array
		 * @access  public
		 * @since   1.2.0
		 */
		public function video_embed_args( $args ) {
			$args['width'] = 1098;
			return $args;
		}

		/**
		 * Set the thumbnail size for campaign widgets.
		 *
		 * @return  string
		 * @access  public
		 * @since   1.2.0
		 */
		public function set_campaign_widget_thumbnail_size() {
			return 'orphan-post-thumbnail-medium';
		}
	}

endif;

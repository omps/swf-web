<?php
/**
 * Single campaign template.
 * This template is only used if Charitable is active.
 * @package Orphan
 */

get_header(); 

// Layout
$position = get_theme_mod('orphan_charitable_sidebar', 'sidebar-right');

?>

<div<?php echo orphan_helper::section(); ?>>
	<div<?php echo orphan_helper::container(); ?>>
		<div<?php echo orphan_helper::grid(); ?>>
			<div class="uk-width-expand">
				<main class="tm-content">
			
					<?php if (have_posts()) : while (have_posts()) : the_post(); 

						/**
						 * @var Charitable_Campaign
						 */
						$campaign = charitable_get_current_campaign();


						/**
						 * @hook charitable_campaign_content_before
						 */
						do_action( 'charitable_campaign_content_before', $campaign );

						?>
						
						<?php do_action( 'charitable_single_campaign_before', $campaign ); ?>

						<div>
							<?php get_template_part( 'charitable/content-campaign', 'campaign' ); ?>
						</div>

						<?php

						/**
						 * @hook charitable_campaign_content_after
						 */
						do_action( 'charitable_campaign_content_after', $campaign );

						get_template_part( 'template-parts/related' );

						get_template_part( 'template-parts/blognextprev' ); 
				
					endwhile; endif; ?>
				</main> <!-- end main -->
			</div>


			<?php if(($position == 'sidebar-left' || $position == 'sidebar-right') && is_active_sidebar( 'campaign-widgets' )) : ?>
				<aside<?php echo orphan_helper::sidebar($position); ?>>
					<?php
						
						/* Charitable Sidebar */
						if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('campaign-widgets') );
					?>
				</aside> <!-- end aside -->
			<?php endif; ?>

		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->

<?php get_footer(); ?>
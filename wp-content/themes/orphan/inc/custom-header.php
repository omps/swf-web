<?php
/**
 * Set up the WordPress core custom header feature.
 * @uses orphan_header_style() fuzzy function for validation
 */
function orphan_custom_header_setup() {
	add_theme_support( 'custom-header');
}
add_action( 'after_setup_theme', 'orphan_custom_header_setup' );

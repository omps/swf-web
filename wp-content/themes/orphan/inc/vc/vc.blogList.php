<?php

if (function_exists('bdthemes_blog_list')) {
	function orphan_blog_list_vc() {

		$post_grid_options = NULL;

		$post_grid_filters = get_terms('category');

		foreach ($post_grid_filters as $filter) {
			$post_grid_options[$filter->name] = $filter->slug;
		}

		vc_map( array(
			"name"					=> esc_html__( "Blog List", 'orphan'),
			"description"			=> esc_html__( "Show blog post in list layout", 'orphan'),
			"base"					=> "bdt_blog_list",
			'category'				=> "Theme Addons",
			"icon"					=> "icon-wpb-post-grid",
			"params"				=> array(
				array(
					"type"        => "number",
					"heading"     => esc_html__( "Post(s)", 'orphan'),
					"param_name"  => "posts",
					"value"       => "3",
					"std"         => "3",
					"description" => esc_html__( "Number of post you want to show.", 'orphan'),
				),
				array(
					"type"			=> "checkbox",
					"class"			=> "",
					"heading"		=> esc_html__( "Only Specific Filters?", 'orphan'),
					"param_name"	=> "categories",
					"value"			=> $post_grid_options,
					"description"	=> esc_html__( "If nothing is selected, it will show Items from <strong>ALL</strong> categories.", 'orphan'),
				),
				array(
					"type"			=> "colorpicker",
					"heading"		=> esc_html__( "Title Color", 'orphan'),
					"param_name"	=> "title_color",
				)
			)
		) );
	}
	add_action( 'vc_before_init', 'orphan_blog_list_vc' );

}
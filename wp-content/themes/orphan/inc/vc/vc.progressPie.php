<?php
	if (function_exists('bdthemes_progress_pie')) {
		function orphan_progress_pie_vc() {
			vc_map( array(
				"name"        => esc_html__( "Progress Pie", 'orphan'),
				"description" => esc_html__( "Customizable progress pie", 'orphan'),
				"base"        => "bdt_progress_pie",
				"icon"        => "vc-progress-pie",
				'category'    => "Theme Addons",
				"params"      => array(
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Percent', 'orphan'),
						"admin_label" => true,
						'param_name'  => 'percent',
						'value'       => 75,
						'description' => esc_html__( 'Specify percentage value.', 'orphan')
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Before Text', 'orphan'),
						'param_name'  => 'before',
						'description' => esc_html__( 'This content will be shown before the percent.', 'orphan')
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Middle Text', 'orphan'),
						'param_name'  => 'text',
						'description' => esc_html__( 'You can show custom text. Leave this field empty to show the percentage value.', 'orphan')
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'After Text', 'orphan'),
						'param_name'  => 'after',
						'description' => esc_html__( 'This content will be shown after the percent.', 'orphan')
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Progress Pie Title', 'orphan'),
						"admin_label" => true,
						'param_name'  => 'after_title',
						'value'       => 'Pie Title',
						'description' => esc_html__( 'This content will be shown as progress pie title.', 'orphan')
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Text Size', 'orphan'),
						'param_name'  => 'text_size',
						'description' => esc_html__( 'Select your text size (pixel)', 'orphan')
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Background', 'orphan'),
						'param_name'  => 'background',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of background from dropdown list', 'orphan'),
						'value'       => array(
							'Default'   => 'default',
							'Muted'     => 'muted',
							'Primary'   => 'primary',
							'Secondary' => 'secondary',
							'Custom'    => 'custom',
							'No'        => 'none',
		                ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Custom Background', 'orphan'),
						'param_name'  => 'custom_background',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter background value for progress pie, example: rgba(0,0,0,0.1)', 'orphan'),
						"dependency"  => array(
							"element" => 'background',
							"value"   => 'custom',
						),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Text Color', 'orphan'),
						'param_name'  => 'text_color',
						'value'       => '#444444',
						'group'       => 'Styles',
						'description' => esc_html__( 'You can select text color from here.', 'orphan')
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Padding', 'orphan'),
						'param_name'  => 'padding',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of padding from dropdown list', 'orphan'),
						'std'         => 'medium',
						'value'       => array(
							'Small'  => 'small',
							'Medium' => 'medium',
							'Large'  => 'large',
							'Custom' => 'custom',
							'No'     => 'none',
		                ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Custom Padding', 'orphan'),
						'param_name'  => 'custom_padding',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter padding value for progress pie, example: 20px', 'orphan'),
						"dependency"  => array(
							"element" => 'padding',
							"value"   => 'custom',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Box Shadow', 'orphan'),
						'param_name'  => 'shadow',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of shadow from dropdown list', 'orphan'),
						'value'       => array(
							'Small'       => 'small',
							'Medium'      => 'medium',
							'Large'       => 'large',
							'Extra Large' => 'xlarge',
							'No'          => 'none',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Hover Shadow', 'orphan'),
						'param_name'  => 'hover_shadow',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of hover shadow from dropdown list', 'orphan'),
						'std'         => 'none',
						'value'       => array(
							'Small'       => 'small',
							'Medium'      => 'medium',
							'Large'       => 'large',
							'Extra Large' => 'xlarge',
							'No'          => 'none',
		                ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Border', 'orphan'),
						'param_name'  => 'border',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter border value for progress pie, for example: 1px solid #dddddd', 'orphan')
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Border Radius', 'orphan'),
						'param_name' => 'radius',
						"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
						'group'      => 'Styles',
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Duration', 'orphan'),
						'param_name'  => 'duration',
						'value'       => 1,
						'description' => esc_html__( 'You can set animation duration as (seconds) units from here.', 'orphan')
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Delay', 'orphan'),
						'param_name'  => 'delay',
						'value'       => 1,
						'description' => esc_html__( 'After mentioned time (in second) animation will start.', 'orphan')
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Line Width', 'orphan'),
						'param_name'  => 'line_width',
						'value'       => 8,
						'description' => esc_html__( 'Set your pie width from here.', 'orphan')
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Line Cap", "orphan"),
						"param_name"  => "line_cap",
						"description" => esc_html__("Set your line edge cap style from here.", "orphan"),
						"group"       => esc_html__( 'Styles', "orphan"),
						'value'       => array(
							'Round'  => 'round',
							'Square' => 'square',
							'Butt'   => 'butt'
		                ),
					)
				)
			) );
		}
		add_action( 'vc_before_init', 'orphan_progress_pie_vc' );
	}
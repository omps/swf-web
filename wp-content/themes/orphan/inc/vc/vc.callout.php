<?php

if (function_exists('bdthemes_callout')) {
	function orphan_callout_vc() {
		vc_map( array(
			"name"        => esc_html__( "Call Out", 'orphan'),
			"description" => esc_html__( "Make your call out easily.", 'orphan'),
			"base"        => "bdt_callout",
			"icon"        => "vc-call-out",
			'category'    => "Theme Addons",
			"params"      => array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Title', 'orphan'),
					"admin_label" => true,
					'param_name'  => 'title',
					'description' => esc_html__( 'Title of the call to aciton.', 'orphan')
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Button Text', 'orphan'),
					'param_name'  => 'button_txt',
					'description' => esc_html__( 'Button text of the call to aciton.', 'orphan')
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Button Link', 'orphan'),
					'value'       => '#',
					'param_name'  => 'button_link',
					'description' => esc_html__( 'You can type here any hyperlink to make link in button.', 'orphan')
				),
				array(
					"type"       => "dropdown",
					"heading"    => esc_html__("Target", "orphan"),
					"param_name" => "target",
					'value'      => array(
						'Self'   => 'self',
						'Blank'  => 'blank'
	                ),
					"description" => esc_html__("Set link target self or blank.", "orphan")
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Background', 'orphan'),
					'param_name'  => 'background',
					'group'       => 'Styles',
					'description' => esc_html__( 'Select a type of background from dropdown list', 'orphan'),
					'value'       => array(
						'Default'   => 'default',
						'Muted'     => 'muted',
						'Primary'   => 'primary',
						'Secondary' => 'secondary',
						'Custom'    => 'custom',
						'No'        => 'none',
	                ),
				),
				array(
					'type'        => 'colorpicker',
					'heading'     => esc_html__( 'Custom Background', 'orphan'),
					'param_name'  => 'custom_background',
					'group'       => 'Styles',
					"dependency"  => array(
						"element" => 'background',
						"value"   => 'custom',
					),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Text Color', 'orphan'),
					'param_name'  => 'color',
					'group'       => 'Styles',
					'description' => esc_html__( 'Select a type of color from dropdown list', 'orphan'),
					'value'       => array(
						'Default' => 'dark',
						'Light'   => 'light',
	                ),
				),
				array(
					'type'        => 'colorpicker',
					'heading'     => esc_html__( 'Custom Color', 'orphan'),
					'param_name'  => 'custom_color',
					'group'       => 'Styles',
					"dependency"  => array(
						"element" => 'color',
						"value"   => 'custom',
					),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Padding', 'orphan'),
					'param_name'  => 'padding',
					'group'       => 'Styles',
					'description' => esc_html__( 'Select a type of padding from dropdown list', 'orphan'),
					'std'         => 'medium',
					'value'       => array(
						'Small'  => 'small',
						'Medium' => 'medium',
						'Large'  => 'large',
						'Custom' => 'custom',
						'No'     => 'none',
	                ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Custom Padding', 'orphan'),
					'param_name'  => 'custom_padding',
					'group'       => 'Styles',
					"dependency"  => array(
						"element" => 'padding',
						"value"   => 'custom',
					),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Shadow', 'orphan'),
					'param_name'  => 'shadow',
					'group'       => 'Styles',
					'description' => esc_html__( 'Select a type of shadow from dropdown list', 'orphan'),
					'std'         => 'none',
					'value'       => array(
						'Small'       => 'small',
						'Medium'      => 'medium',
						'Large'       => 'large',
						'Extra Large' => 'xlarge',
						'No'          => 'none',
	                ),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Hover Shadow', 'orphan'),
					'param_name'  => 'hover_shadow',
					'group'       => 'Styles',
					'description' => esc_html__( 'Select a type of hover shadow from dropdown list', 'orphan'),
					'std'         => 'none',
					'value'       => array(
						'Small'       => 'small',
						'Medium'      => 'medium',
						'Large'       => 'large',
						'Extra Large' => 'xlarge',
						'No'          => 'none',
	                ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Border', 'orphan'),
					'param_name'  => 'border',
					'group'       => 'Styles',
				),
				array(
					'type'       => 'checkbox',
					'heading'    => esc_html__( 'Border Radius', 'orphan'),
					'param_name' => 'radius',
					"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
					'group'      => 'Styles',
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Extra class name', 'orphan' ),
					'param_name'  => 'class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'orphan' ),
				),
				array(
					'type'       => 'textarea_html',
					'heading'    => esc_html__( 'Intro Text', 'orphan'),
					'param_name' => 'intro_txt',
					'value'      => "And it has huge awesome features, unlimited colors, advanced template admin options and so much more!",
				),
			)
		));
	}
	add_action( 'vc_before_init', 'orphan_callout_vc' );
}
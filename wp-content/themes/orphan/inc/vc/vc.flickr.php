<?php
	if (function_exists('bdthemes_flickr')) {
		function orphan_flickr_vc() {
			vc_map(
				array(
					"name"        => esc_html__("Flickr", "orphan"),
					"base"        => "bdt_flickr",
					"icon"        => "vc-flickr",
					"category"    => "Theme Addons",
					"description" => esc_html__("Flickr is for make flickr feed .", "orphan"),
					"params"      => array(
						array(
							"type"        => "textfield",
							"heading"     => esc_html__("Flickr ID", "orphan"),
							'param_name'  => 'id',
							"value"       => "95572727@N00", 
							"description" => esc_html__("Enter your flickr ID, To find your flickID visit <a href='http://idgettr.com/' target='_blank'>idGettr</a>", "orphan")
						),
		                array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Limit', 'orphan'),
							'param_name'  => 'limit',
							'value'       => array( 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 ),
							'description' => esc_html__( 'Select number of photos to display.', 'orphan')
		                ), 
						array(
							'type'        => 'checkbox',
							'heading'     => esc_html__( 'Lightbox', 'orphan'),
							'param_name'  => 'lightbox',
							'description' => esc_html__( 'If checked row will be set to full height.', 'orphan'),
							'value'       => array( esc_html__( 'Yes', 'orphan') => 'yes' )
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__("Radius", "orphan"),
							'param_name'  => 'radius',
							"value"       => "0px", 
							"description" => esc_html__("You can set border radius from here.", "orphan")
						),
					)	
				)
			);
		}
		add_action( 'vc_before_init', 'orphan_flickr_vc' );
	}
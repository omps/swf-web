<?php
	if (function_exists('bdthemes_divider')) {
		function orphan_divider_vc() {
			vc_map( array(
				"name"        => esc_html__( "Divider", 'orphan'),
				"description" => esc_html__( "Setting of Yes can cause for show divider", 'orphan'),
				"base"        => "bdt_divider",
				"icon"        => "vc-divider",
				'category'    => "Theme Addons",
				"params"      => array(
			   		array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Divider Align", "orphan"),
						"param_name"  => "align",
						"description" => esc_html__("Select divider alignment here.", "orphan"),
						'value'       => array(
							esc_html__("Center", "orphan") => 'center',
							esc_html__("Left", "orphan")   => 'left',
							esc_html__("Right", "orphan")  => 'right',
		                ),
					),
					array(
						"type"        => "checkbox",
						"heading"     => esc_html__("Add Divider Icon", "orphan"),
						"param_name"  => "icon",
						"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
						"description" => esc_html__("Add icon of the divider, just check it.", "orphan"),
					),
					array(
						"type"        => "checkbox",
						"heading"     => esc_html__("Small", "orphan"),
						"param_name"  => "small",
						"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
						"description" => esc_html__("Add icon of the divider, just check it.", "orphan"),
					),
			   		array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Margin", "orphan"),
						"param_name"  => "margin",
						"description" => esc_html__("Select divider margin from here.", "orphan"),
						'value'       => array(
							esc_html__("Small", "orphan")  => 'small',
							esc_html__("Medium", "orphan") => 'medium',
							esc_html__("Large", "orphan")  => 'large',
		                ),
					),
				)
			) );
		}
		add_action( 'vc_before_init', 'orphan_divider_vc' );
	}
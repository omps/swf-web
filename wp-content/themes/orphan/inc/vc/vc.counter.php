<?php

if (function_exists('bdthemes_counter_shortcode')) {		
	function orphan_counter_vc() {
		vc_map(
			array(
				"name"        => esc_html__("Counter", "orphan"),
				"base"        => "bdt_counter",
				"class"       => "vc_counter",
				"icon"        => "vc-counter",
				"category"    => "Theme Addons",
				"description" => esc_html__("Add animated counting number", "orphan"),
				"params"      => array(
					array(
						"type"        => "textfield",
						"heading"     => esc_html__("Count Start", "orphan"),
						"param_name"  => "count_start",
						"value"       => "0", 
						"description" => esc_html__("Enter the number that is minimum number of the counter where from counter will start counting", "orphan"),
					),
					array(
						"type"        => "textfield",
						"heading"     => esc_html__("Count End", "orphan"),
						"param_name"  => "count_end",
						"value"       => "5000", 
						"description" => esc_html__("Enter the number that is maximum number of the counter where to counter will finish counting", "orphan"),
					),
					array(
						"type"        => "textfield",
						"heading"     => esc_html__("Count Speed", "orphan"),
						"param_name"  => "counter_speed",
						"value"       => "5", 
						"description" => esc_html__("Counting will finish in specified time (in second)", "orphan"),
					),
					array(
						"type"        => "textfield",
						"heading"     => esc_html__("Prefix Text", "orphan"),
						"param_name"  => "prefix",
						"value"       => "", 
						"description" => esc_html__("You can add text before the count number. For example: $ sign", "orphan"),
					),
					array(
						"type"        => "textfield",
						"heading"     => esc_html__("Suffix Text", "orphan"),
						"param_name"  => "suffix",
						"value"       => "", 
						"description" => esc_html__("You can add text after the count number. For example: /-", "orphan"),
					),
					array(
						"type"        => "checkbox",
						"heading"     => esc_html__("Separator", "orphan"),
						"param_name"  => "separator",
						"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
						"description" => esc_html__("You can separate count text by comma(,) if you select yes.For example: 1,500", "orphan"),
					),
					array(
						"type"        => "checkbox",
						"heading"     => esc_html__("Add Icon", "orphan"),
						"param_name"  => "add_icon",
						"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
						"description" => esc_html__("If you want to add icon with counter, just check it.", "orphan"),
					),
					array(
						'type'       => 'iconpicker',
						'heading'    => esc_html__( 'Icon', 'orphan'),
						'param_name' => 'icon',
						'value'      => '', // default value to backend editor admin_label
						'settings'   => array(
							'emptyIcon'    => false,
							'iconsPerPage' => 500,
						),
						"group" => "Icon",
						'description'      => esc_html__( 'Select icon from library.', 'orphan'),
						'dependency'       => array(
							'element' => 'add_icon',
							'value'   => 'yes',
						),
					),
					array(
						"type"       => "dropdown",
						"heading"    => esc_html__("Icon Align", "orphan"),
						"param_name" => "align",
						"value"      => array(
							"Top"   =>'top',
							"Left"  =>"left",
							"Right" =>"right",
						),
						"group"       => "Icon",
						"description" => esc_html__("You can set alignment from here.", "orphan"),
						'dependency'  => array(
							'element' => 'add_icon',
							'value'   => 'yes',
						),
					),
					array(
						"type"        => "colorpicker",
						"heading"     => esc_html__("Icon Color", "orphan"),
						"param_name"  => "icon_color",
						"description" => esc_html__("Text color for time ticks Period.", "orphan"),
						"group"       => "Icon",
						'dependency'  => array(
							'element' => 'add_icon',
							'value'   => 'yes',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Icon  Size', 'orphan'),
						'param_name'  => 'icon_size',
						'value'       => "24px",
						'description' => esc_html__( 'Select your object size. Its will be set in pixel value', 'orphan'),
						"group"       => "Icon",
						'dependency'  => array(
							'element' => 'add_icon',
							'value'   => 'yes',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Count  Size', 'orphan'),
						'param_name'  => 'count_size',
						'value'       => "32px",
						"group"       => "Styles",
						'description' => esc_html__( 'Select your object size. Its will be set in pixel value', 'orphan'),
					),
					array(
						"type"        => "colorpicker",
						"heading"     => esc_html__("Count Color", "orphan"),
						"param_name"  => "count_color",
						"value"       => "#444444",
						"group"       => "Styles",
						"description" => esc_html__("Text color for time ticks Period.", "orphan"),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Text  Size', 'orphan'),
						'param_name'  => 'text_size',
						'value'       => "14px",
						"group"       => "Styles",
						'description' => esc_html__( 'Select your object size. Its will be set in pixel value', 'orphan'),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Text Color', 'orphan'),
						'param_name'  => 'color',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of color from dropdown list', 'orphan'),
						'value'       => array(
							'Default' => 'dark',
							'Light'   => 'light',
		                ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Custom Color', 'orphan'),
						'param_name'  => 'custom_color',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter color value for counter, example: rgba(0,0,0,0.1)', 'orphan'),
						"dependency"  => array(
							"element" => 'color',
							"value"   => 'custom',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Background', 'orphan'),
						'param_name'  => 'background',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of background from dropdown list', 'orphan'),
						'value'       => array(
							'Default'   => 'default',
							'Muted'     => 'muted',
							'Primary'   => 'primary',
							'Secondary' => 'secondary',
							'Custom'    => 'custom',
							'No'        => 'none',
		                ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Custom Background', 'orphan'),
						'param_name'  => 'custom_background',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter background value for counter, example: rgba(0,0,0,0.1)', 'orphan'),
						"dependency"  => array(
							"element" => 'background',
							"value"   => 'custom',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Padding', 'orphan'),
						'param_name'  => 'padding',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of padding from dropdown list', 'orphan'),
						'std'         => 'medium',
						'value'       => array(
							'Small'  => 'small',
							'Medium' => 'medium',
							'Large'  => 'large',
							'Custom' => 'custom',
							'No'     => 'none',
		                ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Custom Padding', 'orphan'),
						'param_name'  => 'custom_padding',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter padding value for counter, example: 20px', 'orphan'),
						"dependency"  => array(
							"element" => 'padding',
							"value"   => 'custom',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Shadow', 'orphan'),
						'param_name'  => 'shadow',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of shadow from dropdown list', 'orphan'),
						'value'       => array(
							'Small'       => 'small',
							'Medium'      => 'medium',
							'Large'       => 'large',
							'Extra Large' => 'xlarge',
							'No'          => 'none',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Hover Shadow', 'orphan'),
						'param_name'  => 'hover_shadow',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of hover shadow from dropdown list', 'orphan'),
						'std'         => 'none',
						'value'       => array(
							'Small'       => 'small',
							'Medium'      => 'medium',
							'Large'       => 'large',
							'Extra Large' => 'xlarge',
							'No'          => 'none',
		                ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Border', 'orphan'),
						'param_name'  => 'border',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter border value for counter, for example: 1px solid #dddddd', 'orphan')
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Border Radius', 'orphan'),
						'param_name' => 'radius',
						"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
						'group'      => 'Styles',
					),
					array(
						"type"        => "textarea_html",
						"heading"     => esc_html__("Description", "orphan"),
						"param_name"  => "content",
						"value"       => "",
						"description" => esc_html__("Provide the description for this icon box.", "orphan"),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'orphan'),
						'param_name'  => 'el_class',
						'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'orphan'),
					)
				)	
			)
		);
	}
	add_action( 'vc_before_init', 'orphan_counter_vc' );
}	
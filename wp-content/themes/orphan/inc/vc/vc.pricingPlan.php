<?php
	if (function_exists('bdthemes_pricing_plan')) {
		function orphan_pricing_plan_vc() {
			vc_map(
				array(
					"name"        => esc_html__("Pricing Plan", "orphan"),
					"base"        => "bdt_pricing_plan",
					"category"    => "Theme Addons",
					"icon"        => "vc-pricing-plan",
					"description" => esc_html__("Add your pricing table in your site", "orphan"),
					"params"      => array(
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Name', 'orphan'),
							"admin_label" => true,
							'param_name'  => 'name',
							'value'       => esc_html__( 'Standard', 'orphan'),
							'description' => esc_html__( 'Enter text here for your pricing plan', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Price', 'orphan'),
							'param_name'  => 'price',
							'value'       => esc_html__( '19.99', 'orphan'),
							'description' => esc_html__( 'Specify the price for this object.', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Before Price', 'orphan'),
							'param_name'  => 'before',
							'value'       => '$',
							'description' => esc_html__( 'This price will be shown as before price.', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'After Price', 'orphan'),
							'param_name'  => 'after',
							'description' => esc_html__( 'This price will be shown as after price.', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Period', 'orphan'),
							'param_name'  => 'period',
							'value'       => esc_html__( 'per month', 'orphan' ),
							'description' => esc_html__( 'Specify period. Leave this field empty to hide this text.', 'orphan'),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__( "Featured", 'orphan'),
							"param_name"  => "featured",
							'description' => esc_html__( 'Show this plan as featured.', 'orphan'),
							"value"       => array(
								esc_html__( 'No', 'orphan')  => 'no',
								esc_html__( 'Yes', 'orphan') => 'yes',
							),
						),
						array(
							'type'        => 'iconpicker',
							'heading'     => esc_html__( 'Icon', 'orphan'),
							'param_name'  => 'icon',
							'description' => esc_html__( 'Set icon of the pricing plan.', 'orphan'),
							'settings'    => array(
								'emptyIcon'    => true,
								'iconsPerPage' => 500,
							),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Button Text', 'orphan'),
							'param_name'  => 'btn_text',
							'value'       => 'Sign up Now',
							'description' => esc_html__( 'Button text of the pricing plan.', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Button URL', 'orphan'),
							'param_name'  => 'btn_url',
							'value'       => '#',
							'description' => esc_html__( 'Button url of the pricing plan.', 'orphan'),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__( "Button Target", 'orphan'),
							"param_name"  => "btn_target",
							'description' => esc_html__( 'Button target of the pricing plan.', 'orphan'),
							"std"         => "self",
							"value"       => array(
								esc_html__( 'Self', 'orphan' ) => 'self',
								esc_html__( 'Blank', 'orphan' )=> 'blank',
							),
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'btn_style',
							'heading'     => esc_html__( 'Button Style', 'orphan' ),
							'description' => esc_html__( 'Select a button style from dropdown list', 'orphan' ),
							'value'       => array(
								esc_html__( 'Default', 'orphan')    => 'default',
								esc_html__( 'Primary' , 'orphan')   => 'primary',
								esc_html__( 'Secondary' , 'orphan') => 'secondary',
								esc_html__( 'Danger' , 'orphan')    => 'danger',
								esc_html__( 'Text' , 'orphan')      => 'text',
								esc_html__( 'Link' , 'orphan')      => 'link',
			                ),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Badge', 'orphan'),
							'param_name'  => 'badge',
							'description' => esc_html__( 'Type your plan badge from here. Example: <b>Populer</b>', 'orphan'),
						),
						array(
							"type"        => "textarea_html",
							"class"       => "",
							"heading"     => esc_html__("Description", "orphan"),
							"param_name"  => "content",
							"description" => esc_html__("Provide the description for this icon box.", "orphan"),
							"value"       => '<ul>
											 	<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
											 	<li><strong>30GB</strong> Space amount</li>
											 	<li><strong>Unlimited</strong> users</li>
											 	<li><strong>60GB</strong> Bandwidth</li>
											 	<li>Basic Security</li>
											 	<li><strong>20</strong> MySQL Databases</li>
											</ul>',
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Background', 'orphan'),
							'param_name'  => 'background',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of background from dropdown list', 'orphan'),
							'value'       => array(
								'Default'   => 'default',
								'Muted'     => 'muted',
								'Primary'   => 'primary',
								'Secondary' => 'secondary',
								'Custom'    => 'custom',
								'No'        => 'none',
			                ),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Custom Background', 'orphan'),
							'param_name'  => 'custom_background',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter background value for pricing plan, example: rgba(0,0,0,0.1)', 'orphan'),
							"dependency"  => array(
								"element" => 'background',
								"value"   => 'custom',
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Text Color', 'orphan'),
							'param_name'  => 'color',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of color from dropdown list', 'orphan'),
							'value'       => array(
								'Default' => 'dark',
								'Light'   => 'light',
			                ),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Custom Color', 'orphan'),
							'param_name'  => 'custom_color',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter color value for pricing plan, example: rgba(0,0,0,0.1)', 'orphan'),
							"dependency"  => array(
								"element" => 'color',
								"value"   => 'custom',
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Shadow', 'orphan'),
							'param_name'  => 'shadow',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of shadow from dropdown list', 'orphan'),
							'std'         => 'none',
							'value'       => array(
								'Small'       => 'small',
								'Medium'      => 'medium',
								'Large'       => 'large',
								'Extra Large' => 'xlarge',
								'No'          => 'none',
			                ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Hover Shadow', 'orphan'),
							'param_name'  => 'hover_shadow',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of hover shadow from dropdown list', 'orphan'),
							'std'         => 'none',
							'value'       => array(
								'Small'       => 'small',
								'Medium'      => 'medium',
								'Large'       => 'large',
								'Extra Large' => 'xlarge',
								'No'          => 'none',
			                ),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Border', 'orphan'),
							'param_name'  => 'border',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter border value for pricing plan, for example: 1px solid #dddddd', 'orphan')
						),
						array(
							'type'       => 'checkbox',
							'heading'    => esc_html__( 'Border Radius', 'orphan'),
							'param_name' => 'radius',
							"value"      => array( esc_html__("Yes", "orphan") => 'yes' ),
							'group'      => 'Styles',
						),
					)
				)
			);
		}
		add_action( 'vc_before_init', 'orphan_pricing_plan_vc' );
	}
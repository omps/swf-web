<?php
	if (function_exists('bdthemes_spacer_shortcode')) {
		function orphan_spacer_vc() {
			vc_map( 
				array(
					"name"					=> esc_html__( "Spacer", 'orphan'),
					"description"			=> esc_html__( "Empty space with adjust table height", 'orphan'),
					"base"					=> "bdt_spacer",
					"icon"					=> "vc-spacer",
					'category'				=> "Theme Addons",
					"params"				=> array(
						array(
							"type"			=> "textfield",
							"heading"		=> esc_html__( "Spacer Size", 'orphan'),
							"admin_label"	=> true,
							"param_name"	=> "size",
							"value"			=> "20",
							"description"	=> esc_html__( "Set your spacer height. it's set px value", 'orphan')
						)
					)
				)
		 	);
		}
		add_action( 'vc_before_init', 'orphan_spacer_vc' );
	}
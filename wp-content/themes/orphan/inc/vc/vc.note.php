<?php
	if (function_exists('bdthemes_note_shortcode')) {
		function orphan_note_vc() {
			vc_map( array(
				"name"					=> esc_html__( "Note", 'orphan'),
				"description"			=> esc_html__( "Superb! various note style", 'orphan'),
				"base"					=> "bdt_note",
				"icon"					=> "vc-note",
				'category'				=> "Theme Addons",
				"params"				=> array(
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Style", 'orphan'),
						"param_name"  => "style",
						"description" => esc_html__( "You can set four attractive note style. You can set any style with any type", 'orphan'),
						"value"       => array(
							esc_html__( "Style 1", 'orphan') => "1",
							esc_html__( "Style 2", 'orphan') => "2",
							esc_html__( "Style 3", 'orphan') => "3",
							esc_html__( "Style 4", 'orphan') => "4",
							esc_html__( "Style 5", 'orphan') => "5",
							esc_html__( "Style 6", 'orphan') => "6",
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Type", 'orphan'),
						"param_name"  => "type",
						"description" => esc_html__( "You can set any note type into four note type. Please! select as you need.", 'orphan'),
						"value"       => array(
							esc_html__( "Info", "orphan")    => "info",
							esc_html__( "Success", "orphan") => "success",
							esc_html__( "Warning", "orphan") => "warning",
							esc_html__( "Danger", "orphan")  => "danger",
						),
					),
					array(
						"type"        => "checkbox",
						"heading"     => esc_html__( "Icon", 'orphan'),
						"param_name"  => "icon",
						"value"       => array( esc_html__( "Yes", 'orphan') => "yes" ),
						"description" => esc_html__( "If you want to show note icon then please select yes. default value is no", 'orphan')
					),
					array(
						"type"        => "textfield",
						"heading"     => esc_html__( "Radius", 'orphan'),
						"param_name"  => "radius",
						"value"       => "3px",
						"description" => esc_html__( "You can set border radius from here, for example: 3px 10px 25px also you can set value as em, % etc if you need", 'orphan')
					),
					array(
						'type'       => 'textarea_html',
						'holder'     => 'div',
						'heading'    => esc_html__( 'Text', 'orphan'),
						'param_name' => 'content',
						'value'      => esc_html__( "<p>Heads up! This alert needs your attention, but it's not super important.</p>", 'orphan'),
					)
				)
			) );
		}
		add_action( 'vc_before_init', 'orphan_note_vc' );
	}
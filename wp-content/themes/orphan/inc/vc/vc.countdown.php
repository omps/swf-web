<?php
	if (function_exists('bdthemes_countdown')) {
		function orphan_countdown_vc() {
			vc_map(
				array(
					"name"        => esc_html__("Countdown", "orphan"),
					"base"        => "bdt_countdown",
					"icon"        => "vc-countdown",
					"category"    => "Theme Addons",
					"description" => esc_html__("Countdown Timer.", "orphan"),
					"params"      => array(
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__("Select Countdown Date Type", "orphan"),
							"param_name"  => "post_type",
							"value"       => array(
								esc_html__("Campaign Date", "orphan") => "post_id",
								esc_html__("Custom Date", "orphan")   => "custom_date_time",
							), 
						),
						array(
							"type"        => "textfield",
							"admin_label" => true,
							"heading"     => esc_html__("Campaign Date", "orphan"),
							"param_name"  => "post_id",
							"description" => esc_html__("Date will come from campaign ID, just enter expected campaign ID, for Example 1869", "orphan"),
							"dependency"  => array(
								"element" => "post_type",
								"value"   => "post_id",
							),
						),
						array(
							"type"        => "textfield",
							"admin_label" => true,
							"heading"     => esc_html__("Custom Date and Time", "orphan"),
							"param_name"  => "datetime",
							"description" => esc_html__("For Example 2018-11-15T14:15:00, this option will work when campaign id not assigned.", "orphan"),
							"dependency"  => array(
								"element" => "post_type",
								"value"   => "custom_date_time",
							),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__("Count Size", "orphan"),
							"param_name"  => "count_size",
							"description" => esc_html__("Font size of count digit, example: 36px", "orphan"),
							"group"       => esc_html__( 'Styles', "orphan"),
							"std"         => 'default',
							"value"       => array(
								esc_html__("Default", "orphan") => "default",
								esc_html__("Small", "orphan")   => "small",
								esc_html__("Mediun", "orphan")  => "medium",
								esc_html__("Large", "orphan")   => "large",
							),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__("Count Color", "orphan"),
							"param_name"  => "count_color",
							"group"       => esc_html__( 'Styles', "orphan"),
							"description" => esc_html__("Count digit color.", "orphan"),
							"std"         => 'dark',
							"value"       => array(
								esc_html__("Dark", "orphan")  => "dark",
								esc_html__("Light", "orphan") => "light",
							),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__("Text Color", "orphan"),
							"param_name"  => "text",
							"group"       => esc_html__( 'Styles', "orphan"),
							"description" => esc_html__("Text color.", "orphan"),
							"std"         => 'dark',
							"value"       => array(
								esc_html__("Dark", "orphan")  => "dark",
								esc_html__("Light", "orphan") => "light",
							),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__("Layout", "orphan"),
							"param_name"  => "layout",
							"description" => esc_html__("Select countdown layout from dropdown list.", "orphan"),
							"value"       => array(
								esc_html__("Vertical", "orphan")   => "vertical",
								esc_html__("Horizontal", "orphan") => "horizontal",
							),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__("Time Name", "orphan"),
							"param_name"  => "time_name",
							"description" => esc_html__("Select show/hide for countdown time name from dropdown list.", "orphan"),
							"value"       => array(
								esc_html__("Show", "orphan") => "yes",
								esc_html__("Hide", "orphan") => "no",
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Align', 'orphan'),
							'param_name'  => 'align',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of align from dropdown list', 'orphan'),
							'std'         => 'none',
							'value'       => array(
								esc_html__('Left', 'orphan')   => 'left',
								esc_html__('Right', 'orphan')  => 'right',
								esc_html__('Center', 'orphan') => 'center',
			                ),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Custom Background', 'orphan'),
							'param_name'  => 'custom_background',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter background value for countdown, example: rgba(0,0,0,0.1)', 'orphan'),
							"dependency"  => array(
								"element" => 'background',
								"value"   => 'custom',
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Padding', 'orphan'),
							'param_name'  => 'padding',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of padding from dropdown list', 'orphan'),
							'std'         => 'none',
							'value'       => array(
								esc_html__('Small', 'orphan')  => 'small',
								esc_html__('Medium', 'orphan') => 'medium',
								esc_html__('Large', 'orphan')  => 'large',
								esc_html__('Custom', 'orphan') => 'custom',
								esc_html__('No', 'orphan')     => 'none',
			                ),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Custom Padding', 'orphan'),
							'param_name'  => 'custom_padding',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter padding value for countdown, example: 20px', 'orphan'),
							"dependency"  => array(
								"element" => 'padding',
								"value"   => 'custom',
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Shadow', 'orphan'),
							'param_name'  => 'shadow',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of shadow from dropdown list', 'orphan'),
							'std'         => 'none',
							'value'       => array(
								esc_html__('Small', 'orphan')       => 'small',
								esc_html__('Medium', 'orphan')      => 'medium',
								esc_html__('Large', 'orphan')       => 'large',
								esc_html__('Extra Large', 'orphan') => 'xlarge',
								esc_html__('No', 'orphan')          => 'none',
			                ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Hover Shadow', 'orphan'),
							'param_name'  => 'hover_shadow',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of hover shadow from dropdown list', 'orphan'),
							'std'         => 'none',
							'value'       => array(
								esc_html__('Small', 'orphan')       => 'small',
								esc_html__('Medium', 'orphan')      => 'medium',
								esc_html__('Large', 'orphan')       => 'large',
								esc_html__('Extra Large', 'orphan') => 'xlarge',
								esc_html__('No', 'orphan')          => 'none',
			                ),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Border', 'orphan'),
							'param_name'  => 'border',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter border value for countdown, for example: 1px solid #dddddd', 'orphan')
						),
						array(
							'type'       => 'checkbox',
							'heading'    => esc_html__( 'Border Radius', 'orphan'),
							'param_name' => 'radius',
							"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
							'group'      => 'Styles',
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__("Prefix Text", "orphan"),
							"param_name"  => "prefix",
							"value"       => "", 
							"description" => esc_html__("Want to break text with 2 line? use | in two words.", "orphan"),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__("Suffix Text", "orphan"),
							"param_name"  => "suffix",
							"value"       => "", 
							"description" => esc_html__("Want to break text with 2 line? use | in two words.", "orphan"),
						),

						array(
							"type"        => "textfield",
							"heading"     => esc_html__("Extra class name", "orphan"),
							"param_name"  => "class",
							"value"       => "", 
							"description" => esc_html__("Style particular content element differently - add a class name and refer to it in custom CSS.", "orphan"),
						),
					)	
				)
			);
		}
		add_action( 'vc_before_init', 'orphan_countdown_vc' );
	}
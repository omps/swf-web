<?php

if (function_exists('bdthemes_button')) {
	function orphan_button_vc() {
		vc_map( array(
			'name'         => esc_html__( 'Button', 'orphan' ),
			'base'         => 'bdt_button',
			'icon'         => 'icon-wpb-ui-button',
			'category'     => esc_html__( 'Theme Addons', 'orphan' ),
			'description'  => esc_html__( 'Easily create nice looking buttons, which come in different styles.', 'orphan' ),
			'params'       => array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Text', 'orphan'),
					'param_name'  => 'text',
					'value'       => 'BUTTON',
					'description' => esc_html__( 'Enter button text from here.', 'orphan'),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'URL', 'orphan'),
					'param_name'  => 'url',
					'value'       => '#',
					'description' => esc_html__( 'Enter button url from here.', 'orphan'),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html__( "Target", 'orphan'),
					"param_name"  => "target",
					'description' => esc_html__( 'Select a button target from dropdown list', 'orphan'),
					"std"         => "self",
					"value"       => array(
						esc_html__( 'Self', 'orphan' ) => 'self',
						esc_html__( 'Blank', 'orphan' )=> 'blank',
					),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Icon', 'orphan'),
					'param_name'  => 'icon',
					'description' => esc_html__( 'Type only uikit icon name here, ex: home, chevron-right etc.', 'orphan'),
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'style',
					'heading'     => esc_html__( 'Style', 'orphan' ),
					'description' => esc_html__( 'Select a button style from dropdown list', 'orphan' ),
					'value'       => array(
						esc_html__( 'Default', 'orphan')    => 'default',
						esc_html__( 'Primary' , 'orphan')   => 'primary',
						esc_html__( 'Secondary' , 'orphan') => 'secondary',
						esc_html__( 'Danger' , 'orphan')    => 'danger',
						esc_html__( 'Text' , 'orphan')      => 'text',
						esc_html__( 'Link' , 'orphan')      => 'link',
	                ),
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'size',
					'heading'     => esc_html__( 'Size', 'orphan' ),
					'description' => esc_html__( 'Select a button size from dropdown list', 'orphan' ),
					'value'       => array(
						esc_html__( 'Default', 'orphan')     => 'default',
						esc_html__( 'Small' , 'orphan')      => 'small',
						esc_html__( 'Large' , 'orphan')      => 'large',
						esc_html__( 'Full Width' , 'orphan') => 'full_width',
	                ),
				),
				array(
					'type'       => 'checkbox',
					'heading'    => esc_html__( 'Border Radius', 'orphan'),
					'param_name' => 'radius',
					"value"      => array( esc_html__("Yes", "orphan") => 'yes' ),
				),
				array(
					'type'        => 'checkbox',
					'param_name'  => 'disabled',
					'heading'     => esc_html__( 'Want to disabled this button?', 'orphan' ),
					"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Extra class name', 'orphan' ),
					'param_name'  => 'class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'orphan' ),
				),
			),
		));
	}
	add_action( 'vc_before_init', 'orphan_button_vc' );
}
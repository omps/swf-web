<?php
	if (function_exists('bdthemes_gallery')) {
		function orphan_gallery_vc() {
			vc_map( array(
				"name"        => esc_html__( "Photo Gallery", 'orphan'),
				"description" => esc_html__( "Show Photo Gallery", 'orphan'),
				"base"        => "bdt_gallery",
				'category'    => "Theme Addons",
				"icon"        => "vc-gallery",
				"params"      => array(
					array(
						"type"       => "dropdown",
						"heading"    => esc_html__( "Gallery Source", 'orphan'),
						"admin_label" => true,
						"param_name" => "post_type",
						"value"      => array(
							esc_html__( 'Post', 'orphan')     => 'post',
							esc_html__( 'Event', 'orphan')    => 'tribe_events',
							esc_html__( 'Campaign', 'orphan') => 'campaign',
							esc_html__( 'Custom', 'orphan')   => 'custom'
						),
					),
					array(
						"type"        => "attach_images",
						"admin_label" => true,
						"heading"     => esc_html__( "Gallery Images", 'orphan'),
						"param_name"  => "images",
						"description" => esc_html__( "Upload your Images here.", 'orphan'),
						"dependency"  => array(
							"element" => 'post_type',
							"value"   => 'custom',
						),
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Number Of Image", 'orphan'),
						"param_name"	=> "limit",
						"value"			=> 8,
						"description"	=> esc_html__( "Limit of the gallery image.", 'orphan')
					),
					array(
						"type"       => "dropdown",
						"heading"    => esc_html__( "Lightbox", 'orphan'),
						"param_name" => "lightbox",
						"value"      => array(
							esc_html__( 'Yes', 'orphan') => 'yes',
							esc_html__( 'No', 'orphan')  => 'none',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Order", 'orphan'),
						"param_name"  => "order",
						'description' => esc_html__( 'Select order from here.', 'orphan'),
						"value"       => array(
							esc_html__( 'Descending', 'orphan') => 'DESC',
							esc_html__( 'Ascending', 'orphan')  => 'ASC',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Horizontal Gap", 'orphan'),
						"param_name"  => "horizontal_gap",
						"std"         => "10",
						"description" => esc_html__( "Select horizontal gap of the gallery item.", 'orphan'),
						"value"       => array(
							"No" => "0",
							"5"  => "5",
							"10" => "10",
							"15" => "15",
							"20" => "20",
							"25" => "25",
							"35" => "35",
							"45" => "45",
							"50" => "50"
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Vertical Gap", 'orphan'),
						"param_name"  => "vertical_gap",
						"std"         => "10",
						"description" => esc_html__( "Select vertical gap of the gallery item.", 'orphan'),
						"value"       => array(
							"No" => "0",
							"5"  => "5",
							"10" => "10",
							"15" => "15",
							"20" => "20",
							"25" => "25",
							"35" => "35",
							"45" => "45",
							"50" => "50"
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Large View", "orphan"),
						"param_name"  => "large",
						"std"         => 4,
						"group"       => esc_html__( 'Responsive', "orphan"),
						"description" => esc_html__("Large view item of the post carousel.", "orphan"),
						"value"       => array(
							esc_html__('1', 'orphan') => 1,
							esc_html__('2', 'orphan') => 2,
							esc_html__('3', 'orphan') => 3,
							esc_html__('4', 'orphan') => 4,
							esc_html__('5', 'orphan') => 5,
							esc_html__('6', 'orphan') => 6
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Medium View", "orphan"),
						"param_name"  => "medium",
						"std"         => 3,
						"group"       => esc_html__( 'Responsive', "orphan"),
						"description" => esc_html__("Medium view item of the post carousel.", "orphan"),
							"value"       => array(
							esc_html__('1', 'orphan') => 1,
							esc_html__('2', 'orphan') => 2,
							esc_html__('3', 'orphan') => 3,
							esc_html__('4', 'orphan') => 4
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Small View", "orphan"),
						"param_name"  => "small",
						"group"       => esc_html__( 'Responsive', "orphan"),
						"std"         => 2,
						"description" => esc_html__("Small view item of the post carousel.", "orphan"),
						"value"       => array(
							esc_html__('1', 'orphan') => 1,
							esc_html__('2', 'orphan') => 2,
							esc_html__('3', 'orphan') => 3
						),
					),
				)
			) );
		}
		add_action( 'vc_before_init', 'orphan_gallery_vc' );
	}
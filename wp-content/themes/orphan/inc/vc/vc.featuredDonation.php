<?php
	if (function_exists('bdthemes_featured_donation_shortcode')) {
		function orphan_featured_donation_vc() {
			vc_map(
				array(
					"name"        => esc_html__("Featured Donation", "orphan"),
					"base"        => "bdt_featured_donation",
					"icon"        => "vc-featured-donation",
					"category"    => "Theme Addons",
					"description" => esc_html__("You can add featured donation.", "orphan"),
					"params"      => array(
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title', 'orphan'),
							'value'       => 'Featured Donation Title',
							'param_name'  => 'title',
							'description' => esc_html__( 'Type here that you want to show for title', 'orphan'),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Title Color', 'orphan'),
							'value'       => '#ffffff',
							'param_name'  => 'title_color',
							'group'       => 'Style',
							'description' => esc_html__( 'Choose a color for title', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title Size', 'orphan'),
							'value'       => '20px',
							'param_name'  => 'title_size',
							'group'       => 'Style',
							'description' => esc_html__( 'Enter pixel value for title size', 'orphan'),
						),
						array(
							'type'        => 'attach_image',
							'heading'     => esc_html__( 'Photo', 'orphan'),
							'param_name'  => 'image',
							'value'       => '',
							'description' => esc_html__( 'Select image from media library.', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Goal Value', 'orphan'),
							'param_name'  => 'goal',
							'value'       => '',
							'group'       => 'Progress',
							'description' => esc_html__( 'Enter numeric goal value.', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Achieve Value', 'orphan'),
							'param_name'  => 'achieve',
							'value'       => '',
							'group'       => 'Progress',
							'description' => esc_html__( 'Enter numeric achieve value.', 'orphan'),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Bar Color', 'orphan'),
							'param_name'  => 'bar_color',
							'value'       => '#E8E8E8',
							'group'       => 'Progress',
							'description' => esc_html__( 'Choose a color for progress bar', 'orphan'),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Fill Color', 'orphan'),
							'param_name'  => 'fill_color',
							'value'       => '#F39C12',
							'group'       => 'Progress',
							'description' => esc_html__( 'Choose progress bar fill color', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Extra class name', 'orphan'),
							'param_name'  => 'class',
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'orphan'),
						),
						array(
							'type'        => 'textarea_html',
							'heading'     => esc_html__( 'Content Text', 'orphan'),
							'param_name'  => 'content',
							'value'       => esc_html__( 'I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.<br></br>
							<a href="#" class="readon border">Donate Now</a>', 'orphan'),
						)
					)	
				)
			);
		}
		add_action( 'vc_before_init', 'orphan_featured_donation_vc' );
	}

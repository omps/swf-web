<?php
	if (function_exists('bdthemes_inline_icon_shortcode')) {
		function orphan_inline_icon_vc() {
			vc_map(
				array(
					"name"                    => esc_html__("Inline Icon", "orphan"),
					"base"                    => "bdt_inline_icon",
					"icon"                    => "vc-icon",
					"category"                => "Theme Addons",
					"description"             => esc_html__("Adds icon box with custom font icon", "orphan"),
					"params" => array(
						array(
							'type'        => 'iconpicker',
							'heading'     => esc_html__( 'Icon', 'orphan'),
							'param_name'  => 'icon',
							'value'       => 'fa fa-heart',
							'description' => esc_html__( 'Select icon from library.', 'orphan'),
							'settings'    => array(
								'emptyIcon'    => false,
								'iconsPerPage' => 500,
							),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Icon color', 'orphan'),
							'param_name'  => 'color',
							'value'       => '#333333',
							'group'       => esc_html__( 'Style', 'orphan'),
							'description' => esc_html__( 'This color will be applied to the selected icon.', 'orphan'),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Icon Background', 'orphan'),
							'param_name'  => 'background',
							'value'       => 'transparent',
							'group'       => esc_html__( 'Style', 'orphan'),
							'description' => esc_html__( 'Select icon background color.', 'orphan')
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Icon Size', 'orphan'),
							'param_name'  => 'size',
							'value'       => '16',
							'group'       => esc_html__( 'Style', 'orphan'),
							'description' => esc_html__( 'You can set icon size from here. Icon size set only pixel value.', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Icon Border', 'orphan'),
							'param_name'  => 'border',
							'value'       => '0px solid #cccccc',
							'group'       => esc_html__( 'Style', 'orphan'),
							'description' => esc_html__( 'You can set content border from here.', 'orphan'),
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Icon  Radius', 'orphan'),
							'param_name' => 'radius',
							'value'      => '0px',
							'group'      => esc_html__( 'Style', 'orphan'),
							'description' => esc_html__("You can set border radius from here, for example: <b class='su-generator-set-value' title='Click to set this value'>3px</b> <b class='su-generator-set-value' title='Click to set this value'>10px</b> <b class='su-generator-set-value' title='Click to set this value'>25px</b> also you can set value as em, % etc if you need", 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Icon Margin', 'orphan'),
							'param_name'  => 'margin',
							'value'       => '0px',
							'group'       => esc_html__( 'Style', 'orphan'),
							'description' => esc_html__( 'You can set margin from here.', 'orphan'),
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Icon Padding', 'orphan'),
							'param_name' => 'padding',
							'value'      => '15px',
							'group'      => esc_html__( 'Style', 'orphan'),
							'description' => esc_html__( "You can set padding from here, for example: <b class='su-generator-set-value' title='Click to set this value'>5px</b> <b class='su-generator-set-value' title='Click to set this value'>10px</b> <b class='su-generator-set-value' title='Click to set this value'>25px</b> also you can set value as em, % etc if you need", 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'URL', 'orphan'),
							'param_name'  => 'url',
							'description' => esc_html__( 'URL/Link of the author. Leave empty to disable link.', 'orphan'),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Link Target', 'orphan'),
							'param_name'  => 'target',
							'description' => esc_html__( 'Select where to open  custom links.', 'orphan'),
							'value'       => array(
								esc_html__( 'Same window', 'orphan') => '_self',
								esc_html__( 'New window', 'orphan') => '_blank',
							),		
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Inline Text', 'orphan'),
							'param_name'  => 'inline_text',
							'description' => esc_html__( '(optional) if you want show text with icon', 'orphan'),
						)
					) 
				)
			);
		}
		add_action( 'vc_before_init', 'orphan_inline_icon_vc' );
	}
<?php
	if (function_exists('bdthemes_progress_bar')) {
		function orphan_progress_bar_vc() {
			vc_map( array(
				"name"        => esc_html__( "Progress Bar", 'orphan'),
				"description" => esc_html__( "Setting of Yes can cause for show divider", 'orphan'),
				"base"        => "bdt_progress_bar",
				"icon"        => "vc-progress-bar",
				'category'    => "Theme Addons",
				"params"      => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Progress Bar Text', 'orphan'),
						'value'       => "HTML",
						'param_name'  => 'text',
						'description' => esc_html__( 'Type your progress bar text to this input box.', 'orphan')
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Percent', 'orphan'),
						'param_name'  => 'percent',
						'value'       => 75,
						'description' => esc_html__( 'Percentage of the progress bar.', 'orphan')
					),
					array(
						"type"        => "checkbox",
						"heading"     => esc_html__( "Show Percent", 'orphan'),
						"param_name"  => "show_percent",
						"value"       => array( esc_html__( "Yes", 'orphan') => "yes" ),
						"description" => esc_html__( "You can show or hide percent from here.", 'orphan')
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Animation", "orphan"),
						"param_name"  => "animation",
						"description" => esc_html__("Select animation of the progress bar.", "orphan"),
						'value'       => array(
							'linear'           => 'linear',
							'swing'            => 'swing',
							'jswing'           => 'jswing',
							'easeInQuad'       => 'easeInQuad',
							'easeInCubic'      => 'easeInCubic',
							'easeInQuart'      => 'easeInQuart',
							'easeInQuint'      => 'easeInQuint',
							'easeInSine'       => 'easeInSine',
							'easeInExpo'       => 'easeInExpo',
							'easeInCirc'       => 'easeInCirc',
							'easeInElastic'    => 'easeInElastic',
							'easeInBack'       => 'easeInBack',
							'easeInBounce'     => 'easeInBounce',
							'easeOutQuad'      => 'easeOutQuad',
							'easeOutCubic'     => 'easeOutCubic',
							'easeOutQuart'     => 'easeOutQuart',
							'easeOutQuint'     => 'easeOutQuint',
							'easeOutSine'      => 'easeOutSine',
							'easeOutExpo'      => 'easeOutExpo',
							'easeOutCirc'      => 'easeOutCirc',
							'easeOutElastic'   => 'easeOutElastic',
							'easeOutBack'      => 'easeOutBack',
							'easeOutBounce'    => 'easeOutBounce',
							'easeInOutQuad'    => 'easeInOutQuad',
							'easeInOutCubic'   => 'easeInOutCubic',
							'easeInOutQuart'   => 'easeInOutQuart',
							'easeInOutQuint'   => 'easeInOutQuint',
							'easeInOutSine'    => 'easeInOutSine',
							'easeInOutExpo'    => 'easeInOutExpo',
							'easeInOutCirc'    => 'easeInOutCirc',
							'easeInOutElastic' => 'easeInOutElastic',
							'easeInOutBack'    => 'easeInOutBack',
							'easeInOutBounce'  => 'easeInOutBounce'
		                ),
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Duration', 'orphan'),
						'param_name'  => 'duration',
						'value'       => 1.5,
						'description' => esc_html__( 'You can set animation duration as (seconds) units from here.', 'orphan')
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Delay', 'orphan'),
						'param_name'  => 'delay',
						'value'       => 0.3,
						'description' => esc_html__( 'After mentioned time (in second) animation will start.', 'orphan')
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Progress Bar Height', 'orphan'),
						'param_name'  => 'height',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of height from dropdown list', 'orphan'),
						'value'       => array(
							esc_html__( 'Small', 'orphan' )       => 'small',
							esc_html__( 'Medium', 'orphan' )      => 'medium',
							esc_html__( 'Large', 'orphan' )       => 'large',
							esc_html__( 'Extra Large', 'orphan' ) => 'xlarge',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Text Color', 'orphan'),
						'param_name'  => 'text_color',
						'description' => esc_html__( 'This color will be applied to the text.', 'orphan'),
						"group"       => esc_html__( 'Styles', "orphan"),
						'value'       => array(
							esc_html__( 'Dark', 'orphan' )  => 'dark',
							esc_html__( 'Light', 'orphan' ) => 'light',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Bar Color', 'orphan'),
						'param_name'  => 'bar_color',
						'description' => esc_html__( 'You can set progress bar background color from here.', 'orphan'),
						"group"       => esc_html__( 'Styles', "orphan"),
						'value'       => array(
							esc_html__( 'Primary', 'orphan' )   => 'primary',
							esc_html__( 'Secondary', 'orphan' ) => 'secondary',
							esc_html__( 'Muted', 'orphan' )     => 'muted',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Fill Color', 'orphan'),
						'param_name'  => 'fill_color',
						'description' => esc_html__( 'Select progress bar fill color, if you need it transparent color.', 'orphan'),
						"group"       => esc_html__( 'Styles', "orphan"),
						'std'         => 'muted',
						'value'       => array(
							esc_html__( 'Primary', 'orphan' )   => 'primary',
							esc_html__( 'Secondary', 'orphan' ) => 'secondary',
							esc_html__( 'Muted', 'orphan' )     => 'muted',
		                ),
					)
				)
			) );
		}
		add_action( 'vc_before_init', 'orphan_progress_bar_vc' );
	}
<?php

if (function_exists('bdthemes_tab')) {
	function orphan_tab_vc() {
		vc_map( array(
				'name'                    => esc_html__( 'Tab', 'orphan' ),
				'base'                    => 'bdt_tab',
				'icon'                    => 'icon-wpb-ui-tab',
				//'is_container'            => true,
				'show_settings_on_create' => false,
				'category'                => esc_html__( 'Theme Addons', 'orphan' ),
				'description'             => esc_html__( 'Create a tabbed navigation with different styles.', 'orphan' ),
				'as_parent'               => array(
					'only' => 'bdt_tab_item',
				),
				'params' => array(
					array(
						'type'        => 'dropdown',
						'param_name'  => 'align',
						'heading'     => esc_html__( 'Align', 'orphan' ),
						'std'         => 'center',
						'description' => esc_html__( 'Select a type of align from dropdown list', 'orphan' ),
						'value'       => array(
							esc_html__( 'Left', 'orphan')     => 'left',
							esc_html__( 'Right' , 'orphan')   => 'right',
							esc_html__( 'Center' , 'orphan')  => 'center',
							esc_html__( 'Justify' , 'orphan') => 'justify',
		                ),
					),
					array(
						'type'        => 'checkbox',
						'param_name'  => 'bottom',
						'heading'     => esc_html__( 'Bottom Styled Tab?', 'orphan' ),
						"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'active',
						'heading'     => esc_html__( 'Active tab', 'orphan' ),
						'value'       => 1,
						'description' => esc_html__( 'Enter number value for active tab', 'orphan' ),
					),
					array(
						'type'        => 'checkbox',
						'param_name'  => 'animation',
						'heading'     => esc_html__( 'Animation?', 'orphan' ),
						"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Duration', 'orphan'),
						'param_name'  => 'duration',
						'value'       => 200,
						'description' => esc_html__( 'You can set animation duration as (milliseconds) units from here.', 'orphan'),
						"dependency"  => array(
							"element" => 'animation',
							"value"   => 'yes',
						),
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'collapsible',
						'heading'     => esc_html__( 'Collapse All?', 'orphan' ),
						'description' => esc_html__( 'Settings of yes each of item will be close otherwise minimum one item will be open', 'orphan' ),
						'value'       => array(
							esc_html__( 'Yes', 'orphan') => 'yes',
							esc_html__( 'No' , 'orphan') => 'no',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'swiping',
						'heading'     => esc_html__( 'Swiping', 'orphan' ),
						'value'       => array(
							esc_html__( 'Yes', 'orphan') => 'yes',
							esc_html__( 'No' , 'orphan') => 'no',
		                ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'orphan' ),
						'param_name'  => 'class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'orphan' ),
					),
				),
				'js_view' => 'VcColumnView',
				'default_content' => '[bdt_tab_item title="' . sprintf( '%s %d', esc_html__( 'Tab Title', 'orphan' ), 1 ) . '"]Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.[/bdt_tab_item][bdt_tab_item title="' . sprintf( '%s %d', esc_html__( 'Tab Title', 'orphan' ), 2 ) . '"]Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.[/bdt_tab_item]',
		));
	}
	add_action( 'vc_before_init', 'orphan_tab_vc' );

	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	    class WPBakeryShortCode_bdt_tab extends WPBakeryShortCodesContainer {
	    }
	}

	function orphan_tab_item_vc() {
		vc_map( array(
			'name'        => esc_html__( 'Tab Item', 'orphan' ),
			'base'        => 'bdt_tab_item',
			'icon'        => 'icon-wpb-ui-tab-item',
			'description' => esc_html__( 'Tab Item', 'orphan' ),
			"as_child"    => array('only' => 'bdt_tab'),
			'params'      => array(
				array(
					'type'        => 'textfield',
					'param_name'  => 'title',
					'heading'     => esc_html__( 'Tab Title', 'orphan' ),
					'description' => esc_html__( 'Enter text used as tab title', 'orphan' ),
				),
				array(
					'type'        => 'checkbox',
					'param_name'  => 'disable',
					'heading'     => esc_html__( 'Want to disabled this tab?', 'orphan' ),
					"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Extra Class Name', 'orphan' ),
					'param_name'  => 'class',
					'description' => esc_html__( 'if you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'orphan' ),
				),
				array(
					"type"        => "textarea_html",
					"heading"     => esc_html__("Tab Content", "orphan"),
					"param_name"  => "content",
					"value"       => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
				),
			),
		));
	}
	add_action( 'vc_before_init', 'orphan_tab_item_vc' );
}
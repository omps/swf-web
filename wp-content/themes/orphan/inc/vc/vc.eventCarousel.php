<?php
	if (function_exists('bdthemes_event_carousel')) {
		function orphan_event_carousel_vc() {
			vc_map( array(
				"name"					=> esc_html__( "Event Carousel", 'orphan'),
				"description"			=> esc_html__( "Add event in your carousel", 'orphan'),
				"base"					=> "bdt_event_carousel",
				"icon"					=> "vc-event-carousel",
				'category'				=> "Theme Addons",
				"params"				=> array(
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Number Of Event", 'orphan'),
						"admin_label"	=> true,
						"param_name"	=> "limit",
						"value"			=> 5,
						"description"	=> esc_html__( "Limit of the event caraousel.", 'orphan')
					),
					array(
						"type"        => "textfield",
						"heading"     => esc_html__( "Categories", 'orphan'),
						"admin_label" => true,
						"param_name"  => "categories",
						"value"       => "all",
						"description" => esc_html__( "Category Slugs - For example: sports, business, all", 'orphan')
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Order", 'orphan'),
						"param_name"  => "order",
						'description' => esc_html__( 'Select order from here.', 'orphan'),
						"group"       => esc_html__( 'Carousel Settings', "orphan"),
						"value"       => array(
							esc_html__( 'Ascending', 'orphan')  => 'ASC',
							esc_html__( 'Descending', 'orphan') => 'DESC',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Event Image", 'orphan'),
						"param_name"  => "thumb",
						"std"         => 'yes',
						'description' => esc_html__( 'Show or hide event image.', 'orphan'),
						"value"       => array(
							esc_html__( 'Hide', "orphan") => 'no',
							esc_html__( 'Show', "orphan") => 'yes',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Gutter', 'orphan'),
						'param_name'  => 'gutter',
						'std'         => 'medium',
						'description' => esc_html__('Gutter of the event carousel.', 'orphan'),
						'value'       => array(
							esc_html__('Collapse', 'orphan') => 'collapse',
							esc_html__('Large', 'orphan')    => 'large',
							esc_html__('Medium', 'orphan')   => 'medium',
							esc_html__('Small', 'orphan')    => 'small'
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Title", 'orphan'),
						"param_name"  => "title",
						'description' => esc_html__( 'Show or hide title from here.', 'orphan'),
						"std"         => 'yes',
						"value"       => array(
							esc_html__( 'Hide', "orphan") => 'no',
							esc_html__( 'Show', "orphan") => 'yes',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Meta", 'orphan'),
						"param_name"  => "meta",
						'description' => esc_html__( 'Show or hide meta from here.', 'orphan'),
						"std"         => 'yes',
						"value"       => array(
							esc_html__( 'Hide', "orphan") => 'no',
							esc_html__( 'Show', "orphan") => 'yes',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Excerpt", 'orphan'),
						"param_name"  => "excerpt",
						'description' => esc_html__( 'Show or hide excerpt from here.', 'orphan'),
						"std"         => 'yes',
						"value"       => array(
							esc_html__( 'Hide', "orphan") => 'no',
							esc_html__( 'Show', "orphan") => 'yes',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Read More", 'orphan'),
						"param_name"  => "read_more",
						'description' => esc_html__( 'Show or hide read more from here.', 'orphan'),
						"std"         => 'yes',
						"value"       => array(
							esc_html__( 'Hide', "orphan") => 'no',
							esc_html__( 'Show', "orphan") => 'yes',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Navigation", 'orphan'),
						"param_name"  => "nav",
						'description' => esc_html__( 'Show or hide navigation from here.', 'orphan'),
						"group"       => esc_html__( 'Carousel Settings', "orphan"),
						"std"         => 'yes',
						"value"       => array(
							esc_html__( 'Hide', "orphan") => 'no',
							esc_html__( 'Show', "orphan") => 'yes',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Dots", 'orphan'),
						"param_name"  => "dots",
						'description' => esc_html__( 'Show or hide dots from here.', 'orphan'),
						"group"       => esc_html__( 'Carousel Settings', "orphan"),
						"value"       => array(
							esc_html__( 'Hide', "orphan") => 'no',
							esc_html__( 'Show', "orphan") => 'yes',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Auto Play", 'orphan'),
						"param_name"  => "autoplay",
						'description' => esc_html__( 'Show or hide auto play from here.', 'orphan'),
						"group"       => esc_html__( 'Carousel Settings', "orphan"),
						"value"       => array(
							esc_html__( 'No', "orphan") => 'no',
							esc_html__( 'Yes', "orphan") => 'yes',
						),
					),
					array(
						"type"        => "number",
						"heading"     => esc_html__("Autoplay Timeout", "orphan"),
						"param_name"  => "autoplay_timeout",
						"value"       => "4000",
						"group"       => esc_html__( 'Carousel Settings', "orphan"),
						"description" => esc_html__("Autoplay Timeout of the event carousel. It's set ms value.", "orphan"),
					),
					array(
						"type"        => "number",
						"heading"     => esc_html__("Speed", "orphan"),
						"param_name"  => "speed",
						"value"       => "350",
						"group"       => esc_html__( 'Carousel Settings', "orphan"),
						"description" => esc_html__("Speed of the event carousel. It's set ms value.", "orphan"),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Hover Pause", 'orphan'),
						"param_name"  => "hoverpause",
						'description' => esc_html__( 'Set yes or no hover pause from here.', 'orphan'),
						"group"       => esc_html__( 'Carousel Settings', "orphan"),
						"value"       => array(
							esc_html__( 'No', "orphan") => 'no',
							esc_html__( 'Yes', "orphan") => 'yes',
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__( "Loop", 'orphan'),
						"param_name"  => "loop",
						'description' => esc_html__('Set yes or no loop from here.', 'orphan'),
						"group"       => esc_html__( 'Carousel Settings', "orphan"),
						"std"         => 'yes',
						"value"       => array(
							esc_html__( 'No', "orphan") => 'no',
							esc_html__( 'Yes', "orphan") => 'yes',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Background', 'orphan'),
						'param_name'  => 'background',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of background from dropdown list', 'orphan'),
						'value'       => array(
							'Default'   => 'default',
							'Muted'     => 'muted',
							'Primary'   => 'primary',
							'Secondary' => 'secondary',
							'Custom'    => 'custom',
							'No'        => 'none',
		                ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Custom Background', 'orphan'),
						'param_name'  => 'custom_background',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter background value for event carousel, example: rgba(0,0,0,0.1)', 'orphan'),
						"dependency"  => array(
							"element" => 'background',
							"value"   => 'custom',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Color', 'orphan'),
						'param_name'  => 'color',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of color from dropdown list', 'orphan'),
						'value'       => array(
							'Default' => 'dark',
							'Light'   => 'light',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Padding', 'orphan'),
						'param_name'  => 'padding',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of padding from dropdown list', 'orphan'),
						'std'         => 'medium',
						'value'       => array(
							'Small'  => 'small',
							'Medium' => 'medium',
							'Large'  => 'large',
							'Custom' => 'custom',
							'No'     => 'none',
		                ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Custom Padding', 'orphan'),
						'param_name'  => 'custom_padding',
						'group'       => 'Styles',
						'description' => esc_html__( 'Enter padding value for event carousel, example: 20px', 'orphan'),
						"dependency"  => array(
							"element" => 'padding',
							"value"   => 'custom',
						),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Shadow', 'orphan'),
						'param_name'  => 'shadow',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of shadow from dropdown list', 'orphan'),
						'value'       => array(
							'Small'       => 'small',
							'Medium'      => 'medium',
							'Large'       => 'large',
							'Extra Large' => 'xlarge',
							'No'          => 'none',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Hover Shadow', 'orphan'),
						'param_name'  => 'hover_shadow',
						'group'       => 'Styles',
						'description' => esc_html__( 'Select a type of hover shadow from dropdown list', 'orphan'),
						'std'         => 'none',
						'value'       => array(
							'Small'       => 'small',
							'Medium'      => 'medium',
							'Large'       => 'large',
							'Extra Large' => 'xlarge',
							'No'          => 'none',
		                ),
					),
					array(
						'type'       => 'checkbox',
						'heading'    => esc_html__( 'Radius', 'orphan'),
						'param_name' => 'radius',
						"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
						'group'      => 'Styles',
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Large View", "orphan"),
						"param_name"  => "large",
						"std"         => 3,
						"group"       => esc_html__( 'Responsive', "orphan"),
						"description" => esc_html__("Large view item of the event carousel.", "orphan"),
						"value"       => array(
							esc_html__('1', 'orphan') => 1,
							esc_html__('2', 'orphan') => 2,
							esc_html__('3', 'orphan') => 3,
							esc_html__('4', 'orphan') => 4,
							esc_html__('5', 'orphan') => 5,
							esc_html__('6', 'orphan') => 6
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Medium View", "orphan"),
						"param_name"  => "medium",
						"std"         => 2,
						"group"       => esc_html__( 'Responsive', "orphan"),
						"description" => esc_html__("Medium view item of the event carousel.", "orphan"),
							"value"       => array(
							esc_html__('1', 'orphan') => 1,
							esc_html__('2', 'orphan') => 2,
							esc_html__('3', 'orphan') => 3,
							esc_html__('4', 'orphan') => 4
						),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Small View", "orphan"),
						"param_name"  => "small",
						"group"       => esc_html__( 'Responsive', "orphan"),
						"description" => esc_html__("Small view item of the event carousel.", "orphan"),
						"value"       => array(
							esc_html__('1', 'orphan') => 1,
							esc_html__('2', 'orphan') => 2,
							esc_html__('3', 'orphan') => 3
						),
					),
				)
			) );
		}
		add_action( 'vc_before_init', 'orphan_event_carousel_vc' );
	}
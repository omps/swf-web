<?php
//Map to VC - Events
if(function_exists('bdthemes_events')){ 

	function orphan_events_vc() {

		vc_map( array(
			"name"        => esc_html_x( "Events", 'backend', 'orphan' ),
			"description" => esc_html_x( "Show your Events Items", 'backend', 'orphan' ),
			"base"        => "bdt_events",
			'category'    => "Theme Addons",
			"icon"        => "vc-events",
			"params"      => array(
				array(
					"type"        => "textfield",
					"heading"     => esc_html_x( "Limit", 'backend', 'orphan' ),
					"param_name"  => "limit",
					"value"       => "8",
					"description" => esc_html_x( "Number of item you want to show.", 'backend', 'orphan' ),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Filter", 'backend', 'orphan' ),
					"param_name"  => "filter",
					'description' => esc_html_x( 'Show or hide filter from here.', 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Show', 'backend', 'orphan' ) => 'yes',
						esc_html_x( 'Hide', 'backend', 'orphan' ) => 'no',
					),
					"dependency"  => array(
						"element" => 'layout',
						"value"   => 'grid',
					),
				),
				array(
					"type"        => "textfield",
					"heading"     => esc_html_x( "Only Specific Filters?", 'backend', 'orphan' ),
					"param_name"  => "filters",
					"value"       => "all",
					"description" => esc_html_x( "If nothing is selected, it will show Items from ALL filters.", 'backend', 'orphan' ),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Filter Align", 'backend', 'orphan' ),
					"param_name"  => "filter_align",
					"description" => esc_html_x( "Filter align of the events.", 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Left', 'backend', 'orphan' )   => 'left',
						esc_html_x( 'Right', 'backend', 'orphan' )  => 'right',
						esc_html_x( 'Center', 'backend', 'orphan' ) => 'center'
					),
					"dependency"  => array(
						"element" => 'filter',
						"value"   => 'yes',
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Loading Animation", 'backend', 'orphan' ),
					"param_name"  => "loading_animation",
					"description" => esc_html_x( "Loading animation of the events.", 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Default', 'backend', 'orphan' )        => 	'default',
						esc_html_x( 'Fade In', 'backend', 'orphan' )        => 	'fadeIn',     
						esc_html_x( 'LazyLoading', 'backend', 'orphan' )    => 	'lazyLoading',
						esc_html_x( 'Fade In To Top', 'backend', 'orphan' ) => 	'fadeInToTop',
						esc_html_x( 'Sequentially', 'backend', 'orphan' )   => 	'sequentially',
						esc_html_x( 'Bottom To Top', 'backend', 'orphan' )  => 	'bottomToTop'  
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Filter Animation", 'backend', 'orphan' ),
					"param_name"  => "filter_animation",
					"description" => esc_html_x( "Filter animation of the events.", 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Fade Out', 'backend', 'orphan' )       => 'fadeOut',
						esc_html_x( 'Quicksand', 'backend', 'orphan' )      => 'quicksand',
						esc_html_x( 'Box Shadow', 'backend', 'orphan' )     => 'boxShadow',
						esc_html_x( 'Bounce Left', 'backend', 'orphan' )    => 'bounceLeft',
						esc_html_x( 'Bounce Top', 'backend', 'orphan' )     => 'bounceTop',
						esc_html_x( 'Bounce Bottom', 'backend', 'orphan' )  => 'bounceBottom',
						esc_html_x( 'Move Left', 'backend', 'orphan' )      => 'moveLeft',
						esc_html_x( 'Slide Left', 'backend', 'orphan' )     => 'slideLeft',
						esc_html_x( 'Fade Out Top', 'backend', 'orphan' )   => 'fadeOutTop',
						esc_html_x( 'Sequentially', 'backend', 'orphan' )   => 'sequentially',
						esc_html_x( 'Skew', 'backend', 'orphan' )           => 'skew',
						esc_html_x( 'Slide Delay', 'backend', 'orphan' )    => 'slideDelay',
						esc_html_x( '3d Flip', 'backend', 'orphan' )        => '3dflip',
						esc_html_x( 'Rotate Sides', 'backend', 'orphan' )   => 'rotateSides',
						esc_html_x( 'Flip Out Delay', 'backend', 'orphan' ) => 'flipOutDelay',
						esc_html_x( 'Flip Out', 'backend', 'orphan' )       => 'flipOut',
						esc_html_x( 'Unfold', 'backend', 'orphan' )         => 'unfold',
						esc_html_x( 'Fold Left', 'backend', 'orphan' )      => 'foldLeft',
						esc_html_x( 'Scale Down', 'backend', 'orphan' )     => 'scaleDown',
						esc_html_x( 'Scale Sides', 'backend', 'orphan' )    => 'scaleSides',
						esc_html_x( 'Front Row', 'backend', 'orphan' )      => 'frontRow',
						esc_html_x( 'Flip Bottom', 'backend', 'orphan' )    => 'flipBottom',
						esc_html_x( 'Rotate Room', 'backend', 'orphan' )    => 'rotateRoom' 
					),
					"dependency"  => array(
						"element" => 'filter',
						"value"   => 'yes',
					),
				),
				array(
					"type"        => "number",
					"heading"     => esc_html_x( "Horizontal Gap", 'backend', 'orphan' ),
					"param_name"  => "horizontal_gap",
					"value"       => "10", 
					"description" => esc_html_x( "Horizontal Gap of the events.", 'backend', 'orphan' ),
				),
				array(
					"type"        => "number",
					"heading"     => esc_html_x( "vertical Gap", 'backend', 'orphan' ),
					"param_name"  => "vertical_gap",
					"value"       => "10", 
					"description" => esc_html_x( "vertical Gap of the events.", 'backend', 'orphan' ),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Show Title", 'backend', 'orphan' ),
					"param_name"  => "title",
					'description' => esc_html_x( 'Select show or hide title from here.', 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Show', 'backend', 'orphan' ) => 'yes',
						esc_html_x( 'Hide', 'backend', 'orphan' ) => 'no',
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Show Meta", 'backend', 'orphan' ),
					"param_name"  => "meta",
					'description' => esc_html_x( 'Select show or hide meta from here.', 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Show', 'backend', 'orphan' ) => 'yes',
						esc_html_x( 'Hide', 'backend', 'orphan' ) => 'no',
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Show Excerpt", 'backend', 'orphan' ),
					"param_name"  => "excerpt",
					'description' => esc_html_x( 'Select show or hide excerpt from here.', 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Show', 'backend', 'orphan' ) => 'yes',
						esc_html_x( 'Hide', 'backend', 'orphan' ) => 'no',
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Show Readmore", 'backend', 'orphan' ),
					"param_name"  => "read_more",
					'description' => esc_html_x( 'Select show or hide readmore from here.', 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Show', 'backend', 'orphan' ) => 'yes',
						esc_html_x( 'Hide', 'backend', 'orphan' ) => 'no',
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Show Link", 'backend', 'orphan' ),
					"param_name"  => "show_link",
					'description' => esc_html_x( 'Select show or hide link from here.', 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Show', 'backend', 'orphan' ) => 'yes',
						esc_html_x( 'Hide', 'backend', 'orphan' ) => 'no',
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Show Zoom", 'backend', 'orphan' ),
					"param_name"  => "show_zoom",
					'description' => esc_html_x( 'Select show or hide zoom from here.', 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Show', 'backend', 'orphan' ) => 'yes',
						esc_html_x( 'Hide', 'backend', 'orphan' ) => 'no',
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Pagination", 'backend', 'orphan' ),
					"param_name"  => "pagination",
					'description' => esc_html_x( 'Select show or hide zoom from here.', 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x( 'Show', 'backend', 'orphan' ) => 'yes',
						esc_html_x( 'Hide', 'backend', 'orphan' ) => 'no',
					),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Background', 'orphan'),
					'param_name'  => 'background',
					'group'       => 'Styles',
					'description' => esc_html__( 'Select a type of background from dropdown list', 'orphan'),
					'value'       => array(
						'Default'   => 'default',
						'Muted'     => 'muted',
						'Primary'   => 'primary',
						'Secondary' => 'secondary',
						'Custom'    => 'custom',
						'No'        => 'none',
	                ),
				),
				array(
					'type'        => 'colorpicker',
					'heading'     => esc_html__( 'Custom Background', 'orphan'),
					'param_name'  => 'custom_background',
					'group'       => 'Styles',
					'description' => esc_html__( 'Enter background value for event carousel, example: rgba(0,0,0,0.1)', 'orphan'),
					"dependency"  => array(
						"element" => 'background',
						"value"   => 'custom',
					),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Color', 'orphan'),
					'param_name'  => 'color',
					'group'       => 'Styles',
					'description' => esc_html__( 'Select a type of color from dropdown list', 'orphan'),
					'value'       => array(
						'Default' => 'dark',
						'Light'   => 'light',
	                ),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Padding', 'orphan'),
					'param_name'  => 'padding',
					'group'       => 'Styles',
					'description' => esc_html__( 'Select a type of padding from dropdown list', 'orphan'),
					'std'         => 'medium',
					'value'       => array(
						'Small'  => 'small',
						'Medium' => 'medium',
						'Large'  => 'large',
						'Custom' => 'custom',
						'No'     => 'none',
	                ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Custom Padding', 'orphan'),
					'param_name'  => 'custom_padding',
					'group'       => 'Styles',
					'description' => esc_html__( 'Enter padding value for event carousel, example: 20px', 'orphan'),
					"dependency"  => array(
						"element" => 'padding',
						"value"   => 'custom',
					),
				),
				array(
					'type'       => 'checkbox',
					'heading'    => esc_html__( 'Shadow', 'orphan'),
					'param_name' => 'shadow',
					"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
					'group'      => 'Styles',
				),
				array(
					'type'       => 'checkbox',
					'heading'    => esc_html__( 'Hover Shadow', 'orphan'),
					'param_name' => 'hover_shadow',
					"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
					'group'      => 'Styles',
				),
				array(
					'type'       => 'checkbox',
					'heading'    => esc_html__( 'Radius', 'orphan'),
					'param_name' => 'radius',
					"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
					'group'      => 'Styles',
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Large View", 'backend', 'orphan' ),
					"param_name"  => "large",
					"std"         => 4,
					"group"       => esc_html_x( 'Responsive', 'backend', 'orphan' ),
					"description" => esc_html_x( "Large view item of the events", 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x('1', 'backend', 'orphan' ) => 1,
						esc_html_x('2', 'backend', 'orphan' ) => 2,
						esc_html_x('3', 'backend', 'orphan' ) => 3,
						esc_html_x('4', 'backend', 'orphan' ) => 4,
						esc_html_x('5', 'backend', 'orphan' ) => 5,
						esc_html_x('6', 'backend', 'orphan' ) => 6
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Medium View", 'backend', 'orphan' ),
					"param_name"  => "medium",
					"std"         => 3,
					"group"       => esc_html_x( 'Responsive', 'backend', 'orphan' ),
					"description" => esc_html_x( "Medium view item of the events", 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x('1', 'backend', 'orphan' ) => 1,
						esc_html_x('2', 'backend', 'orphan' ) => 2,
						esc_html_x('3', 'backend', 'orphan' ) => 3,
						esc_html_x('4', 'backend', 'orphan' ) => 4
					),
				),
				array(
					"type"        => "dropdown",
					"heading"     => esc_html_x( "Small View", 'backend', 'orphan' ),
					"param_name"  => "small",
					"std"         => 2,
					"group"       => esc_html_x( 'Responsive', 'backend', 'orphan' ),
					"description" => esc_html_x( "Small view item of the events", 'backend', 'orphan' ),
					"value"       => array(
						esc_html_x('1', 'backend', 'orphan' ) => 1,
						esc_html_x('2', 'backend', 'orphan' ) => 2,
						esc_html_x('3', 'backend', 'orphan' ) => 3
					),
				),
			)
		) );
	}
	add_action( 'vc_before_init', 'orphan_events_vc',99);
}
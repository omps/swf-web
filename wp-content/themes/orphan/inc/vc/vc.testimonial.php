<?php
	if (function_exists('bdthemes_testimonial_shortcode')) {
		function orphan_testimonial_vc() {
			vc_map(
				array(
					"name"        => esc_html__("Testimonial", "orphan"),
					"base"        => "bdt_testimonial",
					"icon"        => "vc-testimonial",
					"category"    => "Theme Addons",
					"description" => esc_html__("Testimonial.", "orphan"),
					"params"      => array(
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Name', 'orphan'),
							'value'       => 'John Doe',
							'param_name'  => 'name',
							'description' => esc_html__( 'Type name here that you want to show for title', 'orphan'),
						),
				   		array(
							"type"        => "dropdown",
							"heading"     => esc_html__("Testimonial Style", "orphan"),
							"param_name"  => "style",
							"group"       => 'Style',
							"description" => esc_html__("Select style for Testimonial.", "orphan"),
							'value'       => array(
								esc_html__('Style1', "orphan") => 1,
								esc_html__('Style2', "orphan") => 2,
								esc_html__('Style3', "orphan") => 3
		                    ),
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Title', 'orphan'),
							'param_name' => 'title',
						),
						array(
							'type'        => 'attach_image',
							'heading'     => esc_html__( 'Photo', 'orphan'),
							'param_name'  => 'photo',
							'value'       => '',
							'description' => esc_html__( 'Select image from media library.', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Company', 'orphan'),
							'param_name'  => 'company',
							'description' => esc_html__( 'Type here a company name. Leave this field empty to hide company name', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Website URL', 'orphan'),
							'param_name'  => 'url',
							'description' => esc_html__( 'Enter the client company website url. Leave empty to disable link', 'orphan'),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Target', 'orphan'),
							'param_name'  => 'target',
							'description' => esc_html__( 'Set link target self or blank', 'orphan'),
							'value'       => array(
								esc_html__( 'Same window', 'orphan') => '_self',
								esc_html__( 'New window', 'orphan')  => '_blank',
							),
						),
						array(
							"type"        => "checkbox",
							"class"       => "",
							"heading"     => esc_html__("Italic", "orphan"),
							"param_name"  => "italic",
							"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
							"group"       => 'Style',
							"description" => esc_html__("If you want show content italic, so tick", "orphan"),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Radius', 'orphan'),
							'param_name'  => 'radius',
							"group"       => 'Style',
							'description' => esc_html__( 'You can set border radius from here, for example: 3px, 10px, 25px also you can set value as em, % etc if you need', 'orphan'),
						),
						array(
							'type'       => 'textarea_html',
							'holder'     => 'div',
							'heading'    => esc_html__( 'Text', 'orphan'),
							'param_name' => 'content',
							'value'      => esc_html__( 'I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'orphan'),
						)
					)	
				)
			);
		}
		add_action( 'vc_before_init', 'orphan_testimonial_vc' );
	}
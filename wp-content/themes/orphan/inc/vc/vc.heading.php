<?php
	if (function_exists('bdthemes_heading')) {
		function orphan_heading_vc() {
			vc_map( array(
				"name"        => esc_html__( "Heading", 'orphan'),
				"description" => esc_html__( "Huge heading collection.", 'orphan'),
				"base"        => "bdt_heading",
				"icon"        => "vc-heading",
				'category'    => "Theme Addons",
				"params"      => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Heading Text', 'orphan'),
						"admin_label" => true,
						'value'       => "This is a heading",
						'param_name'  => 'content',
						'description' => esc_html__( 'Type your heading text here.', 'orphan')
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Heading Tag", "orphan"),
						"param_name"  => "heading",
						"description" => esc_html__("Select heading tag from here.", "orphan"),
						'value'       => array(
							'H1' => 'h1',
							'H2' => 'h2',
							'H3' => 'h3',
							'H4' => 'h4',
							'H5' => 'h5',
							'H6' => 'h6'
		                ),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Heading Style", "orphan"),
						"param_name"  => "style",
						"admin_label" => true,
						"description" => esc_html__("Select heading style from here.", "orphan"),
						"group"       => esc_html__( 'Styles', "orphan"),
						'value'       => array(
							'Default'            => 'default',
							'Style 1'            => '1',
							'Style 2'            => '2',
							'Style 3'            => '3',
							'Style 4'            => '4',
							'Style 5'            => '5',
							'Style 6'            => '6',
							'Style 7'            => '7',
							'Style 8'            => '8',
							'Style 9'            => '9',
							'Style 10'           => '10',
							'Modern 1 Dark'      => 'modern-1-dark',
							'Modern 1 Light'     => 'modern-1-light',
							'Modern 1 Blue'      => 'modern-1-blue',
							'Modern 1 Orange'    => 'modern-1-orange',
							'Modern 1 Violet'    => 'modern-1-violet',
							'Modern 2 Dark'      => 'modern-2-dark',
							'Modern 2 Light'     => 'modern-2-light',
							'Modern 2 Blue'      => 'modern-2-blue',
							'Modern 2 Orange'    => 'modern-2-orange',
							'Modern 2 Violet'    => 'modern-2-violet',
							'Line Dark'          => 'line-dark',
							'Line Light'         => 'line-light',
							'Line Blue'          => 'line-blue',
							'Line Orange'        => 'line-orange',
							'Line Violet'        => 'line-violet',
							'Dotted Line Dark'   => 'dotted-line-dark',
							'Dotted Line Light'  => 'dotted-line-light',
							'Dotted Line Blue'   => 'dotted-line-blue',
							'Dotted Line Orange' => 'dotted-line-orange',
							'Dotted Line Violet' => 'dotted-line-violet',
							'Flat Dark'          => 'flat-dark',
							'Flat Light'         => 'flat-light',
							'Flat Blue'          => 'flat-blue',
							'Flat Green'         => 'flat-green',
							'Small Line'         => 'small-line',
							'Fancy'              => 'fancy'
		                ),
					),
					array(
						"type"        => "dropdown",
						"heading"     => esc_html__("Heading Align", "orphan"),
						"param_name"  => 'align',
						"description" => esc_html__("Select heading align from here.", "orphan"),
						'value'       => array(
							'Left'   => 'left',
							'Center' => 'center',
							'Right'  => 'right'
		                ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Heading Color', 'orphan'),
						'param_name'  => 'color',
						'description' => esc_html__( 'Color of the heading text.', 'orphan'),
						"group"       => esc_html__( 'Styles', "orphan"),
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Heading Width', 'orphan'),
						'param_name'  => 'width',
						'description' => esc_html__( 'Set heading width from here.', 'orphan'),
						"group"       => esc_html__( 'Styles', "orphan"),
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Heading Size', 'orphan'),
						'param_name'  => 'size',
						'value'       => 24,
						'description' => esc_html__( 'Set heading size from here.', 'orphan'),
						"group"       => esc_html__( 'Styles', "orphan"),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Margin Bottom', 'orphan'),
						'param_name'  => 'margin',
						'description' => esc_html__( 'Margin bottom of the hading.', 'orphan'),
						"group"       => esc_html__( 'Styles', "orphan"),
					),
				)
			) );
		}
		add_action( 'vc_before_init', 'orphan_heading_vc' );
	}
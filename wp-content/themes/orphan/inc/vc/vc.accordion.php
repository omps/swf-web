<?php

if (function_exists('bdthemes_accordion')) {
	function orphan_accordion_vc() {
		vc_map( array(
				'name'                    => esc_html__( 'Accordion', 'orphan' ),
				'base'                    => 'bdt_accordion',
				'icon'                    => 'icon-wpb-ui-accordion',
				//'is_container'            => true,
				'show_settings_on_create' => false,
				'category'                => esc_html__( 'Theme Addons', 'orphan' ),
				'description'             => esc_html__( 'Collapsible content panels', 'orphan' ),
				'as_parent'               => array(
					'only' => 'bdt_accordion_item',
				),
				'params' => array(
					array(
						'type'        => 'textfield',
						'param_name'  => 'active',
						'heading'     => esc_html__( 'Active item', 'orphan' ),
						'value'       => 1,
						'description' => esc_html__( 'Enter number value for active item', 'orphan' ),
					),
					array(
						'type'        => 'checkbox',
						'param_name'  => 'multiple',
						'heading'     => esc_html__( 'Want to Open Multiple?', 'orphan' ),
						"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'collapsible',
						'heading'     => esc_html__( 'Collapse All?', 'orphan' ),
						'std'         => 'ease',
						'description' => esc_html__( 'Settings of yes each of item will be close otherwise minimum one item will be open', 'orphan' ),
						'value'       => array(
							esc_html__( 'Yes', 'orphan') => 'yes',
							esc_html__( 'No' , 'orphan') => 'no',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'animation',
						'heading'     => esc_html__( 'Animation?', 'orphan' ),
						'description' => esc_html__( 'Allow accordion animation from here.', 'orphan' ),
						'value'       => array(
							esc_html__( 'Yes', 'orphan') => 'yes',
							esc_html__( 'No' , 'orphan') => 'no',
		                ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Animation', 'orphan'),
						'param_name'  => 'transition',
						'description' => esc_html__( 'Select a type of animation from dropdown list', 'orphan'),
						'std'         => 'ease',
						'value'       => array(
							esc_html__( 'linear', 'orphan')      => 'linear',
							esc_html__( 'ease', 'orphan')        => 'ease',
							esc_html__( 'ease-in', 'orphan')     => 'ease-in',
							esc_html__( 'ease-out', 'orphan')    => 'ease-out',
							esc_html__( 'ease-in-out', 'orphan') => 'ease-in-out',
							esc_html__( 'step-start', 'orphan')  => 'step-start',
							esc_html__( 'step-end', 'orphan')    => 'step-end',
		                ),
						"dependency"  => array(
							"element" => 'animation',
							"value"   => 'yes',
						),
					),
					array(
						'type'        => 'number',
						'heading'     => esc_html__( 'Duration', 'orphan'),
						'param_name'  => 'duration',
						'value'       => 200,
						'description' => esc_html__( 'You can set animation duration as (milliseconds) units from here.', 'orphan'),
						"dependency"  => array(
							"element" => 'animation',
							"value"   => 'yes',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Extra class name', 'orphan' ),
						'param_name'  => 'class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'orphan' ),
					),
				),
				'js_view' => 'VcColumnView',
				'default_content' => '[bdt_accordion_item title="' . sprintf( '%s %d', esc_html__( 'Accordion Title', 'orphan' ), 1 ) . '"]Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.[/bdt_accordion_item][bdt_accordion_item title="' . sprintf( '%s %d', esc_html__( 'Accordion Title', 'orphan' ), 2 ) . '"]Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.[/bdt_accordion_item]',
		));
	}
	add_action( 'vc_before_init', 'orphan_accordion_vc' );

	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	    class WPBakeryShortCode_bdt_accordion extends WPBakeryShortCodesContainer {
	    }
	}

	function orphan_accordion_item_vc() {
		vc_map( array(
			'name'        => esc_html__( 'Accordion Item', 'orphan' ),
			'base'        => 'bdt_accordion_item',
			'icon'        => 'icon-wpb-ui-accordion-item',
			'description' => esc_html__( 'Accordion Item', 'orphan' ),
			"as_child"    => array('only' => 'bdt_accordion'),
			'params'      => array(
				array(
					'type'        => 'textfield',
					'param_name'  => 'title',
					'heading'     => esc_html__( 'Accordion Title', 'orphan' ),
					'description' => esc_html__( 'Enter text used as accordion title', 'orphan' ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Extra Class Name', 'orphan' ),
					'param_name'  => 'class',
					'description' => esc_html__( 'if you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'orphan' ),
				),
				array(
					"type"        => "textarea_html",
					"heading"     => esc_html__("Accordion Content", "orphan"),
					"param_name"  => "content",
					"value"       => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
				),
			),
		));
	}
	add_action( 'vc_before_init', 'orphan_accordion_item_vc' );
}
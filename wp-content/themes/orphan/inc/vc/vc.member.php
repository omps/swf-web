<?php
	if (function_exists('bdthemes_member_shortcode')) {
		function orphan_member_vc() {
			vc_map(
				array(
					"name"        => esc_html__("Member", "orphan"),
					"base"        => "bdt_member",
					"icon"        => "vc-team-member",
					"category"    => "Theme Addons",
					"description" => esc_html__("Member.", "orphan"),
					"params"      => array(

						array(
							'type'        => 'attach_image',
							'heading'     => esc_html__( 'Photo', 'orphan'),
							'param_name'  => 'photo',
							'value'       => '',
							'description' => esc_html__( 'Select image from media library.', 'orphan'),
						),
				   		array(
							"type"        => "dropdown",
							"heading"     => esc_html__("Style", "orphan"),
							"admin_label" => true,
							"param_name"  => "style",
							'group'       => esc_html__( 'Styles', 'orphan'),
							"description" => esc_html__("Select style for Member.", "orphan"),
							'value'       => array(
		                        esc_html__('Style 1', 'orphan') => '1',
		                        esc_html__('Style 2', 'orphan') => '2',
		                        esc_html__('Style 3', 'orphan') => '3',
		                        esc_html__('Style 4', 'orphan') => '4'
		                    ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Background', 'orphan'),
							'param_name'  => 'background',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of background from dropdown list', 'orphan'),
							'value'       => array(
								'Default'   => 'default',
								'Muted'     => 'muted',
								'Primary'   => 'primary',
								'Secondary' => 'secondary',
								'Custom'    => 'custom',
								'No'        => 'none',
			                ),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Custom Background', 'orphan'),
							'param_name'  => 'custom_background',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter background value for member, example: rgba(0,0,0,0.1)', 'orphan'),
							"dependency"  => array(
								"element" => 'background',
								"value"   => 'custom',
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Color', 'orphan'),
							'param_name'  => 'color',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of color from dropdown list', 'orphan'),
							'value'       => array(
								'Default' => 'dark',
								'Light'   => 'light',
			                ),
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Custom Color', 'orphan'),
							'param_name'  => 'custom_color',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter color value for member, example: rgba(0,0,0,0.1)', 'orphan'),
							"dependency"  => array(
								"element" => 'color',
								"value"   => 'custom',
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Box Shadow', 'orphan'),
							'param_name'  => 'shadow',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of shadow from dropdown list', 'orphan'),
							'value'       => array(
								'Small'       => 'small',
								'Medium'      => 'medium',
								'Large'       => 'large',
								'Extra Large' => 'xlarge',
								'No'          => 'none',
			                ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Hover Shadow', 'orphan'),
							'param_name'  => 'hover_shadow',
							'group'       => 'Styles',
							'description' => esc_html__( 'Select a type of hover shadow from dropdown list', 'orphan'),
							'std'         => 'none',
							'value'       => array(
								'Small'       => 'small',
								'Medium'      => 'medium',
								'Large'       => 'large',
								'Extra Large' => 'xlarge',
								'No'          => 'none',
			                ),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Border', 'orphan'),
							'param_name'  => 'border',
							'group'       => 'Styles',
							'description' => esc_html__( 'Enter border value for member, for example: 1px solid #dddddd', 'orphan')
						),
						array(
							'type'       => 'checkbox',
							'heading'    => esc_html__( 'Border Radius', 'orphan'),
							'param_name' => 'radius',
							"value"      => array( esc_html__("Yes", "orphan") => "yes" ),
							'group'      => 'Styles',
						),
				   		array(
							"type"        => "dropdown",
							"class"       => "",
							"heading"     => esc_html__("Align", "orphan"),
							"param_name"  => "text_align",
							'group'       => esc_html__( 'Styles', 'orphan'),
							"description" => esc_html__("You can set alignment from here.", "orphan"),
							'value'       => array(
								esc_html__('Center', 'orphan') => 'center',
								esc_html__('Left', 'orphan')   => 'left',
								esc_html__('Right', 'orphan')  => 'right',
		                    ),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Name', 'orphan'),
							"admin_label" => true,
							'value'       => 'John Due',
							'param_name'  => 'name',
							'description' => esc_html__( 'Type name here that you want to show for title', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Role', 'orphan'),
							"admin_label" => true,
							'param_name'  => 'role',
							'description' => esc_html__( 'Member role', 'orphan'),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Social Link', 'orphan'),
							'param_name'  => 'social_link',
							'group'       => esc_html__( 'Social Link', 'orphan'),
							'description' => esc_html__( 'Enter member social links by comma, Leave empty to disable link', 'orphan'),
							'value' => esc_html__( 'https://facebook.com/bdthemes, https://google-plus.com/+BdThemes, https://twitter.com/bdthemescom', 'orphan'),
						),
						array(
							'type'        => 'textarea_html',
							'holder'      => 'div',
							'heading'     => esc_html__( 'Text', 'orphan'),
							'param_name'  => 'content',
							'value'       => esc_html__( 'I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'orphan'),
						)
					)	
				)
			);
		}
		add_action( 'vc_before_init', 'orphan_member_vc' );
	}
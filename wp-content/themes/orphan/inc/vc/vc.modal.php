<?php

if (function_exists('bdthemes_modal')) {
	function orphan_modal_vc() {
		vc_map( array(
			'name'         => esc_html__( 'Modal', 'orphan' ),
			'base'         => 'bdt_modal',
			'icon'         => 'icon-wpb-ui-modal',
			'is_container' => true,
			'category'     => esc_html__( 'Theme Addons', 'orphan' ),
			'description'  => esc_html__( 'Create modal dialogs with different styles and transitions.', 'orphan' ),
			'params' => array(
				array(
					'type'        => 'dropdown',
					'param_name'  => 'btn_style',
					'heading'     => esc_html__( 'Button Style', 'orphan' ),
					'description' => esc_html__( 'Select a button style from dropdown list', 'orphan' ),
					'value'       => array(
						esc_html__( 'Default', 'orphan')    => 'default',
						esc_html__( 'Primary' , 'orphan')   => 'primary',
						esc_html__( 'Secondary' , 'orphan') => 'secondary',
						esc_html__( 'Danger' , 'orphan')    => 'danger',
						esc_html__( 'Text' , 'orphan')      => 'text',
						esc_html__( 'Link' , 'orphan')      => 'link',
	                ),
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'btn_text',
					'heading'     => esc_html__( 'Button Text', 'orphan' ),
					'value'       => 'Open',
					'description' => esc_html__( 'Enter text for modal button', 'orphan' ),
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'size',
					'heading'     => esc_html__( 'Modal Size', 'orphan' ),
					'description' => esc_html__( 'Select a type of modal size from dropdown list', 'orphan' ),
					'value'       => array(
						esc_html__( 'Default', 'orphan')    => 'default',
						esc_html__( 'Container' , 'orphan') => 'container',
						esc_html__( 'Full' , 'orphan')      => 'full',
	                ),
				),
				array(
					'type'        => 'checkbox',
					'param_name'  => 'center',
					'heading'     => esc_html__( 'Center Modal??', 'orphan' ),
					"value"       => array( esc_html__("Yes", "orphan") => "yes" ),
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'title',
					'heading'     => esc_html__( 'Modal Title', 'orphan' ),
					'value'       => 'Modal Title',
					'description' => esc_html__( 'Enter text for modal title', 'orphan' ),
				),
				array(
					'type'        => 'dropdown',
					'param_name'  => 'close_btn_position',
					'heading'     => esc_html__( 'Close Button Position', 'orphan' ),
					'description' => esc_html__( 'Select a type of position for close button from dropdown list', 'orphan' ),
					'value'       => array(
						esc_html__( 'Default', 'orphan')  => 'default',
						esc_html__( 'Outside' , 'orphan') => 'outside',
	                ),
				),
				array(
					'type'        => 'textfield',
					'param_name'  => 'caption',
					'heading'     => esc_html__( 'Modal Caption', 'orphan' ),
					'description' => esc_html__( 'Enter text for modal caption', 'orphan' ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Extra class name', 'orphan' ),
					'param_name'  => 'class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'orphan' ),
				),
				array(
					"type"        => "textarea_html",
					"heading"     => esc_html__("Modal Content", "orphan"),
					"param_name"  => "content",
					"value"       => "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
				),
			),
		));
	}
	add_action( 'vc_before_init', 'orphan_modal_vc' );
}
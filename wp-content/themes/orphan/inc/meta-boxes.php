<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 * You also should read the changelog to know what has been changed before updating.
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/docs/define-meta-boxes
 */

/********************* Meta Box Definitions ***********************/

add_action( 'admin_init', 'rw_register_meta_boxes' );
function rw_register_meta_boxes() {
	
	// load is_plugin_active() function if no available
	if (!function_exists('is_plugin_active')) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
	}
	global $meta_boxes;

	$prefix = 'orphan_';
	$meta_boxes = array();

	/* ----------------------------------------------------- */
	// FAQ Filter Array
	if(is_plugin_active('orphan-faq/orphan-faq.php')){ 

		$types = get_terms('faq_filter', 'hide_empty=0');
		$types_array[0] = 'All categories';
		if($types) {
			foreach($types as $type) {
				$types_array[$type->term_id] = $type->name;
			}
		}

	}


	// SIDEBAR ARRAY
	function get_sidebars_array() {
	    global $wp_registered_sidebars;
	    $list_sidebars = array();
	    foreach ( $wp_registered_sidebars as $sidebar ) {
	        $list_sidebars[$sidebar['id']] = $sidebar['name'];
	    }
	    // remove them from the list for better understand purpose
	    unset($list_sidebars['footer-widgets']);
	    unset($list_sidebars['footer-widgets-2']);
	    unset($list_sidebars['footer-widgets-3']);
	    unset($list_sidebars['footer-widgets-4']);
	    unset($list_sidebars['offcanvas']);
	    unset($list_sidebars['fixed-left']);
	    unset($list_sidebars['fixed-right']);
	    unset($list_sidebars['headerbar']);
	    unset($list_sidebars['drawer']);
	    unset($list_sidebars['bottom']);
	    return $list_sidebars;
	}

	// Rev Slider list generate
	function rev_slider_list() {
		if(shortcode_exists("rev_slider")){
			$return = "";
			$slider = new RevSlider();
			$revolution_sliders = $slider->getArrSliders();

			foreach ( $revolution_sliders as $revolution_slider ) {
			       $alias = $revolution_slider->getAlias();
			       $title = $revolution_slider->getTitle();
			       $return[$alias] = $title;
			}
			return $return;
		}	
	}


	/* ----------------------------------------------------- */
	// Blog Categories Filter Array
	$blog_options = array(); // fixes a PHP warning when no blog posts at all.
	$blog_categories = get_categories();
	if($blog_categories) {
		foreach ($blog_categories as $category) {
			$blog_options[$category->slug] = $category->name;
		}
	}

	/* ----------------------------------------------------- */
	// Blog Categories Filter Array
	$campaign_options = array(); // fixes a PHP warning when no blog posts at all.
	$campaign_categories = get_terms('campaign_category');
	if(isset($campaign_categories)) {
		foreach ($campaign_categories as $category) {
            if(is_object($category))
				$campaign_options[$category->slug] = $category->name;
		}
	}
	/* ----------------------------------------------------- */
	// Page Settings
	/* ----------------------------------------------------- */
	
	$meta_boxes[] = array(
		'id'       => 'pagesettings',
		'title'    => 'Page Settings',
		'pages'    => array( 'page' ),
		'context'  => 'normal',
		'priority' => 'high',

		'tabs'      => array(
			'layout' => array(
                'label' => esc_html__('Layout', 'orphan'),
            ),
            'slider' => array(
                'label' => esc_html__('Slider', 'orphan'),
            ),
            'titlebar' => array(
                'label' => esc_html__('Page Titlebar', 'orphan'),
            ),
            'footer' => array(
                'label' => esc_html__('Footer', 'orphan'),
            ),
            'blog'  => array(
                'label' => esc_html__( 'Blog', 'orphan'),
            ),
            'charitable'  => array(
                'label' => esc_html__( 'Campaign View Settings', 'orphan'),
            ),
            'charitable_category'  => array(
                'label' => esc_html__( 'Campaign Category', 'orphan'),
            ),
        ),

        // Tab style: 'default', 'box' or 'left'. Optional
        'tab_style' => 'default',
	
		// List of meta fields
		'fields' => array(
			array(
				'name'		=> esc_html_x('Page Layout', 'backend', 'orphan'),
				'id'		=> $prefix . "page_layout",
				'type'		=> 'select',
				'options'	=> array(
					'default'       => esc_html_x('Default', 'backend', 'orphan'),
					'full'          => esc_html_x('Fullwidth', 'backend', 'orphan'),
					'sidebar-right' => esc_html_x('Sidebar Right', 'backend', 'orphan'),
					'sidebar-left'  => esc_html_x('Sidebar Left', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( 'default' ),
				'desc'     => wp_kses(_x('<strong>Default:</strong> For usage normal Text Pages<br /> <strong>Full Width:</strong> For pages using Visual Composer Elements (commonly used)<br /> <strong>Sidebar Left:</strong> Sidebar Left Template<br /> <strong>Sidebar Right:</strong> Sidebar Right Template', 'backend', 'orphan'), array('strong'=>array(), 'br'=>array())),
				'tab'      => 'layout',
			),

			array(
				'name'     => esc_html_x('Sidebar', 'backend', 'orphan'),
				'id'       => $prefix . "sidebar",
				'type'     => 'select',
				'options'  => get_sidebars_array(),
				'multiple' => false,
				'std'      => array( 'show' ),
				'desc'     => esc_html_x('Select the sidebar you wish to display on this page.', 'backend', 'orphan'),
				'tab'      => 'layout',
				'visible'  => array($prefix . 'page_layout', 'starts with', 'sidebar'),
			),

			array(
				'name'		=> esc_html_x('Header Layout', 'backend', 'orphan'),
				'id'		=> $prefix . "header_layout",
				'type'		=> 'select',
				'options'	=> array(
					null                   => esc_html_x('Default (as customizer)', 'backend', 'orphan'),
					'horizontal-left'      => esc_html_x('Horizontal Left', 'backend', 'orphan'),
					'horizontal-center'    => esc_html_x('Horizontal Center', 'backend', 'orphan'),
					'horizontal-right'     => esc_html_x('Horizontal Right', 'backend', 'orphan'),
					'stacked-center-a'     => esc_html_x('Stacked Center A', 'backend', 'orphan'),
					'stacked-center-b'     => esc_html_x('Stacked Center B', 'backend', 'orphan'),
					'stacked-center-split' => esc_html_x('Stacked Center Split', 'backend', 'orphan'),
					'stacked-left-a'       => esc_html_x('Stacked Left A', 'backend', 'orphan'),
					'stacked-left-b'       => esc_html_x('Stacked Left B', 'backend', 'orphan'),
					'toggle-offcanvas'     => esc_html_x('Toggle Offcanvas', 'backend', 'orphan'),
					'toggle-modal'         => esc_html_x('Toggle Modal', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( null ),
				'desc'     => esc_html_x('Override the header style for this page.', 'backend', 'orphan'),
				'tab'      => 'layout',
			),


			
			array(
				'name'		=> 'Background Style',
				'id'		=> $prefix . "header_bg_style",
				'type'		=> 'select',
				'options'	=> array(
					null        => esc_html_x('Default (as customizer)', 'backend', 'orphan'),
					'default'   => esc_html_x('Default', 'backend', 'orphan'),
					'muted'     => esc_html_x('Muted', 'backend', 'orphan'),
					'primary'   => esc_html_x('Primary', 'backend', 'orphan'),
					'secondary' => esc_html_x('Secondary', 'backend', 'orphan'),
					'image'     => esc_html_x('Image', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( 'light' ),
				'desc'     => 'Select your header style from here.',
				'tab'      => 'layout',
				'hidden' => array($prefix . 'header_layout', '=', false ),
			),
			array(
				'name'             => 'Background Image',
				'id'               => $prefix . "header_bg_img",
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'desc'             => 'Upload header Image for the header Style.',
				'tab'              => 'layout',
				'visible'           => array($prefix . 'header_bg_style', '=', 'image'),
			),
			array(
				'name'		=> 'Header Transparent',
				'id'		=> $prefix . "header_transparent",
				'type'		=> 'select',
				'options'	=> array(
					null    => esc_html_x('Default (as customizer)', 'backend', 'orphan'),
					0       => esc_html_x('No', 'backend', 'orphan'),
					'light' => esc_html_x('Overlay (Light)', 'backend', 'orphan'),
					'dark'  => esc_html_x('Overlay (Dark)', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( null ),
				'desc'     => 'Select your header style from here.',
				'tab'      => 'layout',
				'hidden' => array($prefix . 'header_layout', '=', false ),
			),
			array(
				'name'		=> esc_html_x('Header Sticky', 'backend', 'orphan'),
				'id'		=> $prefix . "header_sticky",
				'type'		=> 'select',
				'options'	=> array(
					null     => esc_html_x('Default (as customizer)', 'backend', 'orphan'),
					0        => esc_html_x('No', 'backend', 'orphan'),
					'sticky' => esc_html_x('Sticky', 'backend', 'orphan'),
					'smart'  => esc_html_x('Smart Sticky', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( null ),
				'desc'     => esc_html_x('Override the header type for this page.', 'backend', 'orphan'),
				'tab'      => 'layout',
			),
			

			array(
				'name'		=> 'Show Slider',
				'id'		=> $prefix . 'show_rev_slider',
				'type'		=> 'radio',
				'options'	=> array(
					'yes'		=> 'Yes',
					'no'		=> 'No'
				),
				'multiple' => false,
				'std'      => array( 'no' ),
				'desc'     => 'Select yes if you want to show slider in this page.',
				'tab'      => 'slider',
			),
			array(
				'name'     => 'Select Slider',
				'id'       => $prefix . "rev_slider",
				'type'     => 'select',
				'options'  => rev_slider_list(),
				'multiple' => false,
				'desc'     => 'Select which revolution slider you want to show this page.',
				'tab'      => 'slider',
				'hidden' => array($prefix . 'show_rev_slider', '=', 'no'),
			),

			array(
				'name'		=> 'Titlebar',
				'id'		=> $prefix . "titlebar",
				'type'		=> 'select',
				'options'	=> array(
					'show' => 'Enable',
					'hide' => 'Disable'
				),
				'multiple' => false,
				'std'      => array( true ),
				'desc'     => 'Enable or disable the Titlebar on this Page.',
				'tab'      => 'titlebar',
			),
			array(
				'name'		=> 'Layout Alignment',
				'id'		=> $prefix . "titlebar_layout",
				'type'		=> 'select',
				'options'	=> array(
					'default' => esc_html_x('Default (set in Theme Customizer)', 'backend', 'orphan'),
					'left'    => esc_html_x('Left Align', 'backend', 'orphan'),
					'center'  => esc_html_x('Center Align', 'backend', 'orphan'),
					'right'   => esc_html_x('Right Align', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( 'default' ),
				'desc'     => 'Choose your Titlebar Layout for this Page',
				'tab'      => 'titlebar',
				'hidden' => array($prefix . 'titlebar', '=', 'hide'),
			),
			array(
				'name'		=> 'Background Style',
				'id'		=> $prefix . "titlebar_bg_style",
				'type'		=> 'select',
				'options'	=> array(
					null        => esc_html_x('Default (as customizer)', 'backend', 'orphan'),
					'default'   => esc_html_x('Default', 'backend', 'orphan'),
					'muted'     => esc_html_x('Muted', 'backend', 'orphan'),
					'primary'   => esc_html_x('Primary', 'backend', 'orphan'),
					'secondary' => esc_html_x('Secondary', 'backend', 'orphan'),
					'image'     => esc_html_x('Image', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( 'light' ),
				'desc'     => 'Select your titlebar style from here.',
				'tab'      => 'titlebar',
				'hidden' => array($prefix . 'titlebar_layout', '=', 'default'),
			),
			array(
					'name'             => 'Background Image',
					'id'               => $prefix . "titlebar_bg_img",
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'desc'             => 'Upload Titlebar Image for the Titlebar Style.',
					'tab'              => 'titlebar',
					'visible'           => array($prefix . 'titlebar_bg_style', '=', 'image'),
			),
			array(
					'name'		=> 'Text Color',
					'id'		=> $prefix . "titlebar_txt_style",
					'type'		=> 'select',
					'options'	=> array(
						'0'     => esc_html_x('Default', 'backend', 'orphan'),
						'light' => esc_html_x('Light', 'backend', 'orphan'),
						'dark'  => esc_html_x('Dark', 'backend', 'orphan'),
					),
					'multiple' => false,
					'std'      => array( '0' ),
					'desc'     => 'Select your titlebar text color from here.',
					'tab'      => 'titlebar',
					'hidden' => array($prefix . 'titlebar_layout', '=', 'default'),
			),

			array(
					'name'		=> 'Footer Widgets',
					'id'		=> $prefix . "footerwidgets",
					'type'		=> 'select',
					'options'	=> array(
						'show'		=> 'Enable',
						'hide'		=> 'Disable'
					),
					'multiple' => false,
					'std'      => array( 'show' ),
					'desc'     => 'Enable or disable the Footer Widgets on this Page.',
					'tab'      => 'footer',
			),
			array(
					'name'		=> 'Footer Copyright',
					'id'		=> $prefix . "footercopyright",
					'type'		=> 'select',
					'options'	=> array(
						'show'		=> 'Enable',
						'hide'		=> 'Disable'
					),
					'multiple' => false,
					'std'      => array( 'show' ),
					'desc'     => 'Enable or disable the Footer Copyright Section on this Page.',
					'tab'      => 'footer',
			),
			array(
				'name'     => 'Blog Categories',
				'id'       => $prefix . "blogcategories",
				'type'     => 'checkbox_list',
				'options'  => $blog_options,
				'multiple' => true,
				'desc'     => 'If nothing is selected, it will show Items from <strong>ALL</strong> categories.',
				'tab'      => 'blog',
			),
			// Charitable settings
			array(
				'name'		=> esc_html_x('Page Mode', 'backend', 'orphan'),
				'id'		=> $prefix.'charitable_view',
				'type'		=> 'select',
				'options'	=> array(
					'grid'		=> esc_html_x('Grid View', 'backend', 'orphan'),
					'list'		=> esc_html_x('List View', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( 'grid' ),
				'tab'      => 'charitable',
			),

			array(
				'name'		=> esc_html_x('Grid Column (Large)', 'backend', 'orphan'),
				'id'		=> $prefix.'grid_columns',
				'type'		=> 'select',
				'options'	=> array(
					'2'		=> esc_html_x('2 Columns', 'backend', 'orphan'),
					'3'		=> esc_html_x('3 Columns', 'backend', 'orphan'),
					'4'		=> esc_html_x('4 Columns', 'backend', 'orphan'),
					'5'		=> esc_html_x('5 Columns', 'backend', 'orphan'),
					'6'		=> esc_html_x('6 Columns', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( '3' ),
				'tab'      => 'charitable',
				'hidden' => array($prefix.'charitable_view', '=', 'list'),
			),

			array(
				'name'		=> esc_html_x('Grid Column (Medium)', 'backend', 'orphan'),
				'id'		=> $prefix.'grid_columns_medium',
				'type'		=> 'select',
				'options'	=> array(
					'1'		=> esc_html_x('1 Column', 'backend', 'orphan'),
					'2'		=> esc_html_x('2 Columns', 'backend', 'orphan'),
					'3'		=> esc_html_x('3 Columns', 'backend', 'orphan'),
					'4'		=> esc_html_x('4 Columns', 'backend', 'orphan'),
					'5'		=> esc_html_x('5 Columns', 'backend', 'orphan'),
					'6'		=> esc_html_x('6 Columns', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( '2' ),
				'tab'      => 'charitable',
				'hidden' => array($prefix.'charitable_view', '=', 'list'),
			),

			array(
				'name'		=> esc_html_x('Grid Column (Small)', 'backend', 'orphan'),
				'id'		=> $prefix.'grid_columns_small',
				'type'		=> 'select',
				'options'	=> array(
					'1'		=> esc_html_x('1 Column', 'backend', 'orphan'),
					'2'		=> esc_html_x('2 Columns', 'backend', 'orphan'),
					'3'		=> esc_html_x('3 Columns', 'backend', 'orphan'),
					'4'		=> esc_html_x('4 Columns', 'backend', 'orphan'),
					'5'		=> esc_html_x('5 Columns', 'backend', 'orphan'),
					'6'		=> esc_html_x('6 Columns', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( '1' ),
				'tab'      => 'charitable',
				'hidden' => array($prefix.'charitable_view', '=', 'list'),
			),

			array(
				'name'		=> esc_html_x('Gutter (Item Spacing)', 'backend', 'orphan'),
				'id'		=> $prefix.'grid_gutter',
				'type'		=> 'select',
				'options'	=> array(
					'medium'   => esc_html_x('Default', 'backend', 'orphan'),
					'small'    => esc_html_x('Small Gutter', 'backend', 'orphan'),
					'large'    => esc_html_x('Large Gutter', 'backend', 'orphan'),
					'collapse' => esc_html_x('No Gutter', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( '25' ),
				'tab'      => 'charitable',
				'hidden' => array($prefix.'charitable_view', '=', 'list'),
			),

			array(
				'name'		=> esc_html_x('Items Limit', 'backend', 'orphan'),
				'id'		=> $prefix.'charitable_limit',
				'type'		=> 'number',
				'std'      => '12',
				'tab'      => 'charitable',
			),

			array(
				'name'     => 'Select Campaign Category',
				'id'       => $prefix . "campaign_categories",
				'type'     => 'checkbox_list',
				'options'  => $campaign_options,
				'multiple' => true,
				'desc'     => 'If nothing is selected, it will show Items from <strong>ALL</strong> categories.',
				'tab'      => 'charitable_category',
			),
					
		)
	);

	/* ----------------------------------------------------- */
	// Blog Metaboxes
	/* ----------------------------------------------------- */

	$meta_boxes[] = array(
		'id'       => 'pagesettings',
		'title'    => esc_html_x('Blog Post Settings', 'backend', 'orphan' ),
		'pages'    => array( 'post' ),
		'context'  => 'normal',
		'priority' => 'high',
		'tabs'     => array(
            'blog_post'  => array(
                'label' => esc_html__( 'Post Settings', 'orphan'),
            ),
            'gallery'  => array(
                'label' => esc_html__( 'Gallery Settings', 'orphan'),
            ),
            'video'  => array(
                'label' => esc_html__( 'Video Settings', 'orphan'),
            ),
            'audio'  => array(
                'label' => esc_html__( 'Audio Settings', 'orphan'),
            ),
            'link'  => array(
                'label' => esc_html__( 'Link Settings', 'orphan'),
            ),
            'quote'  => array(
                'label' => esc_html__( 'Quote Settings', 'orphan'),
            ),
        ),
        // Tab style: 'default', 'box' or 'left'. Optional
        'tab_style' => 'default',
	
		// List of meta fields
		'fields' => array(
			array(
				'name'		=> 'Titlebar',
				'id'		=> $prefix . "titlebar",
				'type'		=> 'select',
				'options'	=> array(
					'show' => 'Enable',
					'hide' => 'Disable'
				),
				'multiple' => false,
				'std'      => array( true ),
				'desc'     => 'Enable or disable the Titlebar on this Page.',
				'tab'      => 'blog_post',
			),
			array(
				'name'		=> 'Layout Alignment',
				'id'		=> $prefix . "titlebar_layout",
				'type'		=> 'select',
				'options'	=> array(
					'default' => esc_html_x('Default (set in Theme Customizer)', 'backend', 'orphan'),
					'left'    => esc_html_x('Left Align', 'backend', 'orphan'),
					'center'  => esc_html_x('Center Align', 'backend', 'orphan'),
					'right'   => esc_html_x('Right Align', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( 'default' ),
				'desc'     => 'Choose your Titlebar Layout for this Page',
				'tab'      => 'blog_post',
				'hidden' => array($prefix . 'titlebar', '=', 'hide'),
			),
			array(
				'name'		=> 'Background Style',
				'id'		=> $prefix . "titlebar_bg_style",
				'type'		=> 'select',
				'options'	=> array(
					'default'   => esc_html_x('Default', 'backend', 'orphan'),
					'muted'     => esc_html_x('Muted', 'backend', 'orphan'),
					'primary'   => esc_html_x('Primary', 'backend', 'orphan'),
					'secondary' => esc_html_x('Secondary', 'backend', 'orphan'),
					'image'     => esc_html_x('Image', 'backend', 'orphan'),
					//'video'     => esc_html_x('Video', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( 'light' ),
				'desc'     => 'Select your titlebar style from here.',
				'tab'      => 'blog_post',
				'hidden' => array($prefix . 'titlebar_layout', '=', 'default'),
			),
			array(
				'name'             => 'Background Image',
				'id'               => $prefix . "titlebar_bg_img",
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'desc'             => 'Upload Titlebar Image for the Titlebar Style.',
				'tab'              => 'blog_post',
				'visible'          => array($prefix . 'titlebar_bg_style', '=', 'image'),
			),
			array(
				'name'		=> 'Text Color',
				'id'		=> $prefix . "titlebar_txt_style",
				'type'		=> 'select',
				'options'	=> array(
					'0'     => esc_html_x('Default', 'backend', 'orphan'),
					'light' => esc_html_x('Light', 'backend', 'orphan'),
					'dark'  => esc_html_x('Dark', 'backend', 'orphan'),
				),
				'multiple' => false,
				'std'      => array( '0' ),
				'desc'     => 'Select your titlebar text color from here.',
				'tab'      => 'blog_post',
				'hidden' => array($prefix . 'titlebar_layout', '=', 'default'),
			),
			array(
				'name'     => esc_html_x('Hide Featured Image?', 'backend', 'orphan' ),
				'id'       => $prefix . "hideimage",
				'type'     => 'checkbox',
				'multiple' => false,
				'desc'     => esc_html_x('Check this if you want to hide the Featured Image / Gallery on the Blog Detail Page', 'backend', 'orphan' ),
				'tab'      => 'blog_post',
			),

			// Post Format Gallery
			array(
				'name'             => esc_html_x('Gallery Images', 'backend', 'orphan' ),
				'desc'             => esc_html_x('You can upload up to 30 gallery images for a slideshow', 'backend', 'orphan' ),
				'id'               => $prefix . 'blog_gallery',
				'type'             => 'image_advanced',
				'max_file_uploads' => 30,
				'tab'              => 'gallery'
			),

			// Post Format Audio
			array(
				'name'  => esc_html_x('Audio Embed Code', 'backend', 'orphan' ),
				'id'    => $prefix . 'blog_audio',
				'desc'  => esc_html_x('Please enter the Audio Embed Code here.', 'backend', 'orphan' ),
				'clone' => false,
				'type'  => 'textarea',
				'std'   => '',
				'tab'   => 'audio'
			),

			// Post Format Link
			array(
				'name'  => esc_html_x('URL', 'backend', 'orphan' ),
				'id'    => $prefix . 'blog_link',
				'desc'  => esc_html_x('Enter a URL for your link post format. (Don\'t forget the http://)', 'backend', 'orphan' ),
				'clone' => false,
				'type'  => 'text',
				'std'   => '',
				'tab'   => 'link'
			),

			// Post Format Quote
			array(
				'name'  => esc_html_x('Quote', 'backend', 'orphan' ),
				'id'    => $prefix . 'blog_quote',
				'desc'  => esc_html_x('Please enter the text for your quote here.', 'backend', 'orphan' ),
				'clone' => false,
				'type'  => 'textarea',
				'std'   => '',
				'tab'   => 'quote'
			),
			array(
				'name'  => esc_html_x('Quote Source', 'backend', 'orphan' ),
				'id'    => $prefix . 'blog_quotesrc',
				'desc'  => esc_html_x('Please enter the Source of the Quote here.', 'backend', 'orphan' ),
				'clone' => false,
				'type'  => 'text',
				'std'   => '',
				'tab'   => 'quote'
			),


			// Post Format Video
			array(
				'name'      => esc_html_x('Video Source', 'backend', 'orphan' ),
				'id'        => $prefix . 'blog_videosrc',
				'type'      => 'select',
				'options'   => array(
					'videourl'  => esc_html_x('Video URL', 'backend', 'orphan' ),
					'embedcode' => esc_html_x('Embed Code', 'backend', 'orphan' )
				),
				'multiple'  => false,
				'std'       => array( 'videourl' ),
				'tab'       => 'video'
			),
			array(
				'name'  => esc_html_x('Video URL/Embed Code', 'backend', 'orphan' ),
				'id'    => $prefix . 'blog_video',
				'desc'  => wp_kses(_x('If you choose Video URL you can just insert the URL of the <a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">Supported Video Site</a>. Otherwise insert the full embed code.', 'backend', 'orphan' ), array('a'=>array())),
				'clone' => false,
				'type'  => 'textarea',
				'std'   => '',
				'tab'   => 'video'
			),
		)
	);

	
	add_filter( 'rwmb_outside_conditions', function( $conditions ) {
	    $conditions['orphan_page_layout'] = array(
	        'visible' => array('page_template', '!=', 'page-homepage.php')
	    );
	    $conditions['.rwmb-tab-titlebar'] = array(
	        'visible' => array('page_template', '!=', 'page-homepage.php')
	    );
	    $conditions['.rwmb-tab-charitable'] = array(
	        'hidden' => array('page_template', '!=', 'page-charitable.php')
	    );
	    $conditions['.rwmb-tab-charitable_category'] = array(
	        'hidden' => array('page_template', '!=', 'page-charitable.php')
	    );
	    $conditions['.rwmb-tab-blog'] = array(
	        'hidden' => array('page_template', '!=', 'page-blog.php')
	    );

	    $conditions['#pagesettings'] = array(
	        'visible' => array('page_template', '!=', 'page-blank.php')
	    );

	    $conditions['.rwmb-tab-gallery'] = array(
	        'hidden' => array('post_format', '!=', 'gallery')
	    );
	    $conditions['.rwmb-tab-video'] = array(
	        'hidden' => array('post_format', '!=', 'video')
	    );
	    $conditions['.rwmb-tab-audio'] = array(
	        'hidden' => array('post_format', '!=', 'audio')
	    );
	    $conditions['.rwmb-tab-quote'] = array(
	        'hidden' => array('post_format', '!=', 'quote')
	    );
	    $conditions['.rwmb-tab-link'] = array(
	        'hidden' => array('post_format', '!=', 'link')
	    );
	    return $conditions;
	});
	

	// );

	/* ----------------------------------------------------- */
	// FAQ Metabox
	/* ----------------------------------------------------- */
	if(is_plugin_active('bdthemes-faq/bdthemes-faq.php')) { 

		$meta_boxes[] = array(
			'id'      => 'faq_info',
			'title'   => esc_html_x( 'FAQ Additional', 'backend', 'orphan'),
			'pages'   => array( 'faq' ),
			'context' => 'normal',			
			'fields'  => array(
				array(
					'name'		=> esc_html_x('Show FAQ Icon', 'backend', 'orphan'),
					'id'		=> 'bdthemes_show_faq_icon',
					'type'		=> 'radio',
					'options'	=> array(
						'yes'		=> esc_html_x('Yes', 'backend', 'orphan'),
						'no'		=> esc_html_x('No', 'backend', 'orphan')
					),
					'multiple' => false,
					'std'      => array( 'no' ),
				),
				array(
					'name'		=> esc_html_x( 'FAQ Icon', 'backend', 'orphan'),
					'id'		=> 'bdthemes_faq_icon',
					'desc'		=> esc_html_x( 'Please type a fontawesome icon name for your FAQ. for example: fa fa-home', 'backend', 'orphan'),
					'clone'		=> false,
					'type'		=> 'text',
					'std'		=> '',
					'hidden' => array('bdthemes_show_faq_icon', '=', 'no'),
				),			
			)
		);
	}

	
	foreach ( $meta_boxes as $meta_box ) {
		new RW_Meta_Box( $meta_box );
	}
}


/* ----------------------------------------------------- */
// Background Styling
/* ----------------------------------------------------- */
add_action( 'admin_init', 'rw_register_meta_boxes_background' );
function rw_register_meta_boxes_background() {
	
	global $meta_boxes;

	if(get_theme_mod('orphan_global_layout', 'full') == 'boxed') {

		$prefix = 'orphan_';
		$meta_boxes = array();

		$meta_boxes[] = array(
			'id' => 'styling',
			'title' => 'Background Styling Options',
			'pages' => array( 'post', 'page', 'portfolio' ),
			'context' => 'side',
			'priority' => 'low',
		
			// List of meta fields
			'fields' => array(
				array(
					'name'             => 'Background Image URL',
					'id'               => $prefix . 'bgurl',
					'desc'             => '',
					'clone'            => false,
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'std'              => ''
				),
				array(
					'name'		=> 'Style',
					'id'		=> $prefix . "bgstyle",
					'type'		=> 'select',
					'options'	=> array(
						'stretch'		=> 'Stretch Image',
						'repeat'		=> 'Repeat',
						'no-repeat'		=> 'No-Repeat',
						'repeat-x'		=> 'Repeat-X',
						'repeat-y'		=> 'Repeat-Y'
					),
					'multiple'	=> false,
					'std'		=> array( 'stretch' )
				),
				array(
					'name'		=> 'Background Color',
					'id'		=> $prefix . "bgcolor",
					'type'		=> 'color'
				)
			)
		);
		
		foreach ( $meta_boxes as $meta_box ) {
			new RW_Meta_Box( $meta_box );
		}
	}
}
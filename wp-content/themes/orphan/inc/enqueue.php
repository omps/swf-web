<?php

/**
 * Admin related stylesheets
 * @return [type] [description]
 */
function orphan_admin_style() {
	wp_register_style( 'admin-setting', get_template_directory_uri() . '/admin/css/admin-settings.css' );
	wp_enqueue_style( 'admin-setting' );
}
add_action( 'admin_enqueue_scripts', 'orphan_admin_style' );


/**
 * Admin related scripts
 * @return [type] [description]
 */
function orphan_admin_script() {
	wp_register_script('admin-setting', get_template_directory_uri() . '/admin/js/admin-settings.js', array( 'jquery' ), ORPHAN_VER, true);
	$admin_setting_message = array( 'compilingLess' => esc_html_x('Compiling Less...', 'backend', 'orphan'), 'compilingLessError' => esc_html_x('An error while Compiling Less!!!', 'backend', 'orphan') ); 
	wp_localize_script( 'admin-setting', 'adminSettingMessage', $admin_setting_message ); //pass 'adminSettingMessage' to admin-settings.js
	wp_register_script('admin-less', get_template_directory_uri() . '/inc/vendor/less/less.js', array(), ORPHAN_VER, true);

	wp_enqueue_script('admin-setting');
	wp_enqueue_script('admin-less');
}
add_action( 'admin_enqueue_scripts', 'orphan_admin_script' );


/**
 * Site Stylesheets
 * @return [type] [description]
 */
function orphan_styles() {

	$css_dir = get_template_directory_uri().'/css/';

	// Load Visual composer CSS first so it's easier to override
	if ( function_exists( 'vc_map' ) ) {
		wp_enqueue_style( 'js_composer_front' );
	}
	// Deregister Composer Custom CSS
	wp_deregister_style( 'js_composer_custom_css' );

	// Load Primary Stylesheet
	wp_enqueue_style( 'theme-style', $css_dir.'theme.css', array(), ORPHAN_VER, 'all' );
	wp_enqueue_style( 'orphan-style', get_stylesheet_uri(), array(), ORPHAN_VER, 'all' );

}  
add_action( 'wp_enqueue_scripts', 'orphan_styles' ); 


/**
 * Site Scripts
 * @return [type] [description]
 */
function orphan_scripts() {

	$dev_mode      = get_theme_mod('orphan_dev_mode', true);
	$preloader     = get_theme_mod('orphan_preloader');
	$js_dir        = get_template_directory_uri() . '/js/';
	$vendor_js_dir = get_template_directory_uri() . '/inc/vendor/uikit/js/';

	if ($preloader) {
		wp_enqueue_script('please-wait', $js_dir.'please-wait.min.js', array(), ORPHAN_VER, false);
	}

	if ($dev_mode) {
		// Register Script
		wp_register_script('uikit', $vendor_js_dir.'uikit.js', array( 'jquery' ), ORPHAN_VER, false);
		wp_register_script('uikit-icons', $vendor_js_dir.'uikit-icons.min.js', array( 'uikit' ), ORPHAN_VER, true);
		wp_register_script('appear', $js_dir.'jquery.appear.js', array( 'jquery' ), ORPHAN_VER, true);
		wp_register_script('owl-carousel', $js_dir.'owl.carousel.min.js', array( 'jquery' ), ORPHAN_VER, true);
		wp_register_script('magnific-popup', $js_dir.'jquery.magnific-popup.min.js', array( 'jquery' ), ORPHAN_VER, true);
		wp_register_script('fitvids', $js_dir.'fitvids.js', array( 'jquery' ), ORPHAN_VER, true);
		wp_register_script('tick', $js_dir.'tick.js', array( 'jquery' ), ORPHAN_VER, true);
		wp_register_script('cookie-bar', $js_dir.'jquery.cookiebar.js', array( 'jquery' ), ORPHAN_VER, true);
		
		wp_register_script('progress-pie', $js_dir.'jquery.asPieProgress.min.js', array( 'jquery' ), ORPHAN_VER, true);
		wp_register_script('count-up', $js_dir.'jquery.countup.js', array( 'jquery' ), ORPHAN_VER, true);
		wp_register_script('cube-portfolio', $js_dir.'jquery.cubeportfolio.min.js', array( 'jquery' ), ORPHAN_VER, true);
		wp_register_script('flickr', $js_dir.'jquery.flickr.js', array( 'jquery' ), ORPHAN_VER, true);
		
		wp_register_script('orphan-script', $js_dir.'theme.js', array( 'uikit' ), ORPHAN_VER, true);

		// Enqueue
		wp_enqueue_script('uikit');
		wp_enqueue_script('uikit-icons');
		wp_enqueue_script('appear');
		wp_enqueue_script('owl-carousel');
		wp_enqueue_script('magnific-popup');
		wp_enqueue_script('fitvids');
		wp_enqueue_script('tick');
		wp_enqueue_script('cookie-bar');
		wp_enqueue_script('progress-pie');
		wp_enqueue_script('count-up');
		wp_enqueue_script('cube-portfolio');
		wp_enqueue_script('flickr');
		
		wp_enqueue_script('orphan-script');
	} else {
		wp_enqueue_script('uikit', $vendor_js_dir.'uikit.min.js', array( 'jquery' ), ORPHAN_VER, false);
		wp_enqueue_script('orphan-script', $js_dir.'theme.min.js', array( 'uikit' ), ORPHAN_VER, true);
	}

  	// Load WP Comment Reply JS
  	if(is_singular()) { wp_enqueue_script( 'comment-reply' ); }
}
add_action( 'wp_enqueue_scripts', 'orphan_scripts' );  
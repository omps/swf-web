<?php

/**
* visual composer integration
*/

class orphan_vc_elements {
    
    function __construct() {
        add_action('after_setup_theme',array($this,'vc_elements_init'));
    }

    function vc_elements_init() {
        
        // VC Elements 
        load_template(get_template_directory().'/inc/vc/vc.accordion.php');
        load_template(get_template_directory().'/inc/vc/vc.button.php');
        load_template(get_template_directory().'/inc/vc/vc.blogList.php');
        load_template(get_template_directory().'/inc/vc/vc.counter.php');
        load_template(get_template_directory().'/inc/vc/vc.charitable.php');
        load_template(get_template_directory().'/inc/vc/vc.callout.php');
        load_template(get_template_directory().'/inc/vc/vc.countdown.php');
        load_template(get_template_directory().'/inc/vc/vc.customCarousel.php');
        load_template(get_template_directory().'/inc/vc/vc.divider.php');
        load_template(get_template_directory().'/inc/vc/vc.eventCarousel.php');
        load_template(get_template_directory().'/inc/vc/vc.events.php');
        load_template(get_template_directory().'/inc/vc/vc.flickr.php');
        load_template(get_template_directory().'/inc/vc/vc.featuredDonation.php');
        load_template(get_template_directory().'/inc/vc/vc.gallery.php');
        load_template(get_template_directory().'/inc/vc/vc.heading.php');
        load_template(get_template_directory().'/inc/vc/vc.iconListItem.php');
        load_template(get_template_directory().'/inc/vc/vc.inlineIcon.php');
        load_template(get_template_directory().'/inc/vc/vc.member.php');
        load_template(get_template_directory().'/inc/vc/vc.modal.php');
        load_template(get_template_directory().'/inc/vc/vc.note.php');
        load_template(get_template_directory().'/inc/vc/vc.progressBar.php');
        load_template(get_template_directory().'/inc/vc/vc.progressPie.php');
        load_template(get_template_directory().'/inc/vc/vc.pricingPlan.php');
        load_template(get_template_directory().'/inc/vc/vc.postCarousel.php');
        load_template(get_template_directory().'/inc/vc/vc.spacer.php');
        load_template(get_template_directory().'/inc/vc/vc.testimonial.php');
        load_template(get_template_directory().'/inc/vc/vc.tabs.php');

    } // end vc_elements_init
}

new orphan_vc_elements;


// Visual composer theme integration
add_action( 'vc_before_init', 'orphan_vcSetAsTheme' );
function orphan_vcSetAsTheme() {
    if(function_exists('vc_set_as_theme')) vc_set_as_theme(true);
}

function orphan_vc_hide_updates( $value ) {
    if (isset($value->response['js_composer/js_composer.php'])) {
        unset( $value->response['js_composer/js_composer.php'] );
        return $value;
    }
    return null;
}
add_filter( 'site_transient_update_plugins', 'orphan_vc_hide_updates' );


// Remove Default Templates
add_filter( 'vc_load_default_templates', 'orphan_template_modify_array' );
function orphan_template_modify_array($data) {
    return array(); // This will remove all default templates
}

// Disable Instructional/Help Pointers
add_action( 'vc_before_init', 'vc_remove_all_pointers' );
function vc_remove_all_pointers() {
   remove_action( 'admin_enqueue_scripts', 'vc_pointer_load' );
}

/* ------------------------------------------------------------------------ */
/* Remove Visual Composer Elements
/* ------------------------------------------------------------------------ */
vc_remove_element("vc_separator");
vc_remove_element("vc_text_separator");
vc_remove_element("vc_message");
vc_remove_element("vc_empty_space");
vc_remove_element("vc_gallery");
vc_remove_element("vc_images_carousel");
vc_remove_element("vc_posts_slider");
vc_remove_element("vc_flickr");
vc_remove_element("vc_cta");
vc_remove_element("vc_pie");
vc_remove_element("vc_progress_bar");
vc_remove_element("vc_custom_heading");
vc_remove_element("rev_slider_vc");
vc_remove_element("vc_media_grid");
vc_remove_element("vc_masonry_grid");
vc_remove_element("vc_masonry_media_grid");
vc_remove_element("vc_icon");
vc_remove_element("vc_basic_grid");
vc_remove_element("vc_widget_sidebar");
vc_remove_element("vc_line_chart");
vc_remove_element("vc_round_chart");
vc_remove_element("vc_wp_archives");
vc_remove_element("vc_wp_calendar");
vc_remove_element("vc_wp_categories");
vc_remove_element("vc_wp_custommenu");
vc_remove_element("vc_wp_links");
vc_remove_element("vc_wp_meta");
vc_remove_element("vc_wp_pages");
vc_remove_element("vc_wp_posts");
vc_remove_element("vc_wp_recentcomments");
vc_remove_element("vc_wp_rss");
vc_remove_element("vc_wp_search");
vc_remove_element("vc_wp_tagcloud");
vc_remove_element("vc_wp_text");

// Remove from VC 4.7
vc_remove_element("vc_tta_tour");
vc_remove_element("vc_tta_accordion");
vc_remove_element("vc_tta_pageable");


// Deprecated version removed
vc_remove_element("vc_accordion");
vc_remove_element("vc_tour");
vc_remove_element("vc_tabs");
vc_remove_element("vc_button");
vc_remove_element("vc_button2");
vc_remove_element("vc_cta_button");
vc_remove_element("vc_cta_button2");

function orphan_vc_remove_woocommerce() {
    if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
        vc_remove_element( 'woocommerce_cart' );
        vc_remove_element( 'woocommerce_checkout' );
        vc_remove_element( 'woocommerce_order_tracking' );
        // Add other elements that should be removed here
    }
}
// Hook for admin editor.
add_action( 'vc_build_admin_page', 'orphan_vc_remove_woocommerce', 11 );

// Hook for frontend editor.
add_action( 'vc_load_shortcode', 'orphan_vc_remove_woocommerce', 11 );

if (is_admin()) {
    function orphan_vc_admin_styles() {   
        // Register Styles
        wp_register_style( 'bdt-vc-admin-style', get_template_directory_uri() . '/inc/vc/assets/css/admin.css' );
        wp_enqueue_style( 'bdt-vc-admin-style' );

    }  
    add_action( 'admin_enqueue_scripts', 'orphan_vc_admin_styles' );
}



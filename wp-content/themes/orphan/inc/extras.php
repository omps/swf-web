<?php
/**
 * Custom functions that act independently of the theme templates.
 * Eventually, some of the functionality here could be replaced by core features.
 * @package orphan
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function orphan_body_classes( $classes ) {
    $page_layout = (get_post_meta( get_the_ID(), 'orphan_layout', true )) ? 'page-'. get_post_meta( get_the_ID(), 'orphan_layout', true ) : '';
    $header_style = get_post_meta( get_the_ID(), 'orphan_header_layout', true);
    $header_style = ( !empty($header_style) ) ? get_post_meta( get_the_ID(), 'orphan_header_layout', true) : get_theme_mod('orphan_header_sticky');

    $header_transparecny = get_post_meta( get_the_ID(), 'orphan_header_transparent', true);
    $header_transparecny = ( !empty($header_transparecny) ) ? $header_transparecny : get_theme_mod('orphan_header_transparent');

    $navbar_style = get_post_meta( get_the_ID(), 'orphan_navbar_style', true);
    $navbar_style = ( !empty($navbar_style) ) ? get_post_meta( get_the_ID(), 'orphan_navbar_style', true) : get_theme_mod('orphan_navbar_style', 'default');

    $product_columns = get_theme_mod('orphan_woocommerce_columns');

    
    // Adds a class of group-blog to blogs with more than 1 published author.
    if ( is_multi_author() ) {
        $classes[] = 'group-blog';
    }

    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    $classes[] = 'layout-' . get_theme_mod('orphan_global_layout', 'default');

    $classes[] = $page_layout;
    $classes[] = 'navbar-' . $navbar_style;

    $classes[] = 'header-mode-'. $header_style;
    $classes[] = ($header_transparecny) ? 'tm-header-transparent' : '';
    if (class_exists('Woocommerce')) {
        $classes[] = 'columns-'.$product_columns;
    }

	return $classes;
}
add_filter( 'body_class', 'orphan_body_classes' );



add_filter( 'the_password_form', 'orphan_password_form' );
function orphan_password_form() {
    global $post;
    $label = ( empty( $post->ID ) ? uniqid('pf') : $post->ID );
    $output = '<div class="uk-alert uk-alert-warning">' . esc_html__( "This content is password protected. To view it please enter your password below:", "orphan" ) . '</div>
                <form action="' . esc_url( site_url( '/wp-login.php?action=postpass', 'login_post' ) ).'" method="post" class="uk-grid-small uk-margin-bottom" uk-grid>
                    <div class="uk-width-1-2@s">
                        <input name="post_password" id="' . $label . '" type="password" class="uk-input uk-border-rounded" />
                    </div>
                    <div class="uk-width-1-2@s">
                        <input type="submit" name="Submit" class="uk-button uk-button-primary uk-contrast uk-border-rounded" value="' . esc_attr__( "Unlock", "orphan" ) . '" />
                    </div>
                </form>';
    return $output;
}


/**
 * Converts a HEX value to RGB.
 *
 * @since Orphan 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function orphan_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}



add_action('wp_ajax_orphan_search', 'orphan_ajax_search');
add_action('wp_ajax_nopriv_orphan_search', 'orphan_ajax_search');

function orphan_ajax_search() {
    global $wp_query;

    $result = array('results' => array());
    $query  = isset($_REQUEST['s']) ? $_REQUEST['s'] : '';

    if (strlen($query) >= 3) {

        $wp_query->query_vars['posts_per_page'] = 5;
        $wp_query->query_vars['post_status'] = 'publish';
        $wp_query->query_vars['s'] = $query;
        $wp_query->is_search = true;

        foreach ($wp_query->get_posts() as $post) {

            $content = !empty($post->post_excerpt) ? strip_tags(strip_shortcodes($post->post_excerpt)) : strip_tags(strip_shortcodes($post->post_content));

            if (strlen($content) > 180) {
                $content = substr($content, 0, 179).'...';
            }

            $result['results'][] = array(
                'title' => $post->post_title,
                'text'  => $content,
                'url'   => get_permalink($post->ID)
            );
        }
    }

    die(json_encode($result));
}


// Wp override

function orphan_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'orphan_login_logo_url' );

$orphan_logo = get_theme_mod('orphan_logo_default');

if ($orphan_logo) {
    function orphan_login_logo() { 
        $orphan_logo = get_theme_mod('orphan_logo_default'); ?>
        <style type="text/css">
            #login h1 a, .login h1 a {
                background-image: url(<?php echo esc_url($orphan_logo); ?>);
                height:32px;
                width:320px;
                background-size: auto 32px;
                background-repeat: no-repeat;
                background-position: center center;
                padding-bottom: 30px;
            }
        </style>
    <?php }
    add_action( 'login_enqueue_scripts', 'orphan_login_logo' );
}


function orphan_comment($comment, $args, $depth) { ?>
    <li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    
        <article id="div-comment-<?php comment_ID() ?>" class="comment even thread-even uk-comment uk-visible-toggle depth-<?php echo esc_attr($depth); ?>">

            <header class="uk-comment-header uk-position-relative">
                <div class="uk-grid-medium uk-flex-middle uk-grid" uk-grid="">
                    <div class="uk-width-auto uk-first-column">
                        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
                    </div>
                        <div class="uk-width-expand">
                            <h3 class="uk-comment-title uk-margin-remove">
                                <span class="uk-link-reset"><?php comment_author_link(); ?></span>
                            </h3>

                            <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                <li><a class="uk-link-reset" href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                                    <?php /* translators: 1: date, 2: time */
                                        printf( __('%1$s at %2$s', 'orphan'), get_comment_date(),  get_comment_time() ); ?></a>
                                </li>

                                <?php if (get_edit_comment_link()) : ?>
                                    <li class="uk-visible@s"><?php edit_comment_link( esc_html__( 'Edit Comment', 'orphan' ), '  ', '' ); ?></li>
                                <?php endif; ?>

                                <?php if ( $comment->comment_approved == '0' ) : ?>
                                    <li><em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'orphan' ); ?></em></li>
                                <?php endif; ?>
                            </ul>

                        </div>
                </div>
                <div class="uk-position-top-right uk-hidden-hover uk-visible@s">
                     <?php comment_reply_link( array_merge( $args, array( 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>           
                </div>
            </header>            

            <div class="uk-comment-body">
            <?php comment_text(); ?>
            
            </div>

            <ul class="uk-comment-meta uk-hidden@s uk-subnav">
                <li><?php comment_reply_link( array_merge( $args, array( 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                </li>

                <?php if (get_edit_comment_link()) : ?>
                    <li><?php edit_comment_link( esc_html__( 'Edit Comment', 'orphan' ), '  ', '' ); ?></li>
                <?php endif; ?>
            </ul>

        
        </article>
        
    <?php
}
<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package orphan
 */

get_header(); ?>



<div<?php echo orphan_helper::section(); ?>>
	<div<?php echo orphan_helper::container(); ?>>
		<div<?php echo orphan_helper::grid(); ?>>
			
			<div class="uk-width-expand">
				<main class="tm-content">

					<section class="error-404 not-found uk-text-center">


						<div class="uk-width-auto uk-vertical-align-middle uk-margin-large-bottom uk-margin-large-top uk-box-shadow-medium uk-background-default uk-border-rounded uk-padding-large uk-margin-auto">

							<img src="<?php echo get_template_directory_uri() . '/images/404.svg'; ?>" alt="<?php esc_html_e("Page Not Found", "orphan"); ?>" width="450" height="238">

							<p class="uk-margin-large-top uk-text-large"><?php 
								$err_history_link = '<a href="javascript:history.go(-1)">'.esc_html__("Go back", "orphan").'</a>';
								$err_home_link = '<a href="'.home_url('/').'">'.get_bloginfo('name').'</a>';

								printf(esc_html__("The Page you are looking for doesn't exist or an other error occurred. %s or head over to %s %s homepage to choose a new direction.", "orphan"), $err_history_link , '<br class="uk-visible@l">' , $err_home_link ); ?></p>

						</div>

					</section><!-- .error-404 -->

				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
	</div>
</div>

<?php
get_footer();

<?php get_header();

// Layout
$layout = get_post_meta( get_the_ID(), 'orphan_page_layout', true );
$position = (!empty($layout)) ? $layout : get_theme_mod( 'orphan_page_layout', 'sidebar-right' );

$class[] = ($layout !== 'full') ? 'uk-container' : ''; 

?>



<div<?php echo orphan_helper::section(); ?>>
	<div<?php echo orphan_helper::attrs(['class' => $class]) ?>>
		<div<?php echo orphan_helper::grid(); ?>>
			<div class="uk-width-expand">
				<main class="tm-content">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php the_content(); ?>

						<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>

						<?php if(get_theme_mod('orphan_comment_show', 1) == 1 and comments_open()) { ?>
							<hr class="uk-margin-large-top uk-margin-large-bottom">

							<?php comments_template(); ?>
						<?php } ?>

					<?php endwhile; endif; ?>
				</main> <!-- end main -->
			</div> <!-- end content -->

			<?php if($position == 'sidebar-left' || $position == 'sidebar-right') : ?>
				<aside<?php echo orphan_helper::sidebar($position); ?>>
				    <?php get_sidebar(); ?>
				</aside> <!-- end aside -->
			<?php endif; ?>

		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>

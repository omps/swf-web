<?php
/**
 * Contains methods for customizing the theme customization screen.
 *
 * @package Orphan
 * @link http://codex.wordpress.org/Theme_Customization_API
 */

class orphan_Customizer_Base {
	/**
	 * The singleton manager instance
	 *
	 * @see wp-includes/class-wp-customize-manager.php
	 * @var WP_Customize_Manager
	 */
	protected $wp_customize;

	public function __construct( WP_Customize_Manager $wp_manager ) {
		// set the private propery to instance of wp_manager
		$this->wp_customize = $wp_manager;

		// register the settings/panels/sections/controls, main method
		$this->register();

		/**
		 * Action and filters
		 */

		// render the CSS and cache it to the theme_mod when the setting is saved
		add_action( 'customize_save_after' , array( $this, 'cache_rendered_css' ) );

		// save logo width/height dimensions
		add_action( 'customize_save_logo_img' , array( $this, 'save_logo_dimensions' ), 10, 1 );

		// flush the rewrite rules after the customizer settings are saved
		add_action( 'customize_save_after', 'flush_rewrite_rules' );

		// handle the postMessage transfer method with some dynamically generated JS in the footer of the theme
		add_action( 'wp_footer', array( $this, 'customize_footer_js' ), 30 );
		add_action('wp_head',array( $this, 'hook_custom_css' ));


	}

	/**
	* This hooks into 'customize_register' (available as of WP 3.4) and allows
	* you to add new sections and controls to the Theme Customize screen.
	*
	* Note: To enable instant preview, we have to actually write a bit of custom
	* javascript. See live_preview() for more.
	*
	* @see add_action('customize_register',$func)
	*/
	public function register () {
		/**
		 * Settings
		 */

		//$this->wp_customize->remove_section( 'colors');
		$this->wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$this->wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';



		$this->wp_customize->add_setting( 'orphan_logo_default' , array(
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control( new WP_Customize_Image_Control( $this->wp_customize, 'orphan_logo_default', array(
			'priority' => 101,
		    'label'    => esc_html_x( 'Default Logo', 'backend', 'orphan' ),
		    'section'  => 'title_tagline',
		    'settings' => 'orphan_logo_default'
		)));

		$this->wp_customize->add_setting('orphan_logo_width_default', array(
			'sanitize_callback' => 'orphan_sanitize_text'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_logo_width_default', array(
			'label'       => esc_html_x('Default Logo Width', 'backend', 'orphan'),
			'description' => esc_html_x('This is an optional width (example: 150px) settings. maybe this not need if you use 150px x 32px logo.' , 'backend', 'orphan'),
			'priority' => 102,
			'section'     => 'title_tagline',
			'settings'    => 'orphan_logo_width_default', 
			'type'        => 'text',
		)));

		$this->wp_customize->add_setting( 'orphan_logo_mobile' , array(
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control( new WP_Customize_Image_Control( $this->wp_customize, 'orphan_logo_mobile', array(
			'priority' => 103,
		    'label'    => esc_html_x( 'Mobile Logo', 'backend', 'orphan' ),
		    'section'  => 'title_tagline',
		    'settings' => 'orphan_logo_mobile'
		)));


		$this->wp_customize->add_section('toolbar', array(
			'title' => esc_html_x('Toolbar', 'backend', 'orphan'),
			'priority' => 28
		));

		$this->wp_customize->add_setting('orphan_toolbar', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_toolbar', array(
			'label'    => esc_html_x('Show Toolbar', 'backend', 'orphan'),
			'section'  => 'toolbar',
			'settings' => 'orphan_toolbar', 
			'type'     => 'select',
			'choices'  => array(
				1 => esc_html_x('Yes', 'backend', 'orphan'),
				0 => esc_html_x('No', 'backend', 'orphan'),
			)
		)));

		$this->wp_customize->add_setting('orphan_toolbar_fullwidth', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_toolbar_fullwidth', array(
			'label'       => esc_html_x('Toolbar Fullwidth', 'backend', 'orphan'),
			'description' => esc_html_x('(Make your tolbar full width like fluid width.)', 'backend', 'orphan'),
			'section'     => 'toolbar',
			'settings'    => 'orphan_toolbar_fullwidth', 
			'type'        => 'checkbox',
		)));


		// Add footer text color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'toolbar_text_color', array(
			'default'           => '#999999',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'color' => array(
					'.tm-toolbar',
					'.tm-toolbar a',
					'.tm-toolbar .uk-subnav>*>:first-child',
				),
				'color|lighten(30)' => array(
					'.tm-toolbar a:hover',
					'.tm-toolbar .uk-subnav>*>a:hover', 
					'.tm-toolbar .uk-subnav>*>a:focus',
					'.tm-toolbar .uk-subnav>.uk-active>a',
				),
			)
		)));

		// Add toolbar text color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'toolbar_text_color', array(
			'label'       => esc_html_x( 'Text Color', 'backend', 'orphan' ),
			'section'     => 'toolbar',
		)));

		// Add footer background color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'toolbar_background_color', array(
			'default'           => '#222222',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'background-color' => array(
					'.tm-toolbar',
				),
				'border-color|lighten(15)' => array(
					'.tm-toolbar .uk-subnav-divider>:nth-child(n+2):not(.uk-first-column)::before',
				),
			)
		)));

		// Add toolbar background color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'toolbar_background_color', array(
			'label'       => esc_html_x( 'Background Color', 'backend', 'orphan' ),
			'section'     => 'toolbar',
		)));


		$this->wp_customize->add_setting('orphan_toolbar_left', array(
			'default' => 'tagline',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_toolbar_left', array(
			'label'           => esc_html_x('Toolbar Left Area', 'backend', 'orphan'),
			'section'         => 'toolbar',
			'settings'        => 'orphan_toolbar_left', 
			'active_callback' => 'orphan_toolbar_check',
			'type'            => 'select',
			'choices'         => $this->orphan_toolbar_left_elements()
		)));

		$this->wp_customize->add_setting('orphan_toolbar_left_hide_mobile', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_toolbar_left_hide_mobile', array(
			'label'           => esc_html_x('Hide in mobile mode', 'backend', 'orphan'),
			'section'         => 'toolbar',
			'settings'        => 'orphan_toolbar_left_hide_mobile', 
			'type'            => 'checkbox',
			'active_callback' => 'orphan_toolbar_social_check',
		)));


		$this->wp_customize->add_setting('orphan_toolbar_left_custom', array(
			'sanitize_callback' => 'orphan_sanitize_textarea'
		));
		$this->wp_customize->add_control( new orphan_Customize_Textarea_Control( $this->wp_customize, 'orphan_toolbar_left_custom', array(
			'label'           => esc_html_x('Toolbar Left Custom Text', 'backend', 'orphan'),
			'section'         => 'toolbar',
			'settings'        => 'orphan_toolbar_left_custom',
			'active_callback' => 'orphan_toolbar_left_custom_check',
			'type'            => 'textarea',
		)));

		$this->wp_customize->add_setting('orphan_toolbar_right', array(
			'default' => 'social',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_toolbar_right', array(
			'label'           => esc_html_x('Toolbar Right Area', 'backend', 'orphan'),
			'description' 	  => (get_theme_mod( 'orphan_woocommerce_cart' ) == 'toolbar') ? esc_html_x('This element automatically hide on mobile mode, for better preview shopping cart.', 'backend', 'orphan') : '',
			'section'         => 'toolbar',
			'settings'        => 'orphan_toolbar_right', 
			'active_callback' => 'orphan_toolbar_check',
			'type'            => 'select',
			'choices'         => $this->orphan_toolbar_right_elements()
		)));

		$this->wp_customize->add_setting('orphan_toolbar_right_hide_mobile', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_toolbar_right_hide_mobile', array(
			'label'           => esc_html_x('Hide in mobile mode', 'backend', 'orphan'),
			'section'         => 'toolbar',
			'settings'        => 'orphan_toolbar_right_hide_mobile', 
			'type'            => 'checkbox',
			'active_callback' => 'orphan_toolbar_social_check',
		)));


		$this->wp_customize->add_setting('orphan_toolbar_right_custom', array(
			'sanitize_callback' => 'orphan_sanitize_textarea'
		));
		$this->wp_customize->add_control( new orphan_Customize_Textarea_Control( $this->wp_customize, 'orphan_toolbar_right_custom', array(
			'label'           => esc_html_x('Toolbar Right Custom Text', 'backend', 'orphan'),
			'section'         => 'toolbar',
			'settings'        => 'orphan_toolbar_right_custom',
			'active_callback' => 'orphan_toolbar_right_custom_check',
			'type'            => 'textarea',
		)));


		$this->wp_customize->add_setting('orphan_toolbar_social', array(
			'sanitize_callback' => 'esc_html'
		));
		$this->wp_customize->add_control( new orphan_Customize_Social_Control( $this->wp_customize, 'orphan_toolbar_social', array(
			'label'             => esc_html_x('Social Link', 'backend', 'orphan'),
			'description'       => esc_html_x('Enter up to 5 links to your social profiles.', 'backend', 'orphan'),
			'section'           => 'toolbar',
			'settings'          => 'orphan_toolbar_social',
			'type'              => 'social',
			'active_callback' => 'orphan_toolbar_social_check',
		)));

		$this->wp_customize->add_setting('orphan_toolbar_social_style', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_toolbar_social_style', array(
			'label'           => esc_html_x('Social link as button', 'backend', 'orphan'),
			'section'         => 'toolbar',
			'settings'        => 'orphan_toolbar_social_style', 
			'type'            => 'checkbox',
			'active_callback' => 'orphan_toolbar_social_check',
		)));
		
		

		/**
		 * General Customizer Settings
		 */

		//general section
		$this->wp_customize->add_section('general', array(
			'title' => esc_html_x('General', 'backend', 'orphan'),
			'priority' => 21
		));

		$this->wp_customize->add_setting('orphan_global_layout', array(
			'default' => 'full',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_global_layout', array(
			'label'    => esc_html_x('Global Layout', 'backend', 'orphan'),
			'section'  => 'general',
			'settings' => 'orphan_global_layout', 
			'type'     => 'select',
			'choices'  => array(
				'full'  => esc_html_x('Fullwidth', 'backend', 'orphan'),
				'boxed' => esc_html_x('Boxed', 'backend', 'orphan'),
			)
		)));

		$this->wp_customize->add_setting('orphan_comment_show', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_comment_show',
	        array(
				'label'       => esc_html_x('Show Global Page Comment', 'backend', 'orphan'),
				'description' => esc_html_x('Enable / Disable global page comments (not post comment).', 'backend', 'orphan'),
				'section'     => 'general',
				'settings'    => 'orphan_comment_show',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0 => esc_html_x('No', 'backend', 'orphan')
				)
	        )
		));

		$this->wp_customize->add_setting('orphan_offcanvas_search', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_offcanvas_search',
	        array(
				'label'       => esc_html_x('Offcanvas Search', 'backend', 'orphan'),
				'description' => esc_html_x('Enable / Disable Offcanvas search display', 'backend', 'orphan'),
				'section'     => 'general',
				'settings'    => 'orphan_offcanvas_search',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0 => esc_html_x('No', 'backend', 'orphan')
				)
	        )
		));


		$this->wp_customize->add_setting('orphan_dev_mode', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_dev_mode',
	        array(
				'label'       => esc_html_x('Development Mode', 'backend', 'orphan'),
				'description' => esc_html_x('When you develop your website keep it delelopment mode yes, so all script and style will load separately debug site easily.', 'backend', 'orphan'),
				'section'     => 'general',
				'settings'    => 'orphan_dev_mode',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0 => esc_html_x('No, Go Live!', 'backend', 'orphan')
				)
	        )
		));




		//titlebar section
		$this->wp_customize->add_section('titlebar', array(
			'title' => esc_html_x('Titlebar', 'backend', 'orphan'),
			'priority' => 32,
			'active_callback' => 'orphan_titlebar_check'
		));

		$this->wp_customize->add_setting('orphan_titlebar_layout', array(
			'default' => 'left',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control('orphan_titlebar_layout', array(
			'label'    => esc_html_x('Titlebar Layout', 'backend', 'orphan'),
			'section'  => 'titlebar',
			'settings' => 'orphan_titlebar_layout', 
			'type'     => 'select',
			'priority' => 1,
			'choices'  => array(
				'left'   => esc_html_x('Left Align', 'backend', 'orphan'),
				'center'  => esc_html_x('Center Align', 'backend', 'orphan'),
				'right'  => esc_html_x('Right Align', 'backend', 'orphan'),
				'notitle' => esc_html_x('No Titlebar', 'backend', 'orphan')
			)
		));


		$this->wp_customize->add_setting('orphan_titlebar_bg_style', array(
			'default' => 'muted',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_titlebar_bg_style', array(
			'label'    => esc_html_x('Background Style', 'backend', 'orphan'),
			'section'  => 'titlebar',
			'settings' => 'orphan_titlebar_bg_style', 
			'type'     => 'select',
			'choices'  => array(
				'default'   => esc_html_x('Default', 'backend', 'orphan'),
				'muted'     => esc_html_x('Muted', 'backend', 'orphan'),
				'primary'   => esc_html_x('Primary', 'backend', 'orphan'),
				'secondary' => esc_html_x('Secondary', 'backend', 'orphan'),
				'image'     => esc_html_x('Image', 'backend', 'orphan'),
				//'video'     => esc_html_x('Video', 'backend', 'orphan'),
			)
		));
		

		$this->wp_customize->add_setting( 'orphan_titlebar_bg_img' , array(
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control( new WP_Customize_Image_Control( $this->wp_customize, 'orphan_titlebar_bg_img', array(
			'label'           => esc_html_x( 'Background Image', 'backend', 'orphan' ),
			'section'         => 'titlebar',
			'settings'        => 'orphan_titlebar_bg_img',
			'active_callback' => 'orphan_titlebar_bg_check',
		)));


		$this->wp_customize->add_setting('orphan_titlebar_txt_style', array(
			'default'           => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_titlebar_txt_style', array(
			'label'    => esc_html_x('Text Color', 'backend', 'orphan'),
			'section'  => 'titlebar',
			'settings' => 'orphan_titlebar_txt_style', 
			'type'     => 'select',
			'choices'  => array(
				0       => esc_html_x('Default', 'backend', 'orphan'),
				'light' => esc_html_x('Light', 'backend', 'orphan'),
				'dark'  => esc_html_x('Dark', 'backend', 'orphan'),
			)
		));


		$this->wp_customize->add_setting('orphan_titlebar_padding', array(
			'default' => 'medium',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_titlebar_padding', array(
			'label'    => esc_html_x('Padding', 'backend', 'orphan'),
			'section'  => 'titlebar',
			'settings' => 'orphan_titlebar_padding', 
			'type'     => 'select',
			'choices'  => array(
				'medium' => esc_html_x('Default', 'backend', 'orphan'),
				'small'  => esc_html_x('Small', 'backend', 'orphan'),
				'large'  => esc_html_x('Large', 'backend', 'orphan'),
				'none'   => esc_html_x('None', 'backend', 'orphan'),
			)
		));


		$this->wp_customize->add_setting('orphan_blog_title', array(
			'default' => esc_html_x('Blog', 'backend', 'orphan'),
			'sanitize_callback' => 'esc_attr'
		));
		$this->wp_customize->add_control('orphan_blog_title', array(
		    'label'    => esc_html_x('Blog Title: ', 'backend', 'orphan'),
		    'section'  => 'titlebar',
		    'settings' => 'orphan_blog_title'
		));

		if (class_exists('Woocommerce')){
			$this->wp_customize->add_setting('orphan_woocommerce_title', array(
				'default' => esc_html_x('Shop', 'backend', 'orphan'),
				'sanitize_callback' => 'esc_attr'
			));
			$this->wp_customize->add_control('orphan_woocommerce_title', array(
			    'label'    => esc_html_x('WooCommerce Title: ', 'backend', 'orphan'),
			    'section'  => 'titlebar',
			    'settings' => 'orphan_woocommerce_title'
			));
		}



		//blog section
		$this->wp_customize->add_section('blog', array(
			'title' => esc_html_x('Blog', 'backend', 'orphan'),
			'priority' => 35
		));


		$this->wp_customize->add_setting('orphan_blog_layout', array(
			'default' => 'sidebar-right',
			'sanitize_callback' => 'orphan_sanitize_choices',
		));
		$this->wp_customize->add_control(new orphan_Customize_Layout_Control( $this->wp_customize, 'orphan_blog_layout', 
			array(
				'label'       => esc_html_x('Blog Page Layout', 'backend', 'orphan'),
				'description' => esc_html_x('If you select static blog page so you need to select your blog page layout from here.', 'backend', 'orphan'),
				'section'     => 'blog',
				'settings'    => 'orphan_blog_layout', 
				'type'        => 'layout',
				'priority'    => 1,
				'choices' => array(
					"sidebar-left"  => esc_html_x('Sidebar Left', 'backend', 'orphan'), 
					"full"          => esc_html_x('Fullwidth', 'backend', 'orphan'),
					"sidebar-right" => esc_html_x('Sidebar Right', 'backend', 'orphan'),
				),
				//'active_callback' => 'is_front_page',
			)
		));



		$this->wp_customize->add_setting('orphan_blog_readmore', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_blog_readmore',
	        array(
				'priority'    => 2,
				'label'       => esc_html_x('Read More Button in Blog Posts', 'backend', 'orphan'),
				'description' => esc_html_x('Enable / Disable read more button on blog posts.', 'backend', 'orphan'),
				'section'     => 'blog',
				'settings'    => 'orphan_blog_readmore',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0  => esc_html_x('No', 'backend', 'orphan')
				)
	        )
		));

		$this->wp_customize->add_setting('orphan_blog_meta', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_blog_meta',
	        array(
				'priority'    => 3,
				'label'       => esc_html_x('Metadata on Blog Posts', 'backend', 'orphan'),
				'description' => esc_html_x('Enable / Disable metadata on blog post.', 'backend', 'orphan'),
				'section'     => 'blog',
				'settings'    => 'orphan_blog_meta',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0  => esc_html_x('No', 'backend', 'orphan')
				)
	        )
		));

		$this->wp_customize->add_setting('orphan_blog_next_prev', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_blog_next_prev',
	        array(
				'priority'    => 4,
				'label'       => esc_html_x('Previous / Next Pagination', 'backend', 'orphan'),
				'description' => esc_html_x('Enable / Disable next previous button on blog posts.', 'backend', 'orphan'),
				'section'     => 'blog',
				'settings'    => 'orphan_blog_next_prev',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0  => esc_html_x('No', 'backend', 'orphan')
				)
	        )
		));

		$this->wp_customize->add_setting('orphan_author_info', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_author_info',
	        array(
				'priority'    => 5,
				'label'       => esc_html_x('Author Info in Blog Details', 'backend', 'orphan'),
				'description' => esc_html_x('Enable / Disable author info from underneath of blog posts.', 'backend', 'orphan'),
				'section'     => 'blog',
				'settings'    => 'orphan_author_info',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0  => esc_html_x('No', 'backend', 'orphan')
				)
	        )
		));

		$this->wp_customize->add_setting('orphan_related_post', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_related_post',
	        array(
				'priority'    => 6,
				'label'       => esc_html_x('Related Posts in Blog Details', 'backend', 'orphan'),
				'description' => esc_html_x('Enable / Disable related post underneath of blog posts.', 'backend', 'orphan'),
				'section'     => 'blog',
				'settings'    => 'orphan_related_post',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0  => esc_html_x('No', 'backend', 'orphan')
				)
	        )
		));



		
		/**
		 * Layout Customizer Settings
		 */

		//Header section
		$this->wp_customize->add_section('header', array(
			'title' => esc_html_x('Header', 'backend', 'orphan'),
			'priority' => 31
		));


		$this->wp_customize->add_setting('orphan_header_layout', array(
			'default'           => 'horizontal-left',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_header_layout', array(
			'label'    => esc_html_x('Header Layout', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_header_layout', 
			'type'     => 'select',
			'choices'  => array(
				'horizontal-left'      => esc_html_x('Horizontal Left', 'backend', 'orphan'),
				'horizontal-center'    => esc_html_x('Horizontal Center', 'backend', 'orphan'),
				'horizontal-right'     => esc_html_x('Horizontal Right', 'backend', 'orphan'),
				'stacked-center-a'     => esc_html_x('Stacked Center A', 'backend', 'orphan'),
				'stacked-center-b'     => esc_html_x('Stacked Center B', 'backend', 'orphan'),
				'stacked-center-split' => esc_html_x('Stacked Center Split', 'backend', 'orphan'),
				'stacked-left-a'       => esc_html_x('Stacked Left A', 'backend', 'orphan'),
				'stacked-left-b'       => esc_html_x('Stacked Left B', 'backend', 'orphan'),
				'toggle-offcanvas'     => esc_html_x('Toggle Offcanvas', 'backend', 'orphan'),
				'toggle-modal'         => esc_html_x('Toggle Modal', 'backend', 'orphan'),
			)
		)));
		
		$this->wp_customize->add_setting('orphan_header_fullwidth', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_header_fullwidth', array(
			'label'       => esc_html_x('Header Fullwidth', 'backend', 'orphan'),
			'description' => esc_html_x('(Make your header full width like fluid width.)', 'backend', 'orphan'),
			'section'     => 'header',
			'settings'    => 'orphan_header_fullwidth', 
			'type'        => 'checkbox',
		)));

		$this->wp_customize->add_setting('orphan_header_bg_style', array(
			'default' => 'default',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_header_bg_style', array(
			'label'    => esc_html_x('Background Style', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_header_bg_style',
			'type'     => 'select',
			'choices'  => array(
				'default'   => esc_html_x('Default', 'backend', 'orphan'),
				'muted'     => esc_html_x('Muted', 'backend', 'orphan'),
				'primary'   => esc_html_x('Primary', 'backend', 'orphan'),
				'secondary' => esc_html_x('Secondary', 'backend', 'orphan'),
				'image'     => esc_html_x('Image', 'backend', 'orphan'),
				//'video'     => esc_html_x('Video', 'backend', 'orphan'),
			),
			'active_callback' => 'orphan_header_transparent_check',
		));

		$this->wp_customize->add_setting( 'orphan_header_bg_img' , array(
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control( new WP_Customize_Image_Control( $this->wp_customize, 'orphan_header_bg_img', array(
			'label'           => esc_html_x( 'Background Image', 'backend', 'orphan' ),
			'section'         => 'header',
			'settings'        => 'orphan_header_bg_img',
			'active_callback' => 'orphan_header_bg_style_check',
		)));

		$this->wp_customize->add_setting('orphan_header_bg_img_position', array(
			'default' => '',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_header_bg_img_position', array(
			'label'    => esc_html_x('Background Position', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_header_bg_img_position', 
			'type'     => 'select',
			'choices'  => array(
				'top-left'      => esc_html_x('Top Left', 'backend', 'orphan'),
				'top-center'    => esc_html_x('Top Center', 'backend', 'orphan'),
				'top-right'     => esc_html_x('Top Right', 'backend', 'orphan'),
				'center-left'   => esc_html_x('Center Left', 'backend', 'orphan'),
				''              => esc_html_x('Center Center', 'backend', 'orphan'),
				'center-right'  => esc_html_x('Center Right', 'backend', 'orphan'),
				'bottom-left'   => esc_html_x('Bottom Left', 'backend', 'orphan'),
				'bottom-center' => esc_html_x('Bottom Center', 'backend', 'orphan'),
				'bottom-right'  => esc_html_x('Bottom Right', 'backend', 'orphan'),
			),
			'active_callback' => 'orphan_header_bg_img_check',
		)));

		$this->wp_customize->add_setting('orphan_header_txt_style', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_header_txt_style', array(
			'label'    => esc_html_x('Header Text Color', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_header_txt_style', 
			'type'     => 'select',
			'choices'  => array(
				0       => esc_html_x('Default', 'backend', 'orphan'),
				'light' => esc_html_x('Light', 'backend', 'orphan'),
				'dark'  => esc_html_x('Dark', 'backend', 'orphan'),
			),
			//'active_callback' => 'orphan_header_fixed_check',
		)));


		$this->wp_customize->add_setting('orphan_header_transparent', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_header_transparent', array(
			'label'    => esc_html_x('Header Transparent', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_header_transparent', 
			'type'     => 'select',
			'choices'  => array(
				0       => esc_html_x('No', 'backend', 'orphan'),
				'light' => esc_html_x('Overlay (Light)', 'backend', 'orphan'),
				'dark'  => esc_html_x('Overlay (Dark)', 'backend', 'orphan'),
			),
			//'active_callback' => 'orphan_header_fixed_check',
		)));


		$this->wp_customize->add_setting('orphan_header_sticky', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_header_sticky', array(
			'label'    => esc_html_x('Header Sticky', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_header_sticky', 
			'type'     => 'select',
			'choices'  => array(
				0        => esc_html_x('No', 'backend', 'orphan'),
				'sticky' => esc_html_x('Sticky', 'backend', 'orphan'),
				'smart'  => esc_html_x('Smart Sticky', 'backend', 'orphan'),
			)
		)));

		$this->wp_customize->add_setting('orphan_mobile_break_point', array(
			'default' => 'm',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_mobile_break_point', array(
			'label'    => esc_html_x('Mobile Break Point', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_mobile_break_point', 
			'type'     => 'select',
			'choices'  => array(
				's' => esc_html_x('Small', 'backend', 'orphan'),
				'm' => esc_html_x('Medium', 'backend', 'orphan'),
				'l' => esc_html_x('Large', 'backend', 'orphan'),
			)
		)));


		$this->wp_customize->add_setting('orphan_mobile_logo_align', array(
			'default' => 'center',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_mobile_logo_align', array(
			'label'    => esc_html_x('Mobile Logo Align', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_mobile_logo_align', 
			'type'     => 'select',
			'choices'  => array(
				'' => esc_html_x('Hide', 'backend', 'orphan'),
				'left' => esc_html_x('Left', 'backend', 'orphan'),
				'right' => esc_html_x('Right', 'backend', 'orphan'),
				'center' => esc_html_x('Center', 'backend', 'orphan'),
			)
		)));


		$this->wp_customize->add_setting('orphan_mobile_menu_align', array(
			'default' => 'left',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_mobile_menu_align', array(
			'label'    => esc_html_x('Mobile Menu Align', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_mobile_menu_align', 
			'type'     => 'select',
			'choices'  => array(
				''      => esc_html_x('Hide', 'backend', 'orphan'),
				'left'  => esc_html_x('Left', 'backend', 'orphan'),
				'right' => esc_html_x('Right', 'backend', 'orphan'),
			)
		)));


		$this->wp_customize->add_setting('orphan_mobile_offcanvas_style', array(
			'default' => 'offcanvas',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_mobile_offcanvas_style', array(
			'label'       => esc_html_x('Mobile Menu Style', 'backend', 'orphan'),
			'description' => 'Select the menu style displayed in the mobile position.',
			'section'     => 'header',
			'settings'    => 'orphan_mobile_offcanvas_style', 
			'type'        => 'select',
			'choices'     => array(
				'offcanvas' => esc_html_x('Offcanvas', 'backend', 'orphan'),
				'modal'     => esc_html_x('Modal', 'backend', 'orphan'),
				'dropdown'  => esc_html_x('Dropdown', 'backend', 'orphan'),
			),
		)));


		$this->wp_customize->add_setting('orphan_mobile_offcanvas_mode', array(
			'default' => 'offcanvas',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_mobile_offcanvas_mode', array(
			'label'       => esc_html_x('Offcanvas Mode', 'backend', 'orphan'),
			'section'     => 'header',
			'settings'    => 'orphan_mobile_offcanvas_mode', 
			'type'        => 'select',
			'choices'     => array(
				'slide'  => esc_html_x('Slide', 'backend', 'orphan'),
				'reveal' => esc_html_x('Reveal', 'backend', 'orphan'),
				'push'   => esc_html_x('Push', 'backend', 'orphan'),
			),
			'active_callback' => 'orphan_offcanvas_mode_check',
		)));


		$this->wp_customize->add_setting('orphan_mobile_menu_text', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control('orphan_mobile_menu_text', array(
			'label'       => esc_html_x('Display the menu text next to the icon.', 'backend', 'orphan'),
			'section'     => 'header',
			'settings'    => 'orphan_mobile_menu_text',
			'type'        => 'checkbox'
		));



		$this->wp_customize->add_setting('orphan_search_position', array(
			'default' => 'header',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_search_position', array(
			'label'    => esc_html_x('Search', 'backend', 'orphan'),
			'description'    => esc_html_x('Select the position that will display the search.', 'backend', 'orphan'),
			'section'  => 'header',
			'settings' => 'orphan_search_position', 
			'type'     => 'select',
			'choices'  => array(
				0        => esc_html_x('Hide', 'backend', 'orphan'),
				'header' => esc_html_x('Header', 'backend', 'orphan'),
				'menu'   => esc_html_x('With Menu', 'backend', 'orphan'),
			)
		)));

		$this->wp_customize->add_setting('orphan_search_style', array(
			'default' => 'default',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_search_style', array(
			'label'       => esc_html_x('Search Style', 'backend', 'orphan'),
			'description' => esc_html_x('Select search style from here.', 'backend', 'orphan'),
			'section'     => 'header',
			'settings'    => 'orphan_search_style', 
			'type'        => 'select',
			'choices'     => array(
				'default'  => esc_html_x('Default', 'backend', 'orphan'),
				'modal'    => esc_html_x('Modal', 'backend', 'orphan'),
				'dropdown' => esc_html_x('Dropdown', 'backend', 'orphan'),
			)
		)));


		// Main Body Settings
		$this->wp_customize->add_section('mainbody', array(
			'title'       => esc_html_x('Main Body', 'backend', 'orphan'),
			'description' => esc_html_x( 'Default body settings here.', 'backend', 'orphan' ),
			'priority'    => 33
		));

		$this->wp_customize->add_setting('orphan_sidebar_position', array(
			'default' => 'sidebar-right',
			'sanitize_callback' => 'orphan_sanitize_choices',
		));
		$this->wp_customize->add_control(new orphan_Customize_Layout_Control( $this->wp_customize, 'orphan_sidebar_position', 
			array(
				'label'       => esc_html_x('Sidebar Layout', 'backend', 'orphan'),
				'description' => esc_html_x('Select global page sidebar position from here. If you already set any sidebar setting from specific page so it will not applicable for that page.', 'backend', 'orphan'),
				'section'     => 'mainbody',
				'settings'    => 'orphan_sidebar_position', 
				'type'        => 'layout',
				'choices' => array(
					"sidebar-left"  => esc_html_x('Sidebar Left', 'backend', 'orphan'), 
					"full"          => esc_html_x('No Sidebar', 'backend', 'orphan'),
					"sidebar-right" => esc_html_x('Sidebar Right', 'backend', 'orphan'),
				),
				'active_callback' => 'orphan_homepage_check',
			)
		));


		$this->wp_customize->add_setting('orphan_body_bg_style', array(
			'default' => 'default',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_body_bg_style', array(
			'label'    => esc_html_x('Background Style', 'backend', 'orphan'),
			'section'  => 'mainbody',
			'settings' => 'orphan_body_bg_style', 
			'type'     => 'select',
			'choices'  => array(
				'default'   => esc_html_x('Default', 'backend', 'orphan'),
				'muted'     => esc_html_x('Muted', 'backend', 'orphan'),
				'primary'   => esc_html_x('Primary', 'backend', 'orphan'),
				'secondary' => esc_html_x('Secondary', 'backend', 'orphan'),
				'image'     => esc_html_x('Image', 'backend', 'orphan'),
				//'video'     => esc_html_x('Video', 'backend', 'orphan'),
			)
		));
		

		$this->wp_customize->add_setting( 'orphan_body_bg_img' , array(
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control( new WP_Customize_Image_Control( $this->wp_customize, 'orphan_body_bg_img', array(
			'label'           => esc_html_x( 'Background Image', 'backend', 'orphan' ),
			'section'         => 'mainbody',
			'settings'        => 'orphan_body_bg_img',
			'active_callback' => 'orphan_body_bg_check',
		)));


		$this->wp_customize->add_setting('orphan_sidebar_width', array(
			'default' => '1-4',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_sidebar_width', array(
			'label'    => esc_html_x('Sidebar Width', 'backend', 'orphan'),
			'description' => esc_html_x('Set a sidebar width in percent and the content column will adjust accordingly. The width will not go below the Sidebar\'s min-width, which you can set in the Style section.', 'backend', 'orphan'),
			'section'  => 'mainbody',
			'settings' => 'orphan_sidebar_width', 
			'type'     => 'select',
			'choices'  => array(
				'1-5' => esc_html_x('20%', 'backend', 'orphan'),
				'1-4' => esc_html_x('25%', 'backend', 'orphan'),
				'1-3' => esc_html_x('33%', 'backend', 'orphan'),
				'2-5' => esc_html_x('40%', 'backend', 'orphan'),
				'1-2' => esc_html_x('50%', 'backend', 'orphan'),
			)
		));


		$this->wp_customize->add_setting('orphan_sidebar_gutter', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_sidebar_gutter', array(
			'label'    => esc_html_x('Gutter', 'backend', 'orphan'),
			'section'  => 'mainbody',
			'settings' => 'orphan_sidebar_gutter', 
			'type'     => 'select',
			'choices'  => array(
				'small'    => esc_html_x('Small', 'backend', 'orphan'),
				'medium'   => esc_html_x('Medium', 'backend', 'orphan'),
				0          => esc_html_x('Default', 'backend', 'orphan'),
				'large'    => esc_html_x('Large', 'backend', 'orphan'),
				'collapse' => esc_html_x('Collapse', 'backend', 'orphan'),
			)
		));

		$this->wp_customize->add_setting('orphan_sidebar_divider', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control('orphan_sidebar_divider', array(
			'label'           => esc_html_x('Display dividers between body and sidebar', 'backend', 'orphan'),
			'description'     => esc_html_x('(Set the grid gutter width and display dividers between grid cells.)', 'backend', 'orphan'),
			'section'         => 'mainbody',
			'settings'        => 'orphan_sidebar_divider',
			//'active_callback' => 'orphan_bottom_gutter_collapse_check',
			'type'            => 'checkbox'
		));


		$this->wp_customize->add_setting('orphan_sidebar_breakpoint', array(
			'default' => 'm',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_sidebar_breakpoint', array(
			'label'       => esc_html_x('Breakpoint', 'backend', 'orphan'),
			'description' => esc_html_x('Set the breakpoint from which the sidebar and content will stack.', 'backend', 'orphan'),
			'section'     => 'mainbody',
			'settings'    => 'orphan_sidebar_breakpoint', 
			'type'        => 'select',
			'choices'     => array(
				's'  => esc_html_x('Small (Phone Landscape)', 'backend', 'orphan'),
				'm'  => esc_html_x('Medium (Tablet Landscape)', 'backend', 'orphan'),
				'l'  => esc_html_x('Large (Desktop)', 'backend', 'orphan'),
				'xl' => esc_html_x('X-Large (Large Screens)', 'backend', 'orphan'),
			)
		));




		$this->wp_customize->add_setting('orphan_body_txt_style', array(
			'default'           => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_body_txt_style', array(
			'label'    => esc_html_x('Text Color', 'backend', 'orphan'),
			'section'  => 'mainbody',
			'settings' => 'orphan_body_txt_style', 
			'type'     => 'select',
			'choices'  => array(
				0       => esc_html_x('Default', 'backend', 'orphan'),
				'light' => esc_html_x('Light', 'backend', 'orphan'),
				'dark'  => esc_html_x('Dark', 'backend', 'orphan'),
			)
		));


		$this->wp_customize->add_setting('orphan_body_padding', array(
			'default' => 'medium',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_body_padding', array(
			'label'    => esc_html_x('Padding', 'backend', 'orphan'),
			'section'  => 'mainbody',
			'settings' => 'orphan_body_padding', 
			'type'     => 'select',
			'choices'  => array(
				'medium' => esc_html_x('Default', 'backend', 'orphan'),
				'small'  => esc_html_x('Small', 'backend', 'orphan'),
				'large'  => esc_html_x('Large', 'backend', 'orphan'),
				'none'   => esc_html_x('None', 'backend', 'orphan'),
			)
		));



		// Background image for body tag
		$this->wp_customize->add_setting('orphan_bg_note', array(
				'default'           => '',
				'sanitize_callback' => 'esc_attr'
		    )
		);
		$this->wp_customize->add_control( new orphan_Customize_Alert_Control( $this->wp_customize, 'orphan_bg_note', array(
			'label'       => 'Background Alert',
			'section'     => 'background_image',
			'settings'    => 'orphan_bg_note',
			'type'        => 'alert',
			'priority'    => 1,
			'text'        => esc_html_x('You must set your layout mode Boxed for use this feature. Otherwise you can\'t see what happening in background', 'backend', 'orphan'),
			'alert_type' => 'warning',
		    )) 
		);

		$this->wp_customize->add_panel('colors', array(
			'title' => esc_html_x('Colors', 'backend', 'orphan'),
			'priority' => 45
		));

		$this->wp_customize->add_section('colors_global', array(
			'title' => esc_html_x('Global Colors', 'backend', 'orphan'),
			'panel' => 'colors',
		));

		$this->wp_customize->add_section('colors_header', array(
			'title' => esc_html_x('Header Colors', 'backend', 'orphan'),
			'panel' => 'colors',
		));

		$this->wp_customize->add_section('colors_menu', array(
			'title' => esc_html_x('Menu Colors', 'backend', 'orphan'),
			'panel' => 'colors',
		));

		$this->wp_customize->add_section('colors_body', array(
			'title' => esc_html_x('Body Colors', 'backend', 'orphan'),
			'panel' => 'colors',
		));

		$this->wp_customize->add_section('colors_footer', array(
			'title' => esc_html_x('Footer Colors', 'backend', 'orphan'),
			'panel' => 'colors',
		));



		$this->wp_customize->add_control(new WP_Customize_Color_Control( $this->wp_customize, 'background_color', array(
			'label'       => esc_html_x('Global Background Color', 'backend', 'orphan'),
			'section'     => 'colors_global',
			'description' => esc_html_x('Please select layout boxed for check your global page background.', 'backend', 'orphan'),
        )));


        // Add page background color setting and control.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'global_page_background_color', array(
			'default'           => '#ffffff',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'background-color' => array(
					
				),
			)
		)));
        

		$this->wp_customize->add_control(new WP_Customize_Color_Control( $this->wp_customize, 'global_page_background_color', array(
			'label'       => esc_html_x('Page Background Color', 'backend', 'orphan'),
			'section'     => 'colors_body',
        )));
        

        // Add global link hover color setting.
        $this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'global_color', array(
        	'default'           => '#666666',
        	'sanitize_callback' => 'sanitize_hex_color',
        	'css_map' => array(
        		'color' => array(
        			'body',
        		),
        	)
        )));

        // Add global link hover color control.
        $this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'global_color', array(
        	'label'       => esc_html_x( 'Global Text Color', 'backend', 'orphan' ),
        	'section'     => 'colors_body',
        )));


        // Add global link color setting.
        $this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'primary_color', array(
        	'default'           => '#F44336',
        	'sanitize_callback' => 'sanitize_hex_color',
        	'css_map' => array(
        		'color' => array(
					'a',
					'.uk-link',
					'.uk-text-primary',
					'.uk-alert-primary',
				),
				'background-color' => array(
					'.uk-button-primary',
					'.uk-section-primary',
					'.uk-background-primary',
					'.uk-card-primary',
					'.uk-label',
					'.bdt-heading:after',
					'.bdt-owl-carousel .bdt-oc-tags a',
					'.uk-subnav-pill > .uk-active > a',
					'.uk-navbar-nav>li.uk-active>a::after',
				),
				'border-color' => array(
					'.uk-input:focus', 
					'.uk-select:focus', 
					'.uk-textarea:focus', 
					'.woocommerce input.input-text:focus', 
					'.woocommerce select:focus', 
					'.woocommerce #review_form #respond textarea:focus', 
					'.woocommerce form .form-row select:focus', 
					'.woocommerce form .form-row textarea:focus',
					'.tm-bottom.uk-section-custom .uk-button-default:hover',
				),
				'stroke' => array(
					'.bdt-progress-pie svg path'
				),

        	)
        )));

        // Add global link color control.
        $this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'primary_color', array(
        	'label'       => esc_html_x( 'Primary Color', 'backend', 'orphan' ),
        	'section'     => 'colors_global',
        )));



        // Add global link hover color setting.
        $this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'primary_hover_color', array(
        	'default'           => '#f32c1e',
        	'sanitize_callback' => 'sanitize_hex_color',
        	'css_map' => array(
        		'color' => array(
        			'a:hover',
        			'.uk-link:hover',
        			'.uk-text-primary:hover',
        			'.uk-alert-primary:hover',
        		),
        		'background-color' => array(
        			'.uk-button-primary:hover',
        			'.bdt-owl-carousel .bdt-oc-tags a:hover',
        		),
        		'border-color' => array(
        			
        		),
        	)
        )));

        // Add global link hover color control.
        $this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'primary_hover_color', array(
        	'label'       => esc_html_x( 'Primary Hover Color', 'backend', 'orphan' ),
        	'section'     => 'colors_global',
        )));


        // Add secondary color setting.
        $this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'secondary_color', array(
        	'default'           => '#222222',
        	'sanitize_callback' => 'sanitize_hex_color',
        	'css_map' => array(
        		'color' => array(
					'.uk-text-secondary',
					'.uk-alert-secondary',
				),
				'background-color' => array(
					'.uk-button-secondary',
					'.uk-section-secondary',
					'.uk-background-secondary',
					'.uk-card-secondary',
				),
        	)
        )));

        // Add secondary color control.
        $this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'secondary_color', array(
        	'label'       => esc_html_x( 'Secondary Color', 'backend', 'orphan' ),
        	'section'     => 'colors_global',
        )));


        // Add muted color setting.
        $this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'muted_color', array(
        	'default'           => '#999999',
        	'sanitize_callback' => 'sanitize_hex_color',
        	'css_map' => array(
        		'color' => array(
					'.uk-text-muted',
					'.uk-alert-muted',
				),
        	)
        )));

        // Add muted color control.
        $this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'muted_color', array(
        	'label'       => esc_html_x( 'Muted Text Color', 'backend', 'orphan' ),
        	'section'     => 'colors_global',
        )));

        // Add muted color setting.
        $this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'muted_background_color', array(
        	'default'           => '#f8f8f8',
        	'sanitize_callback' => 'sanitize_hex_color',
        	'css_map' => array(
				'background-color' => array(
					'.uk-button-muted',
					'.uk-section-muted',
					'.uk-background-muted',
					'.uk-card-muted',
				),
        	)
        )));

        // Add muted color control.
        $this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'muted_background_color', array(
        	'label'       => esc_html_x( 'Muted Background Color', 'backend', 'orphan' ),
        	'section'     => 'colors_global',
        )));



        // Add page background color setting and control.
		$this->wp_customize->add_setting( 'browser_header_color', array(
			'default'           => '#222222',
			'sanitize_callback' => 'sanitize_hex_color',
		));

		$this->wp_customize->add_control(new WP_Customize_Color_Control( $this->wp_customize, 'browser_header_color', array(
			'label'       => esc_html_x('Browser Header Color', 'backend', 'orphan'),
			'section'     => 'colors_global',
			'description' => esc_html_x('This color for mobile browser header. This color works only mobile view.' , 'backend', 'orphan'),
        )));



		/**
		 * Footer Customizer Settings
		 */

		// footer appearance
		$this->wp_customize->add_section('footer', array(
			'title' => esc_html_x('Footer', 'backend', 'orphan'),
			'description' => esc_html_x( 'All Orphan theme specific settings.', 'backend', 'orphan' ),
			'priority' => 140
		));

		$this->wp_customize->add_setting('orphan_footer_widgets', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control($this->wp_customize, 'orphan_footer_widgets',
	        array(
				'priority'    => 1,
				'label'       => esc_html_x('Show Footer Widgets', 'backend', 'orphan'),
				'section'     => 'footer',
				'settings'    => 'orphan_footer_widgets',
				'type'        => 'select',
				'choices'     => array(
					1 => esc_html_x('Yes', 'backend', 'orphan'),
					0 => esc_html_x('No', 'backend', 'orphan')
				)
	        )
		));


		$this->wp_customize->add_setting('orphan_footer_columns', array(
			'default' => 4,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_footer_columns', array(
			'label'    => esc_html_x('Footer Columns:', 'backend', 'orphan'),
			'section'  => 'footer',
			'settings' => 'orphan_footer_columns', 
			'type'     => 'select',
			'choices'  => array(
				1 => esc_html_x('1 Column', 'backend', 'orphan'),
				2 => esc_html_x('2 Columns', 'backend', 'orphan'),
				3 => esc_html_x('3 Columns', 'backend', 'orphan'),
				4 => esc_html_x('4 Columns', 'backend', 'orphan')
			)
		));
		
		$this->wp_customize->add_setting('orphan_footer_fce', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control('orphan_footer_fce', array(
			'label'       => esc_html_x('First Column Double Width', 'backend', 'orphan'),
			'description' => esc_html_x('some times your need first footer column double size so you can checked it.', 'backend', 'orphan'),
			'section'     => 'footer',
			'settings'    => 'orphan_footer_fce',
			'type'        => 'checkbox'
		));

		//header section
		if (class_exists('Woocommerce')){
			$this->wp_customize->add_section('woocommerce', array(
				'title' => esc_html_x('WooCommerce', 'backend', 'orphan'),
				'priority' => 99
			));


			$this->wp_customize->add_setting('orphan_woocommerce_sidebar', array(
				'default' => 'sidebar-left',
				'sanitize_callback' => 'orphan_sanitize_choices',
			));
			$this->wp_customize->add_control(new orphan_Customize_Layout_Control( $this->wp_customize, 'orphan_woocommerce_sidebar', 
				array(
					'label'       => esc_html_x('Shop Page Sidebar', 'backend', 'orphan'),
					'description' => esc_html_x('Make sure you add your widget in shop widget position.', 'backend', 'orphan'),
					'section'     => 'woocommerce',
					'settings'    => 'orphan_woocommerce_sidebar', 
					'choices' => array(
						"sidebar-left"  => esc_html_x('Sidebar Left', 'backend', 'orphan'), 
						"full"          => esc_html_x('Fullwidth', 'backend', 'orphan'),
						"sidebar-right" => esc_html_x('Sidebar Right', 'backend', 'orphan'),
					),
					//'active_callback' => 'is_front_page',
				)
			));

			//avatar shape
			$this->wp_customize->add_setting('orphan_woocommerce_cart', array(
				'default'           => 'no',
				'sanitize_callback' => 'orphan_sanitize_choices'
			));
			$this->wp_customize->add_control('orphan_woocommerce_cart', array(
				'label'       => esc_html_x('Shopping Cart Icon in Header:', 'backend', 'orphan'),
				'description' => esc_html_x('Enable / Disable Shopping Cart Icon', 'backend', 'orphan'),
				'section'     => 'woocommerce',
				'settings'    => 'orphan_woocommerce_cart', 
				'type'        => 'select',
				'choices'     => array(
					'no'      => esc_html_x('No Need', 'backend', 'orphan'),
					'header'  => esc_html_x('Yes (in header)', 'backend', 'orphan'),
					'toolbar' => esc_html_x('Yes (in toolbar)', 'backend', 'orphan'),
				)
			));


			$this->wp_customize->add_setting('orphan_woocommerce_columns', array(
				'default' => 3,
				'sanitize_callback' => 'orphan_sanitize_choices'
			) );
			$this->wp_customize->add_control('orphan_woocommerce_columns', array(
				'label'    => esc_html_x('Product Columns:', 'backend', 'orphan'),
				'section'  => 'woocommerce',
				'settings' => 'orphan_woocommerce_columns', 
				'type'     => 'select',
				'choices'  => array(
					2 => esc_html_x('2 Columns', 'backend', 'orphan'),
					3 => esc_html_x('3 Columns', 'backend', 'orphan'),
					4 => esc_html_x('4 Columns', 'backend', 'orphan')
				)
			));


			$this->wp_customize->add_setting('orphan_woocommerce_limit', array(
				'default' => 12,
				'sanitize_callback' => 'esc_attr'
			));
			$this->wp_customize->add_control('orphan_woocommerce_limit', array(
				'label'       => esc_html_x('Items per Shop Page: ', 'backend', 'orphan'),
				'description' => esc_html_x('Enter how many items you want to show on Shop pages & Categorie Pages before Pagination shows up (Default: 12)', 'backend', 'orphan'),
				'section'     => 'woocommerce',
				'settings'    => 'orphan_woocommerce_limit'
			));

			$this->wp_customize->add_setting('orphan_woocommerce_sort', array(
				'default' => 1,
				'sanitize_callback' => 'orphan_sanitize_checkbox'
			));
			$this->wp_customize->add_control('orphan_woocommerce_sort', array(
				'label'       => esc_html_x('Shop Sort', 'backend', 'orphan'),
				'description' => esc_html_x('(Enable / Disable sort-by function on Shop Pages)', 'backend', 'orphan'),
				'section'     => 'woocommerce',
				'settings'    => 'orphan_woocommerce_sort',
				'type'        => 'checkbox'
			));


			$this->wp_customize->add_setting('orphan_woocommerce_result_count', array(
				'default' => 1,
				'sanitize_callback' => 'orphan_sanitize_checkbox'
			));
			$this->wp_customize->add_control('orphan_woocommerce_result_count', array(
				'label'       => esc_html_x('Shop Result Count', 'backend', 'orphan'),
				'description' => esc_html_x('(Enable / Disable Result Count on Shop Pages)', 'backend', 'orphan'),
				'section'     => 'woocommerce',
				'settings'    => 'orphan_woocommerce_result_count',
				'type'        => 'checkbox'
			));

			$this->wp_customize->add_setting('orphan_woocommerce_cart_button', array(
				'default' => 1,
				'sanitize_callback' => 'orphan_sanitize_checkbox'
			));
			$this->wp_customize->add_control('orphan_woocommerce_cart_button', array(
				'label'       => esc_html_x('Add to Cart Button', 'backend', 'orphan'),
				'description' => esc_html_x('(Enable / Disable "Add to Cart"-Button on Shop Pages)', 'backend', 'orphan'),
				'section'     => 'woocommerce',
				'settings'    => 'orphan_woocommerce_cart_button',
				'type'        => 'checkbox'
			));

			$this->wp_customize->add_setting('orphan_woocommerce_upsells', array(
				'default' => 0,
				'sanitize_callback' => 'orphan_sanitize_checkbox'
			));
			$this->wp_customize->add_control('orphan_woocommerce_upsells', array(
				'label'       => esc_html_x('Upsells Products', 'backend', 'orphan'),
				'description' => esc_html_x('(Enable / Disable to show upsells Products on Product Item Details)', 'backend', 'orphan'),
				'section'     => 'woocommerce',
				'settings'    => 'orphan_woocommerce_upsells',
				'type'        => 'checkbox'
			));
			$this->wp_customize->add_setting('orphan_woocommerce_related', array(
				'default' => 1,
				'sanitize_callback' => 'orphan_sanitize_checkbox'
			));
			$this->wp_customize->add_control('orphan_woocommerce_related', array(
				'label'       => esc_html_x('Related Products', 'backend', 'orphan'),
				'description' => esc_html_x('(Enable / Disable to show related Products on Product Item Details)', 'backend', 'orphan'),
				'section'     => 'woocommerce',
				'settings'    => 'orphan_woocommerce_related',
				'type'        => 'checkbox'
			));
		}

		// Add header background color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'dropdown_background_color', array(
			'default'           => '#ffffff',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'background-color' => array(
					'.uk-navbar-dropbar',
					'.uk-navbar-dropdown:not(.uk-navbar-dropdown-dropbar)',
					'.tm-header .uk-navbar-dropdown:not(.uk-navbar-dropdown-dropbar) .sub-dropdown>ul',
				),
			)
		)));

		// Add header background color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'dropdown_background_color', array(
			'label'       => esc_html_x( 'Dropdown Background Color', 'backend', 'orphan' ),
			'section'     => 'colors_menu',
		)));


		// Add header background color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'dropdown_link_color', array(
			'default'           => '#999999',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'color' => array(
					'.uk-navbar-dropdown-nav>li>a',
					'.uk-nav li>a',
					'.uk-navbar-dropdown-nav .uk-nav-sub a',
				),
			)
		)));

		// Add header background color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'dropdown_link_color', array(
			'label'       => esc_html_x( 'Dropdown Link Color', 'backend', 'orphan' ),
			'section'     => 'colors_menu',
		)));


		// Add header background color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'dropdown_link_hover_color', array(
			'default'           => '#666666',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'color' => array(
					'.uk-navbar-dropdown-nav>li>a:hover', 
					'.uk-navbar-dropdown-nav>li>a:focus',
					'.uk-navbar-dropdown-nav .uk-nav-sub a:hover', 
					'.uk-navbar-dropdown-nav .uk-nav-sub a:focus',
					'.uk-navbar-dropdown-nav>li.uk-active>a',
					'.tm-header .uk-navbar-dropdown .sub-dropdown>ul li.uk-active a',
				),
			)
		)));

		// Add header background color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'dropdown_link_hover_color', array(
			'label'       => esc_html_x( 'Dropdown Link Hover Color', 'backend', 'orphan' ),
			'section'     => 'colors_menu',
		)));	


		// Bottom bg style setting
		$this->wp_customize->add_setting('orphan_bottom_bg_style', array(
			'default' => 'secondary',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_bottom_bg_style', array(
			'label'    => esc_html_x('Background Style', 'backend', 'orphan'),
			'section'  => 'footer',
			'settings' => 'orphan_bottom_bg_style', 
			'type'     => 'select',
			'choices'  => array(
				'default'   => esc_html_x('Default', 'backend', 'orphan'),
				'muted'     => esc_html_x('Muted', 'backend', 'orphan'),
				'primary'   => esc_html_x('Primary', 'backend', 'orphan'),
				'secondary' => esc_html_x('Secondary', 'backend', 'orphan'),
				'image'     => esc_html_x('Image', 'backend', 'orphan'),
				'custom'    => esc_html_x('Custom Color', 'backend', 'orphan'),
			)
		));

		// Add footer background color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'footer_background_color', array(
			'default'           => '#222222',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'background-color' => array(
					'.tm-bottom.uk-section-custom',
				),
				'border-color|lighten(5)' => array(
					'.tm-bottom.uk-section-custom .uk-grid-divider > :not(.uk-first-column)::before',
					'.tm-bottom.uk-section-custom hr', 
					'.tm-bottom.uk-section-custom .uk-hr',
					'.tm-bottom.uk-section-custom .uk-grid-divider.uk-grid-stack>.uk-grid-margin::before',
				),
				'background-color|lighten(2)' => array(
					'.tm-bottom.uk-section-custom .widget_tag_cloud a',
				),
			)
		)));

		// Add footer background color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'footer_background_color', array(
			'label'           => esc_html_x( 'Custom Background Color', 'backend', 'orphan' ),
			'section'         => 'footer',
			'active_callback' => 'orphan_bottom_bg_custom_color_check',
		)));


		$this->wp_customize->add_setting( 'orphan_bottom_bg_img' , array(
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control( new WP_Customize_Image_Control( $this->wp_customize, 'orphan_bottom_bg_img', array(
		    'label'    => esc_html_x( 'Background Image', 'backend', 'orphan' ),
		    'section'  => 'footer',
		    'settings' => 'orphan_bottom_bg_img',
		    'active_callback' => 'orphan_bottom_bg_style_check',
		)));


		$this->wp_customize->add_setting('orphan_bottom_bg_img_position', array(
			'default' => '',
			'sanitize_callback' => 'orphan_sanitize_choices'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_bottom_bg_img_position', array(
			'label'    => esc_html_x('Background Position', 'backend', 'orphan'),
			'description' => esc_html_x('Set the initial background position, relative to the section layer.', 'backend', 'orphan'),
			'section'  => 'footer',
			'settings' => 'orphan_bottom_bg_img_position', 
			'type'     => 'select',
			'choices'  => array(
				'top-left'      => esc_html_x('Top Left', 'backend', 'orphan'),
				'top-center'    => esc_html_x('Top Center', 'backend', 'orphan'),
				'top-right'     => esc_html_x('Top Right', 'backend', 'orphan'),
				'center-left'   => esc_html_x('Center Left', 'backend', 'orphan'),
				''              => esc_html_x('Center Center', 'backend', 'orphan'),
				'center-right'  => esc_html_x('Center Right', 'backend', 'orphan'),
				'bottom-left'   => esc_html_x('Bottom Left', 'backend', 'orphan'),
				'bottom-center' => esc_html_x('Bottom Center', 'backend', 'orphan'),
				'bottom-right'  => esc_html_x('Bottom Right', 'backend', 'orphan'),
			),
			'active_callback' => 'orphan_bottom_bg_img_check',
		)));

		$this->wp_customize->add_setting('orphan_bottom_bg_img_fixed', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control('orphan_bottom_bg_img_fixed', array(
			'label'           => esc_html_x('Fix the background with regard to the viewport.', 'backend', 'orphan'),
			'section'         => 'footer',
			'settings'        => 'orphan_bottom_bg_img_fixed',
			'type'            => 'checkbox',
			'active_callback' => 'orphan_bottom_bg_img_check',
		));

		$this->wp_customize->add_setting('orphan_bottom_bg_img_visibility', array(
			'default' => 'm',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_bottom_bg_img_visibility', array(
			'label'       => esc_html_x('Background Visibility', 'backend', 'orphan'),
			'description' => esc_html_x('Display the image only on this device width and larger.', 'backend', 'orphan'),
			'section'     => 'footer',
			'settings'    => 'orphan_bottom_bg_img_visibility', 
			'type'        => 'select',
			'choices'     => array(
				's'  => esc_html_x('Small (Phone Landscape)', 'backend', 'orphan'),
				'm'  => esc_html_x('Medium (Tablet Landscape)', 'backend', 'orphan'),
				'l'  => esc_html_x('Large (Desktop)', 'backend', 'orphan'),
				'xl' => esc_html_x('X-Large (Large Screens)', 'backend', 'orphan'),
			),
			'active_callback' => 'orphan_bottom_bg_img_check',
		));


		$this->wp_customize->add_setting('orphan_bottom_txt_style', array(
			'default'           => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_bottom_txt_style', array(
			'label'    => esc_html_x('Text Color', 'backend', 'orphan'),
			'section'  => 'footer',
			'settings' => 'orphan_bottom_txt_style', 
			'type'     => 'select',
			'choices'  => array(
				0        => esc_html_x('Default', 'backend', 'orphan'),
				'light'  => esc_html_x('Light', 'backend', 'orphan'),
				'dark'   => esc_html_x('Dark', 'backend', 'orphan'),
				'custom' => esc_html_x('Custom', 'backend', 'orphan'),
			)
		));

		// Add footer text color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'footer_text_color', array(
			'default'           => '#a5a5a5',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'color' => array(
					'.tm-bottom.uk-section-custom',
					'.tm-bottom a', 
					'.tm-bottom .uk-link', 
					'.tm-bottom .uk-text-primary', 
					'.tm-bottom .uk-alert-primary',
				),
				'color|lighten(30)' => array(
					'.tm-bottom.uk-section-custom .uk-card-title',
					'.tm-bottom.uk-section-custom h3',
					'.tm-bottom a:hover', 
					'.tm-bottom .uk-link:hover', 
					'.tm-bottom .uk-text-primary:hover', 
					'.tm-bottom .uk-alert-primary:hover',
				),
				'color|darken(5)' => array(
					'.tm-bottom.uk-section-custom .widget_tag_cloud a',
				),
				'border-color' => array(
					'.tm-bottom.uk-section-custom .uk-button-default',
				),
			)
		)));

		// Add footer text color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'footer_text_color', array(
			'label'       => esc_html_x( 'Custom Text Color', 'backend', 'orphan' ),
			'section'     => 'footer',
			'active_callback' => 'orphan_bottom_txt_custom_color_check'
		)));


		$this->wp_customize->add_setting('orphan_bottom_width', array(
			'default' => 'default',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_bottom_width', array(
			'label'    => esc_html_x('Width', 'backend', 'orphan'),
			'section'  => 'footer',
			'settings' => 'orphan_bottom_width', 
			'type'     => 'select',
			'choices'  => array(
				'default' => esc_html_x('Default', 'backend', 'orphan'),
				'small'   => esc_html_x('Small', 'backend', 'orphan'),
				'large'   => esc_html_x('Large', 'backend', 'orphan'),
				'expand'  => esc_html_x('Expand', 'backend', 'orphan'),
				0        => esc_html_x('Full', 'backend', 'orphan'),
			)
		));


		$this->wp_customize->add_setting('orphan_bottom_padding', array(
			'default' => 'medium',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_bottom_padding', array(
			'label'    => esc_html_x('Padding', 'backend', 'orphan'),
			'section'  => 'footer',
			'settings' => 'orphan_bottom_padding', 
			'type'     => 'select',
			'choices'  => array(
				'medium' => esc_html_x('Default', 'backend', 'orphan'),
				'small'  => esc_html_x('Small', 'backend', 'orphan'),
				'large'  => esc_html_x('Large', 'backend', 'orphan'),
				'none'   => esc_html_x('None', 'backend', 'orphan'),
			)
		));

		$this->wp_customize->add_setting('orphan_bottom_gutter', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_bottom_gutter', array(
			'label'    => esc_html_x('Gutter', 'backend', 'orphan'),
			'section'  => 'footer',
			'settings' => 'orphan_bottom_gutter', 
			'type'     => 'select',
			'choices'  => array(
				'small'    => esc_html_x('Small', 'backend', 'orphan'),
				'medium'   => esc_html_x('Medium', 'backend', 'orphan'),
				0          => esc_html_x('Default', 'backend', 'orphan'),
				'large'    => esc_html_x('Large', 'backend', 'orphan'),
				'collapse' => esc_html_x('Collapse', 'backend', 'orphan'),
			)
		));



		$this->wp_customize->add_setting('orphan_bottom_column_divider', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control('orphan_bottom_column_divider', array(
			'label'           => esc_html_x('Display dividers between grid cells', 'backend', 'orphan'),
			'description'     => esc_html_x('(Set the grid gutter width and display dividers between grid cells.)', 'backend', 'orphan'),
			'section'         => 'footer',
			'settings'        => 'orphan_bottom_column_divider',
			'active_callback' => 'orphan_bottom_gutter_collapse_check',
			'type'            => 'checkbox'
		));

		$this->wp_customize->add_setting('orphan_bottom_vertical_align', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control('orphan_bottom_vertical_align', array(
			'label'       => esc_html_x('Vertically center grid cells.', 'backend', 'orphan'),
			'section'     => 'footer',
			'settings'    => 'orphan_bottom_vertical_align',
			'type'        => 'checkbox'
		));


		$this->wp_customize->add_setting('orphan_bottom_match_height', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control('orphan_bottom_match_height', array(
			'label'       => esc_html_x('Stretch the panel to match the height of the grid cell.', 'backend', 'orphan'),
			'section'     => 'footer',
			'settings'    => 'orphan_bottom_match_height',
			'type'        => 'checkbox'
		));

		$this->wp_customize->add_setting('orphan_bottom_breakpoint', array(
			'default' => 'm',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_bottom_breakpoint', array(
			'label'       => esc_html_x('Breakpoint', 'backend', 'orphan'),
			'description' => esc_html_x('Set the breakpoint from which grid cells will stack.', 'backend', 'orphan'),
			'section'     => 'footer',
			'settings'    => 'orphan_bottom_breakpoint', 
			'type'        => 'select',
			'choices'     => array(
				's'  => esc_html_x('Small (Phone Landscape)', 'backend', 'orphan'),
				'm'  => esc_html_x('Medium (Tablet Landscape)', 'backend', 'orphan'),
				'l'  => esc_html_x('Large (Desktop)', 'backend', 'orphan'),
				'xl' => esc_html_x('X-Large (Large Screens)', 'backend', 'orphan'),
			)
		));

		// Copyright Section
		$this->wp_customize->add_section('copyright', array(
			'title' => esc_html_x('Copyright', 'backend', 'orphan'),
			'description' => esc_html_x( 'Copyright section settings here.', 'backend', 'orphan' ),
			'priority' => 142
		));


		$this->wp_customize->add_setting('orphan_copyright_bg_style', array(
			'default' => 'default',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_copyright_bg_style', array(
			'label'    => esc_html_x('Background Style', 'backend', 'orphan'),
			'section'  => 'copyright',
			'settings' => 'orphan_copyright_bg_style', 
			'type'     => 'select',
			'choices'  => array(
				'default'   => esc_html_x('Default', 'backend', 'orphan'),
				'muted'     => esc_html_x('Muted', 'backend', 'orphan'),
				'primary'   => esc_html_x('Primary', 'backend', 'orphan'),
				'secondary' => esc_html_x('Secondary', 'backend', 'orphan'),
				'image'     => esc_html_x('Image', 'backend', 'orphan'),
				//'video'     => esc_html_x('Video', 'backend', 'orphan'),
			)
		));


		$this->wp_customize->add_setting( 'orphan_copyright_bg_img' , array(
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control( new WP_Customize_Image_Control( $this->wp_customize, 'orphan_copyright_bg_img', array(
		    'label'    => esc_html_x( 'Background Image', 'backend', 'orphan' ),
		    'section'  => 'copyright',
		    'settings' => 'orphan_copyright_bg_img'
		)));


		$this->wp_customize->add_setting('orphan_copyright_txt_style', array(
			'default'           => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_copyright_txt_style', array(
			'label'    => esc_html_x('Text Color', 'backend', 'orphan'),
			'section'  => 'copyright',
			'settings' => 'orphan_copyright_txt_style', 
			'type'     => 'select',
			'choices'  => array(
				0       => esc_html_x('Default', 'backend', 'orphan'),
				'light' => esc_html_x('Light', 'backend', 'orphan'),
				'dark'  => esc_html_x('Dark', 'backend', 'orphan'),
			)
		));

		$this->wp_customize->add_setting('orphan_copyright_width', array(
			'default' => 'default',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_copyright_width', array(
			'label'    => esc_html_x('Width', 'backend', 'orphan'),
			'section'  => 'copyright',
			'settings' => 'orphan_copyright_width', 
			'type'     => 'select',
			'choices'  => array(
				'default' => esc_html_x('Default', 'backend', 'orphan'),
				'small'   => esc_html_x('Small', 'backend', 'orphan'),
				'large'   => esc_html_x('Large', 'backend', 'orphan'),
				'expand'  => esc_html_x('Expand', 'backend', 'orphan'),
				0        => esc_html_x('Full', 'backend', 'orphan'),
			)
		));


		$this->wp_customize->add_setting('orphan_copyright_padding', array(
			'default' => 'small',
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_copyright_padding', array(
			'label'    => esc_html_x('Padding', 'backend', 'orphan'),
			'section'  => 'copyright',
			'settings' => 'orphan_copyright_padding', 
			'type'     => 'select',
			'choices'  => array(
				'small'  => esc_html_x('Small', 'backend', 'orphan'),
				'medium' => esc_html_x('Medium', 'backend', 'orphan'),
				'large'  => esc_html_x('Large', 'backend', 'orphan'),
				'none'   => esc_html_x('None', 'backend', 'orphan'),
			)
		));


		/*
		 * "go to top" link
		 */
		$this->wp_customize->add_setting('orphan_top_link', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control($this->wp_customize, 'orphan_top_link',
	        array(
				'label'    => esc_html_x('Disable "Go to top" link', 'backend', 'orphan'),
				'section'  => 'copyright',
				'settings' => 'orphan_top_link',
				'type'     => 'checkbox'
	        )
		));

		$this->wp_customize->add_setting('orphan_copyright_text_custom_show', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control($this->wp_customize, 'orphan_copyright_text_custom_show',
	        array(
				'label'    => esc_html_x('Show Custom Copyright Text', 'backend', 'orphan'),
				'section'  => 'copyright',
				'settings' => 'orphan_copyright_text_custom_show',
				'type'     => 'checkbox',
	        )
		));
		
		//copyright Content
		$this->wp_customize->add_setting('orphan_copyright_text_custom', array(
			'default'           => 'Theme Designed by <a href="'.esc_url( esc_html_x( 'https://www.bdthemes.com', 'backend', 'orphan')).' ">BdThemes Ltd</a>',
			'sanitize_callback' => 'orphan_sanitize_textarea'
		));
		$this->wp_customize->add_control( new orphan_Customize_Textarea_Control( $this->wp_customize, 'orphan_copyright_text_custom', array(
			'label'           => esc_html_x('Copyright Text', 'backend', 'orphan'),
			'section'         => 'copyright',
			'settings'        => 'orphan_copyright_text_custom',
			'active_callback' => 'orphan_copyright_text_custom_show_check',
			'type'            => 'textarea',
		)));


		//Cookie bar section and settings
		$this->wp_customize->add_section('cookie', array(
			'title' => esc_html_x('Cookie Bar', 'backend', 'orphan'),
			'description' => esc_html_x( 'Show cookie accept notification on your website.', 'backend', 'orphan' ),
			'priority' => 150
		));


		$this->wp_customize->add_setting('orphan_cookie', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_cookie', array(
			'label'    => esc_html_x('Show Cookie Notification', 'backend', 'orphan'),
			'section'  => 'cookie',
			'settings' => 'orphan_cookie', 
			'type'     => 'select',
			'choices'  => array(
				1  => esc_html_x('Yes please!', 'backend', 'orphan'),
				0 => esc_html_x('No Need', 'backend', 'orphan'),
			)
		));


		// Add cookie background color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'orphan_cookie_background', array(
			'default'           => '#ffffff',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'background-color' => array(
					'#cookie-bar',
				),
			)
		)));

		// Add cookie background color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'orphan_cookie_background', array(
			'label'           => esc_html_x( 'Background Color', 'backend', 'orphan' ),
			'section'         => 'cookie',
			//'active_callback' => 'orphan_bottom_bg_custom_color_check',
		)));

		// Add cookie text color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'orphan_cookie_text_color', array(
			'default'           => '#666666',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'color' => array(
					'#cookie-bar',
				),
			)
		)));

		// Add cookie text color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'orphan_cookie_text_color', array(
			'label'           => esc_html_x( 'Text Color', 'backend', 'orphan' ),
			'section'         => 'cookie',
			//'active_callback' => 'orphan_bottom_bg_custom_color_check',
		)));

		$this->wp_customize->add_setting('orphan_cookie_message', array(
			'default' => esc_html__( 'We use cookies to ensure that we give you the best experience on our website.', 'orphan' ),
			'sanitize_callback' => 'orphan_sanitize_text'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_cookie_message', array(
			'label'       => esc_html_x('Message', 'backend', 'orphan'),
			'description' => esc_html_x('Set cookie message from here.', 'backend', 'orphan'),
			'section'     => 'cookie',
			'settings'    => 'orphan_cookie_message', 
			'type'        => 'text',
		)));

		$this->wp_customize->add_setting('orphan_cookie_expire_days', array(
			'default' => 365,
			'sanitize_callback' => 'orphan_sanitize_text'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_cookie_expire_days', array(
			'label'       => esc_html_x('Expire Days', 'backend', 'orphan'),
			'description' => esc_html_x('Set how many days to keep the cookies', 'backend', 'orphan'),
			'section'     => 'cookie',
			'settings'    => 'orphan_cookie_expire_days', 
			'type'        => 'text',
		)));


		$this->wp_customize->add_setting('orphan_cookie_accept_button', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_cookie_accept_button', array(
			'label'       => esc_html_x('Accept Button', 'backend', 'orphan'),
			'description' => esc_html_x('(Show accept button in cookie message area)', 'backend', 'orphan'),
			'section'     => 'cookie',
			'settings'    => 'orphan_cookie_accept_button', 
			'type'        => 'checkbox',
		)));

		$this->wp_customize->add_setting('orphan_cookie_decline_button', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_cookie_decline_button', array(
			'label'       => esc_html_x('Decline Button', 'backend', 'orphan'),
			'description' => esc_html_x('(Show decline button in cookie message area)', 'backend', 'orphan'),
			'section'     => 'cookie',
			'settings'    => 'orphan_cookie_decline_button', 
			'type'        => 'checkbox',
		)));


		$this->wp_customize->add_setting('orphan_cookie_policy_button', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_cookie_policy_button', array(
			'label'       => esc_html_x('Policy Button', 'backend', 'orphan'),
			'description' => esc_html_x('(Show policy button in cookie message area)', 'backend', 'orphan'),
			'section'     => 'cookie',
			'settings'    => 'orphan_cookie_policy_button', 
			'type'        => 'checkbox',
		)));

		$this->wp_customize->add_setting('orphan_cookie_policy_url', array(
			'default' => '/privacy-policy/',
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_cookie_policy_url', array(
			'label'           => esc_html_x('Policy Page URL', 'backend', 'orphan'),
			'description'     => esc_html_x('Set how many days to keep the cookies', 'backend', 'orphan'),
			'section'         => 'cookie',
			'settings'        => 'orphan_cookie_policy_url', 
			'type'            => 'text',
			'active_callback' => 'orphan_cookie_policy_button_check',
		)));

		$this->wp_customize->add_setting('orphan_cookie_position', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_cookie_position', array(
			'label'       => esc_html_x('Show Message at Button', 'backend', 'orphan'),
			'section'     => 'cookie',
			'settings'    => 'orphan_cookie_position', 
			'type'        => 'checkbox',
		)));


		$this->wp_customize->add_setting('orphan_cookie_debug', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_checkbox'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_cookie_debug', array(
			'label'       => esc_html_x('Debug Mode', 'backend', 'orphan'),
			'description' => esc_html_x('(If you checked so cookie will not hide even you click accept button.)', 'backend', 'orphan'),
			'section'     => 'cookie',
			'settings'    => 'orphan_cookie_debug', 
			'type'        => 'checkbox',
		)));



		//Preloader section and settings
		$this->wp_customize->add_section('preloader', array(
			'title' => esc_html_x('Preloader', 'backend', 'orphan'),
			'description' => esc_html_x( 'Show preloader for pre-loading your website.', 'backend', 'orphan' ),
			'priority' => 150
		));


		$this->wp_customize->add_setting('orphan_preloader', array(
			'default' => 0,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_preloader', array(
			'label'    => esc_html_x('Show Preloader', 'backend', 'orphan'),
			'section'  => 'preloader',
			'settings' => 'orphan_preloader', 
			'type'     => 'select',
			'choices'  => array(
				1  => esc_html_x('Yes please!', 'backend', 'orphan'),
				0 => esc_html_x('No Need', 'backend', 'orphan'),
			)
		));
		

		$this->wp_customize->add_setting('orphan_preloader_logo', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_preloader_logo', array(
			'label'    => esc_html_x('Show Logo', 'backend', 'orphan'),
			'section'  => 'preloader',
			'settings' => 'orphan_preloader_logo', 
			'type'     => 'select',
			'choices'  => array(
				1        => esc_html_x('Yes please!', 'backend', 'orphan'),
				0        => esc_html_x('No Need', 'backend', 'orphan'),
				'custom' => esc_html_x('Custom Logo', 'backend', 'orphan'),
			)
		));

		$this->wp_customize->add_setting( 'orphan_preloader_custom_logo' , array(
			'sanitize_callback' => 'esc_url'
		));
		$this->wp_customize->add_control( new WP_Customize_Image_Control( $this->wp_customize, 'orphan_preloader_custom_logo', array(
			'label'           => esc_html_x( 'Logo', 'backend', 'orphan' ),
			'section'         => 'preloader',
			'settings'        => 'orphan_preloader_custom_logo',
			'active_callback' => 'orphan_preloader_logo_check',
		)));


		$this->wp_customize->add_setting('orphan_preloader_text', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_preloader_text', array(
			'label'    => esc_html_x('Show Text', 'backend', 'orphan'),
			'section'  => 'preloader',
			'settings' => 'orphan_preloader_text', 
			'type'     => 'select',
			'choices'  => array(
				1        => esc_html_x('Yes please!', 'backend', 'orphan'),
				0        => esc_html_x('No Need', 'backend', 'orphan'),
				'custom' => esc_html_x('Custom Text', 'backend', 'orphan'),
			)
		));



		$this->wp_customize->add_setting('orphan_preloader_custom_text', array(
			'sanitize_callback' => 'orphan_sanitize_text'
		));
		$this->wp_customize->add_control(new WP_Customize_Control( $this->wp_customize, 'orphan_preloader_custom_text', array(
			'label'           => esc_html_x('Text', 'backend', 'orphan'),
			'section'         => 'preloader',
			'settings'        => 'orphan_preloader_custom_text', 
			'type'            => 'text',
			'active_callback' => 'orphan_preloader_text_check',
		)));

		// Add preloader background color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'orphan_preloader_background', array(
			'default'           => '#ffffff',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'background-color' => array(
					'.pg-loading-screen',
				),
			)
		)));


		// Add preloader background color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'orphan_preloader_background', array(
			'label'           => esc_html_x( 'Background Color', 'backend', 'orphan' ),
			'section'         => 'preloader',
			//'active_callback' => 'orphan_bottom_bg_custom_color_check',
		)));

		// Add preloader text color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'orphan_preloader_text_color', array(
			'default'           => '#666666',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'color' => array(
					'.pg-loading-screen',
				),
			)
		)));

		// Add preloader text color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'orphan_preloader_text_color', array(
			'label'           => esc_html_x( 'Text Color', 'backend', 'orphan' ),
			'section'         => 'preloader',
			//'active_callback' => 'orphan_bottom_bg_custom_color_check',
		)));

		$this->wp_customize->add_setting('orphan_preloader_animation', array(
			'default' => 1,
			'sanitize_callback' => 'orphan_sanitize_choices'
		) );
		$this->wp_customize->add_control('orphan_preloader_animation', array(
			'label'    => esc_html_x('Show Animation', 'backend', 'orphan'),
			'section'  => 'preloader',
			'settings' => 'orphan_preloader_animation', 
			'type'     => 'select',
			'choices'  => array(
				1        => esc_html_x('Yes please!', 'backend', 'orphan'),
				0        => esc_html_x('No Need', 'backend', 'orphan'),
			)
		));

		// Add preloader text color setting.
		$this->wp_customize->add_setting( new Orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'orphan_preloader_animation_color', array(
			'default'           => '#666666',
			'sanitize_callback' => 'sanitize_hex_color',
			'css_map' => array(
				'background-color' => array(
					'.bdt-spinner > div',
				),
			)
		)));

		// Add preloader text color control.
		$this->wp_customize->add_control( new WP_Customize_Color_Control( $this->wp_customize, 'orphan_preloader_animation_color', array(
			'label'           => esc_html_x( 'Animation Color', 'backend', 'orphan' ),
			'section'         => 'preloader',
			'active_callback' => 'orphan_preloader_animation_check',
		)));



		//typography section
		$this->wp_customize->add_panel('typography', array(
			'title' => esc_html_x('Typography', 'backend', 'orphan'),
			'priority' => 41
		));
		$this->wp_customize->add_section('typography_heading', array(
			'title' => esc_html_x('Heading', 'backend', 'orphan'),
			'panel' => 'typography',
		));
		$this->wp_customize->add_section('typography_menu', array(
			'title' => esc_html_x('Menu', 'backend', 'orphan'),
			'panel' => 'typography',
		));
		$this->wp_customize->add_section('typography_body', array(
			'title' => esc_html_x('Body', 'backend', 'orphan'),
			'panel' => 'typography',
		));
		$this->wp_customize->add_section('typography_global', array(
			'title' => esc_html_x('Global Settings', 'backend', 'orphan'),
			'panel' => 'typography',
		));

		//Add setting Heading font family settings
		$this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'base_heading_font_family', array(
			'default'           => 'Poppins',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		        'css_map' => array(
					'font-family' => array(
	                    'h1, .uk-h1, h2, .uk-h2, h3, .uk-h3, h4, .uk-h4, h5, .uk-h5, h6, .uk-h6',
						'.largeHeading',
						'.largeHeading h1',
						'.largeHeading h2',
						'.largeHeading h3',
						'.largeHeadingWhite',
						'.largeHeadingWhite h1',
	                    '.largeHeadingWhite h2',
	                    '.largeHeadingWhite h3',
	                    '.mediumHeading',
	                    '.mediumHeading h1',
	                    '.mediumHeading h2',
	                    '.mediumHeading h3',
	                    '.mediumHeadingThin',
	                    '.mediumHeadingThin h1',
	                    '.mediumHeadingThin h2',
	                    '.mediumHeadingThin h3',
	                    '.smallHeading',
	                    '.smallHeading h1',
	                    '.smallHeading h2',
	                    '.smallHeading h3',
	                    '.mediumHeadingWhite',
	                    '.mediumHeadingWhite h1',
	                    '.mediumHeadingWhite h2',
	                    '.mediumHeadingWhite h3',
	                    '.mediumHeadingBlack',
	                    '.mediumHeadingBlack h1',
	                    '.mediumHeadingBlack h2',
	                    '.mediumHeadingBlack h3',
	                    '.sup-style1 .mega-hovertitle',
	                    '.pace.pace-active .pace-progress:before',
	                    '.charitable-form-header',
	                    '.charitable-form-header h4',
					)
				)
		)));

		// Add Heading Font Control
		$this->wp_customize->add_control( new orphan_Google_Fonts_Control( $this->wp_customize, 'base_heading_font_family', array(
			'label'    => esc_html_x( 'Heading Font Family', 'backend', 'orphan' ),
			'section'  => 'typography_heading',
			'settings' => 'base_heading_font_family',
		)));
                
        $this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'base_heading_font_weight', array(
			'default'           => '600',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		        'css_map' => array(
					'font-weight' => array(
	                    'h1, .uk-h1, h2, .uk-h2, h3, .uk-h3, h4, .uk-h4, h5, .uk-h5, h6, .uk-h6',
						'.largeHeading',
						'.largeHeading h1',
						'.largeHeading h2',
						'.largeHeading h3',
						'.largeHeadingWhite',
						'.largeHeadingWhite h1',
	                    '.largeHeadingWhite h2',
	                    '.largeHeadingWhite h3',
	                    '.mediumHeading',
	                    '.mediumHeading h1',
	                    '.mediumHeading h2',
	                    '.mediumHeading h3',
	                    '.mediumHeadingThin',
	                    '.mediumHeadingThin h1',
	                    '.mediumHeadingThin h2',
	                    '.mediumHeadingThin h3',
	                    '.smallHeading',
	                    '.smallHeading h1',
	                    '.smallHeading h2',
	                    '.smallHeading h3',
	                    '.mediumHeadingWhite',
	                    '.mediumHeadingWhite h1',
	                    '.mediumHeadingWhite h2',
	                    '.mediumHeadingWhite h3',
	                    '.mediumHeadingBlack',
	                    '.mediumHeadingBlack h1',
	                    '.mediumHeadingBlack h2',
	                    '.mediumHeadingBlack h3',
	                    '.sup-style1 .mega-hovertitle',
	                    '.pace.pace-active .pace-progress:before',
	                    '.charitable-form-header',
	                    '.charitable-form-header h4',
					)
				)
		)));

		$this->wp_customize->add_control( 'base_heading_font_weight', array(
			'label'    => esc_html_x( 'Heading Font Weight', 'backend', 'orphan' ),
			'section'  => 'typography_heading',
			'settings' => 'base_heading_font_weight',
			'type'     => 'select',
			'choices'  => $this->orphan_font_weight(),
			'description' => esc_html_x( 'Important: Not all fonts support every font-weight.', 'backend', 'orphan' ),
		));


		$this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'secondary_heading_font_weight', array(
			'default'           => '300',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		)));

		$this->wp_customize->add_control( 'secondary_heading_font_weight', array(
			'label'    => esc_html_x( 'Extra Font Weight', 'backend', 'orphan' ),
			'section'  => 'typography_heading',
			'settings' => 'secondary_heading_font_weight',
			'type'     => 'text',
			'choices'  => $this->orphan_font_weight(),
			'description' => esc_html_x( 'If you need any extra font weight for styling, you should write separated them by comma. For example: 300,400i,600 etc. Important: Not all fonts support every font-weight.', 'backend', 'orphan' ),
		));



		//Add setting Heading font family settings
		$this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'base_body_font_family', array(
			'default'           => 'Open Sans',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		        'css_map' => array(
					'font-family' => array(
	                    'body'
					)
				)
		)));

		// Add Heading Font Control
		$this->wp_customize->add_control( new orphan_Google_Fonts_Control( $this->wp_customize, 'base_body_font_family', array(
			'label'    => esc_html_x( 'Body Font Family', 'backend', 'orphan' ),
			'section'  => 'typography_body',
			'settings' => 'base_body_font_family',
		)));
                
        $this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'base_body_font_weight', array(
			'default'           => '400',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		        'css_map' => array(
					'font-weight' => array(
	                    'body'
					)
				)
		)));

		$this->wp_customize->add_control( 'base_body_font_weight', array(
			'label'    => esc_html_x( 'Body Font Weight', 'backend', 'orphan' ),
			'section'  => 'typography_body',
			'settings' => 'base_body_font_weight',
			'type'     => 'select',
			'choices'  => $this->orphan_font_weight(),
			'description' => esc_html_x( 'Important: Not all fonts support every font-weight.', 'backend', 'orphan' ),
		));

		$this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'secondary_body_font_weight', array(
			'default'           => '300,400i',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		)));

		$this->wp_customize->add_control( 'secondary_body_font_weight', array(
			'label'    => esc_html_x( 'Extra Font Weight', 'backend', 'orphan' ),
			'section'  => 'typography_body',
			'settings' => 'secondary_body_font_weight',
			'type'     => 'text',
			'choices'  => $this->orphan_font_weight(),
			'description' => esc_html_x( 'If you need any extra font weight for styling, you should write separated them by comma. For example: 300,400i,600 etc. Important: Not all fonts support every font-weight.', 'backend', 'orphan' ),
		));

		
		//Add setting Heading font family settings
		$this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'base_menu_font_family', array(
			'default'           => 'Poppins',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		        'css_map' => array(
					'font-family' => array(
	                    '.uk-navbar-nav > li > a'
					)
				)
		)));

		// Add Menu Font Control
		$this->wp_customize->add_control( new orphan_Google_Fonts_Control( $this->wp_customize, 'base_menu_font_family', array(
			'label'    => esc_html_x( 'Menu Font Family', 'backend', 'orphan' ),
			'section'  => 'typography_menu',
			'settings' => 'base_menu_font_family',
		)));
                
        $this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'base_menu_font_weight', array(
			'default'           => '700',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		        'css_map' => array(
					'font-weight' => array(
	                    '.uk-navbar-nav > li > a'
					)
				)
		)));

		$this->wp_customize->add_control( 'base_menu_font_weight', array(
			'label'       => esc_html_x( 'Menu Font Weight', 'backend', 'orphan' ),
			'section'     => 'typography_menu',
			'settings'    => 'base_menu_font_weight',
			'type'        => 'select',
			'choices'     => $this->orphan_font_weight(),
			'description' => esc_html_x( 'Important: Not all fonts support every font-weight.', 'backend', 'orphan' ),
		));


		$this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'secondary_menu_font_weight', array(
			'default'           => '300',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		)));

		$this->wp_customize->add_control( 'secondary_menu_font_weight', array(
			'label'    => esc_html_x( 'Extra Font Weight', 'backend', 'orphan' ),
			'section'  => 'typography_menu',
			'settings' => 'secondary_menu_font_weight',
			'type'     => 'text',
			'choices'  => $this->orphan_font_weight(),
			'description' => esc_html_x( 'If you need any extra font weight for styling, you should write separated them by comma. For example: 300,400i,600 etc. Important: Not all fonts support every font-weight.', 'backend', 'orphan' ),
		));


		//Add setting Dropdown font family settings
		$this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'dropdown_font_family', array(
			'default'           => 'Open Sans',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		        'css_map' => array(
					'font-family' => array(
	                    '.uk-navbar-dropdown-nav>li>a, .uk-nav li>a', 
	                    '.uk-navbar-dropdown-nav .uk-nav-sub a'
					)
				)
		)));

		// Add Heading Font Control
		$this->wp_customize->add_control( new orphan_Google_Fonts_Control( $this->wp_customize, 'dropdown_font_family', array(
			'label'    => esc_html_x( 'Dropdown Font Family', 'backend', 'orphan' ),
			'section'  => 'typography_menu',
			'settings' => 'dropdown_font_family',
		)));
                
        $this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'dropdown_font_weight', array(
			'default'           => '400',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		        'css_map' => array(
					'font-weight' => array(
	                    '.uk-navbar-dropdown-nav>li>a, .uk-nav li>a', 
	                    '.uk-navbar-dropdown-nav .uk-nav-sub a'
					)
				)
		)));

		$this->wp_customize->add_control( 'dropdown_font_weight', array(
			'label'       => esc_html_x( 'Dropdown Font Weight', 'backend', 'orphan' ),
			'section'     => 'typography_menu',
			'settings'    => 'dropdown_font_weight',
			'type'        => 'select',
			'choices'     => $this->orphan_font_weight(),
			'description' => esc_html_x( 'Important: Not all fonts support every font-weight.', 'backend', 'orphan' ),
		));

		$this->wp_customize->add_setting( new orphan_Customizer_Dynamic_CSS( $this->wp_customize, 'secondary_dropdown_font_weight', array(
			'default'           => '600',
			'transport'         => 'postMessage',
			'sanitize_callback' => false,
		)));

		$this->wp_customize->add_control( 'secondary_dropdown_font_weight', array(
			'label'    => esc_html_x( 'Extra Font Weight', 'backend', 'orphan' ),
			'section'  => 'typography_menu',
			'settings' => 'secondary_dropdown_font_weight',
			'type'     => 'text',
			'choices'  => $this->orphan_font_weight(),
			'description' => esc_html_x( 'If you need any extra font weight for styling, you should write separated them by comma. For example: 300,400i,600 etc. Important: Not all fonts support every font-weight.', 'backend', 'orphan' ),
		));



		// Font subset
		$this->wp_customize->add_setting( 'google_font_subsets', array(
			'default' => 'latin',
			'sanitize_callback' => false,
		) );

		$this->wp_customize->add_control( new orphan_Customize_Multicheck_Control( $this->wp_customize, 'google_font_subsets', array(
			'label'    => esc_html_x( 'Font Subsets', 'backend', 'orphan' ),
			'section'  => 'typography_global',
			'settings' => 'google_font_subsets',
			'choices'  => array(
				'latin'        => 'latin',
				'latin-ext'    => 'latin-ext',
				'cyrillic'     => 'cyrillic',
				'cyrillic-ext' => 'cyrillic-ext',
				'greek'        => 'greek',
				'greek-ext'    => 'greek-ext',
				'vietnamese'   => 'vietnamese',
			),
		)));


		if ( isset( $this->wp_customize->selective_refresh ) ) {
			$this->wp_customize->selective_refresh->add_partial( 'blogname', array(
				'selector' => '.tm-header a.tm-logo-text',
				'container_inclusive' => false,
				'render_callback' => 'orphan_customize_partial_blogname',
			));
			$this->wp_customize->selective_refresh->add_partial( 'orphan_logo_default', array(
				'selector' => '.tm-header a.tm-logo-img',
				'container_inclusive' => false,
			));
			$this->wp_customize->selective_refresh->add_partial( 'blogdescription', array(
				'selector' => '.site-description',
				'container_inclusive' => false,
				'render_callback' => 'orphan_customize_partial_blogdescription',
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_show_copyright_text', array(
				'selector' => '.copyright-txt',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_copyright_text_custom_show', array(
				'selector' => '.copyright-txt',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_search_position', array(
				'selector' => '.tm-header .uk-search',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_search_position', array(
				'selector' => '.tm-header a.uk-search-icon',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_toolbar_social', array(
				'selector' => '.tm-toolbar .social-link',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_toolbar_left_custom', array(
				'selector' => '.tm-toolbar-l .custom-text',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_toolbar_right_custom', array(
				'selector' => '.tm-toolbar-r .custom-text',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_woocommerce_cart', array(
				'selector' => '.tm-cart-popup',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'nav_menu_locations[primary]', array(
				'selector' => '.tm-header .uk-navbar-nav',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'nav_menu_locations[toolbar]', array(
				'selector' => '.tm-toolbar .tm-toolbar-menu',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'nav_menu_locations[footer]', array(
				'selector' => '.tm-copyright .tm-copyright-menu',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_top_link', array(
				'selector' => '.tm-totop-scroller',
				'container_inclusive' => false,
			));


			$this->wp_customize->selective_refresh->add_partial( 'orphan_mobile_offcanvas_style', array(
				'selector' => '.tm-header-mobile .uk-navbar-toggle',
				'container_inclusive' => false,
			));

			$this->wp_customize->selective_refresh->add_partial( 'orphan_titlebar_layout', array(
				'selector' => '.tm-titlebar h1',
				'container_inclusive' => false,
			));
		}
	}


	public function orphan_toolbar_left_elements() {
		$toolbar_elements = array();
		$description      = get_bloginfo( 'description', 'display' );
		if (function_exists('icl_object_id')) {
			$toolbar_elements['wpml'] = esc_html_x( 'Language Switcher', 'backend', 'orphan' );
		}
		if ($description) {
			$toolbar_elements['tagline'] = esc_html_x('Tagline', 'backend', 'orphan');
		}
		if (has_nav_menu('toolbar')) {
			$toolbar_elements['menu'] = esc_html_x('Toolbar Menu', 'backend', 'orphan');
		}
		$toolbar_elements['social'] = esc_html_x('Social Link', 'backend', 'orphan');
		$toolbar_elements['custom-left'] = esc_html_x('Custom Text', 'backend', 'orphan');
		return $toolbar_elements;
	}

	public function orphan_toolbar_right_elements() {
		$toolbar_elements = array();
		$description      = get_bloginfo( 'description', 'display' );
		if (function_exists('icl_object_id')) {
			$toolbar_elements['wpml'] = esc_html_x( 'Language Switcher', 'backend', 'orphan' );
		}
		if ($description) {
			$toolbar_elements['tagline'] = esc_html_x('Tagline', 'backend', 'orphan');
		}
		if (has_nav_menu('toolbar')) {
			$toolbar_elements['menu'] = esc_html_x('Toolbar Menu', 'backend', 'orphan');
		}
		$toolbar_elements['social'] = esc_html_x('Social Link', 'backend', 'orphan');
		$toolbar_elements['custom-right'] = esc_html_x('Custom Text', 'backend', 'orphan');
		return $toolbar_elements;
	}

	public function orphan_font_weight() {
		$font_weight = array(
				''    => esc_html_x( 'Default', 'backend', 'orphan' ),
				'100' => esc_html_x( 'Extra Light: 100', 'backend', 'orphan' ),
				'200' => esc_html_x( 'Light: 200', 'backend', 'orphan' ),
				'300' => esc_html_x( 'Book: 300', 'backend', 'orphan' ),
				'400' => esc_html_x( 'Normal: 400', 'backend', 'orphan' ),
				'600' => esc_html_x( 'Semibold: 600', 'backend', 'orphan' ),
				'700' => esc_html_x( 'Bold: 700', 'backend', 'orphan' ),
				'800' => esc_html_x( 'Extra Bold: 800', 'backend', 'orphan' ),
			);
		return $font_weight;
	}

	/**
	 * Render the site title for the selective refresh partial.
	 *
	 * @since Orphan 1.0
	 * @see orphan_customize_register_colors()
	 *
	 * @return void
	 */
	public function orphan_customize_partial_blogname() {
		bloginfo( 'name' );
	}

	/**
	 * Render the site tagline for the selective refresh partial.
	 *
	 * @since Orphan 1.0
	 * @see orphan_customize_register_colors()
	 *
	 * @return void
	 */
	public function orphan_customize_partial_blogdescription() {
		bloginfo( 'description' );
	}

	/**
	 * Cache the rendered CSS after the settings are saved in the DB.
	 * This is purely a performance improvement.
	 *
	 * Used by hook: add_action( 'customize_save_after' , array( $this, 'cache_rendered_css' ) );
	 *
	 * @return void
	 */
	public function cache_rendered_css() {
 		set_theme_mod( 'cached_css', $this->render_css() );
 		set_theme_mod( 'google-font', json_encode($this->getGoogleFonts()) );
	}

	/**
	 * Get the dimensions of the logo image when the setting is saved
	 * This is purely a performance improvement.
	 *
	 * Used by hook: add_action( 'customize_save_logo_img' , array( $this, 'save_logo_dimensions' ), 10, 1 );
	 *
	 * @return void
	 */
	public static function save_logo_dimensions( $setting ) {
		$logo_width_height = '';
		$img_data          = getimagesize( esc_url( $setting->post_value() ) );

		if ( is_array( $img_data ) ) {
			$logo_width_height = $img_data[3];
		}

		set_theme_mod( 'logo_width_height', $logo_width_height );
	}

	/**
	 * Render the CSS from all the settings which are of type `Orphan_Customizer_Dynamic_CSS`
	 *
	 * @return string text/css
	 */
	public function render_css() {
		$out = '';
		foreach ( $this->get_dynamic_css_settings() as $setting ) {
			$out .= $setting->render_css();
		}

		return $out;
	}

	public function getGoogleFonts(){
		$out           = [];
		$default_fonts = orphan_default_fonts();

        foreach ( $this->get_dynamic_css_settings() as $key => $setting ) {                    
            $out1 = $setting->getGoogleFonts();
            if($out1 || @$default_fonts[$key]){
                $out[$key] = $out1 ? $out1 : @$default_fonts[$key];
            }
        } 
        return $out;
	}

        /**
	 * Get only the CSS settings of type `Orphan_Customizer_Dynamic_CSS`.
	 *
	 * @see is_dynamic_css_setting
	 * @return array
	 */
	public function get_dynamic_css_settings() {
		return array_filter( $this->wp_customize->settings(), array( $this, 'is_dynamic_css_setting' ) );
	}

	/**
	 * Helper conditional function for filtering the settings.
	 *
	 * @see
	 * @param  mixed  $setting
	 * @return boolean
	 */
	protected static function is_dynamic_css_setting( $setting ) {
		return is_a( $setting, 'Orphan_Customizer_Dynamic_CSS' );
	}

	/**
	 * Dynamically generate the JS for previewing the settings of type `Orphan_Customizer_Dynamic_CSS`.
	 *
	 * This function is better for the UX, since all the color changes are transported to the live
	 * preview frame using the 'postMessage' method. Since the JS is generated on-the-fly and we have a single
	 * entry point of entering settings along with related css properties and classes, we cannnot forget to
	 * include the setting in the customizer itself. Neat, man!
	 *
	 * @return string text/javascript
	 */
	public function customize_footer_js() {
		$settings = $this->get_dynamic_css_settings();

		ob_start();
		?>

			<script type="text/javascript">
				'use strict';
				//orphan customizer color live preview
				( function( $ ) {
				    var style = []
			
				<?php
					foreach ( $settings as $key_id => $setting ) :
				?>
					style['<?php echo esc_attr($key_id) ?>'] = '';
					wp.customize( '<?php echo esc_attr($key_id); ?>', function( value ) {
					   
						value.bind( function( newval ) {
						     style['<?php echo esc_attr($key_id) ?>'] = '';
						<?php
							foreach ( $setting->get_css_map() as $css_prop_raw => $css_selectors ) {
								
								extract( $setting->filter_css_property( $css_prop_raw ) );
                                if($lighten){
                                    echo 'newval = LightenDarkenColor(newval,'.$lighten.' ); ';
                                }
								// background image needs a little bit different treatment
								if ( 'background-image' === $css_prop ) {
									echo 'newval = "url(\'" + newval + "\')";' . PHP_EOL;
								}else if ( 'font-family' === $css_prop ) {
                                    echo 'WebFont.load({
                                        google: {
                                          families: [newval]
                                        }
                                    });newval = \'"\'+newval+\'"\';
                                        newval = newval.replace(",", \'","\'); ';
                                }
								printf( 'style["%1$s"]  += "%2$s{ %3$s: "+ newval + " }" %4$s ' .  '+"\r\n"; '."\r\n",$key_id, $setting->plain_selectors_for_all_groups( $css_prop_raw ), $css_prop, PHP_EOL);
							}
						?>
						   add_style(style); 	    
						});
						
					} );
					<?php
					foreach ($setting->get_css_map() as $css_prop_raw => $css_selectors) {
	                                      
					    extract($setting->filter_css_property($css_prop_raw));
						if($lighten){
							$value = $value;
						} else {
							$value = $setting->render_css_save();
						}
					   
					    if ( 'background-image' === $css_prop ) {
							$value = 'url(\''.$value.'\');';
					    }
					    printf('style["%1$s"]  += "%2$s{ %3$s: %5$s }" %4$s ' . '+"\r\n"; ' . "\r\n", $key_id, $setting->plain_selectors_for_all_groups($css_prop_raw),
						    $css_prop, PHP_EOL, $value);
					}
					?>
					add_style(style);
				<?php
					endforeach;
					?>
				    function add_style(style){
					    var str_style = '';
					    var key;
					    for(key in style){
							if(style[key]){
							    str_style += '/*' + key + "*/\r\n";
							    str_style += style[key] + "\r\n";
							}
					    }
					    $('#custome_live_preview').html(str_style)

				    }
	                                
                    function LightenDarkenColor(col, amt) {  
                        var usePound = false;
                        if (col[0] == "#") {
                            col = col.slice(1);
                            usePound = true;
                        }
                        var num = parseInt(col,16);
                        var r = (num >> 16) + amt;
                        if (r > 255) r = 255;
                        else if  (r < 0) r = 0;
                        var b = ((num >> 8) & 0x00FF) + amt;
                        if (b > 255) b = 255;
                        else if  (b < 0) b = 0;
                        var g = (num & 0x0000FF) + amt;
                        if (g > 255) g = 255;
                        else if (g < 0) g = 0;
                        return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
                    }
				} )( jQuery );
			</script>
		<?php
		echo ob_get_clean();
	}
	
    public function hook_custom_css() { ?>
    	<script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
		<style id='custome_live_preview'></style>
    	<?php
    }

}
<?php
/**
 * Class which handles the output of the WP customizer on the frontend.
 * Meaning that this stuff loads always, no matter if the global $wp_cutomize
 * variable is present or not.
 */
class orphan_Customize_Frontent {

	/**
	 * Add actions to load the right staff at the right places (header, footer).
	 */
	function __construct() {
		add_action( 'wp_enqueue_scripts' , array( $this, 'customizer_css' ), 20 );
		add_action( 'wp_enqueue_scripts' , array( $this, 'google_all_fonts' ), 20 );
		add_action( 'wp_head' , array( $this, 'head_output' ) );
	}

	/**
	* This will output the custom WordPress settings to the live theme's WP head.
	* Used by hook: 'wp_head'
	* @see add_action( 'wp_head' , array( $this, 'head_output' ) );
	*/
	public static function customizer_css() {
		// customizer settings
		$cached_css = get_theme_mod( 'cached_css', '' );

		ob_start();

		echo '/* WP Customizer start */' . PHP_EOL;
		echo apply_filters( 'orphan/cached_css', $cached_css );
		echo '/* WP Customizer end */';

		wp_add_inline_style( 'orphan-style', ob_get_clean() );
	}

    /**
	* This will output the google font settings to the live theme's WP head.
	*/
	public static function google_all_fonts() {
		$google_fonts         = json_decode(get_theme_mod( 'google-font'));
		$default_fonts        = orphan_default_fonts();
		$default_font_weights = orphan_default_font_weights();
                
       	if ($google_fonts) {
        	$all_fonts = $google_fonts;
       	} else {
        	$all_fonts = $default_fonts;
       	}
 		//added by sarwar
		$gfont               = array();
		$stored_google_fonts = orphan_google_fonts_array();

        foreach($all_fonts as $key => $font){
            // customizer settings
			$google_font          = $font; //get_theme_mod($key);

			$key_weight            = str_replace("font_family", "font_weight", $key);
			$base_font_weight      = isset($default_font_weights[$key_weight])?$default_font_weights[$key_weight]:"";
			$google_font_weight    = get_theme_mod( $key_weight,$base_font_weight);          
			$secondary_key_weight  = str_replace("base", "secondary", $key_weight);
			$secondary_font_weight = isset($default_font_weights[$secondary_key_weight])?$default_font_weights[$secondary_key_weight]:$google_font_weight;
            if(($secondary_font_weight = get_theme_mod( $secondary_key_weight,$secondary_font_weight)) && $secondary_font_weight != $google_font_weight){
                $google_font_weight .= ','.$secondary_font_weight;
            }

            if(in_array($google_font, $stored_google_fonts)){
	            $fweighr=explode(",", $google_font_weight);
	            if(!isset($gfont[$google_font])){
	            	$gfont[$google_font]=array_unique($fweighr);
	            	continue;
	            }
	            foreach ($fweighr as  $value) {
	            	if(!in_array($value,$gfont[$google_font])){
	            			$gfont[$google_font][]=$value;
	            	}
	            }
        	}
        }
        $fontfamily = "";
        foreach ($gfont as $key => $value) {
			$font_weight   = implode(",", $value);
			$font_weight   = ltrim($font_weight,",");
			$fontfamily .= "|".$key.":".$font_weight;
        }
        $fontfamily=ltrim($fontfamily,"|");
         
        if(count($gfont)>0){
         	// Subset
			$subset = get_theme_mod( 'google_font_subsets', 'latin' );
			$subset = $subset ? $subset : 'latin';
			$subset = '&subset='. $subset;
			$url    =  '//fonts.googleapis.com/css?family='. $fontfamily.$subset;
            wp_enqueue_style( 'orphan-google-fonts', $url, false, false, 'all' );
        }
	}
        

	/**
	* Outputs the code in head of the every page
	* Used by hook: add_action( 'wp_head' , array( $this, 'head_output' ) );
	*/
	public static function head_output() {

		// Theme favicon output, which will be phased out, because of WP core favicon integration
		$favicon = get_theme_mod( 'site_icon', get_template_directory_uri() . '/images/favicon.png' );
		if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
			if( ! empty( $favicon ) ) : ?>
				<link rel="shortcut icon" href="<?php echo esc_attr($favicon); ?>">
			<?php endif;
		} ?>

		<meta name="theme-color" content="<?php echo get_theme_mod( 'browser_header_color', '#f14b51' ); ?>">
		<meta name="msapplication-navbutton-color" content="<?php echo get_theme_mod( 'browser_header_color', '#f14b51' ); ?>">

		<link href="<?php echo esc_attr($favicon); ?>" sizes="152x152" rel="apple-touch-icon-precomposed">

		<?php
	}

}
<?php
function orphan_customize_register_titlebar($wp_customize) {
	//header section
	$wp_customize->add_section('header', array(
		'title' => esc_attr__('Titlebar', 'orphan'),
		'priority' => 31
	));

	$wp_customize->add_setting('orphan_global_header', array(
		'default' => 'title',
		'sanitize_callback' => 'orphan_sanitize_choices'
	));
	$wp_customize->add_control('orphan_global_header', array(
		'label'    => esc_attr__('Titlebar Layout', 'orphan'),
		'section'  => 'header',
		'settings' => 'orphan_global_header', 
		'type'     => 'select',
		'priority' => 1,
		'choices'  => array(
			'title'               => esc_attr__('Titlebar (Left Align)', 'orphan'),
			'featuredimagecenter' => esc_attr__('Titlebar (Center Align)', 'orphan'),
			'notitle'             => esc_attr__('No Titlebar', 'orphan')
		)
	));


	$wp_customize->add_setting('orphan_titlebar_style', array(
		'default' => 'titlebar-dark',
		'sanitize_callback' => 'orphan_sanitize_choices'
	));
	$wp_customize->add_control('orphan_titlebar_style', array(
		'label'    => esc_attr__('Titlebar Style', 'orphan'),
		'section'  => 'header',
		'settings' => 'orphan_titlebar_style', 
		'type'     => 'select',
		'priority' => 1,
		'choices'  => array(
			'titlebar-dark' => esc_attr__('Dark (for dark backgrounds)', 'orphan'),
			'titlebar-light' => esc_attr__('Light (for light backgrounds)', 'orphan')
		)
	));

	$wp_customize->add_setting( 'orphan_titlebar_bg_image' , array(
		'sanitize_callback' => 'esc_url'
	));
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'orphan_titlebar_bg_image', array(
		'priority' => 1,
	    'label'    => esc_attr__( 'Titlebar Background', 'orphan' ),
	    'section'  => 'header',
	    'settings' => 'orphan_titlebar_bg_image'
	)));

	$wp_customize->add_setting('orphan_blog_title', array(
		'default' => esc_attr__('Blog', 'orphan'),
		'sanitize_callback' => 'esc_attr'
	));
	$wp_customize->add_control('orphan_blog_title', array(
		'priority' => 2,
	    'label'    => esc_attr__('Blog Title: ', 'orphan'),
	    'section'  => 'header',
	    'settings' => 'orphan_blog_title'
	));

	if (class_exists('Woocommerce')){
		$wp_customize->add_setting('orphan_woocommerce_title', array(
			'default' => esc_attr__('Shop', 'orphan'),
			'sanitize_callback' => 'esc_attr'
		));
		$wp_customize->add_control('orphan_woocommerce_title', array(
			'priority' => 3,
		    'label'    => esc_attr__('WooCommerce Title: ', 'orphan'),
		    'section'  => 'header',
		    'settings' => 'orphan_woocommerce_title'
		));
	}
	
	$wp_customize->add_setting('orphan_right_element', array(
		'default' => 'back_button',
		'sanitize_callback' => 'orphan_sanitize_choices'
	));
	$wp_customize->add_control('orphan_right_element', array(
		'label'    => esc_attr__('Right Element', 'orphan'),
		'section'  => 'header',
		'settings' => 'orphan_right_element', 
		'type'     => 'select',
		'priority' => 4,
		'choices'  => array(
			0             => esc_attr__('Nothing', 'orphan'),
			'back_button' => esc_attr__('Back Button', 'orphan'),
			'breadcrumb'  => esc_attr__('Breadcrumb', 'orphan')
		)
	));

}

add_action('customize_register', 'orphan_customize_register_titlebar');
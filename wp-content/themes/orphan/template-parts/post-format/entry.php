<article id="post-<?php the_ID() ?>" <?php post_class('uk-article') ?> data-permalink="<?php the_permalink() ?>" typeof="Article">

    <?php get_template_part( 'template-parts/post-format/schema-meta' ); ?>

    <?php if (has_post_thumbnail()) : ?>
        <div class="uk-margin-large-bottom">
            <?php if(is_single()) : ?>
                <?php echo  the_post_thumbnail('orphan_blog', array('class' => 'uk-border-rounded'));  ?>
            <?php else : ?>
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                    <?php echo  the_post_thumbnail('orphan_blog', array('class' => 'uk-border-rounded'));  ?>
                </a>
            <?php endif; ?>           
        </div>
    <?php endif; ?>


    <div class="uk-margin-medium-bottom uk-container uk-container-small uk-text-center">
        <?php get_template_part( 'template-parts/post-format/title' ); ?>

        <?php if(get_theme_mod('orphan_blog_meta', 1)) :?>
        <?php get_template_part( 'template-parts/post-format/meta' ); ?>
        <?php endif; ?>
    </div>
    
    
    <div class="uk-container uk-container-small">
        <?php get_template_part( 'template-parts/post-format/content' ); ?>

        <?php get_template_part( 'template-parts/post-format/read-more' ); ?>
    </div>

</article>
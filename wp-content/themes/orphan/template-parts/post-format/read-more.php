<?php if(is_home() and get_theme_mod('orphan_blog_readmore', 1)) :?>

<p class="uk-text-center uk-margin-medium">
	<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="uk-button uk-button-text"><?php esc_html_e('Read More...', 'orphan'); ?></a>
</p>

<?php endif; ?>
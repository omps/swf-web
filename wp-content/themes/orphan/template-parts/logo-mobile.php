<?php
$logo  = get_theme_mod('orphan_logo_mobile');

?>

<a href="<?php echo esc_url(home_url('/')); ?>"<?php echo orphan_helper::attrs(['class' => 'uk-logo uk-navbar-item']) ?> itemprop="url">
    <?php if ($logo) : ?>
        <img src="<?php echo esc_url($logo); ?>" class="uk-responsive-height" itemprop="logo" alt="<?php bloginfo( 'name' );?>">
    <?php else : ?>
        <?php bloginfo( 'name' );?>
    <?php endif; ?>
</a>
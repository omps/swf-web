<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package orphan
 */


$orphan_show_rev_slider = get_post_meta( get_the_ID(), 'orphan_show_rev_slider', true );
$orphan_rev_slider = get_post_meta( get_the_ID(), 'orphan_rev_slider', true );

if(shortcode_exists("rev_slider") && ($orphan_show_rev_slider == 'yes') && !is_search()) : ?>

<div class="slider-wrapper" id="tmSlider">
	<div>
		<section class="tm-slider uk-child-width-expand@s" uk-grid>
			<div>
				<?php echo(do_shortcode('[rev_slider '.$orphan_rev_slider.']')); ?>
			</div>
		</section>
	</div>
</div>

<?php endif; ?>

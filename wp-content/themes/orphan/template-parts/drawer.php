<?php if (is_active_sidebar('drawer')) : ?>
<?php
	$class             = ['tm-drawer', 'uk-section'];
	$container_class   = [];
	$grid_class        = ['uk-grid'];
	$background_style  = get_theme_mod( 'orphan_drawer_bg_style', 'secondary' );
	$width             = get_theme_mod( 'orphan_drawer_width', 'default');
	$padding           = get_theme_mod( 'orphan_drawer_padding', 'small' );
	$text              = get_theme_mod( 'orphan_drawer_txt_style' );
	$breakpoint        = get_theme_mod( 'orphan_bottom_breakpoint', 'm' );

	$class[]           = ($background_style) ? 'uk-section-'.$background_style : '';
	$class[]           = ($text) ? 'uk-'.$text : '';
	if ($padding != 'none') {
		$class[]       = ($padding) ? 'uk-section-'.$padding : '';
	} elseif ($padding == 'none') {
		$class[]       = ($padding) ? 'uk-padding-remove-vertical' : '';
	}

	$container_class[] = ($width) ? 'uk-container uk-container-'.$width : '';
	$grid_class[]      = ($breakpoint) ? 'uk-child-width-expand@'.$breakpoint : '';
	$wrapper_bg = ($background_style) ? ' uk-background-'.$background_style : '';
?>

<div class="drawer-wrapper<?php echo esc_url($wrapper_bg); ?>">
	<div id="tm-drawer" <?php echo orphan_helper::attrs(['class' => $class]) ?> hidden>
		<div <?php echo orphan_helper::attrs(['class' => $container_class]) ?>>
			<div <?php echo orphan_helper::attrs(['class' => $grid_class]) ?> uk-grid>
				<?php dynamic_sidebar('drawer'); ?>
			</div>
		</div>
	</div>
	<a href="javascript:void(0);" class="drawer-toggle uk-position-top-right uk-margin-small-right" uk-toggle="target: #tm-drawer; animation: uk-animation-slide-top; queued: true"><span uk-icon="icon: chevron-down"></span></a>
</div>
<?php endif; ?>
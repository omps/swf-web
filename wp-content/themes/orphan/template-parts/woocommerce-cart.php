<?php if (class_exists('Woocommerce')) { 

	$cart = get_theme_mod('orphan_woocommerce_cart');

	if($cart !== 'no') { 
	global $woocommerce; 
	$orphan_wcrtl = (is_rtl()) ? 'left' : 'right';
	$offset = ( $cart == 'toolbar') ? 15 : 32;
	?>
	
	<div class="tm-cart-popup">
		<a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" id="shopping-btn" class="tm-shopping-cart" title="<?php esc_html_e('View Cart', 'orphan'); ?>">
			<span uk-icon="icon: cart"></span>
			<?php
				$product_bumber = $woocommerce->cart->cart_contents_count; 
				if ($cart == 'header') {
					if ( sizeof( $woocommerce->cart->cart_contents ) != 0 ) {
						echo '<span class="pcount">'.esc_html($product_bumber).'</span>';
					} 
				}
				if ($cart == 'toolbar') {
					echo '<div class="uk-hidden-small uk-display-inline">';
					if ( sizeof( $woocommerce->cart->cart_contents ) == 0 ) {
						esc_html_e('Cart is Empty', 'orphan');
					} else {
						echo sprintf( _n( '%s Item in cart', '%s Items in cart', $product_bumber, 'orphan' ), $product_bumber );
					}
					echo '</div>';
				} 
			?>
		</a>

		<?php if ( sizeof( $woocommerce->cart->cart_contents ) != 0 and !is_checkout() and !is_cart()) : ?>
			<div class="cart-dropdown" uk-drop="mode: hover; offset: <?php echo esc_attr($offset); ?>">
				<div class="uk-card uk-card-body uk-card-default">
					<?php if ( version_compare( WOOCOMMERCE_VERSION, "2.0.0" ) >= 0 ) { the_widget( 'WC_Widget_Cart', '' ); } ?>
				</div>
			</div>
		<?php endif; ?>

	</div>
	<?php }
} ?>
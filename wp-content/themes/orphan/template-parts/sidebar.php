<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package orphan
 */


$position = (get_post_meta( get_the_ID(), 'orphan_page_layout', true )) ? get_post_meta( get_the_ID(), 'orphan_page_layout', true ) : get_theme_mod( 'orphan_page_layout', 'sidebar-right' );

if($position == 'sidebar-left' || $position == 'sidebar-right') { ?>

	<aside<?php echo orphan_helper::sidebar(); ?>>
	    <?php get_sidebar(); ?>
	</aside> <!-- end aside -->

<?php }

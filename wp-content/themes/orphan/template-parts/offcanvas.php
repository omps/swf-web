<?php
	$offcanvas_style = get_theme_mod( 'orphan_mobile_offcanvas_style');
	if ( $offcanvas_style == 'modal') {
		$menu_class = 'uk-nav uk-nav-primary uk-nav-center uk-nav-parent-icon';
	} elseif ($offcanvas_style == 'offcanvas') {
		$menu_class = 'uk-nav uk-nav-default uk-nav-parent-icon';
	} else {
		$menu_class = 'uk-nav uk-nav-parent-icon';
	}

	if (get_theme_mod('orphan_offcanvas_search', 1) and $offcanvas_style != 'modal') {
		echo '<div class="uk-panel offcanvas-search"><div class="panel-content">';
			get_search_form();
			echo '<hr>';
		echo '</div></div>';
	}
?>

<?php 
	if(has_nav_menu('primary') and !has_nav_menu('offcanvas')) {
		wp_nav_menu( array(
			'theme_location' => 'primary',
			'container'      => false,
			'items_wrap'     => '<ul id="%1$s" class="%2$s" uk-nav>%3$s</ul>',
			'menu_id'        => 'nav-offcanvas',
			'menu_class'     => $menu_class,
			'echo'           => true,
			'before'         => '',
			'after'          => '',
			'link_before'    => '',
			'link_after'     => '',
			'depth'          => 0,
			)
		); 
	}
	elseif(has_nav_menu('offcanvas')) {
		wp_nav_menu( array(
			'theme_location' => 'offcanvas',
			'container'      => false,
			'items_wrap'     => '<ul id="%1$s" class="%2$s" uk-nav>%3$s</ul>',
			'menu_id'        => 'nav-offcanvas',
			'menu_class'     => $menu_class,
			'echo'           => true,
			'before'         => '',
			'after'          => '',
			'link_before'    => '',
			'link_after'     => '',
			'depth'          => 0,
			)
		); 
	}
	else {
		echo '<div class="uk-panel"><div class="panel-content"><div class="uk-alert uk-alert-warning uk-margin-remove-bottom"><strong>NO MENU ASSIGNED</strong> <span>Go To Appearance > <a class="uk-link" href="'.admin_url('/nav-menus.php').'">Menus</a> and create a Menu</span></div></div></div>';
	}



	if (is_active_sidebar('offcanvas') and $offcanvas_style != 'modal') {
		echo '<hr>';
		echo '<div class="offcanvas-widgets">';
		dynamic_sidebar('offcanvas');
		echo '</div>';
	}

?>
<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package orphan
 */

if(get_theme_mod('orphan_footer_widgets', 1) && get_post_meta( get_the_ID(), 'orphan_footer_widgets', true ) != 'hide') {

	$class             = ['tm-copyright', 'uk-section'];
	$container_class   = [];
	$grid_class        = ['uk-grid'];
	$background_style  = get_theme_mod( 'orphan_copyright_bg_style', 'secondary' );
	$width             = get_theme_mod( 'orphan_copyright_width');
	$padding           = get_theme_mod( 'orphan_copyright_padding', 'small' );
	$text              = get_theme_mod( 'orphan_copyright_txt_style' );
	$breakpoint        = get_theme_mod( 'orphan_bottom_breakpoint', 'm' );
	
	$class[]           = ($background_style) ? 'uk-section-'.$background_style : '';
	$class[]           = ($text) ? 'uk-'.$text : '';
	if ($padding != 'none') {
		$class[]       = ($padding) ? 'uk-section-'.$padding : '';
	} elseif ($padding == 'none') {
		$class[]       = ($padding) ? 'uk-padding-remove-vertical' : '';
	}
	
	$container_class[] = ($width) ? 'uk-container uk-container-'.$width : '';
	
	$grid_class[]      = ($breakpoint) ? 'uk-child-width-expand@'.$breakpoint : '';

	?>

	<div id="tmCopyright"<?php echo orphan_helper::attrs(['class' => $class]) ?>>
		<div<?php echo orphan_helper::attrs(['class' => $container_class]) ?>>
			<div<?php echo orphan_helper::attrs(['class' => $grid_class]) ?> uk-grid>
				<div class="uk-text-center">	
					<?php									 
					if (has_nav_menu('footer')) { echo wp_nav_menu( array( 'theme_location' => 'footer', 'container_class' => 'tm-copyright-menu uk-display-inline-block', 'menu_class' => 'uk-subnav uk-subnav-line uk-margin-small-bottom', 'depth' => 1 ) ); }
					
					if(get_theme_mod('orphan_copyright_text_custom_show')) : ?>
						<div class="copyright-txt"><?php echo wp_kses_post(get_theme_mod('orphan_copyright_text_custom')); ?></div>
					<?php else : ?>								
						<div class="copyright-txt">&copy; <?php esc_html_e('Copyright', 'orphan') ?> <?php echo esc_html(date("Y ")); ?> <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo( 'name' );?>"> <?php echo esc_html(bloginfo('name')); ?> </a></div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<?php 
}
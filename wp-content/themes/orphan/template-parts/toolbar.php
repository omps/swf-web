<?php 
$classes            = ['uk-container', 'uk-flex uk-flex-middle'];
$mb_toolbar         = (get_post_meta( get_the_ID(), 'orphan_toolbar', true ) != null) ? get_post_meta( get_the_ID(), 'orphan_toolbar', true ) : null;
$tm_toolbar         = (get_theme_mod( 'orphan_toolbar', 1)) ? 1 : 0;
$toolbar            = ($mb_toolbar != null ) ? $mb_toolbar : $tm_toolbar;
$toolbar_left       = get_theme_mod( 'orphan_toolbar_left', 'tagline' );
$toolbar_right      = get_theme_mod( 'orphan_toolbar_right' );
$toolbar_cart       = get_theme_mod( 'orphan_woocommerce_cart' );
$classes[]          = (get_theme_mod( 'orphan_toolbar_fullwidth' )) ? 'uk-container-expand' : '';
$toolbar_left_hide  = (get_theme_mod( 'orphan_toolbar_left_hide_mobile' )) ? ' uk-visible@s' : '';
$toolbar_right_hide = (get_theme_mod( 'orphan_toolbar_right_hide_mobile' )) ? ' uk-visible@s' : '';
$toolbar_full_hide = ( $toolbar_left_hide and $toolbar_right_hide ) ? ' uk-visible@s' : '';

?>

<?php if ($toolbar) : ?>
	<div class="tm-toolbar<?php echo $toolbar_full_hide; ?>">
		<div<?php echo orphan_helper::attrs(['class' => $classes]) ?>>

			<?php if (!empty($toolbar_left)) : ?>
			<div class="tm-toolbar-l<?php echo $toolbar_left_hide; ?>"><?php get_template_part( 'template-parts/toolbars/'.$toolbar_left ); ?></div>
			<?php endif; ?>

			<?php if (!empty($toolbar_right) or $toolbar_cart == 'toolbar') : ?>
			<div class="tm-toolbar-r uk-margin-auto-left uk-flex<?php echo $toolbar_right_hide; ?>">
				<?php if ($toolbar_cart == 'toolbar') : ?>
					<div class="uk-display-inline-block">
						<?php get_template_part( 'template-parts/toolbars/'.$toolbar_right ); ?>
					</div>
					<div class="uk-display-inline-block uk-margin-small-left">
						<?php get_template_part('template-parts/woocommerce-cart'); ?>
					</div>
				<?php else: ?>
					<?php get_template_part( 'template-parts/toolbars/'.$toolbar_right ); ?>
				<?php endif; ?>
			</div>
			<?php endif; ?>

		</div>
	</div>
<?php endif; ?>
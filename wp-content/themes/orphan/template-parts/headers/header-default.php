<?php
/**
* @package   orphan
* @author    bdthemes http://www.bdthemes.com
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

	$container_media = [];
	$container_image = '';
	// Options
	$layout_c        = get_theme_mod('orphan_header_layout', 'horizontal-left');
	$layout_m        = get_post_meta( get_the_ID(), 'orphan_header_layout', true );
	$layout          = (!empty($layout_m) and $layout_m != 'default') ? $layout_m : $layout_c;
	
	$fullwidth       = get_theme_mod('orphan_header_fullwidth');
	$logo            = get_theme_mod('orphan_logo_default');
	$class           = array_merge(['tm-header', 'uk-visible@' . get_theme_mod('orphan_mobile_break_point', 'm')]);
	$search          = get_theme_mod( 'orphan_search_position', 'header');
	
	$transparent_c   = get_theme_mod( 'orphan_header_transparent');
	$transparent_m   = get_post_meta( get_the_ID(), 'orphan_header_transparent', true );
	$transparent     = (!empty($transparent_m)) ? $transparent_m : $transparent_c;
	
	$sticky_m        = get_post_meta( get_the_ID(), 'orphan_header_sticky', true );
	$sticky_c        = get_theme_mod( 'orphan_header_sticky' );
	$sticky          = (!empty($sticky_m)) ? $sticky_m : $sticky_c;;
	$cart            = get_theme_mod('orphan_woocommerce_cart');
	$menu_text       = get_theme_mod('orphan_mobile_menu_text');
	$offcanvas_mode  = get_theme_mod('orphan_mobile_offcanvas_mode', 'push');


	if ($layout_m) {
	    $bg_style = get_post_meta( get_the_ID(), 'orphan_header_bg_style', true );
	    $bg_style = ( !empty($bg_style) ) ? $bg_style : get_theme_mod( 'orphan_header_bg_style' );
	    $width    = get_post_meta( get_the_ID(), 'orphan_header_width', true );
	    $padding  = get_post_meta( get_the_ID(), 'orphan_header_padding', true );
	    $text     = get_post_meta( get_the_ID(), 'orphan_header_txt_style', true );
	} else {
	    $bg_style = get_theme_mod( 'orphan_header_bg_style' );
	    $width    = get_theme_mod( 'orphan_header_width' );
	    $padding  = get_theme_mod( 'orphan_header_padding' );
	    $text     = get_theme_mod( 'orphan_header_txt_style' );
	}

	if ($layout_m) {
	    $container_images = rwmb_meta( 'orphan_header_bg_img', "type=image_advanced&size=standard" );
	    foreach ( $container_images as $image ) { 
	        $container_image = esc_url($image["url"]);
	    }
	    $container_bg_img_pos    = get_post_meta( get_the_ID(), 'orphan_header_bg_img_position', true );
	    $container_bg_img_attach = get_post_meta( get_the_ID(), 'orphan_header_bg_img_fixed', true );
	    $container_bg_img_vis    = get_post_meta( get_the_ID(), 'orphan_header_bg_img_visibility', true );
	} else {
	    $container_image         = get_theme_mod( 'orphan_header_bg_img' );
	    $container_bg_img_pos    = get_theme_mod( 'orphan_header_bg_img_position' );
	    // $container_bg_img_attach = get_theme_mod( 'orphan_header_bg_img_fixed' );
	    // $container_bg_img_vis    = get_theme_mod( 'orphan_header_bg_img_visibility' );
	}

	// Image
	if ($container_image &&  $bg_style == 'image') {
	    $container_media['style'][] = "background-image: url('{$container_image}');";
	    // Settings
	    $container_media['class'][] = 'uk-background-norepeat';
	    $container_media['class'][] = $container_bg_img_pos ? "uk-background-{$container_bg_img_pos}" : '';
	    // $container_media['class'][] = $container_bg_img_attach ? "uk-background-fixed" : '';
	    // $container_media['class'][] = $container_bg_img_vis ? "uk-background-image@{$container_bg_img_vis}" : '';
	}

	// Container
	$container            = ['class' => ['uk-navbar-container']];
	$container['class'][] = ($bg_style && $transparent == false) ? 'navbar-color-'.$bg_style : '';
	$class[]              = ($text) ? 'uk-'.$text : '';

	// Transparent
	$transparent = isset($transparent) ? $transparent : false;
	if ($transparent) {
	    $class[] = 'tm-header-transparent';
	    $container['class'][] = "uk-navbar-transparent uk-{$transparent}";
	}

	$navbar_attrs = [ 'class' => 'uk-navbar' ];

	// Sticky
	if ($sticky) {
	    $container['uk-sticky'] = json_encode(array_filter([
			'media'       => 768,
			'show-on-up'  => $sticky == 'smart',
			'animation'   => $transparent || $sticky == 'smart' ? 'uk-animation-slide-top' : '',
			'top'         => $transparent ? '!.js-sticky' : 1,
			'clsActive'   => 'uk-active uk-navbar-sticky',
			'clsInactive' => $transparent ? "uk-navbar-transparent uk-{$transparent}" : false,
	    ]));
	}
?>

<?php if ($transparent) : ?>
<div<?php echo orphan_helper::attrs(['class' => 'js-sticky']) ?>>
<?php endif; ?>
	<div<?php echo orphan_helper::attrs(['class' => $class]) ?>>
		<?php if ($layout == 'horizontal-left' or $layout == 'horizontal-center' or $layout == 'horizontal-right') : ?>
		    <div<?php echo orphan_helper::attrs($container, $container_media) ?>>
		        <div class="uk-container <?php echo ($fullwidth) ? 'uk-container-expand' : '' ?>">
		            <nav<?php echo orphan_helper::attrs($navbar_attrs) ?>>

		                <div class="uk-navbar-left">
		                    <?php get_template_part( 'template-parts/logo-default' ); ?>
		                    <?php if ($layout == 'horizontal-left' and has_nav_menu('primary')) : ?>
		                        <?php get_template_part( 'template-parts/menu-primary' ); ?>
		                        <?php if ($search == 'menu' ) : ?>
		                        	<div class="uk-navbar-item">
		                            	<?php get_template_part( 'template-parts/search' ); ?>
		                            </div>
		                        <?php endif ?>
		                    <?php endif ?>
		                </div>

		                <?php if ($layout == 'horizontal-center' && has_nav_menu('primary')) : ?>
		                <div class="uk-navbar-center">
		                    <?php get_template_part( 'template-parts/menu-primary' ); ?>
		                    <?php if ($search == 'menu' ) : ?>
		                    	<div class="uk-navbar-item">
		                        	<?php get_template_part( 'template-parts/search' ); ?>
		                        </div>
		                    <?php endif ?>
		                </div>
		                <?php endif ?>

		                <?php if (is_active_sidebar('headerbar') || $layout == 'horizontal-right' || $search == 'header' || has_nav_menu('primary') || $cart == 'header') : ?>
		                <div class="uk-navbar-right">
		                    <?php if ($layout == 'horizontal-right' && has_nav_menu('primary')) : ?>
		                        <?php get_template_part( 'template-parts/menu-primary' ); ?>
		                    <?php endif ?> 
							
		                    <?php if ($layout == 'horizontal-right' && $search == 'menu' ) : ?>
		                    	<div class="uk-navbar-item">
		                        	<?php get_template_part( 'template-parts/search' ); ?>
		                        </div>
		                    <?php endif ?>

							<?php if (is_active_sidebar('headerbar')) : ?>
								<div class="uk-navbar-item">
			                    	<?php dynamic_sidebar('headerbar') ?>
								</div>
							<?php endif; ?>

							<?php if (($layout == 'horizontal-left' || $layout == 'horizontal-center' || $layout == 'horizontal-right') && $search == 'header' ) : ?>
								<div class="uk-navbar-item">
							    	<?php get_template_part( 'template-parts/search' ); ?>
							    </div>
							<?php endif ?>

							<?php if (($layout == 'horizontal-left' || $layout == 'horizontal-center' || $layout == 'horizontal-right') && $cart == 'header' ) : ?>
								<div class="uk-navbar-item">
							    	<?php get_template_part('template-parts/woocommerce-cart'); ?>
							    </div>
							<?php endif ?>
		                </div>
		                <?php endif ?>
		            </nav>
		        </div>
		    </div>
			<?php //endif ?>		
		<?php elseif (in_array($layout, ['stacked-center-a', 'stacked-center-b', 'stacked-center-split'])) : ?>
		    <?php if ($layout != 'stacked-center-split' || $layout == 'stacked-center-a' && is_active_sidebar('headerbar')) : ?>
		    <div class="tm-headerbar-top">
		        <div class="uk-container<?php echo ($fullwidth) ? ' uk-container-expand' : '' ?>">

		            <?php //if ($logo) : ?>
		            <div class="uk-text-center">
		                <?php get_template_part( 'template-parts/logo-default' ); ?>
		            </div>
		            <?php //endif ?>

		            <?php if ($layout == 'stacked-center-a' && is_active_sidebar('headerbar')) : ?>
		            <div class="tm-headerbar-stacked uk-grid-medium uk-child-width-auto uk-flex-center uk-flex-middle" uk-grid>
		                <?php dynamic_sidebar('headerbar') ?>
		            </div>
		            <?php endif ?>

		        </div>
		    </div>
		    <?php endif ?>

		    <?php if (has_nav_menu('primary')) : ?>
		    <div<?php echo orphan_helper::attrs($container) ?>>

		        <div class="uk-container <?php echo ($fullwidth) ? 'uk-container-expand' : '' ?>">
		            <nav<?php echo orphan_helper::attrs($navbar_attrs) ?>>

		                <div class="uk-navbar-center">
		                    <?php get_template_part( 'template-parts/menu-primary' ); ?>
		                </div>

		            </nav>
		        </div>

		    </div>
		    <?php endif ?>

		    <?php if (in_array($layout, ['stacked-center-b', 'stacked-center-split']) && is_active_sidebar('headerbar')) : ?>
		    <div class="tm-headerbar-bottom">
		        <div class="uk-container <?php echo ($fullwidth) ? 'uk-container-expand' : '' ?>">
		            <div class="uk-grid-medium uk-child-width-auto uk-flex-center uk-flex-middle" uk-grid>
		                <?php dynamic_sidebar('headerbar') ?>
		            </div>
		        </div>
		    </div>
		    <?php endif ?>
		<?php elseif ($layout == 'stacked-left-a' || $layout == 'stacked-left-b') : ?>
		    <?php if ($logo || is_active_sidebar('headerbar')) : ?>
		    <div class="tm-headerbar-top">
		        <div class="uk-container <?php echo ($fullwidth) ? 'uk-container-expand' : '' ?> uk-flex uk-flex-middle">

		            <?php get_template_part( 'template-parts/logo-default' ); ?>

		            <?php if (is_active_sidebar('headerbar') or $search) : ?>
		            <div class="uk-margin-auto-left">
		                <div class="uk-grid-medium uk-child-width-auto uk-flex-middle" uk-grid>
		                    
							<?php if ($layout == 'stacked-left-a') : ?>
		                    	<?php dynamic_sidebar('headerbar') ?>
		                    <?php endif ?>
		                    

		                    <?php if ($search == 'header' ) : ?>
		                    	<div>
		                        	<?php get_template_part( 'template-parts/search' ); ?>
		                        </div>
		                    <?php endif ?>

	                    	<?php if ($cart == 'header' ) : ?>
								<div>
							    	<?php get_template_part('template-parts/woocommerce-cart'); ?>
							    </div>
							<?php endif ?>
		                </div>
		            </div>
		            <?php endif ?>

		        </div>
		    </div>
		    <?php endif ?>

		    <?php if (has_nav_menu('primary')) : ?>
			    <div<?php echo orphan_helper::attrs($container) ?>>
			        <div class="uk-container <?php echo ($fullwidth) ? 'uk-container-expand' : '' ?>">
			            <nav<?php echo orphan_helper::attrs($navbar_attrs) ?>>

			                <?php if ($layout == 'stacked-left-a') : ?>
			                <div class="uk-navbar-left">
			                    <?php get_template_part( 'template-parts/menu-primary' ); ?>

			                    <?php if ($search == 'menu' ) : ?>
			                    	<div class="uk-navbar-item">
			                        	<?php get_template_part( 'template-parts/search' ); ?>
			                        </div>
			                    <?php endif ?>
			                </div>
			                <?php endif ?>

			                <?php if ($layout == 'stacked-left-b') : ?>
			                <div class="uk-navbar-left uk-flex-auto">
			                    <?php get_template_part( 'template-parts/menu-primary' ); ?>

            					<?php if ($layout == 'stacked-left-b') : ?>
            						<div class="uk-margin-auto-left uk-navbar-item">
                                		<?php dynamic_sidebar('headerbar') ?>
                                	</div>
                                <?php endif ?>

			                    <?php if ($search == 'menu' ) : ?>
			                    	<div class="uk-margin-auto-left uk-navbar-item">
			                        	<?php get_template_part( 'template-parts/search' ); ?>
			                        </div>
			                    <?php endif ?>
			                </div>
			                <?php endif ?>

			            </nav>
			        </div>
			    </div>
		    <?php endif ?>
		<?php elseif ($layout == 'toggle-offcanvas' || $layout == 'toggle-modal') : ?>
		    <div<?php echo orphan_helper::attrs($container) ?>>
		        <div class="uk-container <?php echo ($fullwidth) ? 'uk-container-expand' : '' ?>">
		            <nav<?php echo orphan_helper::attrs($navbar_attrs) ?>>

		            <?php if ($logo) : ?>
		            <div class="uk-navbar-left">
		                <?php get_template_part( 'template-parts/logo-default' ); ?>
		            </div>
		            <?php endif ?>

		            <?php if (has_nav_menu('primary')) : ?>
		            <div class="uk-navbar-right">
		                <a class="uk-navbar-toggle" href="#" uk-toggle="target: !.uk-navbar-container + [uk-offcanvas], [uk-modal]">
		                    <?php if ($menu_text) : ?>
		                    <span class="uk-margin-small-right"><?php esc_html_e('Menu', 'orphan') ?></span>
		                    <?php endif ?>
		                    <div uk-navbar-toggle-icon></div>
		                </a>
		            </div>
		            <?php endif ?>

		            </nav>
		        </div>
		    </div>
			<?php if ($layout == 'toggle-offcanvas' && (has_nav_menu('primary') || is_active_sidebar('headerbar'))) : ?>
			    <div uk-offcanvas="flip: true" mode="<?php echo esc_html($offcanvas_mode); ?>" overlay>
			        <div class="uk-offcanvas-bar">

			            <?php
			            	if(has_nav_menu('primary')) {
			            		wp_nav_menu( array(
			            			'theme_location' => 'primary',
			            			'container'      => false,
			            			'items_wrap'     => '<ul id="%1$s" class="%2$s" uk-nav>%3$s</ul>',
			            			'menu_id'        => 'nav-offcanvas',
			            			'menu_class'     => 'uk-nav uk-nav-default uk-nav-parent-icon',
			            			'echo'           => true,
			            			'before'         => '',
			            			'after'          => '',
			            			'link_before'    => '',
			            			'link_after'     => '',
			            			'depth'          => 0,
			            			)
			            		); 
			            	}
			            ?>

    		            <?php if ($search == 'menu' ) : ?>
    		            	<div class="uk-margin-auto-left uk-navbar-item">
    		                	<?php get_template_part( 'template-parts/search' ); ?>
    		                </div>
    		            <?php endif ?>

	                    <?php if (is_active_sidebar('headerbar')) : ?>
	                    <div class="uk-margin-large-top">
	                        <?php dynamic_sidebar('headerbar') ?>
	                    </div>
	                    <?php endif ?>

	                    <?php if ($search == 'header' ) : ?>
	                    	<div class="uk-margin-auto-left uk-navbar-item">
	                        	<?php get_template_part( 'template-parts/search' ); ?>
	                        </div>
	                    <?php endif ?>

			        </div>
			    </div>
		    <?php elseif ($layout == 'toggle-modal' && (has_nav_menu('primary') || is_active_sidebar('headerbar'))) : ?>
			    <div class="uk-modal-full" uk-modal>
			        <div class="uk-modal-dialog uk-modal-body">
			            <button class="uk-modal-close-full" type="button" uk-close></button>
			            <div class="uk-flex uk-flex-center uk-flex-middle uk-text-center" uk-height-viewport>
			                <div>

			                    <?php
	        		            	if(has_nav_menu('primary')) {
	        		            		wp_nav_menu( array(
	        		            			'theme_location' => 'primary',
	        		            			'container'      => false,
	        		            			'items_wrap'     => '<ul id="%1$s" class="%2$s" uk-nav>%3$s</ul>',
	        		            			'menu_id'        => 'nav-offcanvas',
	        		            			'menu_class'     => 'uk-nav uk-nav-primary uk-nav-center uk-nav-parent-icon',
	        		            			'echo'           => true,
	        		            			'before'         => '',
	        		            			'after'          => '',
	        		            			'link_before'    => '',
	        		            			'link_after'     => '',
	        		            			'depth'          => 0,
	        		            			)
	        		            		); 
	        		            	}
	        		            ?>

	        		            <?php if ($search == 'menu' ) : ?>
	        		            	<div class="uk-margin-auto-left uk-navbar-item">
	        		                	<?php get_template_part( 'template-parts/search' ); ?>
	        		                </div>
	        		            <?php endif ?>

			                    <?php if (is_active_sidebar('headerbar')) : ?>
			                    <div class="uk-margin-large-top">
			                        <?php dynamic_sidebar('headerbar') ?>
			                    </div>
			                    <?php endif ?>

			                    <?php if ($search == 'header' ) : ?>
			                    	<div class="uk-margin-auto-left uk-navbar-item">
			                        	<?php get_template_part( 'template-parts/search' ); ?>
			                        </div>
			                    <?php endif ?>

			                </div>
			            </div>
			        </div>
			    </div>
			<?php endif ?>
		<?php endif ?>
	</div>
<?php if ($transparent) : ?>
</div>
<?php endif; ?>
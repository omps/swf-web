<?php

// Options
$logo            = get_theme_mod('orphan_logo_mobile');;
$mobile          = get_theme_mod('mobile', []);
$logo_align      = get_theme_mod('orphan_mobile_logo_align', 'center');
$menu_align      = get_theme_mod('orphan_mobile_menu_align', 'left');
$search_align    = get_theme_mod('orphan_mobile_search_align', 'right');
$offcanvas_style = get_theme_mod('orphan_mobile_offcanvas_style', 'dropdown');
$offcanvas_mode  = get_theme_mod('orphan_mobile_offcanvas_mode', 'push');
$menu_text       = get_theme_mod('orphan_mobile_menu_text');


$search_align = false; // TODO

?>

<nav class="uk-navbar-container" uk-navbar>

    <?php if ($logo_align == 'left' || $menu_align == 'left' || $search_align == 'left') : ?>
    <div class="uk-navbar-left">

        <?php if ($menu_align == 'left') : ?>
        <a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle<?php echo ($offcanvas_style == 'dropdown') ? '="animation: true"' : '' ?>>
            <span uk-navbar-toggle-icon></span>
            <?php if ($menu_text) : ?>
            <span class="uk-margin-small-left"><?php esc_html_e('Menu', 'orphan') ?></span>
            <?php endif ?>
        </a>
        <?php endif ?>

        <?php if ($search_align == 'left') : ?>
        <a class="uk-navbar-item"><?php esc_html_e('Search', 'orphan') ?></a>
        <?php endif ?>

        <?php if ($logo_align == 'left') : ?>
        <?php get_template_part( 'template-parts/logo-mobile' ); ?>
        <?php endif ?>

    </div>
    <?php endif ?>

    <?php if ($logo_align == 'center') : ?>
    <div class="uk-navbar-center">
        <?php get_template_part( 'template-parts/logo-mobile' ); ?>
    </div>
    <?php endif ?>

    <?php if ($logo_align == 'right' || $menu_align == 'right' || $search_align == 'right') : ?>
    <div class="uk-navbar-right">

        <?php if ($logo_align == 'right') : ?>
        <?php get_template_part( 'template-parts/logo-mobile' ); ?>
        <?php endif ?>

        <?php if ($search_align == 'right') : ?>
        <a class="uk-navbar-item"><?php esc_html_e('Search', 'orphan') ?></a>
        <?php endif ?>

        <?php if ($menu_align == 'right') : ?>
        <a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle<?php echo ($offcanvas_style) == 'dropdown' ? '="animation: true"' : '' ?>>
            <?php if ($menu_text) : ?>
            <span class="uk-margin-small-right"><?php esc_html_e('Menu', 'orphan') ?></span>
            <?php endif ?>
            <span uk-navbar-toggle-icon></span>
        </a>
        <?php endif ?>

    </div>
    <?php endif ?>

</nav>

<?php if (is_active_sidebar('offcanvas')) : ?>

    <?php if ($offcanvas_style == 'offcanvas') : ?>
    <div id="tm-mobile" uk-offcanvas mode="<?php echo esc_html($offcanvas_mode); ?>" overlay>
        <div class="uk-offcanvas-bar">
            <?php get_template_part( 'template-parts/offcanvas' ); ?>
        </div>
    </div>
    <?php endif ?>

    <?php if ($offcanvas_style == 'modal') : ?>
    <div id="tm-mobile" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <button class="uk-modal-close-full" type="button" uk-close></button>
            <div class="uk-flex uk-flex-center uk-flex-middle" uk-height-viewport>
                <?php get_template_part( 'template-parts/offcanvas' ); ?>
            </div>
        </div>
    </div>
    <?php endif ?>

    <?php if ($offcanvas_style == 'dropdown') : ?>
    <div class="uk-position-relative uk-position-z-index">
        <div id="tm-mobile" class="uk-box-shadow-medium<?php echo ($offcanvas_mode == 'slide') ? ' uk-position-top' : '' ?>" hidden>
            <div class="uk-background-default uk-padding">
                <?php get_template_part( 'template-parts/offcanvas' ); ?>
            </div>
        </div>
    </div>
    <?php endif ?>

<?php endif ?>

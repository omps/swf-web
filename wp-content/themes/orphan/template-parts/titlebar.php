<?php

$id             = 'tm-titlebar';
$titlebar_show  = rwmb_meta('orphan_titlebar');
$class          = '';
$section        = '';
$section_media  = [];
$section_image  = '';
$layout         = get_post_meta( get_the_ID(), 'orphan_titlebar_layout', true );
$metabox_layout = (!empty($layout) and $layout != 'default') ? true : false;
$position       = (get_post_meta( get_the_ID(), 'orphan_page_layout', true )) ? get_post_meta( get_the_ID(), 'orphan_page_layout', true ) : get_theme_mod( 'orphan_page_layout', 'sidebar-right' );

if ($metabox_layout) {
    $bg_style = get_post_meta( get_the_ID(), 'orphan_titlebar_bg_style', true );
    $bg_style = ( !empty($bg_style) ) ? $bg_style : get_theme_mod( 'orphan_titlebar_bg_style' );
    $width    = get_post_meta( get_the_ID(), 'orphan_titlebar_width', true );
    $padding  = get_post_meta( get_the_ID(), 'orphan_titlebar_padding', true );
    $text     = get_post_meta( get_the_ID(), 'orphan_titlebar_txt_style', true );
} else {
    $bg_style = get_theme_mod( 'orphan_titlebar_bg_style', 'muted' );
    $width    = get_theme_mod( 'orphan_titlebar_width', 'default' );
    $padding  = get_theme_mod( 'orphan_titlebar_padding', 'medium' );
    $text     = get_theme_mod( 'orphan_titlebar_txt_style' );
}

if (is_array($class)) {
    $class = implode(' ', array_filter($class));
}

if ($section) {      
    
    if ($metabox_layout) {
        $section_images = rwmb_meta( 'orphan_titlebar_bg_img', "type=image_advanced&size=standard" );
        foreach ( $section_images as $image ) { 
            $section_image = esc_url($image["url"]);
        }
        // $section_bg_img_pos    = get_post_meta( get_the_ID(), 'orphan_titlebar_bg_img_position', true );
        // $section_bg_img_attach = get_post_meta( get_the_ID(), 'orphan_titlebar_bg_img_fixed', true );
        // $section_bg_img_vis    = get_post_meta( get_the_ID(), 'orphan_titlebar_bg_img_visibility', true );
    } else {
        $section_image         = get_theme_mod( 'orphan_titlebar_bg_img' );
        // $section_bg_img_pos    = get_theme_mod( 'orphan_titlebar_bg_img_position' );
        // $section_bg_img_attach = get_theme_mod( 'orphan_titlebar_bg_img_fixed' );
        // $section_bg_img_vis    = get_theme_mod( 'orphan_titlebar_bg_img_visibility' );
    }

    // Image
    if ($section_image &&  $bg_style == 'image') {
        $section_media['style'][] = "background-image: url('{$section_image}');";
        // Settings
        $section_media['class'][] = 'uk-background-norepeat';
        // $section_media['class'][] = $section_bg_img_pos ? "uk-background-{$section_bg_img_pos}" : '';
        // $section_media['class'][] = $section_bg_img_attach ? "uk-background-fixed" : '';
        // $section_media['class'][] = $section_bg_img_vis ? "uk-background-image@{$section_bg_img_vis}" : '';
    }
}


$class   = [$name, 'uk-section', $class];
$class[] = ($position == 'full' and $name == 'tm-main') ? 'uk-padding-remove-vertical' : ''; // section spacific override

$class[] = ($bg_style) ? 'uk-section-'.$bg_style : '';
$class[] = ($text) ? 'uk-'.$text : '';
if ($padding != 'none') {
    $class[]       = ($padding) ? 'uk-section-'.$padding : '';
} elseif ($padding == 'none') {
    $class[]       = ($padding) ? 'uk-padding-remove-vertical' : '';
}



if ( $titlebar_show !== 'hide') : ?>

	<?php 
		global $post;
		$blog_title          = get_theme_mod('orphan_blog_title', esc_html__('Blog', 'orphan'));
		$woocommerce_title   = get_theme_mod('orphan_woocommerce_title', esc_html__('Shop', 'orphan'));
		$titlebar_global     = get_theme_mod('orphan_titlebar_layout', 'left');
		$titlebar_metabox    = get_post_meta( get_the_ID(), 'orphan_titlebar_layout', true);

	?>

	<?php if( is_object($post) && !is_archive() &&!is_search() && !is_404() && !is_author() && !is_home() && !is_page() ) { ?>

		<?php if($titlebar_metabox != 'default' && !empty($titlebar_metabox)) { ?>

			<?php  if ($titlebar_metabox == 'left' or $titlebar_metabox == 'center' or $titlebar_metabox == 'right') { ?>
				<div<?php echo orphan_helper::attrs(['id' => $id, 'class' => $class], $section_media); ?>>
					<div<?php echo orphan_helper::container(); ?>>
						<div<?php echo orphan_helper::grid(); ?>>
							<div id="title" class="uk-width-expand<?php echo ($titlebar_metabox == 'center')?' uk-text-center':''; ?>">
								<h1 class="uk-margin-small-bottom"><?php echo esc_html($title); ?></h1>
								<?php echo orphan_breadcrumbs($titlebar_global); ?>
							</div>
							<?php if ($titlebar_metabox != 'center') : ?>
							<div class="uk-margin-auto-left uk-position-relative uk-width-small uk-visible@s">
								<div class="uk-position-center-right">
									<a class="uk-button" onclick="history.back()"><span class="uk-margin-small-right" uk-icon="icon: arrow-left"></span> <?php esc_html_e('Back', 'orphan'); ?></a>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php } elseif (rwmb_meta('orphan_titlebar') == 'notitle') { ?>
				<div id="notitlebar" class="titlebar-no"></div>
			<?php } ?>

		<?php } else { ?>
				
				<?php
					// Define the Title for different Pages
					if ( is_home() ) { $title = $blog_title; }
					elseif( is_search() ) { 	
						$allsearch = new WP_Query("s=$s&showposts=-1"); 
						$count = $allsearch->post_count; 
						wp_reset_postdata();
						$title = $count . ' '; 
						$title .= esc_html__('Search results for:', 'orphan');
						$title .= ' ' . get_search_query();
					}
					elseif( class_exists('Woocommerce') && is_woocommerce() ) { $title = $woocommerce_title; }
					elseif( is_archive() ) { 
						if (is_category()) { 	$title = single_cat_title('',false); }
						elseif( is_tag() ) { 	$title = esc_html__('Posts Tagged:', 'orphan') . ' ' . single_tag_title('',false); }
						elseif (is_day()) { 	$title = esc_html__('Archive for', 'orphan') . ' ' . get_the_time('F jS, Y'); }
						elseif (is_month()) { 	$title = esc_html__('Archive for', 'orphan') . ' ' . get_the_time('F Y'); }
						elseif (is_year()) { 	$title = esc_html__('Archive for', 'orphan') . ' ' . get_the_time('Y'); }
						elseif (is_author()) { 	$title = esc_html__('Author Archive for', 'orphan') . ' ' . get_the_author(); }
						elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { $title = esc_html__('Blog Archives', 'orphan'); }
						else{
							$title = single_term_title( "", false );
							if ( $title == '' ) { // Fix for templates that are archives
								$post_id = $post->ID;
								$title = get_the_title($post_id);
							} 
						}
					}
					elseif( is_404() ) { $title = esc_html__('Oops, this Page could not be found.', 'orphan'); }
					elseif( get_post_type() == 'post' ) { $title = $blog_title; }
					else { $title = get_the_title(); }
				?>

				<div<?php echo orphan_helper::attrs(['id' => $id, 'class' => $class], $section_media); ?>>
					<div<?php echo orphan_helper::container(); ?>>
						<div<?php echo orphan_helper::grid(); ?>>
							<div id="title" class="<?php echo ($titlebar_metabox == 'center')?'uk-text-center':''; ?>">
								<h1 class="uk-margin-small-bottom"><?php echo esc_html($title); ?></h1>
								<?php echo orphan_breadcrumbs($titlebar_global); ?>
							</div>
							<?php if ($titlebar_metabox != 'center') :?>
							<div class="uk-margin-auto-left uk-position-relative uk-width-small uk-visible@s">
								<div class="uk-position-center-right">
									<a class="uk-button" onclick="history.back()"><span class="uk-margin-small-right" uk-icon="icon: arrow-left"></span> <?php esc_html_e('Back', 'orphan'); ?></a>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>

		<?php } // End Else ?>

	<?php } else { // If no post page ?>
		<?php
			// Define the Title for different Pages
			if ( is_home() ) { $title = $blog_title; }
			elseif( is_search() ) { 	
				$allsearch = new WP_Query("s=$s&showposts=-1"); 
				$count = $allsearch->post_count; 
				wp_reset_postdata();
				$title = $count . ' '; 
				$title .= esc_html__('Search results for:', 'orphan');
				$title .= ' ' . get_search_query();
			}
			elseif( class_exists('Woocommerce') && is_woocommerce() ) { $title = $woocommerce_title; }
			elseif( is_archive() ) { 
				if (is_category()) { 	$title = single_cat_title('',false); }
				elseif( is_tag() ) { 	$title = esc_html__('Posts Tagged:', 'orphan') . ' ' . single_tag_title('',false); }
				elseif (is_day()) { 	$title = esc_html__('Archive for', 'orphan') . ' ' . get_the_time('F jS, Y'); }
				elseif (is_month()) { 	$title = esc_html__('Archive for', 'orphan') . ' ' . get_the_time('F Y'); }
				elseif (is_year()) { 	$title = esc_html__('Archive for', 'orphan') . ' ' . get_the_time('Y'); }
				elseif (is_author()) { 	$title = esc_html__('Author Archive for', 'orphan') . ' ' . get_the_author(); }
				elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { $title = esc_html__('Blog Archives', 'orphan'); }
				else{
					$title = single_term_title( "", false );
					if ( $title == '' ) { // Fix for templates that are archives
						$post_id = $post->ID;
						$title = get_the_title($post_id);
					} 
				}
			}
			elseif( is_404() ) { $title = esc_html__('Oops, this Page could not be found.', 'orphan'); }
			elseif( get_post_type() == 'post' ) { $title = $blog_title; }
			else { $title = get_the_title(); }
		?>
		
		<?php if($titlebar_global == 'left' or $titlebar_global == 'center' or $titlebar_global == 'right') { ?>
			<div<?php echo orphan_helper::attrs(['id' => $id, 'class' => $class], $section_media); ?>>
				<div<?php echo orphan_helper::container(); ?>>
					<div<?php echo orphan_helper::grid(); ?>>
						<div id="title" class="uk-width-expand<?php echo ($titlebar_global == 'center')?' uk-text-center':''; ?>">
							<h1 class="uk-margin-small-bottom"><?php echo esc_html($title); ?></h1>
							<?php echo orphan_breadcrumbs($titlebar_global); ?>
						</div>
						<?php if ($titlebar_global != 'center') : ?>
						<div class="uk-margin-auto-left uk-position-relative uk-width-small uk-visible@s">
							<div class="uk-position-center-right">
								<a class="uk-button" onclick="history.back()"><span class="uk-margin-small-right" uk-icon="icon: arrow-left"></span> <?php esc_html_e('Back', 'orphan'); ?></a>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php } elseif($titlebar_global == 'notitle') { ?>
			<div id="notitlebar" class="titlebar-no"></div>
		<?php } ?>

	<?php } // End Else ?>

<?php endif;
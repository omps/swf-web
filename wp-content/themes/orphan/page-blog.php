<?php 
/* Template Name: Blog */

get_header(); 

// Layout
$position = (get_post_meta( get_the_ID(), 'orphan_page_layout', true )) ? get_post_meta( get_the_ID(), 'orphan_page_layout', true ) : get_theme_mod( 'orphan_page_layout', 'sidebar-right' );

?>

<div<?php echo orphan_helper::section(); ?>>
	<div<?php echo orphan_helper::container(); ?>>
		<div<?php echo orphan_helper::grid(); ?>>
			
			<div class="uk-width-expand">
				<main class="tm-content">
					<?php 

						global $wp_query;
						// Pagination fix to work when set as Front Page
						// $paged = get_query_var('paged') ? get_query_var('paged') : 1;
						if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } elseif ( get_query_var('page') ) { $paged = get_query_var('page'); } else { $paged = 1; }	

						// Get Categories
						$categories = rwmb_meta( 'orphan_blogcategories', 'type=checkbox_list' );
						$categories = implode( ', ', $categories );	

						$args = array(
							'post_status'   => 'publish',
							'orderby'       => 'date',
							'order'         => 'DESC',
							'category_name' => $categories,
							'paged'         => $paged
						);
						$wp_query = new WP_Query($args);

						if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php get_template_part( 'template-parts/post-format/entry', get_post_format() ); ?>

						<?php endwhile; endif; ?>

					<?php get_template_part( 'template-parts/pagination' ); ?>
				</main> <!-- end main -->
			</div> <!-- end content -->

			<?php if($position == 'sidebar-left' || $position == 'sidebar-right') : ?>
				<aside<?php echo orphan_helper::sidebar($position); ?>>
				    <?php get_sidebar(); ?>
				</aside> <!-- end aside -->
			<?php endif; ?>
			
		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>

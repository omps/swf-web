<?php 
/* Template Name: Charitable */

get_header();

// Layout
$position = (get_post_meta( get_the_ID(), 'orphan_page_layout', true )) ? get_post_meta( get_the_ID(), 'orphan_page_layout', true ) : get_theme_mod( 'orphan_page_layout', 'sidebar-right' );

$limit = (get_post_meta( get_the_ID(), 'orphan_charitable_limit', true ) != null ) ? get_post_meta( get_the_ID(), 'orphan_charitable_limit', true ) : '12';

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } elseif ( get_query_var('page') ) { $paged = get_query_var('page'); } else { $paged = 1; }

$campaign_options = array(); // fixes a PHP warning when no blog posts at all.
$campaign_categories = get_terms('campaign_category');
if($campaign_categories) {
	foreach ($campaign_categories as $category) {
		$campaign_options[] = $category->slug;
	}
}

// Get Categories
$categories = rwmb_meta( 'orphan_campaign_categories', 'type=checkbox_list' );
$categories = ($categories != null) ? $categories : $campaign_options;

$post_args = array(
	'post_type'      => 'campaign',
	'posts_per_page' => intval($limit),
	'order'          => 'DESC',
	'orderby'        => 'date',
	'post_status'    => 'publish',
	'paged'          => $paged
);
  
$post_args['tax_query'][] = array(
	'taxonomy' => 'campaign_category',
	'field'    => 'slug',
	'terms'    => $categories
);

?>


<div<?php echo orphan_helper::section(); ?>>
	<div<?php echo orphan_helper::container(); ?>>
		<div<?php echo orphan_helper::grid(); ?>>

			<div class="uk-width-expand">
				<main class="tm-content">
					<div class="campaigns-grid-wrapper">
						<?php

						$campaigns = Charitable_Campaigns::query($post_args);

						charitable_template_campaign_loop( $campaigns, 3 );

						wp_reset_postdata(); ?>

						<?php get_template_part( 'template-parts/pagination' ); ?>

					</div><!-- .campaigns-grid-wrapper -->

				</main> <!-- end main -->
			</div> <!-- end content -->
			

			<?php if($position == 'sidebar-left' || $position == 'sidebar-right') : ?>
			<aside<?php echo orphan_helper::sidebar($position); ?>>
				<?php get_sidebar(); ?>
			</aside> <!-- end aside -->
			<?php endif; ?>

		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>

<?php get_header(); ?>


<?php 
	$product_columns = get_theme_mod('orphan_woocommerce_columns');
	$wooclass = 'product-columns-'.$product_columns;
	// Get WooCommerce Layout from Theme Options
	$position = get_theme_mod('orphan_woocommerce_sidebar', 'sidebar-left');

?>

<div<?php echo orphan_helper::section(); ?>>
	<div<?php echo orphan_helper::container(); ?>>
		<div<?php echo orphan_helper::grid(); ?>>


		<?php
		// Single Products Page
		if(is_product()){
			?>

			<div class="uk-width-expand">
				<main class="tm-content">

					<?php woocommerce_content(); ?>

				</main> <!-- end main -->
			</div> <!-- end width -->	

			<?php

			// Main Shop Layout
			} else { ?>
				<div class="uk-width-expand">
					<main class="tm-content <?php echo esc_attr($wooclass); ?>">
						<?php woocommerce_content(); ?>
					</main> <!-- end main -->
				</div> <!-- end width -->
			<?php } // end-if main shop layout ?>

			
			<?php if($position == 'sidebar-left' || $position == 'sidebar-right') : ?>
				<aside<?php echo orphan_helper::sidebar($position); ?>>
				   <?php if(is_woocommerce()) {
						/* WooCommerce Sidebar */
						if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('shop-widgets') );
					} ?>
				</aside> <!-- end aside -->
			<?php endif; ?>


			
	
		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>
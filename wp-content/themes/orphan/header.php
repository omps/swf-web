<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package orphan
 */

// Boxed Page Layout
$layout        = get_theme_mod('orphan_global_layout', 'full');
$padding       = get_theme_mod('orphan_global_padding');
$boxed_class   = ['tm-page'];
$boxed_class[] = $padding ? 'tm-page-padding' : '';
$preloader     = get_theme_mod('orphan_preloader');

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif; ?>
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">


<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
    <?php if ($preloader) : ?>
		<?php get_template_part('template-parts/preloader'); ?>
	<?php endif ?>

	<?php if ($layout == 'boxed') : ?>
	<div<?php echo orphan_helper::attrs(['class' => $boxed_class]) ?>>
	    <div <?php echo ($layout == 'boxed') ? 'class="uk-margin-auto"' : '' ?>>
	<?php endif ?>
			

			<?php if (!is_page_template( 'page-blank.php' )) : ?>
				<?php get_template_part('template-parts/drawer');	?>
				
				<div class="tm-header-wrapper">
					<?php get_template_part( 'template-parts/toolbar' ); ?>

					<div class="tm-header-mobile uk-hidden@<?php echo get_theme_mod('orphan_mobile_break_point', 'm'); ?>">
						<?php get_template_part('template-parts/headers/header-mobile'); ?>
					</div>
					
					<?php get_template_part('template-parts/headers/header-default'); ?>
				</div>

				<?php if (!is_front_page() and !is_page_template( 'page-homepage.php' )) : ?>
					<?php get_template_part('template-parts/titlebar');	?>	
				<?php endif; ?>
				
				<?php get_template_part('template-parts/slider');	?>
			<?php endif; ?>
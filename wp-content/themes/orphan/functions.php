<?php
/**
 * orphan functions and definitions.
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package orphan
 */

define('ORPHAN_VER', wp_get_theme()->get('Version'));

// Orphan only works in WordPress 4.4 or later.
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

// Check if in the admin
$orphan_is_admin = is_admin();

require_once(get_template_directory() . '/inc/helper-functions.php');
require_once(get_template_directory() . '/admin/dom-helper.php');
require_once(get_template_directory() . '/inc/nav_walker.php');
require_once(get_template_directory() . '/inc/menu_options.php');
// Custom Widgets
require_once(get_template_directory() . '/inc/sidebars.php');
// Theme customizer integration
require_once(get_template_directory() . '/customizer/theme-customizer.php');


require_once(get_template_directory() . '/admin/revolution-slider.php');

//TGM Plugin Activation
require_once(get_template_directory().'/inc/plugin-activation.php');

require_once(get_template_directory().'/inc/sidebar-generator.php');
/* Meta Box Script */
define( 'ORPHAN_MB_URL', trailingslashit( get_template_directory_uri() . '/inc/meta-box' ) );
define( 'ORPHAN_MB_DIR', trailingslashit( get_template_directory() . '/inc/meta-box' ) );
require_once ORPHAN_MB_DIR. 'meta-box.php';
require_once ORPHAN_MB_DIR . 'meta-box-tabs/meta-box-tabs.php'; // Include Tabs Extension
require_once ORPHAN_MB_DIR . 'meta-box-conditional-logic/meta-box-conditional-logic.php'; // Include Conditional Logic Extension
require_once ORPHAN_MB_DIR . 'meta-box-yoast-seo/mb-yoast-seo.php'; // Include Conditional Logic Extension
require_once get_template_directory() . '/inc/meta-boxes.php';


require_once(get_template_directory().'/inc/breadcrumbs.php');

// Visual composer integration
if (class_exists('WPBakeryVisualComposerAbstract')) {
	require_once(get_template_directory().'/inc/visualComposer.php');
}

// WooCommerce integration
if (class_exists('Woocommerce')) {
	require_once(get_template_directory() . '/inc/woocommerce.php');
}

// enqueue style and script
require_once(get_template_directory().'/inc/enqueue.php');



if ( ! function_exists( 'orphan_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function orphan_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on orphan, use a find and replace
	 * to change 'orphan' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'orphan', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	// Post Formats
	add_theme_support( 'post-formats', array('gallery', 'link', 'quote', 'audio', 'video'));

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	if ( function_exists('add_image_size')) {
		add_image_size( 'orphan_blog', 1200, 600, true); // Standard Blog Image
		add_image_size( 'orphan_carousel', 640, 360, true); // Standard Blog Image
	}

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary'   => esc_html_x('Primary Menu', 'backend', 'orphan'),
		'offcanvas' => esc_html_x('Offcanvas Menu', 'backend', 'orphan'),
		'toolbar'   => esc_html_x('Toolbar Menu', 'backend', 'orphan'),
		'footer'    => esc_html_x('Footer Menu', 'backend', 'orphan'),
	));

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'orphan_custom_background_args', array(
		'default-color' => 'f5f5f5',
		'default-image' => '',
	)));

	/**
	 * Set the content width in pixels, based on the theme's design and stylesheet.
	 *
	 * Priority 0 to make it available to lower priority callbacks.
	 *
	 * @global int $content_width
	 */
	$GLOBALS['content_width'] = apply_filters( 'orphan_content_width', 890 );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( 'admin/css/editor-style.css' );

	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;

add_action( 'after_setup_theme', 'orphan_setup' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



/* 
 * Custom Excerpts
 */

// Set new Default Excerpt Length
function orphan_new_excerpt_length($length) {
    return 200;
}
add_filter('excerpt_length', 'orphan_new_excerpt_length');

// Custom Excerpt Length
function orphan_custom_excerpt($limit=50) {
    return strip_shortcodes(wp_trim_words(get_the_content(), $limit, '...'));
}

// Word Limiter
function orphan_limit_words($string, $word_limit) {
	$words = explode(' ', $string);
	return implode(' ', array_slice($words, 0, $word_limit));
}

// Remove Shortcodes from Search Results Excerpt
function orphan_remove_shortcode_from_excerpt($excerpt) {
  if ( is_search() ) {
    $excerpt = strip_shortcodes( $excerpt );
  }
  return $excerpt;
}
add_filter('the_excerpt', 'orphan_remove_shortcode_from_excerpt');

/* custom sanitization */
function orphan_stripslashes($string) {
    if(get_magic_quotes_gpc()) {
        return stripslashes($string);
    } else {
        return $string;
    }
}

function orphan_sanitize_text($string) {
	return orphan_stripslashes(htmlspecialchars($string));
}

function orphan_sanitize_text_decode($string) {
	return orphan_stripslashes(htmlspecialchars_decode($string));
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since 1.2.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function orphan_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'orphan-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'orphan_resource_hints', 10, 2 );

/*
 * Helper - expand allowed tags()
 * Source: https://gist.github.com/adamsilverstein/10783774
*/
function orphan_allowed_tags() {
	$allowed_tag = wp_kses_allowed_html( 'post' );
	// iframe
	$allowed_tag['iframe'] = array(
		'src'             => array(),
		'height'          => array(),
		'width'           => array(),
		'frameborder'     => array(),
		'allowfullscreen' => array(),
	); 
	return $allowed_tag;
}

// Charitable custom function active
if ( class_exists( 'Charitable' ) ) {
	require_once( get_template_directory() . '/charitable/class-charitable.php' );
	add_action( 'charitable_custom_start', array( 'Orphan_Charitable', 'start' ) );
	do_action( 'charitable_custom_start');
}


// set custom menu walker
add_filter('wp_nav_menu_args', function($args) {

    if (empty($args['walker'])) {
        $args['walker'] = new orphan_menu_walker;
    }

    return $args; }
);
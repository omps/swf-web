<?php


// Load the main stylesheet
add_action( 'wp_enqueue_scripts', 'orphan_theme_style' );
function orphan_theme_style() {
	wp_enqueue_style( 'orphan-theme-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'orphan-childtheme-style', get_stylesheet_uri(), array('orphan-theme-style') );
}


// Load child theme's textdomain.
add_action( 'after_setup_theme', 'orphan_child_textdomain' );
function orphan_child_textdomain(){
   load_child_theme_textdomain( 'orphan', get_stylesheet_directory().'/languages' );
}


// Example code loading JS in footer. Uncomment to use.
 

/* ====== REMOVE COMMENT

add_action('wp_footer', 'orphanChildFooterScript' );
function orphanChildFooterScript(){

	echo '
	<script type="text/javascript">

	// Your JS code here

	</script>';

}
 ====== REMOVE COMMENT */

/* ======================================================== */



<?php
function bdthemes_shortcode_styles() {
    // Register Styles
    wp_register_style( 'bdt-shortcodes-css', BDTCURI.'css/shortcodes.css' );
    wp_register_style( 'cube-portfolio', BDTCURI.'css/cubeportfolio.min.css' );
    wp_enqueue_style( 'bdt-shortcodes-css' );

}
add_action( 'wp_enqueue_scripts', 'bdthemes_shortcode_styles' );
<?php
// BdThemes Counter Shortcode

if (!function_exists('bdthemes_counter_shortcode')) {
    function bdthemes_counter_shortcode($atts, $content=null) {
        $atts = shortcode_atts(array(
            'id'                       => uniqid('suc'),
            'align'                    => 'top',
            'count_start'              => 1,
            'count_end'                => 5000,
            'counter_refresh_interval' => 50,
            'counter_speed'            => 5,
            'separator'                => 'no',
            'decimal'                  => 'no',
            'prefix'                   => '',
            'suffix'                   => '',
            'count_color'              => '',
            'count_size'               => '32px',
            'text_size'                => '14px',
            'icon'                     => '',
            'icon_color'               => '',
            'icon_size'                => '24',
            'background'               => 'default',
            'custom_background'        => '',
            'color'                    => 'dark',
            'custom_color'             => '',
            'padding'                  => 'medium',
            'custom_padding'           => '',
            'shadow'                   => 'small',
            'hover_shadow'             => 'none',
            'border'                   => '',
            'radius'                   => 'no',
            'class'                    => ''
        ), $atts);       

        $id                = $atts['id'];
        $classes           = array('bdt-counter-wrapper clearfix', 'bdt-counter-'.$atts['align'], bdt_ecssc($atts));
        
        $count_color       = ($atts['count_color']) ? 'color:' . $atts['count_color'] . ';' : '';
        $icon_color        = ($atts['icon_color']) ? 'color:' . $atts['icon_color'] . ';' : '';
        $icon_size         = ($atts['icon_size']) ? 'font-size: '.intval($atts['icon_size']).'px;' : 'font-size: '.$atts['count_size'].';';
        
        $custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
        $custom_color      = ($atts['custom_color']) ? 'color:' . $atts['custom_color']. ';' : '';
        $custom_padding    = ($atts['custom_padding']) ? 'padding:' . $atts['custom_padding']. ';' : '';
        $border            = ($atts['border']) ? 'border:' . $atts['border']. ';' : '';
        $classes[]         = ($atts['radius']) ? 'uk-border-rounded' : '';


        if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $classes[] = 'uk-'.$atts['color'];
        }
        if (($atts['padding'] !=='custom') and ($atts['padding'] !=='none')) {
            $classes[] = ($atts['padding'] =='medium') ? 'uk-padding' : 'uk-padding-'.$atts['padding'];
        }
        if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }
        if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }

        // Font-Awesome icon
        if (strpos($atts['icon'], 'uk-icon-') !== false) {
            $atts['icon'] = '<i class="'.esc_attr($atts['icon']).'" style="' . esc_attr($icon_color) .esc_attr($icon_size) .'"></i>';
        }
        elseif (strpos($atts['icon'], 'fa fa-') !== false) {
            $atts['icon'] = '<i class="'.esc_attr($atts['icon']).'" style="' . esc_attr($icon_color) .esc_attr($icon_size) .'"></i>';
        }

        $icon = ($atts['icon']) ? '<div class="bdt-counter-icon">'. $atts['icon'] .'</div>' : '';

        wp_enqueue_script('jquery-appear');
        wp_enqueue_script('jquery-countup');


        $output = '<div id="'. esc_attr($id) .'" class="'.bdt_acssc($classes).'" data-id="'.$id.'" data-from="'.esc_attr($atts['count_start']).'" data-to="'.esc_attr($atts['count_end']).'" data-speed="'.esc_attr($atts['counter_speed']).'" data-separator="'.esc_attr($atts['separator']).'" data-prefix="'.esc_attr($atts['prefix']).'" data-suffix="'.esc_attr($atts['suffix']).'" style="'.$custom_background.$custom_color.$custom_padding.$border.'">';
        $output .= $icon;
        $output .= '<div class="bdt-counter-desc">
                <div id="'. esc_attr($id) .'_count" class="bdt-counter-number" style="font-size: '.esc_attr($atts['count_size']).'; '. esc_attr($count_color) .'">
                </div>
                <div class="bdt-counter-text" style="font-size: '.esc_attr($atts['text_size']).';">'. do_shortcode($content) .'</div>
            </div>
        </div>';

        return $output;
    }
    // end of counter shortcode

    add_shortcode('bdt_counter', 'bdthemes_counter_shortcode');
}

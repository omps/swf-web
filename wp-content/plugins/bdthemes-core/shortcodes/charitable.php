<?php
// Bdthemes charitable

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); } // load is_plugin_active() function if no available
if(is_plugin_active('charitable/charitable.php')){ 
	if (!function_exists('bdthemes_charitable')) {
		function bdthemes_charitable($atts){
		extract(shortcode_atts(array( 
			'layout'                => 'grid',
			'limit'                 => '8',
			'filters'               => 'all',
			'loading_animation'     => 'fadeOut',
			'filter_animation'      => 'rotateSides',
			'horizontal_gap'        => 10,
			'vertical_gap'          => 10,
			'filter'                => 'yes',
			'filter_align'          => 'left',
			'title'                 => 'yes',
			'category'              => 'yes',
			'donated_amount'        => 'no',
			'progress_bar'          => 'no',
			'intro_text'            => 'no',
			'donate_button'         => 'no',
			'show_link'             => 'yes',
			'show_zoom'             => 'yes',
			'include_article_image' => 'no',
			'navigation'            => 'yes',
			'pagination'            => 'yes',
			'large'                 => 4,
			'medium'                => 3,
			'small'                 => 2,
			'class'                 => '',
		), $atts));
		
		global $wp_query;
		global $post;
		global $paged;
		$zoom_link_icon = '';

		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } elseif ( get_query_var('page') ) { $paged = get_query_var('page'); } else { $paged = 1; }

		$args = array(
			'post_type'      => 'campaign',
			'posts_per_page' => intval($limit),
			'order'          => 'DESC',
			'orderby'        => 'date',
			'post_status'    => 'publish',
			'paged'          => $paged
		);
			
		if($filters != 'all' && $filters != ''){
			// string to array
			$str = $filters;
			$arr = explode(',', $str);
			  
			$args['tax_query'][] = array(
				'taxonomy' => 'campaign_category',
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		query_posts($args);
		ob_start(); // start buffer

		if( $wp_query->have_posts() ) :
			
		$charitable_id = uniqid('bdtc'); 
		$filter_id    = uniqid('bdtfi');
		$value = '';
		 
?>
		<div class="charitable-element">

		<?php 
		if($filter == 'yes' and $layout !='slider'){ 

			// Get Filters from Shortcode Options
			if($filters != '' && $filters != 'all') {
				$campaign_category = explode(',', $filters);
			} else {
				$campaign_category = get_terms('campaign_category');
				$arraytostring = '';
				foreach($campaign_category as $campaign_category){
					$arraytostring .= $campaign_category->slug . ',';
				}
				$arraytostring = rtrim($arraytostring,','); // Remove last commata;
				$campaign_category = explode(',', $arraytostring); // Create array
			} 
		?>

			<div id="<?php echo $filter_id; ?>" class="charitable-filters bdt-filter-align-<?php echo esc_attr($filter_align);?>" >
				<?php if($campaign_category): ?>
					
					<div data-filter="*" class="cbp-filter-item-active cbp-filter-item"><?php esc_html_e('All', 'orphan'); ?></div>	
					<?php foreach($campaign_category as $campaign_category => $value) { ?>
						<?php $filter_name = get_term_by('slug',$value,'campaign_category'); ?>
						<div class="cbp-filter-item" data-filter=".pfi-<?php echo esc_attr($value); ?>">
							<?php echo esc_html($filter_name->name); ?>
						</div>
					<?php } ?>
				<?php endif; ?>
			</div>

		<?php } //end if filter ?>


		<div id="<?php echo $charitable_id; ?>" class="cbp bdt-charitable-grid" data-id="<?php echo intval($charitable_id); ?>">  
			<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

			<?php // LIGHTBOX

				$campaign = charitable_get_current_campaign();
				$progress = $campaign->get_percent_donated_raw();

				if(get_post_meta( get_the_ID(), "bdthemes_charitable-lightbox", true ) == '1') {
					$lightboxicon = 'icon-bdt-search';
				} else {
					$lightboxicon = 'icon-bdt-plus';
				} 

				if ($show_zoom === 'yes' or $show_link === 'yes') {
					$zoom_link_icon = '<div class="bdtp-link-wrap">';
					if ($show_zoom ==='yes') {
						$zoom_link_icon .= '<a href="'.esc_url(wp_get_attachment_url( get_post_thumbnail_id() )).'" class="lightbox-item bdtp-zoom" title="'. esc_attr(get_the_title()) .'"><span uk-icon="icon: search"></span></a>';
					}
					if ($show_link ==='yes') {
						$zoom_link_icon .= '<a href="'.esc_url(get_permalink()).'" class="bdtp-link" title="'. esc_attr(get_the_title()) .'"><span uk-icon="icon: link"></a>';
					}
					$zoom_link_icon .= '</div>';
				} 

			?>

			<?php 
				if ( has_post_thumbnail()) { 
					$charitable_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'bdthemes-portfolio' );
				}

				$terms       = get_the_terms( get_the_ID(), 'campaign_category' ); 
				$filter_name = get_term_by('slug',$value,'campaign_category'); 
			?>  

				<div class="<?php if($terms) : foreach ($terms as $term) { echo 'pfi-'.esc_attr($term->slug).' '; } endif; ?>cbp-item">
					<div class="cbp-caption">
						<div class="bdtp-img-wrap">
							<?php if(get_post_meta( get_the_ID(), "bdthemes_charitable-lightbox", true ) == '1') { 
								echo '<a href="'. esc_url(wp_get_attachment_url( get_post_thumbnail_id() )) .'" title="'.esc_attr(get_the_title()).'">'; } 
								else { echo '<a href="'. esc_url(get_permalink()) .'" title="'.esc_attr(get_the_title()).'">'; } ?>
									<div class="charitable-image-img">
										<img src="<?php echo esc_url($charitable_thumbnail[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" />
									</div>
								</a>
						</div>
						<div class="cbp-caption-activeWrap">
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body bdt-lightbox-gallery">
									<?php echo $zoom_link_icon; ?>
								</div>
							</div>
						</div>
					</div>
						
					<div class="bdtp-meta-wrap uk-card uk-card-default uk-padding-medium">

						<?php if ($title === 'yes') { ?>
							<h4 class="bdtp-title"><a href="<?php echo the_permalink(); ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h4>
						<?php } ?>

						<div class="bdtp-meta">

							<?php if ($category === 'yes') { ?>
								<span class="charitable-category"><?php echo esc_attr($term->name); ?></span>
							<?php } ?>
							
							<?php if ($donated_amount === 'yes' or $progress_bar === 'yes') { ?>
								<ul class="campaign-stats uk-list">

									<?php if ($donated_amount === 'yes') { ?>
										<li class="campaign-pledged">
											<span><?php esc_html_e( 'Donations:', 'generous' ) ?> <?php echo charitable_format_money( $campaign->get_donated_amount() ) ?> / <?php echo charitable_format_money( $campaign->get_goal() ) ?></span>
											               
										</li>
										<?php } ?>

									<?php if ($progress_bar === 'yes') { ?>
										<li class="progress-bar">
											<progress class="uk-progress uk-margin-remove-bottom" value="<?php echo number_format( $progress, 2 ); ?>" max="100"></progress>
										</li>
									<?php } ?>

								</ul>
							<?php } ?>

							<?php if ($intro_text === 'yes') { ?>
								<div class="campaign-description">  
									<?php echo $campaign->description ?>
								</div>
							<?php } ?>

							<?php if ($donate_button === 'yes') { ?>
								<form class="campaign-donation" method="post">
									<?php wp_nonce_field( 'charitable-donate', 'charitable-donate-now' ) ?>
									<input type="hidden" name="charitable_action" value="start_donation" />
									<input type="hidden" name="campaign_id" value="<?php echo $campaign->ID ?>" />
									<button type="submit" name="charitable_submit" class="uk-button uk-button-default uk-margin-small-right uk-text-bold"><?php esc_attr_e( 'Donate Now', 'orphan' ) ?> <span uk-icon="icon: chevron-right;"></span></button>
								</form>
							<?php } ?>

						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>


	<?php if($layout !='slider' and $pagination =='yes') : ?>
	<div class="pagination-wrapper">
		<?php bdthemes_pagination(); ?>
		<p class="uk-hidden"><?php posts_nav_link(); ?></p>
	</div>
	<?php endif; ?>

	<script>
		jQuery(document).ready(function($) {
		    'use strict';

		    // init cubeportfolio
		    jQuery('#<?php echo $charitable_id; ?>').cubeportfolio({
		        filters: '#<?php echo $filter_id; ?>',
		        loadMoreAction: 'click',
		        layoutMode: '<?php echo $layout; ?>',
		        defaultFilter: '*',
		        animationType: '<?php echo $filter_animation; ?>',
		        gapHorizontal: <?php echo $horizontal_gap; ?>,
		        gapVertical: <?php echo $vertical_gap; ?>,
		        gridAdjustment: 'responsive',
		        mediaQueries: [{
		            width: 1100,
		            cols: <?php echo $large; ?>
		        }, {
		            width: 800,
		            cols: <?php echo $medium; ?>
		        }, {
		            width: 480,
		            cols: <?php echo $small; ?>
		        }, {
		            width: 320,
		            cols: 1
		        }],
		        caption: 'zoom',
		        displayType: '<?php echo $loading_animation; ?>',
		        displayTypeSpeed: 100,
		        showNavigation: <?php echo ($navigation =='yes') ? 'true' : 'false'; ?>,
		        showPagination: <?php echo ($pagination =='yes') ? 'true' : 'false'; ?>,
		    });
		});
	</script>
		
	<?php wp_reset_query(); endif; // needs to be wp_reset_query() instead of wp_reset_postdata() so that pagination works.

	$charitable_output = ob_get_contents(); // get buffered content

	ob_end_clean(); // clean buffer

	wp_enqueue_style('cube-portfolio');
	wp_enqueue_script('cube-portfolio');

	return $charitable_output;
	
	}
	add_shortcode('bdt_charitable', 'bdthemes_charitable');
	}
}
<?php

if (!function_exists('bdthemes_blog_list')) {
	function bdthemes_blog_list( $atts ){
		extract(shortcode_atts(array(
			'posts'       => '3',
			'categories'  => 'all',
			'title_color' => '',
			'class'       => '',
		), $atts));
		
		global $post;

		$args = array(
			'post_type' => 'post',
			'posts_per_page' => $posts,
			'order'          => 'DESC',
			'orderby'        => 'date',
			'post_status'    => 'publish'
		);

		if($categories != 'all'){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy'  => 'category',
				'field'   => 'slug',
				'terms'   => $arr
			);
		}

		$title_color = ( $title_color ) ? 'color: '.$title_color.';' : '';
		$wp_query = new WP_Query($args);
		$output = '';

		if( $wp_query->have_posts() ) :

			$output .= '<div class="bdt-blog-list clearfix">';  

			while ( $wp_query->have_posts() ) : $wp_query->the_post();


				$output .= '<div class="bdt-blog-list-item">
							  	<div class="blog-list-item-date">
								  	'.esc_attr(get_the_date('d')).'
								  	<span>'.esc_attr(get_the_time('M')).'</span>
								</div>
								<div class="blog-list-item-description"><a style="'.$title_color.'" href="'.esc_url(get_permalink()).'" title="' . esc_attr(get_the_title()) . '">'.esc_html(get_the_title()) .'</a>
									<p>'.wp_kses_post(bdthemes_custom_excerpt(17)).'</p>
								</div>
				  			</div>';
		  
			endwhile;

			$output .= '</div>';  
		
			wp_reset_postdata();
	  
		endif;

		return $output;
	}
	add_shortcode('bdt_blog_list', 'bdthemes_blog_list');
}
<?php
// Bdthemes heading Shortcode

if (!function_exists('bdthemes_heading')) {
	function bdthemes_heading($atts = null, $content = null) {
		extract(shortcode_atts(array(
			'style'   => 'default',
			'size'    => '24',
			'align'   => 'left',
			'margin'  => '',
			'width'   => '',
			'heading' => 'h3',
			'color'   => ''
		), $atts));

		$id     = uniqid('suhead');
		
		$width  = ($width) ? 'width: ' . intVal($width) . '%;' : '';
		$margin = ($margin) ? 'margin-bottom: ' . intVal($margin) . 'px;' : '';
		$size   = ($size) ? 'font-size: ' . intVal($size) . 'px;line-height: '.$size.'px;' : '';
		$color  = ($color) ? ' color: ' . $color . ';' : '';
		
		$title  = $content;
		$tpos   = mb_strpos($title, ' ');
		$title  = '<span>'.mb_substr($title, 0, $tpos).'</span>'.mb_substr($title, $tpos);

		return '<div id="'.$id.'" class="bdt-heading bdt-heading-style-' . $style . ' bdt-heading-align-'. $align .'" style="'.$width.$margin.'"><'.$heading.' class="bdt-heading-inner" style="'.$size.$color.'">' . do_shortcode($title) . '</'.$heading.'></div>';
	}
	add_shortcode('bdt_heading', 'bdthemes_heading');
}
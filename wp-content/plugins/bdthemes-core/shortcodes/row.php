<?php
// BdThemes row Shortcode

if (!function_exists('bdthemes_row_shortcode')) {
    function bdthemes_row_shortcode($atts = null, $content = null) {
        $atts = shortcode_atts(array(
            'gutter'       => '', // small, medium, large, collapse
            'divider'      => '',
            'match_height' => '',
            'column'       => '',
            
            'xlarge'       => '', // 1 2 3 4 5 6
            'large'        => '',
            'medium'       => '',
            'small'        => '',
            
            'visible'      => '', // xl, l, m, s
            'hidden'       => '', // xl, l, m, s
            
            'class'        => '',
        ), $atts);
        

        $id        = uniqid('surow');
        $classes   = array('bdt-row', bdt_ecssc($atts));
        $classes[] = ($atts['match_height']) ? 'uk-grid-match' : '';
        $classes[] = ($atts['divider']) ? 'uk-grid-divider' : '' ;
        
        $classes[] = ($atts['gutter']) ? 'uk-grid-'.$atts['gutter'] : '' ;


        if ($atts['small'] or $atts['medium'] or $atts['large'] or $atts['xlarge']) {
            if ($atts['small']) {
                $classes[] = 'uk-child-width-1-' . $atts['small'].'@s';
            }
            if ($atts['medium']) {
                $classes[] = 'uk-child-width-1-' . $atts['medium'].'@m';
            }
            if ($atts['large']) {
                $classes[] = 'uk-child-width-1-' . $atts['large'].'@l';
            }
            if ($atts['xlarge']) {
                $classes[] = 'uk-child-width-1-' . $atts['xlarge'].'@xl';
            }
        } else {
            $classes[] = 'uk-child-width-expand@m';
        }

        if ($atts['visible']) {
            $classes[] = 'uk-hidden@' . $atts['visible'];
        }
        if ($atts['hidden']) {
            $classes[] = 'uk-visible@' . $atts['hidden'];
        }
       

        return '<div id="'.$id.'" class="'.bdt_acssc($classes).'" uk-grid>' . do_shortcode($content) . '</div>';
    }

    add_shortcode('bdt_row', 'bdthemes_row_shortcode');
}

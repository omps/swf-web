<?php
// Bdthemes Post Carousel

if (!function_exists('bdthemes_post_carousel')) {
	function bdthemes_post_carousel($atts){
		extract(shortcode_atts(array(
			'posts'             => '6',
			'categories'        => 'all',
			'large'             => 3,
			'medium'            => 2,
			'small'             => 1,
			'nav'               => 'yes',
			'dots'              => 'no',
			'autoplay'          => 'no',
			'autoplay_timeout'  => 4000,
			'speed'             => 350,
			'hoverpause'        => 'no',
			'loop'              => 'yes',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'padding'           => 'medium',
			'custom_padding'    => '',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'radius'            => 'no',
			'order'             => 'DESC',
			'thumb'             => 'yes',
			'tags'              => 'yes',
			'title'             => 'yes',
			'meta'              => 'yes',
			'excerpt'           => 'yes',
			'read_more'         => 'yes',
			'gutter'            => '',
			'class'             => '',
		), $atts));

		global $post;
		$desc_class = ['bdt-oc-desc'];
		$classes    = ['bdt-oc-slide', 'uk-overflow-hidden'];

		if ($gutter     == 'large') { $gutter = 50; }
		elseif ($gutter == 'medium') { $gutter = 25;}
		elseif ($gutter == 'small') { $gutter = 10;}
		elseif ($gutter == 'collapse') { $gutter = 0;}
		else { $gutter  = 35; }

		$classes[]         = ($radius == 'yes') ? 'uk-border-rounded' : '';
		$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';
		$custom_padding    = ($custom_padding) ? 'padding:' . $custom_padding. ';' : '';

		if (($background !=='custom') and ($background !=='none')) {
            $classes[] = ($background) ? 'uk-background-'.$background : '';
        }
        if ($color) {
            $classes[] = 'uk-'.$color;
        }		
		if ($shadow !=='none') {
            $classes[] = 'uk-box-shadow-'.$shadow;
        }		
		if ($hover_shadow !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$hover_shadow;
        }
        if (($padding !=='custom') and ($padding !=='none')) {
            $desc_class[] = ($padding =='medium') ? 'uk-padding' : 'uk-padding-'.$padding;
        }

		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => $posts,
			'order'          => $order,
			'orderby'        => 'date',
			'post_status'    => 'publish'
		);
		
		if($categories != 'all'){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		wp_enqueue_script('owl-carousel');

		$wp_query = new WP_Query($args);
		$return = '';

		if( $wp_query->have_posts() ) :

			$settings = [
				'responsiveClass'    => true,
				'mouseDrag'          => true,
				'navText'            => false,
				'margin'             => $gutter,
				'autoplayTimeout'    => $autoplay_timeout,
				'smartSpeed'         => $speed,
				'autoplay'           => ($autoplay == 'yes') ? true : false,
				'autoplayHoverPause' => ($hoverpause == 'yes') ? true : false,
				'loop'               => ($loop == 'yes') ? true : false,
				'responsive'         => [
					0 => [
						'items' => $small,
						'dots'  => ($dots =='yes') ? true : false,
						'nav'   => ($nav == 'yes') ? true : false,
					],
					768 => [
						'items' => $medium,
						'dots'  => ($dots =='yes') ? true : false,
						'nav'   => ($nav =='yes') ? true : false,
					],
					1000 => [
						'items' => $large,
						'dots'  => ($dots =='yes') ? true : false,
						'nav'   => ($nav =='yes') ? true : false,
					]
				]
			];

			$output[] = '<div class="bdt-owl-carousel">';
			$output[] = '<div class="owl-carousel owl-theme" data-owl-carousel=\''.json_encode($settings).'\'>';
		
			while ( $wp_query->have_posts() ) : $wp_query->the_post();

		  		$output[] = '<div class="'.bdt_acssc($classes).'" style="'.$custom_background.'">';
		  
		  		if( $thumb == 'yes') {
			  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );

			  		if($blog_thumbnail[0] != ''){
			  
			  		$output[] = '<a href="'.esc_url(get_permalink()).'" title="' . esc_attr(get_the_title()) . '" class="bdt-oc-thumb"><img src="'.esc_url($blog_thumbnail[0]).'" alt="' . esc_attr(get_the_title()) . '" /></a>';
			  		
			  		}
			  	}
		  		
		  		$output[] = '<div class="'.esc_attr(implode(' ', $desc_class)).'" style="'.$custom_padding.'">';

		  			if ( $tags == 'yes' ) {
			  			$tags_list = get_the_tag_list( '<span class="bdt-oc-tags">', '</span> <span class="bdt-oc-tags">', '</span>');

		                if ( $tags_list ) {
		                    $output[] = '<p class="uk-margin-small-bottom" >'. wp_kses_post($tags_list) . '</p>'; // WPCS: XSS OK.
		                }
	            	}

	            	if ( $title == 'yes' ) {
						$output[] = '<h4 class="uk-margin-small-bottom"><a href="'.esc_url(get_permalink()).'" title="' . esc_attr(get_the_title()) . '">'.esc_html(get_the_title()) .'</a></h4>';
					}

					if ( $meta == 'yes' ) {
						$output[] = '<ul class="uk-subnav uk-margin-small-top" uk-margin><li><span>'.esc_html__('Posted:', 'orphan').' '.esc_attr(get_the_date()).'</span></li><li><span>'.sprintf(esc_html__('By: %s', 'orphan'), '<a href="'.get_author_posts_url(get_the_author_meta('ID')).'" title="'.get_the_author().'">'.get_the_author().'</a>').'</span></li></ul>';
					}

					if ( $excerpt == 'yes' ) {
						$output[] = '<div>'.wp_kses_post(bdthemes_custom_excerpt(15)).'</div>';
					}

					if ( $read_more == 'yes') {
						$output[] = '<a href="'.esc_url(get_permalink()).'" class="uk-button uk-padding-remove-horizontal uk-margin-top uk-margin-small-right uk-text-bold">'.esc_html('Read More', 'orphan').'<span uk-icon="icon: chevron-right"></span></a>';
					}
				  		
			  		$output[] = '</div>';
				$output[] = '</div>';
		  
			endwhile;

			$output[] ='</div>';
		
			$output[] ='</div><div class="clear"></div>';
		
		 	wp_reset_postdata();
	  
		endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_post_carousel', 'bdthemes_post_carousel');
}
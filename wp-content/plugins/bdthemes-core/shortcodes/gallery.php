<?php
// Bdthemes gallery

if (!function_exists('bdthemes_gallery')) {
	function bdthemes_gallery($atts){
		extract(shortcode_atts(array( 
			'post_type'      => 'post',
			'limit'          => '12',
			'lightbox'       => 'yes',
			'order'          => 'DESC',
			'horizontal_gap' => 10,
			'vertical_gap'   => 10,
			'large'          => 4,
			'images'         => '',
			'medium'         => 3,
			'small'          => 2,
			'class'          => '',
		), $atts));

		global $wp_query;
		global $post;
		global $paged;
		$lightbox_icon = $img_size ='';

		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } elseif ( get_query_var('page') ) { $paged = get_query_var('page'); } else { $paged = 1; }

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => intval($limit),
			'order'          => $order,
			'orderby'        => 'date',
			'post_status'    => 'publish',
			'paged'          => $paged
		);

		query_posts($args);
		ob_start(); // start buffer

			
		$gallery_id = uniqid('bdtg'); 
		$value = '';


			 
	?>
	
	<?php if ($post_type != 'custom') : ?>
		<?php if( $wp_query->have_posts() ) : ?>
		<div id="<?php echo $gallery_id; ?>" class="cbp bdt-gallery-grid" data-id="<?php echo intval($gallery_id); ?>">  
			<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

			<?php 
				
				if ($lightbox == 'yes') {
					$lightbox_icon = '<div class="bdtp-link-wrap">';
						$lightbox_icon .= '<a href="'.esc_url(wp_get_attachment_url( get_post_thumbnail_id() )).'" class="lightbox-item bdtp-zoom" title="'. esc_attr(get_the_title()) .'"><span uk-icon="icon: search"></span></a>';
					$lightbox_icon .= '</div>';
				}

			?>

			<?php 
				if ( has_post_thumbnail()) { 
					$gallery_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'bdthemes-portfolio' );
				}

				$terms       = get_the_terms( get_the_ID(), 'gallery_category' ); 
				$filter_name = get_term_by('slug',$value,'gallery_category'); 
			?>  

				<div class="cbp-item">
					<div class="cbp-caption">
						<div class="bdtp-img-wrap">
							<?php if($lightbox == 'yes') : ?>
								<a href="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id() )); ?>" title="<?php esc_attr(get_the_title()); ?>">
							<?php else : ?>
								<a href="<?php echo esc_url(get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
							<?php endif; ?>
								<div class="gallery-image-img">
									<img src="<?php echo esc_url($gallery_thumbnail[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" />
								</div>
							</a>
						</div>
						<div class="cbp-caption-activeWrap">
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body bdt-lightbox-gallery">
									<?php echo $lightbox_icon; ?>
								</div>
							</div>
						</div>
					</div>

				</div>
			<?php endwhile; ?>
		</div>
	<?php wp_reset_query(); endif;  // needs to be wp_reset_query() instead of wp_reset_postdata() so that pagination works. ?>

	<div class="pagination-wrapper">
		<?php bdthemes_pagination(); ?>
		<p class="uk-hidden"><?php posts_nav_link(); ?></p>
	</div>

	
	<?php else : ?>
		<div id="<?php echo $gallery_id; ?>" class="cbp bdt-gallery-grid" data-id="<?php echo intval($gallery_id); ?>">
			<?php 

				$images = ($post_type == 'custom' ) ? explode( ',', $images ) : '';

				foreach ( $images as $i => $image ) : ?>

				<?php

					$img = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => $img_size ) );

					$gallery_thumbnail = $img['thumbnail'];
					$large_img_src = $img['p_img_large'][0];

					if ($lightbox == 'yes') {
						$lightbox_icon = '<div class="bdtp-link-wrap">';
							$lightbox_icon .= '<a href="'.$large_img_src.'" class="lightbox-item bdtp-zoom" title="'. esc_attr(get_the_title()) .'"><span uk-icon="icon: search"></span></a>';
						$lightbox_icon .= '</div>';
					}

				?>

				<div class="cbp-item">
					<div class="cbp-caption">
						<div class="bdtp-img-wrap">
							<?php if($lightbox == 'yes') : ?>
								<a href="<?php echo $large_img_src; ?>"> 
								<?php else : ?>
								<a href="<?php echo esc_url(get_permalink()); ?>">
							<?php endif; ?>
								<div class="gallery-image-img">
									<?php echo $gallery_thumbnail; ?>
								</div>
							</a>
						</div>
						<div class="cbp-caption-activeWrap">
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body bdt-lightbox-gallery">
									<?php echo $lightbox_icon; ?>
								</div>
							</div>
						</div>
					</div>

				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>

	<script>
		jQuery(document).ready(function($) {
		    'use strict';
		    // init cubeportfolio
		    jQuery('#<?php echo $gallery_id; ?>').cubeportfolio({
		        loadMoreAction: 'click',
		        layoutMode: 'grid',
		        gapHorizontal: <?php echo $horizontal_gap; ?>,
		        gapVertical: <?php echo $vertical_gap; ?>,
		        gridAdjustment: 'responsive',
		        mediaQueries: [{
		            width: 1100,
		            cols: <?php echo $large; ?>
		        }, {
		            width: 800,
		            cols: <?php echo $medium; ?>
		        }, {
		            width: 480,
		            cols: <?php echo $small; ?>
		        }, {
		            width: 320,
		            cols: 1
		        }],
		        caption: 'zoom',
		        displayTypeSpeed: 100,
		    });
		});
	</script>
		
	<?php 

	$gallery_output = ob_get_contents(); // get buffered content

	ob_end_clean(); // clean buffer

	wp_enqueue_style('cube-portfolio');

	return $gallery_output;
	
	}
	add_shortcode('bdt_gallery', 'bdthemes_gallery');
}
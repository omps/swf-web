<?php
// BdThemes Dropcap Shortcode

if (!function_exists('bdthemes_dropcap_shortcode')) {
    function bdthemes_dropcap_shortcode($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'class' => ''
        ), $atts));

        $classes = ['uk-dropcap'];

        return '<p class="'. bdt_acssc($classes) . '">' . do_shortcode($content) . '</p>';
    }

    add_shortcode('bdt_dropcap', 'bdthemes_dropcap_shortcode');
}

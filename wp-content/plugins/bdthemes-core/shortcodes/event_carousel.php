<?php
// Bdthemes Event Carousel

if (!function_exists('bdthemes_event_carousel')) {
	function bdthemes_event_carousel($atts){
		/**
		 * Check if events calendar plugin method exists
		 */
		if ( !function_exists( 'tribe_get_events' ) ) {
			return;
		}

		global $wp_query, $post;
		$output = '';

		$atts = shortcode_atts( array(
			'categories'        => '',
			'month'             => '',
			'limit'             => 5,
			'eventdetails'      => 'yes',
			'time'              => null,
			'past'              => null,
			'venue'             => 'no',
			'author'            => null,
			'message'           => 'There are no upcoming events at this time.',
			'key'               => 'End Date',
			'order'             => 'ASC',
			'viewall'           => 'no',
			'thumb'             => 'yes',
			'thumbwidth'        => '',
			'thumbheight'       => '',
			'event_tax'         => '',
			'large'             => 3,
			'medium'            => 2,
			'small'             => 1,
			'nav'               => 'yes',
			'dots'              => 'no',
			'autoplay'          => 'no',
			'autoplay_timeout'  => 4000,
			'speed'             => 350,
			'hoverpause'        => 'no',
			'loop'              => 'yes',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'padding'           => 'medium',
			'custom_padding'    => '',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'radius'            => 'yes',
			'title'             => 'yes',
			'meta'              => 'yes',
			'excerpt'           => 'yes',
			'read_more'         => 'yes',
			'gutter'            => '',
			'class'            => '',
		), $atts, 'bdt_event_carousel' );

		$rtl        = (is_rtl()) ? 'true' : 'false';
		$desc_class = ['bdt-oc-desc'];
		$classes    = ['bdt-oc-slide', 'uk-overflow-hidden'];

		if ($atts['gutter']     == 'large') { $atts['gutter'] = 50; }
		elseif ($atts['gutter'] == 'medium') { $atts['gutter'] = 25;}
		elseif ($atts['gutter'] == 'small') { $atts['gutter'] = 10;}
		elseif ($atts['gutter'] == 'collapse') { $atts['gutter'] = 0;}
		else { $atts['gutter']  = 35; }

		$classes[]         = ($atts['radius'] =='yes') ? 'uk-border-rounded' : '';
		$custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
		$custom_padding    = ($atts['custom_padding']) ? 'padding:' . $atts['custom_padding']. ';' : '';

		if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $classes[] = 'uk-'.$atts['color'];
        }		
		if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }		
		if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }
        if (($atts['padding'] !=='custom') and ($atts['padding'] !=='none')) {
            $desc_class[] = ($atts['padding'] =='medium') ? 'uk-padding' : 'uk-padding-'.$atts['padding'];
        }

		// Category
		if ( $atts['categories'] ) {
			if ( strpos( $atts['categories'], "," ) !== false ) {
				$atts['categories'] = explode( ",", $atts['categories'] );
				$atts['categories'] = array_map( 'trim', $atts['categories'] );
			} else {
				$atts['categories'] = $atts['categories'];
			}

			$atts['event_tax'] = array(
				'relation' => 'OR',
				array(
					'taxonomy' => 'tribe_events_cat',
					'field'    => 'name',
					'terms'    => $atts['categories'],
				),
				array(
					'taxonomy' => 'tribe_events_cat',
					'field'    => 'slug',
					'terms'    => $atts['categories'],
				)
			);
		}

		// Past Event
		$meta_date_compare = '>=';
		$meta_date_date = date( 'Y-m-d' );

		if ( $atts['time'] == 'past' || !empty( $atts['past'] ) ) {
			$meta_date_compare = '<';
		}

		// Key
		if ( str_replace( ' ', '', trim( strtolower( $atts['key'] ) ) ) == 'startdate' ) {
			$atts['key'] = '_EventStartDate';
		} else {
			$atts['key'] = '_EventEndDate';
		}
		// Date
		$atts['meta_date'] = array(
			array(
				'key'     => $atts['key'],
				'value'   => $meta_date_date,
				'compare' => $meta_date_compare,
				'type'    => 'DATETIME'
			)
		);

		// Specific Month
		if ( $atts['month'] == 'current' ) {
			$atts['month'] = date( 'Y-m' );
		}
		if ($atts['month']) {
			$month_array = explode("-", $atts['month']);

			$month_yearstr = $month_array[0];
			$month_monthstr = $month_array[1];

			$month_startdate = date($month_yearstr . "-" . $month_monthstr . "-1");
			$month_enddate = date($month_yearstr . "-" . $month_monthstr . "-t");

			$atts['meta_date'] = array(
				array(
					'key'     => $atts['key'],
					'value'   => array($month_startdate, $month_enddate),
					'compare' => 'BETWEEN',
					'type'    => 'DATETIME'
				)
			);
		}

		$posts = get_posts( array(
			'post_type'      => 'tribe_events',
			'posts_per_page' => $atts['limit'],
			'tax_query'      => $atts['event_tax'],
			'meta_key'       => $atts['key'],
			'orderby'        => 'meta_value',
			'author'         => $atts['author'],
			'order'          => $atts['order'],
			'meta_query'     => array( $atts['meta_date'] )
		) );

		$settings = [
			'stagePadding'       => 5,
			'responsiveClass'    => true,
			'mouseDrag'          => true,
			'navText'            => false,
			'margin'             => $atts['gutter'],
			'autoplayTimeout'    => $atts['autoplay_timeout'],
			'smartSpeed'         => $atts['speed'],
			'autoplay'           => ($atts['autoplay'] == 'yes') ? true : false,
			'autoplayHoverPause' => ($atts['hoverpause'] == 'yes') ? true : false,
			'loop'               => ($atts['loop'] == 'yes') ? true : false,
			'responsive'         => [
					0 => [
						'items' => $atts['small'],
						'dots'  => ($atts['dots'] == 'yes') ? true : false,
						'nav'   => ($atts['nav'] == 'yes') ? true : false,
					],
					768 => [
						'items' => $atts['medium'],
						'dots'  => ($atts['dots'] == 'yes') ? true : false,
						'nav'   => ($atts['nav'] == 'yes') ? true : false,
					],
					1000 => [
						'items' => $atts['large'],
						'dots'  => ($atts['dots'] == 'yes') ? true : false,
						'nav'   => ($atts['nav'] == 'yes') ? true : false,
					]
				]
			];

		if ($posts) {
			$output[] = '<div class="bdt-owl-carousel" >';
			$output[] = '<div class="owl-carousel owl-theme" data-owl-carousel=\''.json_encode($settings).'\'>';

			foreach( $posts as $post ) :
				setup_postdata( $post );
				$start_date = tribe_get_start_date( null, false, 'M d, Y' );
				$end_date   = tribe_get_display_end_date( null, false, 'M d, Y' );

					$output[] = '<div class="'.bdt_acssc($classes).'" style="'.$custom_background.'">';
						if( $atts['thumb'] != 'no') {
							$thumbWidth = is_numeric($atts['thumbwidth']) ? $atts['thumbwidth'] : '';
							$thumbHeight = is_numeric($atts['thumbheight']) ? $atts['thumbheight'] : '';
							if( !empty($thumbWidth) && !empty($thumbHeight) ) {
								$output[] = get_the_post_thumbnail(get_the_ID(), array($thumbWidth, $thumbHeight) );
							} else {

								$size = ( !empty($thumbWidth) && !empty($thumbHeight) ) ? array( $thumbWidth, $thumbHeight ) : 'medium';

								if ( $thumb = get_the_post_thumbnail( get_the_ID(), $size ) ) {
									$output[] = '<a href="' . tribe_get_event_link() . '">';
									$output[] = $thumb;
									$output[] = '</a>';
								}
							}
						}


						$output[] = '<div class="'.esc_attr(implode(' ', $desc_class)).'" style="'.$custom_padding.'">';
							if( $atts['title']) {
								$output[] = '<h4 class="uk-margin-small-bottom">' .
											'<a href="' . tribe_get_event_link() . '" rel="bookmark">' . apply_filters( 'ecs_event_list_title', get_the_title(), $atts ) . '</a>
										</h4>';
							}
							if( $atts['meta']) {
								$output[] = '<ul class="uk-subnav uk-margin-small-top" uk-margin>';
									$output[] = '<li><span>'.esc_html__('Start:', 'orphan').' '.sprintf(esc_html__('%s', 'orphan'),$start_date).'</span></li>';
									$output[] = '<li><span>'.esc_html__('End:', 'orphan').' '.sprintf(esc_html__('%s', 'orphan'),$end_date).'</span></li>';
								$output[] = '</ul>';
							};
						
							if($atts['excerpt'] != 'no') {
								$excerptLength = is_numeric($atts['excerpt']) ? $atts['excerpt'] : 15;
								$output[] = '<div class="bdt-oc-txt">' .
												bdthemes_custom_excerpt($excerptLength) .
											'</div>';
							}

							if( $atts['venue'] != 'no' ) {
								$output[] = '<span class="duration venue"><em> at </em>' . apply_filters( 'ecs_event_list_venue', tribe_get_venue(), $atts ) . '</span>';
							}

							if( $atts['read_more'] != 'no' ) {
								$output[] = '<a href="'.esc_url(get_permalink()).'" class="uk-button uk-padding-remove-horizontal uk-margin-top uk-margin-small-right uk-text-bold">'.esc_html('View Details', 'orphan').'<span uk-icon="icon: chevron-right"></span></a>';
							}
						$output[] = '</div>';
					$output[] = '</div>';
			endforeach;
			$output[] = '</div>';
				
			$output[] = '</div>';

			if( $atts['viewall'] !== 'no' ) {
				$output[] = '<span class="ecs-all-events"><a href="' . apply_filters( 'ecs_event_list_viewall_link', tribe_get_events_link(), $atts ) .'" rel="bookmark">' . esc_html__( 'View All Events', 'warp' ) . '</a></span>';
			}
		} else { //No Events were Found
			$output[] = do_shortcode('[bdt_note style="1" type="warning"]'. esc_html__($atts["message"], 'bdthemes-core').'[/bdt_note]');
		} // endif

		wp_reset_query();

		wp_enqueue_script('owl-carousel');

		return implode("\n", $output);
	}
	add_shortcode('bdt_event_carousel', 'bdthemes_event_carousel');
}
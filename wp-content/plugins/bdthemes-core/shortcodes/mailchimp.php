<?php
// BdThemes mailchimp Shortcode

if (!function_exists('bdthemes_mailchimp_shortcode')) {
    function bdthemes_mailchimp_shortcode($atts = null, $content = null) {
        $atts = shortcode_atts(array(
            'api_key'     => '',
            'email_list'  => '',
            'before_text' => '',
            'after_text'  => '',
            'button_text' => '',
            'class'       => '',
        ), $atts, 'bdt_mailchimp');

        $id            = uniqid('sutm');
        $mailchimp_api = get_option( 'bdthemes_mailchimp_api_key' );
        $mailchimp_api = ( $mailchimp_api ) ? $mailchimp_api : $atts['api_key'];
        $alrt_class    = '';

        if ( isset ( $_POST['bdt_mc_email'] ) ) {
            if ( ! empty ( $mailchimp_api ) ) {
                if ( ! class_exists( 'MCAPI' ) ) {
                    include_once( WP_PLUGIN_DIR . '/bdthemes-core/includes/MCAPI.class.php' );
                }

                $api_key = $mailchimp_api;
                $mcapi = new MCAPI( $api_key );
                $merge_vars = Array (
                    'EMAIL' => $_POST['bdt_mc_email']
                );
                $list_id = $atts['email_list'];

                if ( $mcapi->listSubscribe( $list_id, $_POST['bdt_mc_email'], $merge_vars ) ) {
                    // It worked!
                    $msg = '<p>' . esc_html__( 'Success!&nbsp; Check your inbox or spam folder for a message containing a confirmation link.', 'bdthemes-core' ) . '</p>';
                    $alrt_class = 'uk-alert-success';
                }
                else {
                    // An error ocurred, return error message
                    $msg = '<p><b>' . esc_html__( 'Error:', 'bdthemes-core' ) . '</b>&nbsp; ' . $mcapi->errorMessage . '</p>';
                    $alrt_class = 'uk-alert-warning';
                }
            }
        }

        if ( empty( $mailchimp_api ) ) {
            echo '<div class="newsletter-signup bdt-newsletter">';
            echo '<p>' . esc_html__( 'No mailchimp list selected. Please set your mailchimp API key in the theme admin panel and then configure the widget from the widget options.', 'bdthemes-core' ) . '</p>';
            echo '  </div>';
            return;
        }

        $output[] = '<div class="newsletter-signup bdt-newsletter-wrapper">';

        // GET INTRO TEXT
        if ( ! empty( $atts['before_text'] ) ) {
            $output[] = '<p>' . $atts['before_text'] . '</p>';
        }

        $button_text = !empty( $atts['button_text'] ) ? $atts['button_text'] : esc_html__( "SIGNUP", 'bdthemes-core' );
        $output[] = '      <form method="post" class="bdt-newsletter uk-margin" data-url="' . trailingslashit( home_url() ) . '" name="newsletter_form" uk-margin>';
        $output[] = '<div class="uk-inline">';
        $output[] = '<span class="uk-form-icon" href="" uk-icon="icon: mail"></span>';
        $output[] = '          <input type="text" name="bdt_mc_email" class="uk-input uk-form-width-medium" value="" placeholder="' . esc_html__( "mail@domain.com", 'bdthemes-core' ) . '" />';
        $output[] = '</div>';
        $output[] = '          <input type="hidden" name="bdt_list_class" class="bdt-lid" value="' . $atts['email_list'] . '" />';
        $output[] = '          <input type="submit" name="submit" class="bdt-newsletter-submit uk-button uk-button-primary" value="' . $button_text . '" />';
        $output[] = '      </form>';

        if ( isset ( $msg ) ) {
            $output[] = '<div class="'.$alrt_class.'" uk-alert><a class="uk-alert-close" uk-close></a>' . $msg . '</div>';
        }

        // GET INTRO TEXT
        if ( ! empty( $atts['after_text'] ) ) {
            $output[] = '<p>' . $atts['after_text'] . '</p>';
        }

        $output[] = '  </div>';
                
        return implode("\n", $output);
    }
    // end of testimonial shortcode

    add_shortcode('bdt_mailchimp', 'bdthemes_mailchimp_shortcode');
}
<?php
// Bdthemes Divider Shortcode

if (!function_exists('bdthemes_divider')) {
	function bdthemes_divider($atts){
		extract(shortcode_atts(array(
			'align'  => '', // left, right, center
			'icon'   => 'no',
			'small'  => 'no', // true, false
			'margin' => '', // small, medium, large
			'class' => '',
		    ), $atts));

			$id        = uniqid('sud');
			$classes   = ['bdt-divider', ''];
			$classes[] = ($small == 'yes') ? 'uk-divider-small' : '';
			$classes[] = ($icon == 'yes') ? 'uk-divider-icon' : '';
			$classes[] = $margin ? 'uk-margin-'.$margin.'-top uk-margin-'.$margin.'-bottom' : '';
			$classes[] = $align ? 'uk-text-'.$align : '';

		

		// Output HTML
		return '<hr id="'.$id.'" class="'.bdt_acssc($classes).'">';
	}
	add_shortcode('bdt_divider', 'bdthemes_divider');
}
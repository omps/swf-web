<?php
// Bdthemes Pricing Plan
if (!function_exists('bdthemes_pricing_plan')) {
	function bdthemes_pricing_plan($atts=null, $content=null){
		$atts = shortcode_atts(array(
			'name'              => 'Standard',
			'price'             => '19.99',
			'before'            => '$',
			'after'             => '',
			'period'            => '',
			'featured'          => 'no',
			'icon'              => '',
			'btn_text'          => 'Sign up Now',
			'btn_url'           => '#',
			'btn_target'        => 'self',
			'btn_style'         => 'default',
			'badge'             => '',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'custom_color'      => '',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'border'            => '',
			'radius'            => 'no',
			'class'             => ''
		), $atts, 'plan');

		$id                = uniqid('bdtplan_');
		$icon              = '';
		$badge             = '';
		$featured          = '';
		$output            = [];
		
		$classes           = array('bdt-plan', bdt_ecssc($atts));
		$custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
		$custom_color      = ($atts['custom_color']) ? 'color:' . $atts['custom_color']. ';' : '';
		$border            = ($atts['border']) ? 'border:' . $atts['border']. ';' : '';
		$classes[]         = ($atts['radius'] == 'yes') ? 'uk-border-rounded' : '';


        if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $classes[] = 'uk-'.$atts['color'];
        }
        if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }
        if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }
		
		if ($atts['icon']) {
		    $icon .= '<div class="bdt-plan-icon">';
		        if (strpos($atts['icon'], 'uk-icon-') !== false) {
		            $icon .= '<i class="'.$atts['icon'].'"></i>';
		        }
		        elseif (strpos($atts['icon'], 'fa fa-') !== false) {
		            $icon .= '<i class="uk-icon-' . trim(str_replace('fa fa-', '', $atts['icon'])) . '"></i>';
		        }
		    $icon .= '</div>';
		}

		if ($atts['before'])
		    $atts['before'] = '<span class="bdt-plan-price-before">' . $atts['before'] . '</span>';
		if ($atts['after'])
		    $atts['after']  = '<span class="bdt-plan-price-after">' . $atts['after'] . '</span>';
		if ($atts['period'])
		    $atts['period'] = '<div class="bdt-plan-period">' . $atts['period'] . '</div>';
		if ($atts['featured'] == 'yes')
		    $classes[] = ' bdt-plan-featured';
		if ($atts['badge']) {
		    $badge = '<div class="bdt-plan-badge">'.$atts['badge'].'</div>';
		}

		$content = trim(do_shortcode($content), '<br><ul><li><a><b><strong><i><em><span>');

		$button = ( $atts['btn_text'] && $atts['btn_url'] ) ? '<a href="' . $atts['btn_url'] . '" class="uk-button uk-button-'.$atts['btn_style'].'" target="_' . $atts['btn_target'] . '">' . $atts['btn_text'] . '</a>' : '';

		$footer = ( $button ) ? '<div class="bdt-plan-footer">' . $button . '</div>' : '';

		$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" style="'.$custom_background.$custom_color.$border.'">';
		    $output[] = '<div class="bdt-plan-head">';
		        $output[] = $badge;
		        $output[] = '<div class="bdt-plan-name">' . $atts['name'] . '</div>';
		        $output[] = '<div class="bdt-plan-price">' . $atts['before'];
		            $output[] = '<span class="bdt-plan-price-value">' . $atts['price'] . '</span>' . $atts['after'];
		        $output[] = '</div>';
		        $output[] = $atts['period'] . $icon;
		    $output[] = '</div>';
		    $output[] = '<div class="bdt-plan-options">' . $content . '</div>';
		    $output[] = $footer;
		$output[] = '</div>';
		return implode("",$output);
	}
	add_shortcode('bdt_pricing_plan', 'bdthemes_pricing_plan');
}

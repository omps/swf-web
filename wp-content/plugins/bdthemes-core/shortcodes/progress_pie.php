<?php
// Bdthemes progress pie shortcode

if (!function_exists('bdthemes_progress_pie')) {
	function bdthemes_progress_pie($atts) {
		$atts = shortcode_atts(array(
			'id'                => uniqid('bdtpp'),
			'percent'           => 75,
			'before'            => '',
			'text'              => '',
			'after'             => '',
			'after_title'       => 'Pie Title',
			'step'              => 1,
			'line_width'        => 8,
			'text_size'         => '',
			'background'        => 'default',
			'custom_background' => '',
			'text_color'        => '#444444',
			'padding'           => 'medium',
			'custom_padding'    => '',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'border'            => '',
			'radius'            => 'no',
			'align'             => 'center',
			'line_cap'          => 'round', //butt/square/round
			'dash_array'        => '', //5,5/20,10,5,5,5,10/10,10
			'duration'          => 1,
			'delay'             => 1,
			'class'             => ''
		), $atts, 'bdt_progress_pie');

		$id                = $atts['id'];
		$js                = array();
		$output            = array();
		$parrent_classes   = array('bdt-progress-pie-wrapper', bdt_ecssc($atts));
		$classes           = array('bdt-progress-pie', 'bdt-pp-align-' . $atts['align'], 'bdt-pp-lc-'.$atts['line_cap']);
		
		$custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
		$text_color        = ($atts['text_color']) ? 'color:' . $atts['text_color']. ';' : '';
		$custom_padding    = ($atts['custom_padding']) ? 'padding:' . $atts['custom_padding']. ';' : '';
		$border            = ($atts['border']) ? 'border:' . $atts['border']. ';' : '';
		$parrent_classes[] = ($atts['radius'] == 'yes') ? 'uk-border-rounded' : '';
		$text_size         = ($atts['text_size']) ? 'font-size:' . $atts['text_size']. ';' : '';

		if (!$atts['text']) {
		    $classes[] = 'bdt-pp-percent';
		}
		if ($atts['before'] !== '') {
		    $atts['before'] = '<div class="bdt-progress-pie-before"> '.$atts['before'].'</div>';
		}
		if ($atts['text'] !== '') {
		    $atts['text'] = '<div class="bdt-progress-pie-text" style="'.$text_color.$text_size.'"> '.$atts['text'].'</div>';
		}
		if ($atts['after'] !== '') {
		    $atts['after'] = '<div class="bdt-progress-pie-after"> '.$atts['after'].'</div>';
		}
		if ($atts['after_title'] !== '') {
		    $atts['after_title'] = '<h4 class="bdt-progress-pie-after-title"> '.$atts['after_title'].'</h4>';
		}
		if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $parrent_classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if (($atts['padding'] !=='custom') and ($atts['padding'] !=='none')) {
            $classes[] = ($atts['padding'] =='medium') ? 'uk-padding' : 'uk-padding-'.$atts['padding'];
        }
        if ($atts['shadow'] !=='none') {
            $parrent_classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }
        if ($atts['hover_shadow'] !=='none') {
            $parrent_classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }  

		wp_enqueue_script('jquery-asPieProgress');
		wp_enqueue_script('progress-pie');

		$output[] = '<div id="'.$id.'_container" class="'.bdt_acssc($parrent_classes).'" style="'.$custom_background.$custom_padding.$border.'">';
			$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" role="progressbar" data-goal="'.$atts['percent'].'" aria-valuemin="0" data-step="'.$atts['step'].'" data-speed="'.($atts['duration']*15).'" data-delay="'.($atts['delay']*1000).'" data-barsize="'.intval($atts['line_width']).'" aria-valuemax="100">';
			    $output[] = '<div class="bdt-progress-pie-label">';
			        $output[] = $atts['before'];
			        if ($atts['text']) {
			            $output[] = $atts['text'];
			        } else {
			            $output[] = '<div class="bdt-progress-pie-number" style="'.$text_color.$text_size.'"></div>';
			        }
			        $output[] = $atts['after'];
			    $output[] ='</div>';
			$output[] = '</div>';
			$output[] = $atts['after_title'];
		$output[] = '</div>';
		return implode("\n", $output);
	}
	add_shortcode('bdt_progress_pie', 'bdthemes_progress_pie');
}
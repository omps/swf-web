<?php
// BdThemes member Shortcode

if (!function_exists('bdthemes_member_shortcode')) {
    function bdthemes_member_shortcode($atts=NULL, $content=NULL) {
        $atts = shortcode_atts(array(
            'background'        => 'default',
            'custom_background' => '',
            'color'             => 'dark',
            'custom_color'      => '',
            'shadow'            => 'small',
            'hover_shadow'      => 'none',
            'radius'            => 'no',
            'text_align'        => 'center',
            'photo'             => '',
            'name'              => __('John Due', 'warp'),
            'role'              => __('Role', 'warp'),
            'social_link'       => ['https://facebook.com/bdthemes', 'https://google-plus.com/+BdThemes', 'https://twitter.com/bdthemescom'],
            'class'             => ''
        ), $atts, 'bdt_member');
        
        $id                = uniqid('bdtm_');
        $icons             = [];
        $output            = [];
        $show_icons        = [];
        $member_photo      = '';
        $classes           = array('bdt-member', 'bdt-member-align-'.$atts['text_align']);
        $desc_class        = array('bdt-member-info');
        
        $custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
        $custom_color      = ($atts['custom_color']) ? 'color:' . $atts['custom_color']. ';' : '';
        $classes[]         = ($atts['radius'] == 'yes') ? 'uk-border-rounded' : '';


        if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color'] !=='custom') {
            $classes[] = 'uk-'.$atts['color'];
        }
        if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }       
        if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }
       
        if ($atts['social_link']) {
            $links = (!is_array($atts['social_link'])) ? explode(",", $atts['social_link']) : $atts['social_link'];

            foreach ($links as $link) {
                $link = trim($link);
                $show_icons[] = '<li>
                    <a '.bdthemes_helper::attrs(['href' => $link]).' uk-icon="icon: '.bdthemes_helper::icon($link).'"></a>
                </li>';
            }

            $show_icons = '<div class="bdt-member-icons"><ul class="uk-iconnav">'.implode("",$show_icons).'</ul></div>';
        }
        
        if ($atts['photo']) {
            $post_image = wp_get_attachment_image_src( $atts['photo'], 'large' );
            $caption    = $atts['name'];
            if($post_image == '') {
                $post_image = BDTCURI.'images/member.svg';
            } else {
                $post_image = $post_image[0];
            }
            $member_photo .= '<img src="' .$post_image.'" alt="'. $caption . '" />';            

        } else {
            $default_image = BDTCURI.'images/member.svg';
            $member_photo = '<img src="' .$default_image.'" alt="'.$atts['name'].'" />';
        }

        $title = '<span class="bdt-member-name">' . $atts['name'] . '</span><span class="bdt-member-role">' . $atts['role'] . '</span>';


        // HTML Layout
        $output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" style="'.$custom_background.$custom_color.'">';
            $output[] = '<div class="bdt-member-photo">';
                $output[] = $member_photo;
                $output[] = $show_icons;
            $output[] = '</div>';

            $output[] = '<div class="'.bdt_acssc($desc_class).'">';
                $output[] = $title;
                $output[] = ($content) ? '<div class="bdt-member-desc bdt-content-wrap">' . do_shortcode($content) . '</div>' : '';
            $output[] = '</div>';

           

        $output[] = '</div>';
        
        return implode("\n",$output);
    }
    // end of member shortcode

    add_shortcode('bdt_member', 'bdthemes_member_shortcode');
}

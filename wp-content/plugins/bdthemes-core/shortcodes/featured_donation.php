<?php
// BdThemes featured_donation Shortcode

if (!function_exists('bdthemes_featured_donation_shortcode')) {
    function bdthemes_featured_donation_shortcode($atts = null, $content = null) {
        $atts = shortcode_atts(array(
            'title'       => 'Featured Donation Title',
            'title_color' => '#ffffff',
            'title_size'  => '20px',
            'image'       => '',
            'goal'        => '',
            'achieve'     => '',
            'bar_color'   => '#E8E8E8',
            'fill_color'  => '#F39C12',
            'class'       => '',
        ), $atts, 'bdt_featured_donation');
        
        $id            = uniqid('sutb');
        $return        = array();

        $goal    = $achieve = $percent = '';
        $goal    = intval($atts['goal']);
        $achieve = intval($atts['achieve']);
        if (( $goal != '' ) and ( $achieve != '' )) {
            $percent = round($achieve/$goal*100);
        }

        $title_color = ($atts['title_color']) ? 'color: '.$atts['title_color'].';': '';
        $title_size = ($atts['title_size']) ? 'font-size: '.$atts['title_size'].';line-height: '.$atts['title_size'].';}' : '';

        if ($atts['image'] != '') {
            $post_image = wp_get_attachment_image_src( $atts['image'], 'large' );
            $post_image = $post_image[0];
        }
        else {
            $post_image = plugins_url().'/bdthemes-shortcodes/images/no-image.jpg';
        }

        // Add CSS and JS in head
        $return[] = '<div id="'.$id.'" class="bdt-featured-donation-wrapper'.bdt_ecssc($atts).'" >
                        <div class="bdt-featured-donation">
                            <img class="bdt-featured-donation-img" src="' .$post_image.'" alt="'.$atts['title'].'">
                        <div class="bdt-featured-donation-title-wrapper">';
                        if ($atts['title'] != '') {
                            $return[] = '<h2 style="'.$title_color.$title_size.'">' . $atts['title'] . '</h2>';
                    }         
            $return[] = '</div></div>';
            $return[] = do_shortcode('[bdt_progress_bar style="3" percent="'.$percent.'" show_percent="yes" text="" text_color="#FFFFFF" bar_color="'.$atts['bar_color'].'" fill_color="'.$atts['fill_color'].'" animation="easeInOutExpo" duration="1.5" delay="0.3" class="uk-margin-top-remove"]');

            $return[] = '<div class="bdt-featured-donation-desc"><h4>Donation : $'.number_format($achieve).' / <span>$'.number_format($goal).'</span></h4>' . do_shortcode($content) . '</div>';
        $return[] = '</div>';

        return implode("\n", $return);
    }
    // end of featured_donation shortcode

    add_shortcode('bdt_featured_donation', 'bdthemes_featured_donation_shortcode');
}

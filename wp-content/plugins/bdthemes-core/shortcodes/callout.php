<?php
// Bdthemes callout shortcode

if (!function_exists('bdthemes_callout')) {
	function bdthemes_callout($atts) {
        $title = $button_txt = $button_link = $target = $intro_txt = $class = $background = $custom_background = $color = $custom_color = $padding = $custom_padding = $shadow = $hover_shadow = $border = $radius = '';
        extract(shortcode_atts( array(
			'title'             => esc_html('Call out title', 'orphan'),
			'intro_txt'         => esc_html('Intro Text', 'orphan'),
			'button_txt'        => esc_html('Button Text', 'orphan'),
			'button_link'       => '#',
			'target'            => 'self',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'custom_color'      => '',
			'padding'           => 'medium',
			'custom_padding'    => '',
			'shadow'            => 'none',
			'hover_shadow'      => 'none',
			'border'            => '',
			'radius'            => 'no',
			'class'             => ''
		),$atts));

		$id                = uniqid('suco_');
		$return            = array();
		$classes           = array('bdt-call-out uk-section', bdt_acssc($class));
		
		$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';
		$custom_color      = ($custom_color) ? 'color:' . $custom_color. ';' : '';
		$custom_padding    = ($custom_padding) ? 'padding:' . $custom_padding. ';' : '';
		$border            = ($border) ? 'border:' . $border. ';' : '';
		$radius            = ($radius == 'yes') ? 'uk-border-rounded' : '';
		$classes[]         = ($radius == 'yes') ? 'uk-border-rounded' : '';


        if (($background !=='custom') and ($background !=='none')) {
            $classes[] = ($background) ? 'uk-background-'.$background : '';
        }
        if ($color) {
            $classes[] = 'uk-'.$color;
        }
        if (($padding !=='custom') and ($padding !=='none')) {
            $classes[] = ($padding =='medium') ? 'uk-padding' : 'uk-padding-'.$padding;
        }
        if ($padding =='none') {
            $classes[] = 'uk-padding-remove';
        }
        if ($shadow !=='none') {
            $classes[] = 'uk-box-shadow-'.$shadow;
        }
        if ($hover_shadow !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$hover_shadow;
        }


		$return[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" style="'.$custom_background.$custom_color.$custom_padding.$border.'">';
		    $return[] = '<div class="uk-container">
                <div class="uk-grid-large uk-flex-middle" uk-grid>
                    <div class="uk-width-expand uk-first-column">';
                    	if ($title) {
                        	$return[] = '<h3 class="uk-text-large uk-margin-remove-bottom">'.esc_html($title).'</h3>';
                    	}
						if ($intro_txt) {
                        	$return[] = '<div class="uk-margin-remove-top">'.$intro_txt.'</div>';
						}
                    $return[] = '</div>

                    <div class="uk-width-auto@m">
                        <a class="uk-button uk-button-default uk-button-large" href="'.esc_html( $button_link ).'" target="_'.$target.'">'.esc_html( $button_txt ).'</a>
                    </div>
                </div>
            </div>
        </div>';

		return implode("\n", $return);
	}
	add_shortcode('bdt_callout', 'bdthemes_callout');
}
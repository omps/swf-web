<?php
// BdThemes inline_icon Shortcode

    if (!function_exists('bdthemes_icon_shortcode')) {
        function bdthemes_icon_shortcode($atts = null, $content = null) {
            extract(shortcode_atts(array(
                'icon'        => 'home', // uikit icons: https://getuikit.com/docs/icon
                'background'  => '',
                'color'       => '',
                'size'        => '',
                'radius'      => '', // radius, circle
                'border'      => '',
                'margin'      => '',
                'padding'     => '',
                'url'         => '',
                'target'      => 'blank',
                'inline_text' => '',
                'button'      => 'no',
                'class'       => '',
            ), $atts));

            $id        = uniqid('suico_');
            $size      = intval($size);
            $style     = [];
            $style[]   = ($background) ? 'background-color:' . $background .';' : '';
            $style[]   = ($color) ? 'color:' . $color .';' : '';
            $style[]   = ($border) ? 'border:' . $border .';' : '';
            $style[]   = ($padding) ? 'padding:' . $padding .';' : '';
            $style[]   = ($margin) ? 'margin:' . $margin .';' : '';
            $classes   = [$class];
            $classes[] = $radius ? 'uk-border-'.$radius : '';
            $classes[] = ($button == 'yes') ? 'uk-icon-button' : '';
            $classes[] = ($content) ? 'uk-margin-small-right' : '';
            
            if ($style) {
                $style = 'style="'.implode("", $style).'"';
            }
            
            // uikit icon
            if ($icon !== false) {
                $icon = '<span class="'.bdt_acssc($classes).'" uk-icon="icon:'.$icon .'" '.$style.'></span>';
            }

            // Prepare text
            if ($content) {
                $content = '<span class="bdt-icon-text">' . $content . '</span>';
            } elseif ($inline_text) {
                $content = '<span class="bdt-icon-text">' .$inline_text .'</span>';
            }

            if ($url or $button == 'yes') {
                $output = '<a id="'.$id.'" class="bdt-icon" href="' . $url . '" target="_' . $target . '">' . $icon .do_shortcode($content) . '</a>';
            } else {
                $output = '<span id="'.$id.'" class="bdt-icon">' . $icon . do_shortcode($content) . '</span>';
            }         

            return $output;
        }
        // end of inline_icon shortcode

        add_shortcode('bdt_icon', 'bdthemes_icon_shortcode');
    }

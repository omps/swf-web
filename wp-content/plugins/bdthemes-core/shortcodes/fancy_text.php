<?php
// Bdthemes fancy text shortcode

if (!function_exists('bdthemes_fancy_text')) {
	function bdthemes_fancy_text($atts) {
		$atts = shortcode_atts(array(
		    'type'          => '1',
		    'tags'          => 'Hello, Text',
		    'class'         => ''
		), $atts, 'bdt_fancy_text');

		// Inistial Variables
		$id = uniqid("ft_");
		$tags = array();
		$tag = '';

		// class manage
		$classes = array('bdt-fancy-text', 'bdt-fteffect'.$atts['type'], bdt_ecssc($atts));

		// Fancy Text interchangeable tag spliting 
		if($atts['tags']) {
		    $tags = explode(',', $atts['tags']);
		    foreach ($tags as $word) {
		        $tag .='<b>'.$word.'</b>';
		    }
		    $tag = str_replace('<b>'.$tags['0'].'</b>', '<b class="is-visible">'.$tags['0'].'</b>' , $tag);
		}

		// Manage class for different type of Fancy Text
		if ($atts['type'] == 1 or $atts['type'] == 2 or $atts['type'] == 4 or $atts['type'] == 5)
		    $classes[] = 'bdt-ft-letters';

		wp_enqueue_script( 'fancy-text' );

		// HTML Ourput
		$return = "
		    <span id='".$id."' class='".bdt_acssc($classes). "'>
		        <span class='bdt-ft-wrap'>
		            ".$tag."
		        </span>
		    </span>";
		
		return $return;
	}
	add_shortcode('bdt_fancy_text', 'bdthemes_fancy_text');
}
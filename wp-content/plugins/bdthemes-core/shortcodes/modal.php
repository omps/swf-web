<?php
// BdThemes Note Shortcode

if (!function_exists('bdthemes_modal')) {
    function bdthemes_modal($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'id'                 => uniqid('bdtmod_'),
            'btn_style'          => 'default',
            'btn_text'           => 'Open',
            'size'               => 'default',
            'center'             => 'no',
            'title'              => 'Modal Title',
            'close_btn_position' => 'default',
            'caption'            => '',
            'class'              => '',
        ), $atts));

        $output    = [];
        $classes   = [$class];
        
        $settings = [
            'center' => ($center == 'yes') ? true : false,
        ];

        $classes[]          = ( $size =='default' ) ? 'uk-modal' : 'uk-modal-'.$size ;
        $close_btn_position = ( $size =='full' ) ? 'full' : $close_btn_position ;

        //This is a button toggling the modal
        $output[] = '<button class="uk-button uk-button-'.$btn_style.'" type="button" uk-toggle="target: #'.$id.'">'.$btn_text.'</button>';

        // This is the modal
        $output[] = '<div id="'.$id.'" class="'. bdt_acssc($classes) .'" uk-modal=\''.json_encode($settings).'\'>
                        <div class="uk-modal-dialog uk-modal-body">';
                            
                if ($title) {
                    $output[] = '<div class="uk-modal-header"><h2 class="uk-modal-title">'.$title.'</h2></div>';
                }
                
                $output[] = do_shortcode( $content );
                $output[] = '<button class="uk-modal-close-'.$close_btn_position.'" type="button" uk-close></button>
                ';

                if ($caption) {
                    $output[] = '<div class="uk-modal-caption">'.$caption.'</div>';
                }

        $output[] = '</div></div>';

        return implode("", $output);
    }
    // end of Accordion shortcode

    add_shortcode('bdt_modal', 'bdthemes_modal');
}

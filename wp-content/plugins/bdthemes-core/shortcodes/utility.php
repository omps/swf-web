<?php

if (!function_exists('bdthemes_site_url')) {
	function bdthemes_site_url($atts = null, $content = null) {
		return get_bloginfo('url');
	}
	add_shortcode('site_url','bdthemes_site_url');
}
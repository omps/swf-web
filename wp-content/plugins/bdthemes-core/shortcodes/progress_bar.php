<?php
// Bdthemes progress bar shortcode

if (!function_exists('bdthemes_progress_bar')) {
	function bdthemes_progress_bar($atts) {
		$atts = shortcode_atts(array(
			'height'       => 'small',
			'percent'      => 75,
			'show_percent' => 'no',
			'text'         => 'HTML',
			'bar_color'    => 'primary',
			'fill_color'   => 'muted',
			'text_color'   => 'dark',
			'animation'    => 'easeInOutExpo',
			'duration'     => 1.5,
			'delay'        => 0.3,
			'class'        => ''
		), $atts, 'bdt_progress_bar');

		$id         = uniqid('supb');
		$css        = array();
		$bar_color  = '';
		$fill_color = '';
		$text_color = '';
		$output     = [];

		$classes    = array('bdt-progress-bar', 'uk-margin', $atts['class']);
		$classes [] = 'progress-bar-height-'.$atts['height'];
		$classes [] = 'uk-'.$atts['text_color'];
		$classes [] = 'progress-bar-bc-'.$atts['bar_color'];
		$classes [] = 'progress-bar-fc-'.$atts['fill_color'];

		if ($atts['bar_color']) {
		    $bar_color = ($atts['bar_color']) ? 'background-color:' . $atts['bar_color'] . '; border-color:' . bdt_color::darken($atts['bar_color'], '10%') . ';' : '';
		}
		if (($atts['fill_color']) or ($atts['text_color'])) {
		    $fill_color = ($atts['fill_color']) ? 'background-color:' . $atts['fill_color'] . ';' : '';
		    $text_color = ($atts['text_color']) ? 'color:' . $atts['text_color'] . ';' : '';          
		}
		$text = ($atts['text']) ? '<span class="bdt-pb-text uk-margin-small-right">' . $atts['text'] . '</span>' : '';
		$show_percent = ($atts['show_percent'] !== 'no') ? '<span class="bdt-pb-percent">'. $atts['percent'] . '%</span>' : '';

		// Add CSS and JS in head
		// wp_enqueue_script('jquery-appear');
		// wp_enqueue_script('jquery-progress-bar');
		// wp_enqueue_script('jquery-easing');

		$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'">';
		//$output[] = '<span class="bdt-pb-fill" data-percent="' . $atts['percent'] . '" data-animation="' . $atts['animation'] . '" data-duration="' . $atts['duration'] . '" data-delay="' . $atts['delay'] . '" style="'.$fill_color.$text_color.'">'.$text.$show_percent.'</span>';
		$output[] = '<progress class="uk-progress uk-margin-remove-bottom" value="'.number_format( $atts['percent'], 2 ).'" max="100"></progress>';
		$output[] = $text.$show_percent;
		$output[] = '</div>';
		return implode("", $output);
	}
	add_shortcode('bdt_progress_bar', 'bdthemes_progress_bar');
}
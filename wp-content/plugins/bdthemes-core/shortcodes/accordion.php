<?php
// BdThemes Note Shortcode

if (!function_exists('bdthemes_accordion')) {
    function bdthemes_accordion($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'id'          => uniqid('bdtacc_'),
            'active'      => 1,
            'animation'   => 'yes',
            'collapsible' => 'yes',
            'multiple'    => 'no',
            'transition'  => 'ease',
            'duration'    => 200,
            'class'       => ''
        ), $atts));

        $output    = [];
        $classes   = [$class];

        $settings = [
            'collapsible' => ($collapsible == 'yes') ? true : false,
            'multiple'    => ($multiple == 'yes') ? true : false,
            'active'      => intval($active)-1,
            'animation'   => ($animation == 'yes') ? true : false,
            'transition'  => $transition,
            'duration'    => $duration,

        ];

        $output[] = '<div id="'.$id.'" class="'. bdt_acssc($classes) .'">';
        $output[] = '<ul uk-accordion=\''.json_encode($settings).'\'>';
        $output[] = do_shortcode( $content);
        $output[] = '</ul>';


        $output[] = '</div>';

        return implode("", $output);
    }
    // end of Accordion shortcode

    add_shortcode('bdt_accordion', 'bdthemes_accordion');
}

if (!function_exists('bdthemes_accordion_item')) {
    function bdthemes_accordion_item($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'title' => 'Accordion Title',
            'class'     => ''
        ), $atts));

        $output    = [];
        $classes   = [$class];

        $output[] = '<li class="'.bdt_acssc($classes).'">';
		  $output[] = '<h3 class="uk-accordion-title">'.$title.'</h3>';
	      $output[] = '<div class="uk-accordion-content">';
		      $output[] = do_shortcode( $content);	            
		  $output[] = '</div>';
	    $output[] = '</li>';

        return implode("", $output);
    }
    // end of Accordion Item shortcode

    add_shortcode('bdt_accordion_item', 'bdthemes_accordion_item');
}

<?php
// BdThemes spacer Shortcode

if (!function_exists('bdthemes_spacer_shortcode')) {
    function bdthemes_spacer_shortcode($atts = null) {
        extract(shortcode_atts(array(
            'size'        => '', // spacific value for example: 15px
            'class'       => ''
        ), $atts));

        $classes = array('bdt-spacer', bdt_acssc($class));
        $size    = ($size) ? 'height: '. intval($atts['size']).'px;' : '';
        
        return '<div class="'.bdt_acssc($classes).'" style="'.$size.'"></div>';
    }
    // end of spacer shortcode
    add_shortcode('bdt_spacer', 'bdthemes_spacer_shortcode');
}

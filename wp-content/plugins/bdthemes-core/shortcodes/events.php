<?php
// Bdthemes events

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); } // load is_plugin_active() function if no available
if(is_plugin_active('the-events-calendar/the-events-calendar.php')){ 
	if (!function_exists('bdthemes_events')) {
		function bdthemes_events($atts){
		extract(shortcode_atts(array( 
			'limit'             => '8',
			'filters'           => 'all',
			'loading_animation' => 'fadeOut',
			'filter_animation'  => 'rotateSides',
			'horizontal_gap'    => 10,
			'vertical_gap'      => 10,
			'filter'            => 'yes',
			'filter_align'      => 'center',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'padding'           => 'medium',
			'custom_padding'    => '',
			'shadow'            => 'no',
			'hover_shadow'      => 'no',
			'radius'            => 'no',
			'show_link'         => 'yes',
			'show_zoom'         => 'yes',
			'title'             => 'yes',
			'meta'              => 'yes',
			'excerpt'           => 'yes',
			'read_more'         => 'yes',
			'pagination'        => 'yes',
			'large'             => 4,
			'medium'            => 3,
			'small'             => 2,
			'class'             => '',
		), $atts));
		
		global $wp_query;
		global $post;
		global $paged;
		$zoom_link_icon = '';

		$parrent_class     = ['cbp', 'bdt-events-grid'];
		$desc_class        = ['bdtp-meta-wrap'];
		$classes           = ['cbp-item'];
		$parrent_class[]   = ($radius =='yes') ? 'bdt-events-radius' : '';
		$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';
		$custom_padding    = ($custom_padding) ? 'padding:' . $custom_padding. ';' : '';

		if (($background !=='custom') and ($background !=='none')) {
            $classes[] = ($background) ? 'uk-background-'.$background : '';
        }
        if ($color) {
            $classes[] = 'uk-'.$color;
        }		
		if ($shadow =='yes') {
            $parrent_class[] = 'bdt-events-box-shadow';
        }		
		if ($hover_shadow =='yes') {
            $parrent_class[] = 'bdt-events-box-shadow-hover';
        }
        if (($padding !=='custom') and ($padding !=='none')) {
            $desc_class[] = ($padding =='medium') ? 'uk-padding' : 'uk-padding-'.$padding;
        }

		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } elseif ( get_query_var('page') ) { $paged = get_query_var('page'); } else { $paged = 1; }

		$args = array(
			'post_type'      => 'tribe_events',
			'posts_per_page' => intval($limit),
			'order'          => 'DESC',
			'orderby'        => 'date',
			'post_status'    => 'publish',
			'paged'          => $paged
		);
			
		if($filters != 'all' && $filters != ''){
			// string to array
			$str = $filters;
			$arr = explode(',', $str);
			  
			$args['tax_query'][] = array(
				'taxonomy' => 'tribe_events_cat',
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		query_posts($args);
		ob_start(); // start buffer

		if( $wp_query->have_posts() ) :
			
		$events_id = uniqid('bdtc'); 
		$filter_id    = uniqid('bdtfi');
		$value = '';
		 
?>
		<div class="events-element">

		<?php 
		if($filter == 'yes'){ 

			// Get Filters from Shortcode Options
			if($filters != '' && $filters != 'all') {
				$tribe_events_cat = explode(',', $filters);
			} else {
				$tribe_events_cat = get_terms('tribe_events_cat');
				$arraytostring = '';
				foreach($tribe_events_cat as $tribe_events_cat){
					$arraytostring .= $tribe_events_cat->slug . ',';
				}
				$arraytostring = rtrim($arraytostring,','); // Remove last commata;
				$tribe_events_cat = explode(',', $arraytostring); // Create array
			} 
		?>

			<div id="<?php echo $filter_id; ?>" class="cbp-l-filters-underline events-filters bdt-filter-align-<?php echo esc_attr($filter_align);?>" >
				<?php if($tribe_events_cat): ?>
					
					<div data-filter="*" class="cbp-filter-item-active cbp-filter-item"><?php esc_html_e('All', 'orphan'); ?></div>	
					<?php foreach($tribe_events_cat as $tribe_events_cat => $value) { ?>
						<?php $filter_name = get_term_by('slug',$value,'tribe_events_cat'); ?>
						<div class="cbp-filter-item" data-filter=".pfi-<?php echo esc_attr($value); ?>">
							<?php echo esc_html($filter_name->name); ?>
						</div>
					<?php } ?>
				<?php endif; ?>
			</div>

		<?php } //end if filter ?>


		<div id="<?php echo $events_id; ?>" class="<?php echo bdt_acssc($parrent_class); ?>" data-id="<?php echo intval($events_id); ?>">  
			<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

			<?php // LIGHTBOX

				//$campaign = events_get_current_campaign();

				if(get_post_meta( get_the_ID(), "bdthemes_events-lightbox", true ) == '1') {
					$lightboxicon = 'icon-bdt-search';
				} else {
					$lightboxicon = 'icon-bdt-plus';
				} 

				if ($show_zoom === 'yes' or $show_link === 'yes') {
					$zoom_link_icon = '<div class="bdtp-link-wrap">';
					if ($show_zoom ==='yes') {
						$zoom_link_icon .= '<a href="'.esc_url(wp_get_attachment_url( get_post_thumbnail_id() )).'" class="lightbox-item bdtp-zoom" title="'. esc_attr(get_the_title()) .'"><span uk-icon="icon: search"></span></a>';
					}
					if ($show_link ==='yes') {
						$zoom_link_icon .= '<a href="'.esc_url(get_permalink()).'" class="bdtp-link" title="'. esc_attr(get_the_title()) .'"><span uk-icon="icon: link"></a>';
					}
					$zoom_link_icon .= '</div>';
				} 

			?>

			<?php 
				if ( has_post_thumbnail()) { 
					$events_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'bdthemes-portfolio' );
				}

				$terms       = get_the_terms( get_the_ID(), 'tribe_events_cat' ); 
				$filter_name = get_term_by('slug',$value,'tribe_events_cat');
				$excerptLength = is_numeric($excerpt) ? $excerpt : 15; 
			?>  

				<div class="<?php if($terms) : foreach ($terms as $term) { echo 'pfi-'.esc_attr($term->slug).' '; } endif; ?><?php echo bdt_acssc($classes); ?>" style="<?php echo $custom_background; ?>">
					<?php
						$start_date = tribe_get_start_date( null, false, 'M d, Y' );
						$end_date   = tribe_get_display_end_date( null, false, 'M d, Y' );
					?> 

					<div class="cbp-caption">
						<div class="bdtp-img-wrap">
							<?php if(get_post_meta( get_the_ID(), "bdthemes_events-lightbox", true ) == '1') { 
								echo '<a href="'. esc_url(wp_get_attachment_url( get_post_thumbnail_id() )) .'" title="'.esc_attr(get_the_title()).'">'; } 
								else { echo '<a href="'. esc_url(get_permalink()) .'" title="'.esc_attr(get_the_title()).'">'; } ?>
									<div class="events-image-img">
										<img src="<?php echo esc_url($events_thumbnail[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" />
									</div>
								</a>
						</div>
						<div class="cbp-caption-activeWrap">
							<div class="cbp-l-caption-alignCenter">
								<div class="cbp-l-caption-body bdt-lightbox-gallery">
									<?php echo $zoom_link_icon; ?>
								</div>
							</div>
						</div>
					</div>
						
					<div class="<?php echo bdt_acssc($desc_class); ?>" style="<?php echo $custom_padding; ?>">

						<?php if ($title === 'yes') { ?>
							<h4 class="bdt-events-title uk-margin-remove"><a href="<?php echo the_permalink(); ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h4>
						<?php }; ?>

						<?php if( $meta==='yes') { ?>
							<ul class="uk-subnav uk-margin-small-top" uk-margin>
								<li><span><?php echo esc_html('Start: ', 'orphan').$start_date; ?></span></li>
								<li><span><?php echo esc_html('End: ', 'orphan').$end_date; ?></span></li>
							</ul>
						<?php }; ?>

						<?php if($excerpt != 'no') { ?>
							<div class="bdt-oc-txt"><?php echo bdthemes_custom_excerpt($excerptLength); ?></div>
						<?php }; ?>

						<?php if( $read_more != 'no' ) { ?>
							<a href="<?php echo esc_url(get_permalink()); ?>" class="uk-button uk-padding-remove-horizontal uk-margin-top uk-margin-small-right uk-text-bold"><?php echo esc_html('View Details', 'orphan'); ?> <span uk-icon="icon: chevron-right"></span></a>
						<?php }; ?>

					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
	<?php if( $pagination == 'yes' ) { ?>
		<div class="pagination-wrapper">
			<?php bdthemes_pagination(); ?>
			<p class="uk-hidden"><?php posts_nav_link(); ?></p>
		</div>
	<?php }; ?>

	<script>
		jQuery(document).ready(function($) {
		    'use strict';

		    // init cubeportfolio
		    jQuery('#<?php echo $events_id; ?>').cubeportfolio({
		        filters: '#<?php echo $filter_id; ?>',
		        loadMoreAction: 'click',
		        defaultFilter: '*',
		        animationType: '<?php echo $filter_animation; ?>',
		        gapHorizontal: <?php echo $horizontal_gap; ?>,
		        gapVertical: <?php echo $vertical_gap; ?>,
		        gridAdjustment: 'responsive',
		        mediaQueries: [{
		            width: 1100,
		            cols: <?php echo $large; ?>
		        }, {
		            width: 800,
		            cols: <?php echo $medium; ?>
		        }, {
		            width: 480,
		            cols: <?php echo $small; ?>
		        }, {
		            width: 320,
		            cols: 1
		        }],
		        caption: 'zoom',
		        displayType: '<?php echo $loading_animation; ?>',
		        displayTypeSpeed: 100,
		    });
		});
	</script>
		
	<?php wp_reset_query(); endif; // needs to be wp_reset_query() instead of wp_reset_postdata() so that pagination works.

	$events_output = ob_get_contents(); // get buffered content

	ob_end_clean(); // clean buffer

	wp_enqueue_style('cube-portfolio');
	wp_enqueue_script('cube-portfolio');

	return $events_output;
	
	}
	add_shortcode('bdt_events', 'bdthemes_events');
	}
}
<?php
// Bdthemes Custom Carousel

if (!function_exists('bdthemes_custom_carousel')) {
	function bdthemes_custom_carousel($atts=null, $content=null){
		$atts = shortcode_atts(array(
			'large'             => 4,
			'medium'            => 3,
			'small'             => 1,
			'nav'               => 'yes',
			'dots'              => 'no',
			'autoplay'          => 'no',
			'autoplay_timeout'  => 4000,
			'speed'             => 350,
			'hoverpause'        => 'no',
			'loop'              => 'yes',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'padding'           => 'none',
			'custom_padding'    => '',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'radius'            => 'no',
			'gutter'            => '',
			'class'            => '',
		), $atts, 'bdt_custom_carousel');

		$rtl        = (is_rtl()) ? 'true' : 'false';

		$classes    = ['bdt-oc-slide', 'uk-overflow-hidden'];

		if ($atts['gutter']     == 'large') { $atts['gutter'] = 50; }
		elseif ($atts['gutter'] == 'medium') { $atts['gutter'] = 25;}
		elseif ($atts['gutter'] == 'small') { $atts['gutter'] = 10;}
		elseif ($atts['gutter'] == 'collapse') { $atts['gutter'] = 0;}
		else { $atts['gutter']  = 35; }

		$classes[]         = ($atts['radius'] =='yes') ? 'uk-border-rounded' : '';
		$custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
		$custom_padding    = ($atts['custom_padding']) ? 'padding:' . $atts['custom_padding']. ';' : '';

		if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $classes[] = 'uk-'.$atts['color'];
        }		
		if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }		
		if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }
        if (($atts['padding'] !=='custom') and ($atts['padding'] !=='none')) {
            $classes[] = ($atts['padding'] =='medium') ? 'uk-padding' : 'uk-padding-'.$atts['padding'];
        }

        $settings = [
			'stagePadding'       => 5,
			'responsiveClass'    => true,
			'mouseDrag'          => true,
			'navText'            => false,
			'margin'             => $atts['gutter'],
			'autoplayTimeout'    => $atts['autoplay_timeout'],
			'smartSpeed'         => $atts['speed'],
			'autoplay'           => ($atts['autoplay'] =='yes') ? true : false,
			'autoplayHoverPause' => ($atts['hoverpause'] =='yes') ? true : false,
			'loop'               => ($atts['loop'] =='yes') ? true : false,
			'responsive'         => [
				0 => [
					'items' => $atts['small'],
					'dots'  => ($atts['dots'] =='yes') ? true : false,
					'nav'   => ($atts['nav'] =='yes') ? true : false,
				],
				768 => [
					'items' => $atts['medium'],
					'dots'  => ($atts['dots'] =='yes') ? true : false,
					'nav'   => ($atts['nav'] =='yes') ? true : false,
				],
				1000 => [
					'items' => $atts['large'],
					'dots'  => ($atts['dots'] =='yes') ? true : false,
					'nav'   => ($atts['nav'] =='yes') ? true : false,
				]
			]
		];
		
		$output = array();
		$output[] = '<div class="bdt-owl-carousel">';
		$output[] = '<div class="owl-carousel owl-carousel owl-theme" data-owl-carousel=\''.json_encode($settings).'\'>';
               	bdthemes_override_shortcodes($classes, $custom_background, $custom_padding);
				$output[] = do_shortcode($content);
				bdthemes_restore_shortcodes();
        $output[]= '</div></div>';

        wp_enqueue_script( 'owl-carousel' );

		return implode("\n", $output);
	}
	add_shortcode('bdt_custom_carousel', 'bdthemes_custom_carousel');


	function bdthemes_override_shortcodes($classes, $custom_background, $custom_padding) {
	    global $shortcode_tags, $_shortcode_tags;
	    // Let's make a back-up of the shortcodes
	    $_shortcode_tags = $shortcode_tags;
	    // Add any shortcode tags that we shouldn't touch here
	    $disabled_tags = array( '' );
	    foreach ( $shortcode_tags as $tag => $cb ) {
	        if ( in_array( $tag, $disabled_tags ) ) {
	            continue;
	        }
	        // Overwrite the callback function
	        $shortcode_tags[ $tag ] = 'bdthemes_wrap_shortcode_in_div';
	        $_shortcode_tags["classes"] = $classes;
	        $_shortcode_tags["custom_background"] = $custom_background;
	        $_shortcode_tags["custom_padding"] = $custom_padding;
	    }
	}


	function bdthemes_restore_shortcodes() {
	    global $shortcode_tags, $_shortcode_tags;
	    // Restore the original callbacks
	    if ( isset( $_shortcode_tags ) ) {
	        $shortcode_tags = $_shortcode_tags;
	    }
	}

	function bdthemes_wrap_shortcode_in_div( $attr, $content = null, $tag ) {
	    global $_shortcode_tags;
	    return '<div class="'.bdt_acssc($_shortcode_tags["classes"]).'" style="'.$_shortcode_tags["custom_background"].$_shortcode_tags["custom_padding"].'">' . call_user_func( $_shortcode_tags[ $tag ], $attr, $content, $tag ) . '</div>';
	}
}
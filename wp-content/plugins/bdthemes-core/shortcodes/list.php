<?php
// BdThemes List Shortcode

if (!function_exists('bdthemes_list_shortcode')) {
    function bdthemes_list_shortcode($atts = null, $content = null) {
        $atts = shortcode_atts(array(
            'icon'          => 'uk-icon-star',
            'icon_color'    => '#333333',
            'style'         => 'default',
            'class'         => ''
        ), $atts, 'bdt_list');

        $id = uniqid('sul_');

        // Font-Awesome icon
        if (strpos($atts['icon'], 'uk-icon-') !== false) {
            $atts['icon'] = '<i class="'.$atts['icon'].'" style="color: '.$atts['icon_color'].'"></i>';
        }
        elseif (strpos($atts['icon'], 'fa fa-') !== false) {
            $atts['icon'] = '<i class="uk-icon-' . trim(str_replace('fa fa-', '', $atts['icon'])) . '" style="color: '.$atts['icon_color'].'"></i>';
        }

        return '<div id="'.$id.'" class="bdt-list bdt-list-style-' . $atts['style'] . '">' . str_replace('<li>', '<li>' . $atts['icon'] . ' ', do_shortcode($content)) . '</div>';
    }

    add_shortcode('bdt_list', 'bdthemes_list_shortcode');
}

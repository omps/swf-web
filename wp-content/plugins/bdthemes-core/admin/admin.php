<?php

function bdthemes_core_settings_page() { ?>
	    <div class="wrap">
		    <h1>BdThemes Settings</h1>
		    <form method="post" action="options.php">
		        <?php
		            settings_fields("section");
		            do_settings_sections("plugin-options");      
		            submit_button(); 
		        ?>          
		    </form>
		</div>
	<?php
}


function bdthemes_mailchimp_api_key() { ?>
    <input type="text" name="bdthemes_mailchimp_api_key" id="bdthemes_mailchimp_api_key" value="<?php echo get_option('bdthemes_mailchimp_api_key'); ?>" />
    <?php
}

function bdthemes_settings_fields() {
	add_settings_section(
		"bdthemes_settings_section", // Section ID
		esc_html__("MailChimp Settings", "bdthemes-core"), // Section Title
		'mailchimp_options_callback', // Callback
		'general' // What Page?  This makes the section show up on the General Settings Page
	);
	
	add_settings_field(
		"bdthemes_mailchimp_api_key", // Option ID
		esc_html__("Mailchimp API Key", "bdthemes-core"), // Label
		"bdthemes_mailchimp_api_key", // !important - This is where the args go!
		"general", // Page it will be displayed (General Settings)
		"bdthemes_settings_section" // Name of our section
	);

    register_setting("section", "bdthemes_mailchimp_api_key");
}

add_action("admin_init", "bdthemes_settings_fields");

function mailchimp_options_callback() { // Section Callback
    echo '<p>This settings work with visual composer mailchimp element and widget.</p>';  
}



// function bdthemes_core_admin_menu() {
// 	add_menu_page("BdThemes Settings", "BdThemes Settings", "manage_options", "bdthemes-settings", "bdthemes_core_settings_page");
// }

// add_action("admin_menu", "bdthemes_core_admin_menu");


// function wpdocs_register_my_custom_menu_page() {
//     add_menu_page(
//         __( 'Custom Menu Title', 'textdomain' ),
//         'custom menu',
//         'manage_options',
//         'myplugin/myplugin-admin.php',
//         '',
//         plugins_url( 'myplugin/images/icon.png' ),
//         6
//     );
// }
// add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
<?php
/*
Plugin Name: BdThemes Core
Plugin URI: http://bdthemes.com/
Description: Core is BdThemes WordPress themes feature extending plugin that you give some extra features like shortcode, widgets etc.
Version: 1.0.1
Author: Bdthemes
Author URI: http://bdthemes.com/
License: GPL2

BdThemes Core is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

BdThemes Core is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BdThemes Core. If not, see {License URI}.
*/

$plugin_dir_path = dirname(__FILE__);
define('BDTCURI', plugin_dir_url(__file__));
$is_admin = is_admin();

require_once(dirname(__FILE__).'/admin/admin.php');
require_once(dirname(__FILE__).'/includes/demo-importer/import.php');


function wp_gear_manager_admin_styles() {
    wp_enqueue_style('style', BDTCURI.'css/bdt-sc-admin.css',true);
}

add_action('admin_print_styles', 'wp_gear_manager_admin_styles');

require_once($plugin_dir_path.'/add_button.php');

function bdthemes_shortcode_empty_paragraph_fix($content){
    $array = array (
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br />' => ']'
    );

    $content = strtr($content, $array);
    return $content;
}

add_filter('the_content', 'bdthemes_shortcode_empty_paragraph_fix');

//Remove e.g. from values
function bdthemes_arrangement_shortcode_value($value) {
    return preg_replace('/^e.g.\s*/', '', $value);
}

function bdthemes_arrangement_shortcode_arr_value(&$value) {
    $value = preg_replace('/^e.g.\s*/', '', $value);
}

// Helper function here
include(dirname(__FILE__).'/helper.php');

// Shortcodes function here
if(file_exists(dirname(__FILE__).'/shortcodes/')){  
    $shotcode_dir = scandir(dirname(__FILE__).'/shortcodes/');
    foreach ($shotcode_dir as $file){
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if(strtolower($ext) == 'php'){
            include_once(dirname(__FILE__).'/shortcodes/'.$file);
        }     
    }
}
//override shortcode folder
if(file_exists(get_template_directory().'/inc/overrides/')){  
    $dir = scandir(get_template_directory().'/inc/overrides/');
    foreach ($dir as $file){
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if(strtolower($ext) == 'php'){
            require_once(get_template_directory().'/inc/overrides/'.$file);
        }     
    }
}

// Widgets functions here
include(dirname(__FILE__).'/widgets/address.php');
include(dirname(__FILE__).'/widgets/flickr.php');
include(dirname(__FILE__).'/widgets/countdown.php');
include(dirname(__FILE__).'/widgets/mailchimp.php');
include(dirname(__FILE__).'/widgets/callout.php');


function bdthemes_vc_tools() {
    if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); } // load is_plugin_active() function if no available

    if (is_plugin_active('js_composer/js_composer.php')) {
        // VC Params
        require_once(dirname(__FILE__).'/includes/vc-params/number.php');
        require_once(dirname(__FILE__).'/includes/vc-params/row-column.php');
        require_once(dirname(__FILE__).'/includes/vc-params/mailChimp.php');
        require_once(dirname(__FILE__).'/includes/vc-tweaks.php');
    }

    // Allow Any Shortcodes in Text Widget
    add_filter('widget_text', 'do_shortcode');
    
}

add_action('after_setup_theme', 'bdthemes_vc_tools');

if (!$is_admin) {
    require_once(dirname(__FILE__).'/includes/pagination.php');
}


// Custom Excerpt Length
function bdthemes_custom_excerpt($limit=50) {
    return strip_shortcodes(wp_trim_words(get_the_content(), $limit, '...'));
}
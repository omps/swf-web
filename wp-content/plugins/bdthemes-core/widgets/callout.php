<?php

class widget_callout extends WP_Widget { 
	
	// Widget Settings
	function __construct() {
		$widget_ops  = array('description' => esc_html__('Display your callout to any widget position beautifully.', 'bdthemes-core'));
		$control_ops = array('width' => 300, 'height' => 350, 'id_base' => 'callout');
		parent::__construct('callout', esc_html__('BdThemes CallOut', 'bdthemes-core'), $widget_ops, $control_ops );
	}
	
	// Widget Output
	function widget($args, $instance) {
		extract($args);
		$title       = apply_filters('widget_title', esc_html($instance['title']));
		$intro_txt   = $instance['intro_txt'];
		$button_txt  = $instance['button_txt'];
		$target      = $instance['target'];
		$button_link = $instance['button_link'];

		echo $before_widget;
		//echo $before_title . esc_html($title) . $after_title;

		echo do_shortcode('[bdt_callout title="'.$title.'" background="none" padding="none" intro_txt="'.$intro_txt.'" button_txt="'.$button_txt.'" target="'.$target.'" button_link="'.$button_link.'"]');

		echo $after_widget;
	}
	
	// Update
	function update($new_instance, $old_instance) {  
		$instance                = $old_instance; 
		$instance['title']       = strip_tags( $new_instance['title'] );
		$instance['intro_txt']   = strip_tags( $new_instance['intro_txt'] );
		$instance['button_txt']  = strip_tags( $new_instance['button_txt'] );
		$instance['target']      = strip_tags( $new_instance['target'] );
		$instance['button_link'] = strip_tags($new_instance['button_link']);

		return $instance;
	}
	
	// Backend Form
	function form($instance) {
		
		$defaults = array('title' => 'Callout Widget', 'button_txt' => 'Donate Now', 'target' => '', 'intro_txt' => '', 'button_link' => ''); // Default Values
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		?>
        
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
        <p>
			<label for="<?php echo esc_attr($this->get_field_id('intro_txt')); ?>">Callout Intro Text:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('intro_txt')); ?>" name="<?php echo esc_attr($this->get_field_name('intro_txt')); ?>" value="<?php echo esc_attr($instance['intro_txt']); ?>" placeholder="BdThemes Ltd, Lathifpur, Bogra, Bangladesh" />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('button_txt')); ?>">Button Text:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('button_txt')); ?>" name="<?php echo esc_attr($this->get_field_name('button_txt')); ?>" value="<?php echo esc_attr($instance['button_txt']); ?>" placeholder="+880-1718-542596" />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('button_link')); ?>">Button Link:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('button_link')); ?>" name="<?php echo esc_attr($this->get_field_name('button_link')); ?>" value="<?php echo esc_attr($instance['button_link']); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'target' ) ?>"><?php esc_html_e( 'Target', 'bdthemes-core' ) ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name( 'target' ) ?>" id="<?php echo $this->get_field_id( 'target' ) ?>">
				<option value="self" <?php selected( 'self', $instance['target'] ) ?>><?php _e( 'Self', 'bdthemes-core' ) ?></option>
				<option value="blank" <?php selected( 'blank', $instance['target'] ) ?>><?php _e( 'Blank', 'bdthemes-core' ) ?></option>
			</select>
		</p>

    <?php }
}

// Add Widget
function widget_callout_init() {
	register_widget('widget_callout');
}
add_action('widgets_init', 'widget_callout_init');

?>
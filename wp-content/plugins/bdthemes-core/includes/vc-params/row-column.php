<?php

vc_remove_param("vc_row", "full_width");
vc_remove_param("vc_row", "bg_color");
vc_remove_param("vc_row", "font_color");
vc_remove_param("vc_row", "margin_bottom");
vc_remove_param("vc_row", "bg_image");
vc_remove_param("vc_row", "bg_image_repeat");
vc_remove_param("vc_row", "padding");
vc_remove_param("vc_row", "el_id");
vc_remove_param("vc_row", "el_class");
vc_remove_param("vc_row", "css" );
vc_remove_param("vc_row", "css_animation" );
vc_remove_param("vc_row", "equal_height");
vc_remove_param("vc_row", "full_height");

vc_remove_param("vc_row", "parallax");
vc_remove_param("vc_row", "parallax_image");

// Visual Composer 4.6+
vc_remove_param("vc_row", "video_bg");

// Visual Composer 4.9+
vc_remove_param("vc_row", "gap");
vc_remove_param("vc_row", "columns_placement");
vc_remove_param("vc_row", "content_placement");
vc_remove_param("vc_row", "parallax_speed_bg");

vc_remove_param("vc_row", "video_bg_url");
vc_remove_param("vc_row", "video_bg_parallax");
vc_remove_param("vc_row", "parallax_speed_video");
vc_remove_param("vc_row", "disable_element");
vc_remove_param("vc_row_inner", "disable_element");

// Row option added as per needed

vc_add_param("vc_row", array(
	'type'       => 'dropdown',
	'heading'    => esc_html__( 'Row Padding', 'bdthemes-core' ),
	'param_name' => 'row_padding',
	'value'      => array(
		esc_html__( 'Default', 'bdthemes-core' ) => 'default',
		esc_html__( 'Small', 'bdthemes-core' )   => 'small',
		esc_html__( 'Medium', 'bdthemes-core' )  => 'medium',
		esc_html__( 'Large', 'bdthemes-core' )   => 'large',
		esc_html__( 'No', 'bdthemes-core' )      => 'no',
		esc_html__( 'Custom', 'bdthemes-core' )  => 'custom',
	),
	'description' => esc_html__( 'Select row section padding from here.', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"        => "textfield",
	"class"       => "",
	"heading"     => esc_html__( "Padding Top", 'bdthemes-core' ),
	"value"       => "",
	"param_name"  => "top_padding",
	"description" => esc_html__( "Do not include \"px\" in your string. For example: 40", 'bdthemes-core' ),
	'dependency'  => array(
		'element' => 'row_padding',
		'value'   => 'custom',
	),
));

vc_add_param("vc_row", array(
	"type"        => "textfield",
	"class"       => "",
	"heading"     => esc_html__( "Padding Bottom", 'bdthemes-core' ),
	"value"       => "",
	"param_name"  => "bottom_padding",
	"description" => esc_html__( "Do not include \"px\" in your string. For example: 40", 'bdthemes-core' ),
	'dependency'  => array(
		'element' => 'row_padding',
		'value'   => 'custom',
	),
));


vc_add_param("vc_row", array(
	"type"        => "dropdown",
	"class"       => "",
	"heading"     => esc_html__( "Section Type", 'bdthemes-core' ),
	'description' => esc_html__( 'make sure you select Fullwidth layout from page settings at bottom. Otherwise this option not work.', 'bdthemes-core' ),
	"param_name"  => "type",
	"value"      => array(
		esc_html__( "Standard Section", 'bdthemes-core' )   => "standard_section",
		esc_html__( "Full Width Section", 'bdthemes-core' ) => "full_width_section"		
	),
));

vc_add_param("vc_row", array(
	'type'        => 'checkbox',
	'heading'     => esc_html__( 'Equal height', 'bdthemes-core' ),
	'param_name'  => 'equal_height',
	'description' => esc_html__( 'If checked columns will be set to equal height.', 'bdthemes-core' ),
	'value'       => array(
		esc_html__( 'Yes', 'bdthemes-core' ) => 'yes'
	)
));

vc_add_param("vc_row", array(
	'type'        => 'checkbox',
	'heading'     => esc_html__( 'Full height row?', 'bdthemes-core' ),
	'param_name'  => 'full_height',
	'description' => esc_html__( 'If checked row will be set to full height.', 'bdthemes-core' ),
	'value'       => array(
		esc_html__( 'Yes', 'bdthemes-core' ) => 'yes'
	),
));

vc_add_param("vc_row", array(
	'type'       => 'dropdown',
	'heading'    => esc_html__( 'Columns position', 'bdthemes-core' ),
	'param_name' => 'columns_placement',
	'value'      => array(
		esc_html__( 'Middle', 'bdthemes-core' )  => 'middle',
		esc_html__( 'Top', 'bdthemes-core' )     => 'top',
		esc_html__( 'Bottom', 'bdthemes-core' )  => 'bottom',
		esc_html__( 'Stretch', 'bdthemes-core' ) => 'stretch',
	),
	'description' => esc_html__( 'Select columns position within row.', 'bdthemes-core' ),
	'dependency'  => array(
		'element'   => 'full_height',
		'not_empty' => true,
	),
));

vc_add_param("vc_row", array(
	'type'       => 'dropdown',
	'heading'    => esc_html__( 'Content position', 'bdthemes-core' ),
	'param_name' => 'content_placement',
	'value'      => array(
		esc_html__( 'Default', 'bdthemes-core' ) => '',
		esc_html__( 'Top', 'bdthemes-core' )     => 'top',
		esc_html__( 'Middle', 'bdthemes-core' )  => 'middle',
		esc_html__( 'Bottom', 'bdthemes-core' )  => 'bottom',
	),
	'description' => __( 'Select content position within columns.', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"        => "attach_image",
	"class"       => "",
	"heading"     => esc_html__( "Background Image", 'bdthemes-core' ),
	"param_name"  => "bg_image",
	"value"       => "",
	"description" => "",
	"group"       => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Background Repeat", 'bdthemes-core' ),
	"param_name" => "bg_repeat",
	"value"      => array(
		esc_html__( "Stretch", 'bdthemes-core' )   => "stretch",
		esc_html__( "No Repeat", 'bdthemes-core' ) => "no-repeat",
		esc_html__( "Repeat", 'bdthemes-core' )    => "repeat"
	),
	"dependency" => array(
		'element' => "bg_image",
		'not_empty' => true
	),
	"group"      => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Background Size", 'bdthemes-core' ),
	'description' => esc_html__( 'if you select parallax mode so background size will not work.', 'bdthemes-core' ),
	"param_name" => "bg_size",
	"value"      => array(
		esc_html__( "Default", 'bdthemes-core' )   => "default",
		esc_html__( "Contain", 'bdthemes-core' )   => "contain",
		esc_html__( "Cover", 'bdthemes-core' ) => "cover",
	),
	"dependency" => array(
		'element' => "bg_image",
		'not_empty' => true
	),
	"group"      => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Background Position", 'bdthemes-core' ),
	'description' => esc_html__( 'if you select parallax mode so background position will not work.', 'bdthemes-core' ),
	"param_name" => "bg_position",
	"value"      => array(
		esc_html__('Default', 'bdthemes-core')       => 'default',
		esc_html__('Top Left', 'bdthemes-core')      => 'top-left',
		esc_html__('Top Center', 'bdthemes-core')    => 'top-center',
		esc_html__('Top Right', 'bdthemes-core')     => 'top-right',
		esc_html__('Center Left', 'bdthemes-core')   => 'center-left',
		esc_html__('Center Center', 'bdthemes-core') => 'center-center',
		esc_html__('Center Right', 'bdthemes-core')  => 'center-right',
		esc_html__('Bottom Left', 'bdthemes-core')   => 'bottom-left',
		esc_html__('Bottom Center', 'bdthemes-core') => 'bottom-center',
		esc_html__('Bottom Right', 'bdthemes-core')  => 'bottom-right',
	),
	"dependency" => array(
		'element' => "bg_image",
		'not_empty' => true
	),
	"group"      => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Background Blend", 'bdthemes-core' ),
	"param_name" => "bg_blend",
	"value"      => array(
		esc_html__('Default', 'bdthemes-core')    => 'default',
		esc_html__('Multiply', 'bdthemes-core')   => 'multiply',
		esc_html__('Screen', 'bdthemes-core')     => 'screen',
		esc_html__('Overlay', 'bdthemes-core')    => 'overlay',
		esc_html__('Darken', 'bdthemes-core')     => 'darken',
		esc_html__('Lighten', 'bdthemes-core')    => 'lighten',
		esc_html__('Color', 'bdthemes-core')      => 'color',
		esc_html__('Color', 'bdthemes-core')      => 'color',
		esc_html__('Hard', 'bdthemes-core')       => 'hard',
		esc_html__('Soft', 'bdthemes-core')       => 'soft',
		esc_html__('Difference', 'bdthemes-core') => 'difference',
		esc_html__('Exclusion', 'bdthemes-core')  => 'exclusion',
		esc_html__('Hue', 'bdthemes-core')        => 'hue',
		esc_html__('Saturation', 'bdthemes-core') => 'saturation',
		esc_html__('Color', 'bdthemes-core')      => 'color',
		esc_html__('Luminosity', 'bdthemes-core') => 'luminosity',
	),
	"dependency" => array(
		'element' => "bg_image",
		'not_empty' => true
	),
	"group"      => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Background Color", 'bdthemes-core' ),
	"param_name" => "bg_style",
	"value"      => array(
		esc_html__( "Default", 'bdthemes-core' )   => "default",
		esc_html__( "Primary", 'bdthemes-core' )   => "primary",
		esc_html__( "Secondary", 'bdthemes-core' ) => "secondary",
		esc_html__( "Muted", 'bdthemes-core' )     => "muted",
		esc_html__( "Custom", 'bdthemes-core' )    => "custom",
	),
	"group"      => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"        => "colorpicker",
	"class"       => "",
	"heading"     => esc_html__( "Background Color", 'bdthemes-core' ),
	"param_name"  => "bg_color",
	"value"       => "",
	"description" => "",
	"group"       => esc_html__( 'Background', 'bdthemes-core' ),
	"dependency" => array(
		'element' => "bg_style",
		'value' => array('custom')
	),
));

vc_add_param("vc_row", array(
	"type"        => "checkbox",
	"class"       => "",
	"heading"     => esc_html__( "Don't Display Background Image at Small Device.", 'bdthemes-core' ),
	"param_name"  => "bg_hidden_small",
	"value"       => array(
		esc_html__( "Yes please!", 'bdthemes-core' ) => true
	),
	"admin_label" => false,
	"description" => "",
	"group"       => esc_html__( 'Background', 'bdthemes-core' ),
	"dependency" => array(
		'element' => "bg_image",
		'not_empty' => true
	),
));

vc_add_param("vc_row", array(
	"type"        => "checkbox",
	"class"       => "",
	"heading"     => esc_html__( "Video Background", 'bdthemes-core' ),
	"value"       => array(
		esc_html__( "Want to use a Video Background?", 'bdthemes-core' ) => "use_video"
	),
	"param_name"  => "video_bg",
	"description" => "",
	"group"       => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"        => "checkbox",
	"class"       => "",
	"heading"     => esc_html__( "Overlay Color", 'bdthemes-core' ),
	"value"       => array(
		esc_html__( "Add video overlay color?", 'bdthemes-core' ) => "true"
	),
	"param_name"  => "enable_video_color_overlay",
	"description" => "",
	"dependency"  => array(
		'element' => "video_bg",
		'value' => array('use_video')
	),
	"group"       => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"        => "colorpicker",
	"class"       => "",
	"heading"     => esc_html__( "Overlay Color", 'bdthemes-core' ),
	"param_name"  => "video_overlay_color",
	"value"       => "",
	"description" => "",
	"dependency"  => array(
		'element' => "enable_video_color_overlay",
		'value' => array('true')
	),
	"group"       => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	'type'        => 'textfield',
	'heading'     => esc_html__( 'YouTube link', 'bdthemes-core' ),
	'param_name'  => 'video_bg_url',
	'value'       => 'https://www.youtube.com/watch?v=lMJXxhRFO1k',
	// default video url
	'description' => esc_html__( 'Add YouTube link.', 'bdthemes-core' ),
	'dependency'  => array(
		'element'   => 'video_bg',
		'not_empty' => true,
	),
	"group"	=> esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	'type'       => 'dropdown',
	'heading'    => esc_html__( 'Parallax Image', 'bdthemes-core' ),
	'param_name' => 'parallax',
	'value'      => array(
		esc_html__( 'None', 'bdthemes-core' )      => '',
		esc_html__( 'Simple', 'bdthemes-core' )    => 'content-moving',
		esc_html__( 'With fade', 'bdthemes-core' ) => 'content-moving-fade',
	),
	'description' => esc_html__( 'Add parallax type background for row (Note: If no image is specified, parallax will use background image from Design Options).', 'bdthemes-core' ),
	'dependency' => array(
		'element'  => 'video_bg',
		'is_empty' => true,
	),
	"group"	=> esc_html__( 'Background', 'bdthemes-core' ),
));
vc_add_param("vc_row", array(
	'type'        => 'attach_image',
	'heading'     => esc_html__( 'Image', 'bdthemes-core' ),
	'param_name'  => 'parallax_image',
	'value'       => '',
	'description' => esc_html__( 'Select image from media library.', 'bdthemes-core' ),
	'dependency'  => array(
		'element'   => 'parallax',
		'not_empty' => true,
	),
	"group"	=> esc_html__( 'Background', 'bdthemes-core' ),
));
vc_add_param("vc_row", array(
	'type'        => 'textfield',
	'heading'     => esc_html__( 'Parallax speed', 'bdthemes-core' ),
	'param_name'  => 'parallax_speed_bg',
	'value'       => '1.5',
	'description' => esc_html__( 'Enter parallax speed ratio (Note: Default value is 1.5, min value is 1)', 'bdthemes-core' ),
	'dependency'  => array(
		'element'   => 'parallax',
		'not_empty' => true,
	),
	"group"	=> esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Text Color", 'bdthemes-core' ),
	"param_name" => "text_color",
	"value"      => array(
		esc_html__( "Dark", 'bdthemes-core' )   => "dark",
		esc_html__( "Light", 'bdthemes-core' )  => "light",
		esc_html__( "Custom", 'bdthemes-core' ) => "custom"
	),
	"group"	=> esc_html__( 'Text', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"        => "colorpicker",
	"class"       => "",
	"heading"     => esc_html__( "Custom Text Color", 'bdthemes-core' ),
	"param_name"  => "custom_text_color",
	"value"       => "",
	"description" => "",
	"dependency"  => array(
		'element' => "text_color",
		'value' => array('custom')
	),
	"group"       => esc_html__( 'Text', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Text Alignment", 'bdthemes-core' ),
	"param_name" => "text_align",
	"value"      => array(
		esc_html__( "Left", 'bdthemes-core' )    => "left",
		esc_html__( "Center", 'bdthemes-core' )  => "center",
		esc_html__( "Right", 'bdthemes-core' )   => "right",
		esc_html__( "Justify", 'bdthemes-core' ) => "justify"
	),
	"group"	=> esc_html__( 'Text', 'bdthemes-core' ),
));

vc_add_param("vc_row", array(
	"type"       => "textfield",
	"class"      => "",
	"heading"    => esc_html__( "Extra Class Name", 'bdthemes-core' ),
	"param_name" => "el_class",
	"value"      => "",
));

vc_add_param("vc_row", array(
	"type"        => "textfield",
	"class"       => "",
	"heading"     => esc_html__( "Row ID", 'bdthemes-core' ),
	"value"       => "",
	"param_name"  => "el_id",
	"description" => esc_html__( "Optional: Insert a unique name for that row. You can then link to that row with #rowid (useful for One Page Layouts).", 'bdthemes-core' ),
));



// Row Inner remove unnecessary option 
vc_remove_param("vc_row_inner", "css");
vc_remove_param("vc_row_inner", "font_color");
vc_remove_param("vc_row_inner", "equal_height");
vc_remove_param("vc_row_inner", "content_placement");
vc_remove_param("vc_row_inner", "gap");

// Column remove unnecessary option 
vc_remove_param("vc_column", "css" );
vc_remove_param("vc_column", "font_color" );
vc_remove_param("vc_column", "css_animation" );


// Column option added as per needed
vc_add_param("vc_column", array(
	"type" => "dropdown",
	"class" => "",
	"heading" => esc_html__( "Animation", 'bdthemes-core' ),
	'description' => esc_html__( 'Animation for these column, If you need sequential animation so set animation in row.', 'bdthemes-core' ),
	"param_name" => "animation",
	"value" => array(
		esc_html__( "None", 'bdthemes-core' )        => "none",
		esc_html__( "Fade In", 'bdthemes-core' )     => "uk-animation-fade",
		esc_html__( "From Top", 'bdthemes-core' )    => "uk-animation-slide-top",
		esc_html__( "From Right", 'bdthemes-core' )  => "uk-animation-slide-right",
		esc_html__( "From Bottom", 'bdthemes-core' ) => "uk-animation-slide-bottom",
		esc_html__( "From Left", 'bdthemes-core' )   => "uk-animation-slide-left",
		esc_html__( "Scale", 'bdthemes-core' )       => "uk-animation-scale",
		esc_html__( "Scale Up", 'bdthemes-core' )    => "uk-animation-scale-up",
		esc_html__( "Scale Down", 'bdthemes-core' )  => "uk-animation-scale-down",
		esc_html__( "Shake", 'bdthemes-core' )       => "uk-animation-shake",
	),
	"group"	=> esc_html__( 'Animation', 'bdthemes-core' ),
));

vc_add_param("vc_column", array(
	"type"        => "textfield",
	"class"       => "",
	"heading"     => esc_html__( "Animation Delay", 'bdthemes-core' ),
	"value"       => "900",
	"param_name"  => "delay",
	"admin_label" => false,
	"description" => esc_html__( "type your value as millisecond 1000 means 1 second", 'bdthemes-core' ),
	"group"       => esc_html__( 'Animation', 'bdthemes-core' ),
));

vc_add_param("vc_column", array(
	"type"        => "checkbox",
	"class"       => "",
	"heading"     => esc_html__( "Animation Repeat?", 'bdthemes-core' ),
	"param_name"  => "repeat",
	"admin_label" => false,
	"description" => "",
	"group"       => esc_html__( 'Animation', 'bdthemes-core' ),
));

vc_add_param("vc_column", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Background Color", 'bdthemes-core' ),
	"param_name" => "bg_style",
	"value"      => array(
		esc_html__( "Default", 'bdthemes-core' )   => "default",
		esc_html__( "Primary", 'bdthemes-core' )   => "primary",
		esc_html__( "Secondary", 'bdthemes-core' ) => "secondary",
		esc_html__( "Muted", 'bdthemes-core' )     => "muted",
		esc_html__( "Custom", 'bdthemes-core' )    => "custom",
	),
	"group"      => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_column", array(
	"type"        => "colorpicker",
	"class"       => "",
	"heading"     => esc_html__( "Background Color", 'bdthemes-core' ),
	"param_name"  => "bg_color",
	"value"       => "",
	"description" => "",
	"group"       => esc_html__( 'Background', 'bdthemes-core' ),
	"dependency" => array(
		'element' => "bg_style",
		'value' => array('custom')
	),
	// "weight" => 2
));

vc_add_param("vc_column", array(
	"type"        => "attach_image",
	"class"       => "",
	"heading"     => esc_html__( "Background Image", 'bdthemes-core' ),
	"param_name"  => "bg_image",
	"value"       => "",
	"description" => "",
	"group"       => esc_html__( 'Background', 'bdthemes-core' )
));

vc_add_param("vc_column", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Column Padding", 'bdthemes-core' ),
	"param_name" => "column_padding",
	"value"      => array(
		esc_html__( "Default (Standard)", 'bdthemes-core' ) => "no-padding",
		esc_html__( "1%", 'bdthemes-core' )                 => "padding-1",
		esc_html__( "2%", 'bdthemes-core' )                 => "padding-2",
		esc_html__( "3%", 'bdthemes-core' )                 => "padding-3",
		esc_html__( "4%", 'bdthemes-core' )                 => "padding-4",
		esc_html__( "5%", 'bdthemes-core' )                 => "padding-5",
		esc_html__( "6%", 'bdthemes-core' )                 => "padding-6",
		esc_html__( "7%", 'bdthemes-core' )                 => "padding-7",
		esc_html__( "8%", 'bdthemes-core' )                 => "padding-8",
		esc_html__( "9%", 'bdthemes-core' )                 => "padding-9",
		esc_html__( "10%", 'bdthemes-core' )                => "padding-10",
		esc_html__( "Custom", 'bdthemes-core' )             => "custom-padding",
	),
	"description" => esc_html__( "Set the padding of your column. Note: Only works when the parent row is set to Full-Width.", 'bdthemes-core' ),
	//"group"       => esc_html__( 'Background', 'bdthemes-core' ),
));

vc_add_param("vc_column", array(
	"type"        => "textfield",
	"class"       => "",
	"heading"     => esc_html__( "Custom Padding", 'bdthemes-core' ),
	"param_name"  => "column_custompadding",
	"admin_label" => false,
	"value"       => "0px 0px 0px 0px",
	"description" => esc_html__( "Set Padding - toppadding rightpadding bottompadding leftpadding", 'bdthemes-core' ),
	"dependency"  => array(
		'element' => "column_padding",
		'value' => array('custom-padding')
	),
));

// vc_add_param("vc_column", array(
// 	"type"        => "checkbox",
// 	"class"       => "",
// 	"heading"     => "Vertically Center Column?",
// 	"value"       => array("Yes, please!" => "true" ),
// 	"param_name"  => "column_center",
// 	"description" => "Useful if you have columns with different heights.",
// 	"group"       => esc_html__( 'Background', 'bdthemes-core' )
// ));

vc_add_param("vc_column", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Text Color", 'bdthemes-core' ),
	"param_name" => "text_color",
	"value"      => array(
		esc_html__( "Dark", 'bdthemes-core' )   => "dark",
		esc_html__( "Light", 'bdthemes-core' )  => "light",
		esc_html__( "Custom", 'bdthemes-core' ) => "custom"
	),
	"group"	=> esc_html__( 'Text', 'bdthemes-core' )
));

vc_add_param("vc_column", array(
	"type"        => "colorpicker",
	"class"       => "",
	"heading"     => esc_html__( "Custom Text Color", 'bdthemes-core' ),
	"param_name"  => "custom_text_color",
	"value"       => "",
	"description" => "",
	"dependency"  => array(
		'element' => "text_color",
		'value' => array('custom')
	),
	"group"       => esc_html__( 'Text', 'bdthemes-core' )
));

vc_add_param("vc_column", array(
	"type"       => "dropdown",
	"class"      => "",
	"heading"    => esc_html__( "Text Alignment", 'bdthemes-core' ),
	"param_name" => "text_align",
	"value"      => array(
		esc_html__( "Left", 'bdthemes-core' )    => "left",
		esc_html__( "Center", 'bdthemes-core' )  => "center",
		esc_html__( "Right", 'bdthemes-core' )   => "right",
		esc_html__( "Justify", 'bdthemes-core' ) => "justify"
	),
	"group"	=> esc_html__( 'Text', 'bdthemes-core' )
));
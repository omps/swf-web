<?php

// Override VC Templates Directory Path
if ( function_exists( 'vc_map' ) && !is_admin() ) {
	$dir = dirname(__FILE__). '/vc-templates/';
	vc_set_shortcodes_templates_dir($dir);
}
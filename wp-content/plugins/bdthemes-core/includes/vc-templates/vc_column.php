<?php
$output = $el_class = $width = $offset = $bg_style = '';

extract(shortcode_atts(array(
	'el_class'             => '',
	'width'                => '1/1',
	'css'                  => '',
	'offset'               => '',
	'animation'            => 'none', 
	'delay'                => '900',
	'repeat'               => 'false',
	'column_padding'       => 'no-padding',
	'column_custompadding' => '0px 0px 0px 0px',
	'bg_style'             => '',
	'bg_color'             => '',
	'bg_image'             => '',
	'text_color'           => 'dark',
	'custom_text_color'    => '',
	'text_align'           => 'uk-text-left',
), $atts));

$width = wpb_translateColumnWidthToSpan( $width );
$width = vc_column_offset_class_merge( $offset, $width );


$style = null;
$custom_padding = null;
$column_animation = null;
$scrollspy = '';

$css_classes = array(
	$this->getExtraClass( $el_class ),
	'wpb_column',
	'vc_column_container',
	'col',
	$width,
);



// Text-Color
if($text_color == 'light'){
	$css_classes[] = 'uk-light';
} else if($text_color == 'dark'){
	$css_classes[] = 'uk-dark';
}

// Custom Text Color
if($text_color == 'custom' && !empty($custom_text_color)) {
	$style .= 'color: '. $custom_text_color .'; ';
	$css_classes[] = 'color-custom';
}

// Text-Align
if($text_align == 'center'){
	$css_classes[] = "uk-text-center";
} else if($text_align == 'right'){
	$css_classes[] = "uk-text-right";
}else if($text_align == 'justify'){
	$css_classes[] = "uk-text-justify";
}

// Background Color
if ($bg_style != '' and $bg_style != 'custom') {
	$css_classes[] = ($bg_style) ? 'uk-background-'.$bg_style : 'uk-background-default';
} elseif (!empty($bg_color)) {
	$style .= 'background-color: '. $bg_color.'; ';
}

// Background Image
if(!empty($bg_image)) {
	$bg_image_src = wp_get_attachment_image_src($bg_image, 'full');
	$style .= 'background-image: url('. $bg_image_src[0]. '); ';
}

// Column Padding
if($column_padding == 'custom-padding'){
	$custom_padding = 'padding: ' . $column_custompadding . '; ';
	$css_classes[] = 'custom-padding';
} else {
	$css_classes[] = $column_padding;
}


// Animation
if(!empty($animation) && $animation != 'none') {
	
	 $column_animation = str_replace(" ","-",$animation);
	 $delay = intval($delay);

	 $scrollspy = ' uk-scrollspy="cls: '.$column_animation.'; target: > div > div > div; delay: '.$delay.'; repeat: '.$repeat.'"';
}

$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $width.$el_class, $this->settings['base']);

$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

$output .= "\n\t".'<div ' . implode( ' ', $wrapper_attributes ) .$scrollspy. ' style="'.esc_attr($style).'">';
$output .= '<div class="vc_column-inner" style="'.esc_attr($custom_padding).'">';
$output .= "\n\t\t".'<div class="wpb_wrapper">';
$output .= "\n\t\t\t".wpb_js_remove_wpautop($content);
$output .= "\n\t\t".'</div> '.$this->endBlockComment('.wpb_wrapper');
$output .= '</div>';
$output .= "\n\t".'</div> '.$this->endBlockComment($el_class) . "\n";

echo $output;
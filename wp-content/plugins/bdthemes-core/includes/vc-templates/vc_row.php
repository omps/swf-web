<?php
$el_class = $full_height = $parallax_speed_bg = $parallax_speed_video = $full_width = $equal_height = $flex_row = $columns_placement = $content_placement = $parallax = $parallax_image = $css = $el_id = $video_bg = $video_bg_url = $video_bg_parallax = $animation = $delay = $repeat = $scrollspy = $bg_style = $bg_size = $bg_position = $bg_blend = $video_overlay_color = $overlay = '';
$disable_element = '';
$output = $after_output = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

wp_enqueue_script( 'wpb_composer_front_js' );

$style       = null;
$el_class    = $this->getExtraClass( $el_class );
$css_classes = array(
	'vc_row',
	'vc_row-fluid',
	$el_class,
	vc_shortcode_custom_css_class( $css ),
);

if ( 'yes' === $disable_element ) {
	if ( vc_is_page_editable() ) {
		$css_classes[] = 'vc_hidden-lg vc_hidden-xs vc_hidden-sm vc_hidden-md';
	} else {
		return '';
	}
}

if ( ! empty( $full_height ) ) {
	$css_classes[] = 'vc_row-o-full-height';
	if ( ! empty( $columns_placement ) ) {
		$flex_row = true;
		$css_classes[] = 'vc_row-o-columns-' . $columns_placement;
		if ( 'stretch' === $columns_placement ) {
			$css_classes[] = 'vc_row-o-equal-height';
		}
	}
}

if ( ! empty( $equal_height ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-equal-height';
}

// Background Image
if(!empty($bg_image)) {
	if(!preg_match('/^\d+$/',$bg_image)){
		$style .= 'background-image: url('. $bg_image . '); ';	
	} else {
		$bg_image_src = wp_get_attachment_image_src($bg_image, 'full');
		$style .= 'background-image: url('. $bg_image_src[0]. '); ';
	}

	//for pattern bgs
	if($bg_repeat == 'repeat'){
		$style .= 'background-repeat: '. $bg_repeat .'; ';
		$css_classes[] = null;
	} else if($bg_repeat == 'no-repeat'){
		$style .= 'background-repeat: '. $bg_repeat .'; ';
		$css_classes[] = null;
	} else if($bg_repeat == 'stretch'){
		$style .= null;
		$css_classes[] = 'bg-stretch';
	}

	if($bg_size !== 'default' and $parallax == ''){
		$css_classes[] = 'uk-background-'.$bg_size;
	}

	if($bg_position !== 'default' and $parallax == ''){
		$css_classes[] = 'uk-background-'.$bg_position;
	}

	if($bg_blend !== 'default' and $parallax == ''){
		$css_classes[] = 'uk-background-blend-'.$bg_blend;
	}

	if($bg_hidden_small){
		$css_classes[] = 'uk-background-image@m';
	}
}

// Overlay style for background and video
if ($video_overlay_color) {
	$overlay = '<div class="vc-bg-overlay" style="background-color: '.$video_overlay_color.'"></div>';
	$css_classes[] = 'vc-has-bg-overlay';
}


// Background Color
if ($bg_style != '' and $bg_style != 'custom') {
	$css_classes[] = ($bg_style) ? 'uk-background-'.$bg_style : 'uk-background-default';
} elseif (!empty($bg_color)) {
	$style .= 'background-color: '. $bg_color.'; ';
}

// Padding
if ($row_padding !== 'custom') {
	$css_classes[] = 'row-padding-'.$row_padding;
} else {
	if($top_padding != ''){
		$style .= 'padding-top: '. intval($top_padding) .'px; ';
	}
	if($bottom_padding != ''){
		$style .= 'padding-bottom: '. intval($bottom_padding) .'px; ';
	}
}

// Main Class
if($type == 'standard_section'){
	$css_classes[] = "standard-section section ";
} else if($type == 'full_width_section'){
	$css_classes[] = "full-width-section section ";
}

// Text-Align
if($text_align == 'center'){
	$text_align = "uk-text-center";
} else if($text_align == 'right'){
	$text_align = "uk-text-right";
}else if($text_align == 'justify'){
	$text_align = "uk-text-justify";
}

// Text-Color
if($text_color == 'light'){
	$text_color = 'uk-light';
} else if($text_color == 'dark'){
	$text_color = 'uk-dark';
}

// Custom Text Color
if($text_color == 'custom' && !empty($custom_text_color)) {
	$style .= 'color: '. $custom_text_color .'; ';
	$text_color = 'color-custom';
}

$wrapper_attributes = array();
// build attributes for wrapper
if ( ! empty( $el_id ) ) {
	$wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
}

if ( ! empty( $full_height ) ) {
	$css_classes[] = 'vc_row-o-full-height';
	if ( ! empty( $columns_placement ) ) {
		$flex_row = true;
		$css_classes[] = 'vc_row-o-columns-' . $columns_placement;
		if ( 'stretch' === $columns_placement ) {
			$css_classes[] = 'vc_row-o-equal-height';
		}
	}
}

if ( ! empty( $equal_height ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-equal-height';
}

if ( ! empty( $content_placement ) ) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-content-' . $content_placement;
}

if ( ! empty( $flex_row ) ) {
	$css_classes[] = 'vc_row-flex';
}

$has_video_bg = ( ! empty( $video_bg ) && ! empty( $video_bg_url ) && vc_extract_youtube_id( $video_bg_url ) );

$parallax_speed = $parallax_speed_bg;
if ( $has_video_bg ) {
	$parallax = $video_bg_parallax;
	$parallax_speed = $parallax_speed_video;
	$parallax_image = $video_bg_url;
	$css_classes[] = 'vc_video-bg-container';
	wp_enqueue_script( 'vc_youtube_iframe_api_js' );
}

if ( ! empty( $parallax ) ) {
	wp_enqueue_script( 'vc_jquery_skrollr_js' );
	$wrapper_attributes[] = 'data-vc-parallax="' . esc_attr( $parallax_speed ) . '"'; // parallax speed
	$css_classes[] = 'vc_general vc_parallax vc_parallax-' . $parallax;
	if ( false !== strpos( $parallax, 'fade' ) ) {
		$css_classes[] = 'js-vc_parallax-o-fade';
		$wrapper_attributes[] = 'data-vc-parallax-o-fade="on"';
	} elseif ( false !== strpos( $parallax, 'fixed' ) ) {
		$css_classes[] = 'js-vc_parallax-o-fixed';
	}
}

if ( ! empty( $parallax_image ) ) {
	if ( $has_video_bg ) {
		$parallax_image_src = $parallax_image;
	} else {
		$parallax_image_id = preg_replace( '/[^\d]/', '', $parallax_image );
		$parallax_image_src = wp_get_attachment_image_src( $parallax_image_id, 'full' );
		if ( ! empty( $parallax_image_src[0] ) ) {
			$parallax_image_src = $parallax_image_src[0];
		}
	}
	$wrapper_attributes[] = 'data-vc-parallax-image="' . esc_attr( $parallax_image_src ) . '"';
}
if ( ! $parallax && $has_video_bg ) {
	$wrapper_attributes[] = 'data-vc-video-bg="' . esc_attr( $video_bg_url ) . '"';
}

$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( array_unique( $css_classes ) ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';



$output .= '<div ' . implode( ' ', $wrapper_attributes ) .$scrollspy.' style="'.$style.'">';
	$output .= $overlay;
	$output .= '<div class="col vc-column-wrapper '.esc_attr($text_color).' '.esc_attr($text_align).'">';
	$output .= wpb_js_remove_wpautop( $content );
	$output .= '</div>';
$output .= '</div>';
$output .= $after_output;

echo $output;
